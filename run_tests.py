#!/usr/bin/env python3
# vim:fileencoding=utf-8
import tests.all_tests
from tests.time_logging_test_runner import TimeLoggingTestRunner

test_suite = tests.all_tests.create_test_suite()
TimeLoggingTestRunner().run(test_suite)

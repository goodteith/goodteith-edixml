#!/usr/bin/env python3
# vim:fileencoding=utf-8

import datetime

try:
    unicode
    python3 = False
except NameError:
    python3 = True
    unicode = bytes


def to_str(bytes_or_str):
    """Convert bytestring to string.

    :bytes_or_str: bytecode or string to check
    :returns: simple string

    """
    if python3:
        if isinstance(bytes_or_str, bytes):
            value = bytes_or_str.decode('utf-8')
        else:
            value = bytes_or_str
    else:
        if isinstance(bytes_or_str, unicode):
            value = bytes_or_str.encode('utf-8')
        else:
            value = bytes_or_str

    return value


def to_unicode(unicode_or_str):
    """Convert a python2 string to a unicode string.

    :unicode_or_str: unicode or string to check.
    :returns: unicode-encoding string.

    """
    if python3:
        value = unicode_or_str
    else:
        value = unicode(unicode_or_str)

    return value


def to_bytes(bytes_or_str):
    """Convert string to bytestring (or unicode, if python2)

    :bytes_or_str: bytecode or string to convert
    :returns: bytecode (or unicode, if python2)

    """
    if python3:
        if isinstance(bytes_or_str, str):
            value = bytes_or_str.encode('utf-8')
        else:
            value = bytes_or_str
    else:
        if isinstance(bytes_or_str, str):
            value = bytes_or_str.decode('utf-8')
        else:
            value = bytes_or_str

    return value


def datetime_as_iso_string(dt):
    """Return an isoformatted datetime string, without the milliseconds.

    A 2/3 compatible version of the isoformat timespec argument.

    """
    dt_string = dt.isoformat()
    try:
        dot_position = dt_string.index('.')
        return dt_string[:dot_position]
    except ValueError:
        return dt_string


def date_as_iso_string(dt):
    """Return an string representation of a date object, in ISO format.

    """
    return dt.strftime("%Y-%m-%d")


def get_string_repr(value):
    """Return the appropriate string representation of object.

    :param obj value: value to represent.
    :return: String representation
    :rtype: str

    """
    if isinstance(value, datetime.datetime):
        return datetime_as_iso_string(value)
    if isinstance(value, datetime.date):
        return date_as_iso_string(value)
    if value is True:
        return 'true'
    if value is False:
        return 'false'
    return str(value)

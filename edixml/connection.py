#!/usr/bin/env python3
# vim:fileencoding=utf-8

from __future__ import print_function

import datetime
import glob
import logging
import os
import random
import shutil
import subprocess
import sys
import time
import yaml

try:
    import win32com.client
    import win32api
except ImportError:
    pass

from lxml import etree

from edixml.file_writers import write_response_to_file
from edixml.interfaces import (
    etree_to_csv,
    load_yaml_document,
    wrap_tree_in_envelope,
    yaml_to_class,
)
from edixml.settings import (
    ADAPTER_SERVICE_NAME,
    CSV_ARCHIVE_DIR,
    CSV_FILENAMES,
    CSV_SAVEDIR,
    RESPONSE_SAVEDIR,
    XML_ARCHIVE_DIR,
    XML_OUTBOX,
    XML_OUTBOX_MAX_AGE,
    XML_SAVEDIR,
)


if getattr(sys, 'frozen', False):
    # We are running in a bundle
    BASE_DIR = sys._MEIPASS
else:
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
logger = logging.getLogger(__name__)


def write_tree_to_file(root, timestamp=None):
    message_type = root.node_name
    # message_type = etree.QName(root.tag).localname
    logger.debug('Message type detected as %s', message_type)
    message = wrap_tree_in_envelope(root, timestamp)
    filename = '{}-{:03}-{}.xml'.format(
        datetime.datetime.now().strftime("%Y-%m-%d-%H%M%S"),
        random.randrange(10**3),
        message_type)
    filename_archive = os.path.join(XML_SAVEDIR, filename)
    filename_outbox = os.path.join(XML_OUTBOX, filename)
    logger.debug("Writing XML data to %s", filename)
    message_tree = etree.ElementTree(message)
    with open(filename_archive, 'wb') as f:
        message_tree.write(
            f, pretty_print=True, encoding='utf-8', xml_declaration=True
        )
    logger.debug("Copying %s to %s", filename_archive, filename_outbox)
    shutil.copyfile(filename_archive, filename_outbox)
    logger.debug("Wrote XML data to %s", filename)
    return filename_outbox


def validate_xml(filename):
    logger.debug("Reading in %s for validation", filename)
    with open(filename) as f:
        logger.debug("Parsing %s as xml file", filename)
        data = etree.parse(f)
    root = data.getroot()
    message_type = etree.QName(root.tag).localname
    logger.debug("Detected %s as root type", message_type)
    schema_version = root.attrib['SchemaVersion'].replace('.', '-')
    logger.debug("Detected %s as schema version", message_type)
    schema = os.path.join(BASE_DIR, 'schema', '{}-v{}.xsd'.format(
        message_type, schema_version))
    logger.debug("Detected %s as schema file", schema)

    with open(schema) as f:
        logger.debug("Parsing %s as schema file", schema)
        xmlschema_doc = etree.parse(f)

    xmlschema = etree.XMLSchema(xmlschema_doc)
    if xmlschema.validate(data):
        logger.info("%s is valid, according to %s", filename, schema)
    else:
        logger.error("%s is invalid, according to %s", filename, schema)
        logger.error("Schema error log: %s", xmlschema.error_log)
        os.remove(filename)


def _print_xml_to_screen(filename):
    logger.info('Printing xml to screen')
    with open(filename) as f:
        print(f.read(), end="")


def open_envelope(tree):
    logger.info('Retrieving element name')
    root = tree.getroot()
    element_name = root.tag
    logger.debug('Element name: %s', element_name)
    if element_name.endswith('Env'):
        logger.debug('Envelope detected')
        payload_name = element_name.replace('Env', '')
        human_friendly_payload_name = payload_name.split('}')[-1]
        logger.debug('Payload name: %s', payload_name)
        logger.info('Finding data payload in envelope')
        if human_friendly_payload_name.startswith(('A', 'E', 'I', 'O', 'U')):
            logger.warning(
                'An %s has been received', human_friendly_payload_name)
        else:
            logger.warning(
                'A %s has been received', human_friendly_payload_name)
        root = root.find('.//{}'.format(payload_name))
        if root is not None:
            logger.debug('Data payload: %s', etree.tostring(root))
        else:
            logger.warning('Could not extract payload')
    return root


def parse_yaml_file(input_file, adapter, print_to_screen=False, dry_run=False):
    logger.debug("Parsing %s yaml file", input_file.name)
    logger.debug("Reading in yaml from %s", input_file.name)
    data = input_file.read()
    input_file.close()
    logger.debug("Got %s data from yaml", data)

    payload = load_yaml_document(data)
    try:
        for item in payload:
            cls = yaml_to_class(item)
            timestamp = item.get('RequestDateTime', None)
            # tree = cls.as_etree()
            xml_filename = write_tree_to_file(cls, timestamp)
            validate_xml(xml_filename)
            if print_to_screen:
                _print_xml_to_screen(xml_filename)
            if not dry_run:
                send_request(xml_filename, adapter)
            else:
                logger.debug('Dry run detected. Not sending to adapter')
    except (yaml.parser.ParserError, yaml.scanner.ScannerError):
        logger.exception("Could not parse yaml")


def parse_response(filename):
    logger.info('Parsing response')
    tree = etree.parse(filename)
    logger.debug('Response: %s', etree.tostring(tree))
    root = open_envelope(tree)
    etree_to_csv(root)


def check_for_responses(adapter, first_run=True):
    if adapter is not None:
        logger.info("Checking for response messages")
        (result, response, error_code,
         error_description) = adapter.GetResponse()
        logger.debug('Response result: %s', result)
        logger.debug('Response: %s', response)
        logger.debug('error_code: %s', error_code)
        logger.debug('error_description: %s', error_description)
        if error_code > 0:
            logger.info('Checking for responses resulted in error code %s',
                        error_code)
    else:
        logger.warning("Could not retrieve responses on this platform")
        return False

    if response:
        filename = write_response_to_file(response)
        logger.info('Parsing response from adapter')
        parse_response(filename)
        check_for_responses(adapter, first_run=False)
    elif first_run:
        logger.info('Nothing new from the DPD')


def send_request(xml_filename, adapter):
    with open(xml_filename) as f:
        payload = f.read()

    if adapter is not None:
        logger.info('Sending %s to adapter', xml_filename)
        (result, error_code, error_description) = adapter.SendRequest(payload)
        logger.debug('Request result: %s', result)
        logger.debug('error_code: %s', error_code)
        logger.debug('error_description: %s', error_description)
        if error_code > 0:
            logger.info('Sending %s resulted in error code %s',
                        xml_filename, error_code)
    else:
        logger.warning("Could not send message on this platform")


def connect_to_adapter():
    """Attempt to connect and activate the ePharmacy adapter.

    Attempts to retrieve a connection, and if it fails, returns None.

    """

    logger.info("Attempting to connect to adapter")
    if sys.platform.startswith('win32'):
        (adapter, active) = _connect_to_adapter()
    else:
        adapter = None
        active = True
        logger.warning("Adapter not available on this platform")

    if not active:
        logger.error("Could not connect to adapter")
        adapter = None

    return adapter


def _connect_to_adapter():
    """Connect and activate ePharmacy adapter.

    Logs any errors from the adapter.

    """
    adapter = win32com.client.Dispatch(
        "AdapterSuite.ClientApi.epRequestManager")
    active = activate_adapter(adapter)

    return (adapter, active)


def activate_adapter(adapter):
    """Activates adapter, returning success or failure.
    """
    if adapter is not None:
        logger.info('Activating adapter %s', adapter)
        (active, error_code, error_description) = adapter.Activate()
        logger.debug('Adapter activation details: %s, %s, %s',
                     active, error_code, error_description)
        if not active:
            logger.error("Could not activate adapter.")
            logger.error("Error code: %s", error_code)
            logger.error("Error description: %s", error_description)

        return active
    else:
        return False


def _restart_adapter():
    logger.warning("Attempting to restart adapter")
    for command in ['stop', 'start']:
        cmd = "net {} {}".format(command, ADAPTER_SERVICE_NAME)
        logger.info("Running %s", cmd)
        process = subprocess.Popen(
            cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE
        )
        stdout, stderr = process.communicate()

        for line in stdout.split('\n'):
            line = line.strip('\r')
            if line:
                logger.info("Process returned: %s", line)
        for line in stderr.split('\n'):
            line = line.strip('\r')
            if line:
                logger.error("Process returned: %s", line)


def check_adapter(adapter):
    logger.info("Checking adapter status")
    active = False
    if adapter is None:
        logger.warning("Adapter is None.")
        logger.warning("Attempting to reconnect.")
        adapter = connect_to_adapter()
        if adapter is None:
            logger.warning("Adapter is still None")
            logger.warning("Attempting to restart.")
            _restart_adapter()
            adapter = connect_to_adapter()

    if adapter is not None:
        active = activate_adapter(adapter)
        if not active:
            logger.warning("Could not activate adapter")
            logger.warning("Attempting to reconnect")
            _restart_adapter()
            adapter = connect_to_adapter()
            if adapter is not None:
                active = activate_adapter(adapter)
                if not active:
                    logger.warning("Still could not activate adapter")
                    logger.warning("Attempting to restart")
                    _restart_adapter()
                    adapter = connect_to_adapter()
                    if adapter is not None:
                        active = activate_adapter(adapter)

    return (adapter, active)


def ensure_dirs_exist():
    for directory in [CSV_ARCHIVE_DIR, CSV_SAVEDIR, RESPONSE_SAVEDIR,
                      XML_SAVEDIR, XML_ARCHIVE_DIR, XML_OUTBOX]:
        try:
            os.makedirs(directory)
            logger.info("Creating %s directory for saved files", directory)
        except OSError:
            logger.debug("%s directory exists so not creating it", directory)


def ensure_files_exist():
    for csv in CSV_FILENAMES:
        location = os.path.join(CSV_SAVEDIR, csv)
        try:
            os.utime(location, None)
        except OSError:
            open(location, 'a').close()
            logger.debug('Creating empty %s', location)


def check_outbox_is_not_clogged():
    now = time.time()
    youngest_allowed_file = now - XML_OUTBOX_MAX_AGE
    for f in glob.glob(os.path.join(XML_OUTBOX, '*.xml')):
        if os.path.getctime(f) < youngest_allowed_file:
            break
    else:
        return

    logger.info(
        "Stale files found in outbox. Please check services on server")
    if sys.platform.startswith('win32'):
        win32api.MessageBox(
            0,
            ("Messages are not being sent by adapter.\n"
             "Try restarting the services on the server."),
            "STALE OUTBOX!"
        )

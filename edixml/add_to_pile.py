#!/usr/bin/env python3
# vim:fileencoding=utf-8

import argparse
import logging
import sys

from edixml import __version__
from edixml.connection import (
    check_outbox_is_not_clogged,
    ensure_dirs_exist,
    ensure_files_exist,
    parse_yaml_file,
)


logger = logging.getLogger(__name__)


def _create_parser():
    parser = argparse.ArgumentParser(
        description="Sends a claim to the server for transmission to DPD")
    parser.add_argument(
        'input', type=argparse.FileType('r'), nargs='?',
        help="Location of input yaml file")
    parser.add_argument(
        '-p', '--print', action='store_true', dest='print_to_screen',
        help='Print resulting xml to screen')
    parser.add_argument(
        '--version', action='version',
        version='%(prog)s {version}'.format(version=__version__))

    return parser


def main():
    logger.info("Executing %s", " ".join(sys.argv))
    logger.info("%s version %s", __name__, __version__)
    parser = _create_parser()
    args = parser.parse_args(sys.argv[1:])
    ensure_dirs_exist()
    ensure_files_exist()
    check_outbox_is_not_clogged()
    if args.input:
        parse_yaml_file(
            args.input, None, args.print_to_screen, True)


if __name__ == '__main__':
    sys.exit(main())

#!/usr/bin/env python3
# vim:fileencoding=utf-8

import argparse
import datetime
import glob
import logging
import os
import random
import sys
import time


from edixml import __version__
from edixml.settings import (
    ARCHIVE_FILES_THRESHOLD,
    CSV_ARCHIVE_DIR,
    CSV_SAVEDIR,
    FILEMAKER_LOG_FILE
)


logger = logging.getLogger(__name__)


def _create_parser():
    parser = argparse.ArgumentParser(
        description="Archive old response csv files to archive directory."
    )
    parser.add_argument(
        'filename', nargs='?', help="Name of file to archive"
    )
    parser.add_argument(
        '--version', action='version',
        version='%(prog)s {version}'.format(version=__version__))

    return parser


def main():
    logger.info("Executing %s", " ".join(sys.argv))
    logger.info("%s version %s", __name__, __version__)
    now = time.time()
    logger.info("Time detected: %s", now)
    try:
        logger.debug("Ensuring %s directory exists", CSV_ARCHIVE_DIR)
        os.makedirs(CSV_ARCHIVE_DIR)
        logger.info("Created %s directory.", CSV_ARCHIVE_DIR)
    except OSError:
        pass
    parser = _create_parser()
    args = parser.parse_args(sys.argv[1:])

    if args.filename:
        if os.path.isfile(args.filename):
            filelist = [args.filename]
        else:
            logger.error('Could not find %s', args.filename)
            return 1
    else:
        filelist = glob.glob(os.path.join(CSV_SAVEDIR, '*.csv'))

    date = datetime.datetime.now().strftime('%Y-%m-%d-%H%M%S')
    logger.debug("Current datetime is %s", date)

    # Clear out the Filemaker log file
    with open(FILEMAKER_LOG_FILE, 'w') as f:
        pass

    for f in filelist:
        logger.info("Detected %s in %s", f, CSV_SAVEDIR)
        filename = os.path.split(f)[-1]
        logger.debug("Filename calculated: %s", filename)
        name, ext = os.path.splitext(filename)
        logger.debug("Filename: %s, extension: %s", name, ext)
        target_name = '{}-{}-{:03}{}'.format(
            date, name, random.randrange(10**3), ext)
        logger.debug("Target name calculated: %s", target_name)
        target = os.path.join(CSV_ARCHIVE_DIR, target_name)
        logger.debug("Target full path: %s", target)
        try:
            logger.debug("Moving %s to %s", f, target)
            if os.stat(f).st_size != 0:
                # File has content
                if now - os.path.getmtime(f) > ARCHIVE_FILES_THRESHOLD:
                    # Maybe this check could be done by comparing the
                    # atime with the mtime. If they differ, then
                    # something read the file without modifying it
                    # (filemaker, presumably). If they are the same,
                    # then the edixml service wrote to it last and it
                    # hasn't been interacted with since.
                    os.rename(f, target)
                    logger.info("Moved %s to %s", f, target)
                    open(f, 'a').close()
                    logger.debug('Creating empty %s', f)
                else:
                    logger.warning(
                        "Not archiving %s. Modified within %s seconds",
                        f, ARCHIVE_FILES_THRESHOLD,
                    )
        except OSError:
            logger.warning("Could not move %s to %s, file exists", f, target)


if __name__ == '__main__':
    logger.info("Executing %s", " ".join(sys.argv))
    sys.exit(main())

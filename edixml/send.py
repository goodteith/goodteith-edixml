#!/usr/bin/env python3
# vim:fileencoding=utf-8

from __future__ import print_function

import argparse
import logging
import sys

from edixml.connection import (
    connect_to_adapter,
    ensure_dirs_exist,
    ensure_files_exist,
    parse_yaml_file,
)


logger = logging.getLogger(__name__)


def _create_parser():
    parser = argparse.ArgumentParser(
        description="Send a claims to the Practice Board")
    parser.add_argument(
        'input', type=argparse.FileType('r'), nargs='?',
        help="Location of input yaml file")
    parser.add_argument(
        '-p', '--print', action='store_true', dest='print_to_screen',
        help='Print resulting xml to screen')
    parser.add_argument(
        '-d', '--dry-run', action='store_true', dest='dry_run',
        help="Simulate a send/receive, don't actually connect to adapter")

    return parser


def main():
    logger.info("Executing %s", " ".join(sys.argv))
    parser = _create_parser()
    args = parser.parse_args(sys.argv[1:])
    ensure_dirs_exist()
    ensure_files_exist()
    if args.dry_run:
        adapter = None
    else:
        adapter = connect_to_adapter()
    if args.input:
        parse_yaml_file(
            args.input, adapter, args.print_to_screen, args.dry_run)
    adapter = None


if __name__ == '__main__':
    sys.exit(main())

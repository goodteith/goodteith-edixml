#!/usr/bin/env python3
# vim:fileencoding=utf-8

from __future__ import print_function

import glob
import logging
import os
import sys

try:
    import win32service
    import win32serviceutil
    import win32event
except ImportError:
    pass

from edixml import __version__
from edixml.connection import (
    check_for_responses,
    check_adapter,
    connect_to_adapter,
    ensure_dirs_exist,
    ensure_files_exist,
)
from edixml.settings import XML_OUTBOX


logger = logging.getLogger(__name__)


class SendAndReceiveService(win32serviceutil.ServiceFramework):
    # Taken from http://www.chrisumbel.com/article/windows_services_in_python
    # You can NET START/STOP the server by the following name
    _svc_name_ = "goodteith-edixml"
    _svc_display_name_ = "Goodteith edixml service"
    _svc_description_ = ("This service reads xml files "
                         "and sends them to the epharmacy adapter")

    def __init__(self, args):
        win32serviceutil.ServiceFramework.__init__(self, args)
        # Create an event to listen for stop requests on
        self.hWaitStop = win32event.CreateEvent(None, 0, 0, None)

    # Core logic of the service
    def SvcDoRun(self):
        rc = None
        logger.info("Executing %s", " ".join(sys.argv))
        logger.info("%s version %s", __name__, __version__)
        ensure_dirs_exist()
        ensure_files_exist()
        adapter = connect_to_adapter()

        # If the stop event hasn't been fired, keep looping
        while rc != win32event.WAIT_OBJECT_0:
            # Check the connection to the adapter. If it isn't working,
            # try restarting the adapter
            (adapter, active) = check_adapter(adapter)

            if adapter is not None and active:
                logger.info("Checking %s for messages", XML_OUTBOX)
                for fn in glob.glob(os.path.join(XML_OUTBOX, '*.xml')):
                    logger.info("Opening %s", fn)
                    with open(fn) as f:
                        payload = f.read()

                    logger.debug("%s payload is %s", fn, payload)
                    logger.info("Sending %s to adapter", fn)
                    adapter.SendRequest(payload)
                    logger.info("Deleting %s", fn)
                    os.remove(fn)

                logger.debug("Waiting 30 seconds to checking for a response")
                rc = win32event.WaitForSingleObject(self.hWaitStop, 30000)
                check_for_responses(adapter)

            # Block for 30 seconds and listen for a stop event
            logger.debug("Sleeping for 30 seconds")
            rc = win32event.WaitForSingleObject(self.hWaitStop, 30000)

        adapter = None

    # Called when we're being shut down
    def SvcStop(self):
        # Tell the SCM we're shutting down
        self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
        # fire the stop event
        win32event.SetEvent(self.hWaitStop)


def main():
    logger.info("Executing %s", " ".join(sys.argv))

    if '--version' in sys.argv:
        print("{} {}".format(__name__, __version__))
        sys.exit(0)

    ensure_dirs_exist()
    ensure_files_exist()

    if sys.platform.startswith('win32'):
        win32serviceutil.HandleCommandLine(SendAndReceiveService)
    elif sys.platform.startswith('linux'):
        adapter = connect_to_adapter()
        for fn in glob.glob(os.path.join(XML_OUTBOX, '*.xml')):
            logger.info("Opening %s", fn)
            with open(fn) as f:
                payload = f.read()

            logger.debug("%s payload is %s", fn, payload)
            logger.info("Sending %s to adapter", fn)
            adapter.SendRequest(payload)
            logger.info("Deleting %s", fn)
            os.remove(fn)

        check_for_responses(adapter)

        adapter = None


if __name__ == '__main__':
    sys.exit(main())

#!/usr/bin/env python3
# vim:fileencoding=utf-8

import logging
import uuid

from lxml import etree


logger = logging.getLogger(__name__)


def parse_acknowledge_response_to_list(tree):
    logger.info('Parsing acknowledge response to list')
    root = tree.getroot()
    logger.debug('Root is %s', root)
    message_type = etree.QName(root.tag).localname
    logger.debug('Message type is %s', message_type)
    ns = root.nsmap[None]
    logger.debug('Default namespace is %s', ns)
    code = tree.find('.//{{{}}}Code'.format(ns))
    logger.debug('Code is %s', code.text)
    description = tree.find('.//{{{}}}Description'.format(ns))
    logger.debug('Description is %s', description.text)
    details = tree.findall('.//{{{}}}Details'.format(ns))
    details_list = []
    for detail in details:
        details_list.append(detail.text)
        logger.debug('Detail is %s', detail.text)
    details = '|'.join(details_list)
    logger.debug('Details joined to single string: %s', details)

    return [[[code.text, description.text, details]]]


def parse_claim_response_to_list(tree):
    """Return a list of lists (files) for conversion to csv"""
    logger.info('Parsing claim response to list')
    root = tree.getroot()
    logger.debug('Root is %s', root)
    message_type = etree.QName(root.tag).localname
    logger.debug('Message type is %s', message_type)
    ns = root.nsmap[None]
    logger.debug('Default namespace is %s', ns)
    result = tree.find('.//{{{}}}ClaimValidationFlag'.format(ns))
    logger.debug('Result is %s', result.text)
    reference = tree.find('.//{{{}}}PracticeClaimReference'.format(ns))
    logger.debug('Reference is %s', reference.text)
    profcode = tree.find('.//{{{}}}ProfCode'.format(ns))
    logger.debug('List number is %s', profcode.text)
    submission_count = tree.find('.//{{{}}}SubmissionClaimCount'.format(ns))
    logger.debug('Submission count is %s', submission_count.text)
    errors = tree.findall('.//{{{}}}Errors'.format(ns))
    if errors is not None:
        logger.info('Errors included in payload')

    error_list = []
    for error in errors:
        error_data = [message_type, reference.text, profcode.text]
        code = error.find('.//{{{}}}ErrorCode'.format(ns))
        if code is not None:
            error_data.append(code.text)
        else:
            error_data.append('')
        description = error.find('.//{{{}}}ErrorDescription'.format(ns))
        if description is not None:
            error_data.append(description.text)
        else:
            error_data.append('')
        additional = error.find(
            './/{{{}}}ErrorAdditionalInfo'.format(ns))
        if additional is not None:
            error_data.append(additional.text)
        else:
            error_data.append('')
        error_data.append(submission_count.text)
        logger.debug('Error data: %s', error_data)
        error_list.append(error_data)

    return [
        [
            [
                reference.text, profcode.text, submission_count.text,
                result.text
            ]
        ],
        error_list
    ]


def parse_patient_details_response_to_list(tree):
    logger.info('Parsing patient details response to list')
    root = tree.getroot()
    logger.debug('Root is %s', root)
    message_type = etree.QName(root.tag).localname
    logger.debug('Message type is %s', message_type)
    ns = root.nsmap[None]
    logger.debug('Default namespace is %s', ns)
    result = tree.find('.//{{{}}}PatientFoundFlag'.format(ns))
    logger.debug('Result is %s', result.text)
    reference = tree.find('.//{{{}}}PatientDetailsRequestReference'.format(ns))
    logger.debug('Reference is %s', reference.text)
    response_reference = tree.find(
        './/{{{}}}PatientDetailsResponseReference'.format(ns))
    logger.debug('Response reference is %s', response_reference.text)
    profcode = tree.find('.//{{{}}}ProfCode'.format(ns))
    logger.debug('List number is %s', profcode.text)
    found_patient = [reference.text, result.text, response_reference.text]
    chi_number = tree.find(
        './/{{{}}}MatchedCHINumber'.format(ns))
    if chi_number is not None:
        logger.debug('Matched Chi Number is %s', chi_number.text)
        found_patient.append(chi_number.text)

    patient_list_tree = tree.findall('.//{{{}}}PatientDetails'.format(ns))
    if patient_list_tree is not None:
        logger.info('Patient list in details response')
    patient_list = []
    for patient in patient_list_tree:
        patient_data = [reference.text]
        for elem in ['Title', 'FamilyName', 'BirthFamilyName',
                     'GivenName', 'DateOfBirth', 'Sex', 'PostCode']:
            item = patient.find('.//{{{}}}{}'.format(ns, elem))
            if item is not None:
                patient_data.append(item.text)
            else:
                patient_data.append('')
        logger.debug('Patient data: %s', patient_data)
        patient_list.append(patient_data)

    treatment_list_tree = tree.findall('.//{{{}}}Items'.format(ns))
    if treatment_list_tree is not None:
        logger.info('Patient treatment in details response')
    treatment_list = []
    for item in treatment_list_tree:
        treatment_data = [reference.text]
        for elem in ['TreatmentItem', 'TreatmentDescription',
                     'ToothNotation', 'LastTreatmentClaimDate']:
            data_nugget = item.find('.//{{{}}}{}'.format(ns, elem))
            if data_nugget is not None:
                treatment_data.append(data_nugget.text)
            else:
                treatment_data.append('')
        logger.debug('Treatment data: %s', treatment_data)
        treatment_list.append(treatment_data)

    errors = tree.findall('.//{{{}}}Errors'.format(ns))
    if errors is not None:
        logger.info('Errors included in payload')

    error_list = []
    for error in errors:
        error_data = [message_type, reference.text, profcode.text]
        error_data.extend([elem.text for elem in error.iterchildren()])
        logger.debug('Error data: %s', error_data)
        error_list.append(error_data)

    data = [
        [found_patient],
        patient_list,
        treatment_list,
        error_list,
    ]

    return data


def parse_reconciliation_response_to_list(tree):
    logger.info('Parsing reconciliation response to list')
    root = tree.getroot()
    logger.debug('Root is %s', root)
    message_type = etree.QName(root.tag).localname
    logger.debug('Message type is %s', message_type)
    ns = root.nsmap[None]
    logger.debug('Default namespace is %s', ns)
    result = tree.find('.//{{{}}}DetailsAvailableFlag'.format(ns))
    if result.text == 'true':
        logger.debug('Result is %s', result.text)
    else:
        logger.warning('No reconciliation file available')

    schedules = tree.findall('.//{{{}}}ScheduleDetails'.format(ns))

    schedule_list = []
    items_service_list = []
    adjustments_list = []
    tooth_specific_list = []
    for schedule in schedules:
        list_number = schedule.find('.//{{{}}}ListNumber'.format(ns))
        logger.debug('List number: %s', list_number.text)
        schedule_number = schedule.find('.//{{{}}}ScheduleNumber'.format(ns))
        logger.debug('Schedule number: %s', schedule_number.text)
        schedule_data = [list_number.text, schedule_number.text]
        logger.debug('Schedule data: %s', schedule_data)
        schedule_list.append(schedule_data)

        items_of_service_tree = schedule.findall(
            './/{{{}}}ItemsOfService'.format(ns))
        for item in items_of_service_tree:
            item_data = [list_number.text, schedule_number.text]
            reference_number = item.find('.//{{{}}}ReferenceNumber'.format(ns))
            item_data.append(reference_number.text)
            for elem in ['AmountAuthorised', 'PatientChargeAuthorised',
                         'RemissionAmount', 'AdditionalPercentage',
                         'CHINumber']:
                nugget = item.find('.//{{{}}}{}'.format(ns, elem))
                if nugget is not None:
                    item_data.append(nugget.text)
                else:
                    item_data.append('')

            logger.debug('Item data: %s', item_data)
            items_service_list.append(item_data)

            adjustment_narrative_tree = item.findall(
                './/{{{}}}AdjustmentNarratives'.format(ns))
            for adjustment in adjustment_narrative_tree:
                unique_id = str(uuid.uuid4())
                adjustment_data = [
                    list_number.text, schedule_number.text,
                    reference_number.text, unique_id
                ]
                adjustment_data.extend([
                    elem.text for elem in adjustment.iterchildren()
                    if elem.text is not None]
                )
                logger.debug('Adjustment data: %s', adjustment_data)
                adjustments_list.append(adjustment_data)

                tooth_tree = adjustment.findall(
                    './/{{{}}}ToothSpecificNarratives'.format(ns))
                for tooth in tooth_tree:
                    tooth_data = [
                        list_number.text, schedule_number.text,
                        reference_number.text, unique_id,
                    ]
                    tooth_data.extend([
                        elem.text for elem in tooth.iterchildren()]
                    )
                    logger.debug('Tooth specific data: %s', tooth_data)
                    tooth_specific_list.append(tooth_data)

    errors = tree.findall('.//{{{}}}Errors'.format(ns))
    if errors is not None:
        logger.info('Errors included in payload')

    error_list = []
    for error in errors:
        error_data = [message_type, '', '']
        error_data.extend([elem.text for elem in error.iterchildren()])
        logger.debug('Error data: %s', error_data)
        error_list.append(error_data)

    data = [
        schedule_list,
        items_service_list,
        adjustments_list,
        tooth_specific_list,
        error_list,
    ]

    return data

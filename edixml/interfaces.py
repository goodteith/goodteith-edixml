#!/usr/bin/env python3
# vim:fileencoding=utf-8

import datetime
import logging
import uuid
import yaml

from lxml import etree

from edixml import __version__ as VERSION
from edixml.exceptions.base import (
    InvalidYamlFileError,
    UnknownMessageTypeError,
)

from edixml.parsers import (
    parse_acknowledge_response_to_list,
    parse_claim_response_to_list,
    parse_patient_details_response_to_list,
    parse_reconciliation_response_to_list,
)
from edixml.models.dental_claim import (
    DentalClaimRequest,
    DentalClaimAppBodies,
    DentalClaimRequestEnv,
)
from edixml.models.patient_details_request import (
    DentalPatientDetailsRequest,
    DentalPatientDetailsAppBodies,
    DentalPatientDetailsRequestEnv,
)
from edixml.models.reconciliation_file_request import (
    DentalReconciliationFileRequest,
    DentalReconciliationFileAppBodies,
    DentalReconciliationFileRequestEnv,
)
from edixml.models.transaction_header import (
    AppServiceName,
    AppSttl,
    AppTransDateTime,
    AppTransId,
    AppTransStepRef,
    BodyDefinition,
    BodyDefinitions,
    MsgPriority,
    MsgStatus,
    Name,
    OrganisationId,
    OrganisationName,
    OrganisationType,
    ProductAuthor,
    ProductName,
    SenderDetails,
    Signing,
    Software,
    TransactionHeader,
)
from edixml.exceptions.base import XmlValidationError
from edixml.settings import (
    APPSTTL_VALUES,
    IMMEDIATE_MESSAGES,
    MSG_STATUS,
    SOFTWARE_NAME,
    SOFTWARE_AUTHOR
)
from edixml.file_writers import (
    write_acknowledge_csv_to_file,
    write_claim_csv_to_file,
    write_patient_details_csv_to_file,
    write_reconciliation_csv_to_file,
)


logger = logging.getLogger(__name__)

MESSAGE_CLASS_DICT = {
    'DentalClaimRequest': DentalClaimRequest,
    'DentalReconciliationFileRequest': DentalReconciliationFileRequest,
    'DentalPatientDetailsRequest': DentalPatientDetailsRequest,
}

ENVELOPE_CLASS_DICT = {
    'DentalClaimRequest': DentalClaimRequestEnv,
    'DentalReconciliationFileRequest': DentalReconciliationFileRequestEnv,
    'DentalPatientDetailsRequest': DentalPatientDetailsRequestEnv,
}

APPBODIES_CLASS_DICT = {
    'DentalClaimRequest': DentalClaimAppBodies,
    'DentalReconciliationFileRequest': DentalReconciliationFileAppBodies,
    'DentalPatientDetailsRequest': DentalPatientDetailsAppBodies,
}

RESPONSE_PARSE_FUNC_DICT = {
    'Acknowledge': parse_acknowledge_response_to_list,
    'DentalClaimResponse': parse_claim_response_to_list,
    'DentalPatientDetailsResponse': parse_patient_details_response_to_list,
    'DentalReconciliationFileResponse': parse_reconciliation_response_to_list,
}

CSV_WRITER_FUNC_DICT = {
    'Acknowledge': write_acknowledge_csv_to_file,
    'DentalClaimResponse': write_claim_csv_to_file,
    'DentalPatientDetailsResponse': write_patient_details_csv_to_file,
    'DentalReconciliationFileResponse': write_reconciliation_csv_to_file,
}


def load_yaml_document(yaml_string):
    payload = yaml.load_all(yaml_string, Loader=yaml.loader.BaseLoader)
    logger.debug('Payload from yaml: %s', payload)
    return payload


def yaml_to_class(payload):
    # Strip out any empty items in top of dictionary
    payload = {i: j for i, j in payload.items() if j != ''}
    logger.debug('Payload with empties removed: %s', payload)
    try:
        message_type = payload['MessageType']
        logger.info('Messagetype from payload: %s', message_type)
    except KeyError:
        logger.exception('MessageType not included in yaml')
        raise InvalidYamlFileError("YAML is missing MessageType")

    try:
        cls = MESSAGE_CLASS_DICT[message_type]
        logger.debug('Class for message type: %s', cls.__name__)
    except KeyError:
        logger.exception('No class associated with %s', message_type)
        raise InvalidYamlFileError("Unrecognised message type: '{}'".format(
            message_type))

    result = cls().bootstrap_from_yaml(payload)
    return result


def build_transaction_header(tree, timestamp=None):
    """Create a transaction header for an existing class tree.

    NOTE: tree must have passed validation before using this builder.

    """
    logger.info('Building transaction header')
    header = TransactionHeader()
    logger.debug('Adding msg priority')
    header.msg_priority = _create_msg_priority(tree)
    logger.debug('Adding msg status')
    header.msg_status = _create_msg_status(tree)
    logger.debug('Adding sender details')
    header.sender_details = _create_sender_details(tree)
    logger.debug('Adding software')
    header.software = _create_software()
    logger.debug('Adding app_service_name')
    header.app_service_name = _create_app_service_name()
    logger.debug('Adding app_sttl')
    header.app_sttl = _create_app_sttl(tree)
    logger.debug('Adding app_trans_id')
    header.app_trans_id = _create_app_trans_id()
    logger.debug('Adding app_trans_step_ref')
    header.app_trans_step_ref = _create_app_trans_step_ref()
    logger.debug('Adding app_trans_date_time')
    header.app_trans_date_time = _create_app_trans_date_time(timestamp)
    header.body_definitions = _create_body_definitions(tree)

    return header


def etree_to_csv(tree):
    logger.info('Generating csv from etree')
    try:
        root = tree.getroot()
    except AttributeError:
        root = tree
        tree = etree.ElementTree(root)
    logger.debug('Root retrieved: %s', root)
    message_type = etree.QName(root.tag).localname
    logger.debug('Message type retrieved: %s', message_type)
    try:
        func = RESPONSE_PARSE_FUNC_DICT[message_type]
        logger.debug('parser calculated as %s', func)
        writer = CSV_WRITER_FUNC_DICT[message_type]
        logger.debug('writer calculated as %s', writer)
    except KeyError:
        logger.exception('No parser or writer associated with %s',
                         message_type)
        raise UnknownMessageTypeError(
            "Don't recognise {}".format(message_type)
        )
    data = func(tree)
    writer(data)


def wrap_tree_in_envelope(tree, timestamp=None):
    message_type = tree.node_name
    logger.info('Requesting transaction header')
    header = build_transaction_header(tree, timestamp)
    AppBody = APPBODIES_CLASS_DICT[message_type]
    appbodies = AppBody(tree)
    Envelope = ENVELOPE_CLASS_DICT[message_type]
    envelope = Envelope(header, appbodies)
    logger.info('Converting document to etree')

    return envelope.as_etree()


def _create_msg_priority(tree):
    if tree.node_name in IMMEDIATE_MESSAGES:
        logger.debug('Marking message as immediate priority')
        msg_priority = MsgPriority('immediate')
    else:
        logger.debug('Marking message as normal priority')
        msg_priority = MsgPriority('normal')

    return msg_priority


def _create_msg_status(tree):
    return MsgStatus(MSG_STATUS)


def _create_sender_details(tree):
    if tree.node_name == 'DentalClaimRequest':
        practice_details = tree.practitioner_organisation_details
    elif tree.node_name == 'DentalReconciliationFileRequest':
        practice_details = tree.requesting_organisation_details
    elif tree.node_name == 'DentalPatientDetailsRequest':
        practice_details = tree.requesting_organisation_details
    else:
        logger.critical("%s is not supported by _create_sender_details",
                        tree.node_name)

    try:
        org_name = practice_details.organisation_name.text
        logger.debug('Org name: %s', org_name)
        org_type = practice_details.organisation_type.text
        logger.debug('Org type: %s', org_type)
        org_id = practice_details.organisation_id.id_value.text
        logger.debug('Org id: %s', org_id)
    except AttributeError:
        logger.exception('Couldn\'t retrieve organisation details')
        raise XmlValidationError(
            "Can't build transaction header without sender details")

    return SenderDetails(
        OrganisationType(org_type),
        OrganisationId(org_id),
        OrganisationName(org_name)
    )


def _create_software():
    return Software(
        product_name=ProductName(
            SOFTWARE_NAME, attributes={
                'ProductVersion': VERSION
            }
        ),
        product_author=ProductAuthor(SOFTWARE_AUTHOR),
    )


def _create_app_service_name():
    return AppServiceName('Dental', attributes={'ServiceVersion': '1.0'})


def _create_app_sttl(tree):
    if tree.node_name in APPSTTL_VALUES:
        sttl = APPSTTL_VALUES[tree.node_name]
        logger.debug('sttl value detected: %d', sttl)
        return AppSttl(sttl)
    else:
        logger.exception('No appsttl for %s', tree.node_name)
        raise XmlValidationError("Can't recognise this message type")


def _create_app_trans_id():
    return AppTransId(text=uuid.uuid4())


def _create_app_trans_step_ref():
    return AppTransStepRef(1)


def _create_app_trans_date_time(timestamp=None):
    if timestamp is None:
        return AppTransDateTime(datetime.datetime.now())
    else:
        return AppTransDateTime(timestamp)


def _create_body_definitions(tree):
    try:
        if tree.signature is not None:
            logger.debug('Signature has been detected. Signing set to True')
            signing = True
        else:
            logger.debug('No Signature detected. Signing set to False')
            signing = False
    except AttributeError:
        logger.debug('Model %s does not support signatures', tree.node_name)
        signing = False
    return BodyDefinitions(
        body_definition=[
            BodyDefinition(
                name=Name(tree.node_name),
                signing=Signing(signing),
            )
        ]
    )

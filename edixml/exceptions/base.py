#!/usr/bin/env python3
# vim:fileencoding=utf-8


class ProcessClaimError(Exception):
    """Base-class for all exceptions raised by this module."""

    def __init__(self, *args, **kwargs):
        super(ProcessClaimError, self).__init__(*args)


class InvalidYamlFileError(ProcessClaimError):
    """Yaml syntax is incorrect."""


class UnknownMessageTypeError(ProcessClaimError):
    """Unknown message type."""


class XmlMissingChildError(ProcessClaimError):
    """Missing child."""

    def __init__(self, *args, **kwargs):
        model = kwargs.get('model', None)
        child = kwargs.get('child', None)
        if child is not None and model is not None:
            msg = "{} requires {} child.".format(model, child)
        try:
            super(XmlMissingChildError, self).__init__(msg, *args, **kwargs)
        except NameError:
            # Allow for exception to be used like a normal one.
            super(XmlMissingChildError, self).__init__(*args, **kwargs)


class XmlValidationError(ProcessClaimError):
    """Validation of element failed."""

    def __init__(self, *args, **kwargs):
        value = kwargs.get('value', None)
        target = kwargs.get('target', None)
        node = kwargs.get('node', None)
        if value is not None:
            if target is not None:
                msg = ("Value '{}' does not match xds pattern "
                       "restrictions for '{}'".format(value, target))
            else:
                msg = ("Value '{}' does not match xds pattern "
                       "restrictions".format(value))
        else:
            if target is not None:
                msg = ("Value does not match xds pattern "
                       "restrictions for ".format(target))
            else:
                msg = ("Value is invalid")

        if node is not None:
            msg += " for model {}".format(node)

        try:
            super(XmlValidationError, self).__init__(msg, *args, **kwargs)
        except NameError:
            # Allow for exception to be used like a normal one.
            super(XmlValidationError, self).__init__(*args, **kwargs)


class CsvValidationError(ProcessClaimError):
    pass

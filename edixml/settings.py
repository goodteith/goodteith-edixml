#!/usr/bin/env python3
# vim:fileencoding=utf-8

from __future__ import print_function

import logging
import logging.config
import os
import sys

try:
    # Python 3
    import configparser
except ImportError:
    import ConfigParser as configparser

from appdirs import AppDirs


DEBUG = False
MSG_STATUS = 'live'
IMMEDIATE_MESSAGES = ('DentalPatientDetailsRequest',)
WRITE_CSV_HEADERS = True
SOFTWARE_NAME = 'Goodteith:eDental1'
SOFTWARE_AUTHOR = 'Goodteith Software'
ADAPTER_SERVICE_NAME = 'AdapterServiceHost'

APPDIRS = AppDirs('goodteith-edixml', 'goodteith')

# don't archive files that have been modified within this many seconds
ARCHIVE_FILES_THRESHOLD = 2

if 'PythonService.exe' in sys.argv[0]:
    LOGDIR = APPDIRS.site_data_dir
else:
    LOGDIR = APPDIRS.user_log_dir

try:
    os.makedirs(LOGDIR)
except OSError:
    pass


APPSTTL_VALUES = {
    'DentalClaimRequest': 259200,
    'DentalPriorApprovalRequest': 259200,
    'DentalPatientDetailsRequest': 120,
    'DentalPriorApprovalUpdatesRequest': 15840,
    'DentalReconciliationFileRequest': 600,
}


DEBUG_LOG_FILE = os.path.join(LOGDIR, 'edixml.log')
fm_log_name = 'edental-log.txt'
fm_send_error_name = 'edental-send-errors.txt'

if 'PythonService.exe' in sys.argv[0]:
    CONFIGDIR = APPDIRS.site_config_dir
else:
    CONFIGDIR = APPDIRS.user_config_dir

CONFIGFILE = 'edixml.ini'
CONFIG_PATH_SECTION = 'paths'
CONFIG_COMM_DIR_OPTION = 'fm_communication_directory'

config = configparser.ConfigParser()
config.read(os.path.join(CONFIGDIR, CONFIGFILE))

if not config.has_section(CONFIG_PATH_SECTION):
    config.add_section(CONFIG_PATH_SECTION)

# Check for config file here
# If it doesn't exist, use the defaults in this file
# Otherwise, use the values in the config file
if not config.has_option(CONFIG_PATH_SECTION, CONFIG_COMM_DIR_OPTION):
    if sys.platform.startswith('win32'):
        if os.path.isdir('S:'):
            drive = 'S:'
            base_communication_dir = os.path.join(drive, os.sep, 'edixml')
            if not os.path.isdir(base_communication_dir):
                try:
                    os.makedirs(base_communication_dir)
                except PermissionError:
                    print(
                        "Could not create {}".format(base_communication_dir),
                        file=sys.stderr)
                    sys.exit(1)
        else:
            import tempfile
            base_communication_dir = tempfile.mkdtemp()
    elif sys.platform.startswith('linux'):
        base_communication_dir = APPDIRS.user_data_dir
    else:
        base_communication_dir = ''
    # Set a flag to say that the config file needs to be written if it
    # doesn't exist yet
    config.set(
        CONFIG_PATH_SECTION, CONFIG_COMM_DIR_OPTION, base_communication_dir)
    NO_CONFIG_FILE = True
else:
    base_communication_dir = config.get(
        CONFIG_PATH_SECTION, CONFIG_COMM_DIR_OPTION)
    NO_CONFIG_FILE = False

FILEMAKER_LOG_FILE = os.path.join(base_communication_dir, fm_log_name)
FILEMAKER_SEND_ERROR_FILE = os.path.join(
    base_communication_dir, fm_send_error_name)


LOGGING_CONFIG = dict(
    version=1,
    formatters={
        'precise': {
            'format': '%(asctime)s %(name)-12s %(levelname)-8s %(message)s'
        }
    },
    handlers={
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'precise',
            'level': logging.WARNING,
        },
        'file': {
            'class': 'logging.handlers.RotatingFileHandler',
            'formatter': 'precise',
            'level': logging.DEBUG,
            'filename': DEBUG_LOG_FILE,
            'maxBytes': 10485760,
            'backupCount': 10,
        },
        'fmfile': {
            'class': 'logging.FileHandler',
            'mode': 'a',
            'filename': FILEMAKER_LOG_FILE,
            'level': logging.WARNING,
        }
    },
    root={
        'handlers': ['console', 'file', 'fmfile'],
        'level': logging.DEBUG,
    },
)

if 'add_to_pile' in sys.argv[0]:
    LOGGING_CONFIG['handlers']['fmerrorfile'] = {
        'class': 'logging.FileHandler',
        'mode': 'w',
        'filename': FILEMAKER_SEND_ERROR_FILE,
        'level': logging.WARNING,
    }
    LOGGING_CONFIG['root']['handlers'].append('fmerrorfile')

logging.config.dictConfig(LOGGING_CONFIG)
logger = logging.getLogger(__name__)

SAVEDIR = base_communication_dir

# If the config file flag is set (need to write config file, write it
if NO_CONFIG_FILE:
    try:
        os.makedirs(CONFIGDIR)
    except OSError:
        pass
    with open(os.path.join(CONFIGDIR, CONFIGFILE), 'wb') as configfile:
        config.write(configfile)
    print("Config file has been written to '{}'.".format(
        os.path.join(CONFIGDIR, CONFIGFILE)))
    print("You should confirm the paths are correct.")
    logger.info("Writing config to %s", os.path.join(CONFIGDIR, CONFIGFILE))
else:
    logger.info("Using %s as config file", os.path.join(CONFIGDIR, CONFIGFILE))

CSV_SAVEDIR = os.path.join(SAVEDIR, 'csvs')
XML_SAVEDIR = os.path.join(SAVEDIR, 'xmls')
RESPONSE_SAVEDIR = os.path.join(SAVEDIR, '.responses')
CSV_ARCHIVE_DIR = os.path.join(CSV_SAVEDIR, '.archive')
XML_ARCHIVE_DIR = os.path.join(XML_SAVEDIR, '.archive')
XML_OUTBOX = os.path.join(SAVEDIR, 'outbox')
# Maximum age of outbox messages in seconds
XML_OUTBOX_MAX_AGE = 120

ACKNOWLEDGE_CSV_FILENAME = 'acknowledge.csv'
ERROR_CSV_FILENAME = 'errors.csv'
CLAIM_RESPONSE_CSV_FILENAME = 'dental-claim-response.csv'
PATIENT_DETAILS_RESPONSE_CSV_FILENAME = 'patient-details-response.csv'
PATIENT_DETAILS_LIST_CSV_FILENAME = 'patient-list.csv'
PATIENT_DETAILS_TREATMENT_CSV_FILENAME = 'patient-treatment.csv'
RECONCILIATION_ADJUSTMENT_CSV_FILENAME = (
    'dental-reconciliation-file-adjustment-narratives.csv'
)
RECONCILIATION_ITEMS_OF_SERVICE_CSV_FILENAME = (
    'dental-reconciliation-file-items-of-service.csv'
)
RECONCILIATION_FILE_RESPONSE_CSV_FILENAME = (
    'dental-reconciliation-file-response.csv'
)
RECONCILIATION_TOOTH_NARRATIVES_CSV_FILENAME = (
    'dental-reconciliation-file-tooth-specific-narratives.csv'
)

CSV_FILENAMES = [
    ACKNOWLEDGE_CSV_FILENAME, ERROR_CSV_FILENAME,
    CLAIM_RESPONSE_CSV_FILENAME, PATIENT_DETAILS_RESPONSE_CSV_FILENAME,
    PATIENT_DETAILS_LIST_CSV_FILENAME,
    PATIENT_DETAILS_TREATMENT_CSV_FILENAME,
    RECONCILIATION_ADJUSTMENT_CSV_FILENAME,
    RECONCILIATION_ITEMS_OF_SERVICE_CSV_FILENAME,
    RECONCILIATION_FILE_RESPONSE_CSV_FILENAME,
    RECONCILIATION_TOOTH_NARRATIVES_CSV_FILENAME
]

#!/usr/bin/env python3
# vim:fileencoding=utf-8

import argparse
import csv
import logging
import sys
try:
    import tkinter
except ImportError:
    import Tkinter as tkinter

try:
    import tkinter.filedialog as filedialog
except ImportError:
    import tkFileDialog as filedialog


from edixml import __version__
from edixml.connection import (
    ensure_dirs_exist,
    ensure_files_exist,
)
from edixml.file_writers import write_reconciliation_csv_to_file

logger = logging.getLogger(__name__)


def _create_parser():
    parser = argparse.ArgumentParser(
        description="Import in an eSchedule as if it was an epharm schedule")
    parser.add_argument(
        'input', type=argparse.FileType('r'), nargs='?',
        help="Location of the eSchedule csv file to parse"
    )
    parser.add_argument(
        '--version', action='version',
        version='%(prog)s {version}'.format(version=__version__))

    return parser


def _get_list_number(s):
    return s.replace('List Number ', '')


def _split_case_id(s):
    logger.info("Splitting Case ID %s into constituent parts", s)
    list_number = s[:5]
    logger.debug("List number calculated as %s", list_number)
    reference_number = s[5:-1]
    logger.debug("Reference number calculated as %s", reference_number)
    submission_count = s[-1]
    logger.debug("Submission count calculated as %s", submission_count)

    return (list_number, reference_number, submission_count)


def _strip_pound(s):
    return s.replace('£', '')


def parse_eschedule(input_stream):
    logger.info("Parsing eschedule")
    logger.debug("Creating blank schedules dictionary to hold info")
    schedules = {
    }
    logger.debug("Schedule dict: %s", schedules)
    logger.info("Opening input file %s", input_stream.name)
    reader = csv.reader(input_stream)
    logger.debug("Looping through data from input stream")
    for row in reader:
        logger.debug("Row contains: %s", row)
        if len(row) == 0:
            logger.debug("Row is blank. Skipping")
            continue
        elif len(row) == 1:
            payload = row[0]
            if payload.startswith('Load Date'):
                logger.debug("Header line detected. Ignoring")
                continue
            else:
                logger.info("Extracting the schedule number")
                schedule_number = payload
                logger.debug("Schedule number: %s", schedule_number)
        elif len(row) == 14:
            logger.info("Row is the footer column names. Ignoring")
            continue
        elif len(row) == 17:
            if row[0].startswith('List Number'):
                logger.info("Column header row detected")
                pat_detail_headers = row[:]
                logger.debug("Headers recorded as %s", pat_detail_headers)
            else:
                logger.info("Retrieving data from row")
                payload = dict(zip(pat_detail_headers, row))
                logger.debug("Payload stored as %s", payload)
                case_id = payload.get('Case ID')
                logger.debug("Case ID is %s", case_id)
                schedules[case_id].update(payload)
                logger.debug("Storing schedule number as %s", schedule_number)
                schedules[case_id]['Schedule Number'] = schedule_number
        elif len(row) == 25:
            if row[0].startswith('List Number'):
                logger.info("Column header row detected")
                payment_headers = row[:]
                logger.debug("Headers recorded as %s", payment_headers)
            else:
                logger.info("Retrieving data from row")
                payload = dict(zip(payment_headers, row))
                logger.debug("Payload stored as %s", payload)
                case_id = payload.get('Case ID')
                logger.debug("Case ID is %s", case_id)
                (_, reference_number, _) = _split_case_id(
                    payload.get('Case ID', ''))
                logger.debug("Reference number is %s", reference_number)
                payload['ReferenceNumber'] = reference_number
                schedules[case_id] = payload
        else:
            continue

    parsed_schedules = {}

    logger.info("Tidying up schedules into mutiple schedules")
    for caseid, payload in schedules.items():
        logger.debug("Parsing CaseID %s", caseid)
        schedule_number = payload.get('Schedule Number')
        logger.debug("Schedule number: %s", schedule_number)
        if schedule_number not in parsed_schedules:
            logger.info("Not seen this schedule before. Adding to dict")
            parsed_schedules[schedule_number] = {}
        list_number = payload.get('List Number')
        logger.debug("List number: %s", list_number)
        if list_number not in parsed_schedules[schedule_number]:
            logger.info("Not seen this list number before. Adding to dict.")
            parsed_schedules[schedule_number][list_number] = []
        logger.info("Constructing item of service")
        item = {
            'ReferenceNumber': payload.get('ReferenceNumber', ''),
            'AmountAuthorised': payload.get('Item of Service Fee', ''),
            'PatientChargeAuthorised': payload.get(
                'Patient Statutory Charge', ''),
            'RemissionAmount': payload.get('DSS Remission', ''),
            'AdditionalPercentage': payload.get('% Award', ''),
            'CHINumber': payload.get('CHI Number', '')
        }
        logger.debug("Item of service: %s", item)
        parsed_schedules[schedule_number][list_number].append(item)

    return parsed_schedules


def parse_schedule_dict_to_lists(schedules):
    logger.info("Converting dict to lists")
    
    headers = [
        "ReferenceNumber", "AmountAuthorised",
        "PatientChargeAuthorised", "RemissionAmount",
        "AdditionalPercentage", "CHINumber",
    ]

    schedule_list = []
    items_service_list = []
    for schedule_number, payload in schedules.items():
        logger.debug("Schedule number: %s", schedule_number)
        for list_number, items in payload.items():
            logger.debug("List number: %s", list_number)
            schedule_list.append([list_number, schedule_number])
            for item in items:
                service_item = [list_number, schedule_number]
                for header in headers:
                    value = item.get(header, '')
                    logger.debug("Retrieved %s: %s from item", header, value)
                    service_item.append(value)
                logger.debug("Adding %s to items_service_list", service_item)
                items_service_list.append(service_item)

    adjustments_list = []
    errors_list = []
    tooth_specific_list = []

    return [
        schedule_list, items_service_list, adjustments_list,
        tooth_specific_list, errors_list
    ]


def _get_filename_from_user():
    root = tkinter.Tk()
    logger.info("Getting filename from user")
    filename = filedialog.askopenfilename(
        parent=root,
        title="Select schedule csv",
        filetypes=(("CSV files", "*.csv"),))
    root.destroy()
    if len(filename) > 0:
        logger.info("User selected %s", filename)
        return filename
    else:
        logger.warning("User did not supply filename")
        return False


def main():
    logger.info("Executing %s", " ".join(sys.argv))
    logger.info("%s version %s", __name__, __version__)
    parser = _create_parser()
    args = parser.parse_args(sys.argv[1:])
    ensure_dirs_exist()
    ensure_files_exist()
    if args.input:
        close_file = False
        input = args.input
    else:
        close_file = True
        inputfilename = _get_filename_from_user()
        input = open(inputfilename, 'r')

    if input:
        schedule = parse_eschedule(input)
        output_csv_data = parse_schedule_dict_to_lists(schedule)
        write_reconciliation_csv_to_file(output_csv_data)

    if close_file:
        input.close()


if __name__ == "__main__":
    sys.exit(main())

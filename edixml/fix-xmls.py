#!/usr/bin/env python3
# vim:fileencoding=utf-8

import glob
import os
import re
import uuid


xml_files = os.path.join(
    os.path.expanduser('~'), 'nextcloud', 'goodteith',
    'accred-xmls-copy')
for fn in glob.glob(os.path.join(xml_files, '*.xml')):
    with open(fn, 'r') as f:
        lines = f.readlines()
    uid = str(uuid.uuid4())
    for line in lines:
        if 'AppTransID' in line:
            old = line.rstrip().replace(
                '<AppTransID>', '').replace('</AppTransID>', '')
            print('{} -> {}'.format(old, uid))
    with open(fn, 'w') as f:
        for line in lines:
            line_old = line
            line = re.sub(
                r'<AppTransID>.*</AppTransID>',
                '<AppTransID>{}</AppTransID>'.format(uid), line
            )
            line = re.sub(
                r'<OrganisationID>000',
                '<OrganisationID>', line
            )
            line = re.sub(
                r'<IdValue>000',
                '<IdValue>', line
            )
            f.write(line)

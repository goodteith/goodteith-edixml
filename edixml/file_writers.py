#!/usr/bin/env python3
# vim:fileencoding=utf-8


import csv
import datetime
import logging
import os
import random
import sys


from edixml.settings import (
    ACKNOWLEDGE_CSV_FILENAME,
    CLAIM_RESPONSE_CSV_FILENAME,
    CSV_SAVEDIR,
    ERROR_CSV_FILENAME,
    PATIENT_DETAILS_LIST_CSV_FILENAME,
    PATIENT_DETAILS_RESPONSE_CSV_FILENAME,
    PATIENT_DETAILS_TREATMENT_CSV_FILENAME,
    RECONCILIATION_ADJUSTMENT_CSV_FILENAME,
    RECONCILIATION_FILE_RESPONSE_CSV_FILENAME,
    RECONCILIATION_ITEMS_OF_SERVICE_CSV_FILENAME,
    RECONCILIATION_TOOTH_NARRATIVES_CSV_FILENAME,
    RESPONSE_SAVEDIR,
    WRITE_CSV_HEADERS,
)


logger = logging.getLogger(__name__)


def write_response_to_file(response):
    logger.debug('Got %s response to write to %s', response, RESPONSE_SAVEDIR)
    name = datetime.datetime.now().strftime('%Y-%m-%d-%H%M%S')
    name = "{}-{:03}.xml".format(name, random.randrange(10**3))
    target = os.path.join(RESPONSE_SAVEDIR, name)
    logger.info('Target file is %s', target)
    with open(target, 'w') as f:
        f.write(response)

    return target


def _write_csv_to_file(doc_name, data, header):
    logger.debug('Got %s to write to %s', data, doc_name)
    target = os.path.join(CSV_SAVEDIR, doc_name)
    logger.info('Target file is %s', target)
    fileexists = os.path.isfile(target)
    logger.debug('Fileexists is %s', fileexists)
    if sys.platform.startswith('win32'):
        mode = 'ab'
        logger.info('Detected windows platform. Mode is \'%s\'', mode)
    elif sys.platform.startswith('linux'):
        mode = 'a'
        logger.info('Detected linux platform. Mode is \'%s\'', mode)
    with open(target, mode) as csvfile:
        logger.debug('Writing csv to %s', target)
        csv_writer = csv.writer(csvfile)
        if not fileexists and WRITE_CSV_HEADERS:
            logger.info('Writing header \'%s\' to %s', header, target)
            csv_writer.writerow(header)
        logger.info('Writing \'%s\' to %s', data, target)
        csv_writer.writerows(data)


def write_acknowledge_csv_to_file(data):
    doc_table = [
        ACKNOWLEDGE_CSV_FILENAME,
    ]
    headers = [
        [
            'Code', 'Description', 'Details'
        ]
    ]

    for name, doc, header in zip(doc_table, data, headers):
        _write_csv_to_file(name, doc, header)


def write_claim_csv_to_file(data):
    doc_table = [
        CLAIM_RESPONSE_CSV_FILENAME,
        ERROR_CSV_FILENAME,
    ]
    headers = [
        [
            "PracticeClaimReference", "ListNumber", "SubmissionCount",
            "ClaimValidationFlag",
        ],
        [
            "MesssageType", "PracticeClaimReference", "ListNumber",
            "ErrorCode", "ErrorText", "ErrorAdditionalInfo",
            "SubmissionCount",
        ],
    ]

    for name, doc, header in zip(doc_table, data, headers):
        _write_csv_to_file(name, doc, header)


def write_patient_details_csv_to_file(data):
    doc_table = [
        PATIENT_DETAILS_RESPONSE_CSV_FILENAME,
        PATIENT_DETAILS_LIST_CSV_FILENAME,
        PATIENT_DETAILS_TREATMENT_CSV_FILENAME,
        ERROR_CSV_FILENAME,
    ]
    headers = [
        [
            "PatientDetailsRequestReference", "PatientFoundFlag",
            "PatientDetailsResponseReference", "MatchedChiNumber"
        ],
        [
            "PatientDetailsRequestReference", "Title", "FamilyName",
            "BirthFamilyName", "GivenName", "DateOfBirth", "Sex",
            "PostCode"
        ],
        [
            "PatientDetailsRequestReference", "TreatmentItem",
            "TreatmentDescription", "ToothNotation", "LastTreatmentClaimDate"
        ],
        [
            "MesssageType", "Reference", "ListNumber", "ErrorCode",
            "ErrorText", "ErrorAdditionalInfo"
        ],
    ]

    for name, doc, header in zip(doc_table, data, headers):
        _write_csv_to_file(name, doc, header)


def write_reconciliation_csv_to_file(data):
    doc_table = [
        RECONCILIATION_FILE_RESPONSE_CSV_FILENAME,
        RECONCILIATION_ITEMS_OF_SERVICE_CSV_FILENAME,
        RECONCILIATION_ADJUSTMENT_CSV_FILENAME,
        RECONCILIATION_TOOTH_NARRATIVES_CSV_FILENAME,
        ERROR_CSV_FILENAME,
    ]
    headers = [
        ["ListNumber", "ScheduleNumber"],
        [
            "ListNumber", "ScheduleNumber", "ReferenceNumber",
            "AmountAuthorised", "PatientChargeAuthorised",
            "RemissionAmount", "AdditionalPercentage", "CHINumber",
        ],
        [
            "ListNumber", "ScheduleNumber", "ReferenceNumber",
            "UniqueID", "Item", "FeeCode", "AdjustmentText",
            "DentistFee"
        ],
        [
            "ListNumber", "ScheduleNumber", "ReferenceNumber", "UniqueID",
            "Item", "FeeCode", "AdjustmentText", "DentistFee"
        ],
        [
            "MesssageType", "Reference", "ListNumber", "ErrorCode",
            "ErrorText", "ErrorAdditionalInfo"
        ],
    ]

    for name, doc, header in zip(doc_table, data, headers):
        _write_csv_to_file(name, doc, header)

#!/usr/bin/env python3
# vim:fileencoding=utf-8


import sys
import time

try:
    import win32com.client
    import pythoncom
except ImportError:
    pass


ListenerBase = win32com.client.getevents(
    'AdapterSuite.ClientApi.epRequestManager')


class Listener(ListenerBase):
    def onResponded(self, *args, **kwargs):
        print("Hey, someone responded")

    def onOutstanding(self, *args, **kwargs):
        print("Hey! There is stuff outstanding!")


def main():
    adapter = win32com.client.DispatchWithEvents(
        'AdapterSuite.ClientApi.epRequestManager',
        Listener)
    adapter.Activate()
    while True:
        pythoncom.PumpWaitingMessages()
        time.sleep(1)


if __name__ == '__main__':
    sys.exit(main())

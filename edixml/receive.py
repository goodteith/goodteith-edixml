#!/usr/bin/env python3
# vim:fileencoding=utf-8

from __future__ import print_function

import argparse
import logging
import sys

from edixml.connection import (
    connect_to_adapter,
    check_for_responses,
    ensure_dirs_exist,
    ensure_files_exist,
    parse_response,
)

logger = logging.getLogger(__name__)


def _create_parser():
    parser = argparse.ArgumentParser(
        description="Receive claims from the Practice Board")
    parser.add_argument(
        '-p', '--print', action='store_true', dest='print_to_screen',
        help='Print resulting xml to screen')
    parser.add_argument(
        '-r', '--read-response', type=argparse.FileType('r'),
        help="Parse a adapter response from a file")

    return parser


def main():
    logger.info("Executing %s", " ".join(sys.argv))
    parser = _create_parser()
    args = parser.parse_args(sys.argv[1:])
    ensure_dirs_exist()
    ensure_files_exist()
    adapter = connect_to_adapter()
    check_for_responses(adapter)
    adapter = None
    if args.read_response:
        logger.info('Parsing response from file')
        parse_response(args.read_response.name)


if __name__ == '__main__':
    sys.exit(main())

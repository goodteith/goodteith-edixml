#!/usr/bin/env python3
# vim:fileencoding=utf-8


import glob
import logging
import os
import shutil
import subprocess
import sys

try:
    import tkinter
except ImportError:
    import Tkinter as tkinter

try:
    import tkinter.filedialog as filedialog
except ImportError:
    import tkFileDialog as filedialog


import win32api

try:
    # Python 3
    import configparser
except ImportError:
    import ConfigParser as configparser

try:
    import _winreg as winreg
except ImportError:
    import winreg

from edixml import __version__
from edixml.settings import (
    APPDIRS,
    CONFIGFILE,
    CONFIG_PATH_SECTION,
    CONFIG_COMM_DIR_OPTION,
)


logger = logging.getLogger(__name__)


if getattr(sys, 'frozen', False):
    # We are running in a bundle
    BASE_DIR = os.path.dirname(sys.executable)
else:
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

logger.info("Current working directory: %s", BASE_DIR)

try:
    PYTHON = winreg.QueryValue(
        winreg.HKEY_LOCAL_MACHINE,
        'SOFTWARE\\WOW6432Node\\Microsoft\\Windows\\CurrentVersion\\App '
        'Paths\\Python.exe')
except WindowsError:
    PYTHON = 'C:\\Python27\\Python.exe'
    logger.warning("Using fallback python location")

logger.info("Looking for python in %s", PYTHON)

if not os.path.isfile(PYTHON):
    logger.critical("Could not find python.exe")
    sys.exit(1)

PIP = os.path.join(
    os.path.dirname(PYTHON),
    'Scripts',
    'pip.exe')

logger.info("Looking for pip in %s", PIP)

if not os.path.isfile(PIP):
    logger.critical("Could not find pip.exe")
    sys.exit(1)

CONFIGDIR = APPDIRS.site_config_dir
logger.info("Config directory detected as: %s", CONFIGDIR)
CONFIG = os.path.join(CONFIGDIR, CONFIGFILE)
logger.info("Config file is %s", CONFIG)


def install_wheel():
    logger.info("Installing wheel")
    files = glob.glob(os.path.join(BASE_DIR, '*.whl'))
    logger.debug("Wheel files detected: %s", files)
    for f in files:
        logger.debug("Checking %s", f)
        if __version__ in f:
            logger.info("%s matches current version %s", f, __version__)
            wheel = f
            break
    else:
        logger.warning("No appropriate wheel file detected")
        return False

    logger.info("Installing %s using %s", wheel, PIP)
    return run_process("\"{}\" install \"{}\"".format(PIP, wheel))


def update_service():
    logger.info("Updating python service")
    script_path = os.path.join(
        os.path.dirname(PYTHON),
        "Lib",
        "site-packages",
        "edixml",
        "send_and_receive.py")

    logger.debug("Script path: %s", script_path)
    if run_process('sc query goodteith-edixml'):
        logger.info("Goodteith edixml is already installed")
        logger.info("Upgrading service")
        return run_process("\"{}\" \"{}\" update".format(PYTHON, script_path))
    else:
        logger.info("Goodteith edixml not installed yet")
        logger.info("Installing com library")
        retcodes = []
        makepy_path = os.path.join(
            os.path.dirname(PYTHON),
            "Lib",
            "site-packages",
            "win32com",
            "client",
            "makepy.py")
        retcodes.append(run_process(
            "\"{}\" \"{}\" \"AdapterSuite_ClientApi\"".format(PYTHON, makepy_path)))
        logger.info("Installing the service")
        retcodes.append(run_process(
            "\"{}\" \"{}\" --startup auto --interactive install".format(
                PYTHON, script_path)))
        retcodes.append(start_service())
        retcodes.append(stop_service())
        return (False not in retcodes)


def stop_service():
    logger.info("Stopping service")
    return run_process("net stop goodteith-edixml")


def start_service():
    logger.info("Starting service")
    return run_process("net start goodteith-edixml")


def run_process(command):
    process = subprocess.Popen(
        command,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )

    stdout, stderr = process.communicate()

    for line in stdout.split('\n'):
        line = line.strip('\r')
        if line:
            logger.info("Process returned: %s", line)
    for line in stderr.split('\n'):
        line = line.strip('\r')
        if line:
            logger.error("Process returned: %s", line)

    if process.returncode != 0:
        logger.warning(
            "%s was terminated by signal %s", command, process.returncode)
        return False
    else:
        logger.info("%s completed successfully", command)
        return True


def get_s_drive():
    s_drive_from_config = _get_s_drive_from_config()
    if s_drive_from_config:
        return s_drive_from_config
    s_drive_from_user = _get_s_drive_from_user()
    if s_drive_from_user:
        return s_drive_from_user

    # Failed to get an s drive.
    return False


def _get_s_drive_from_config():
    config = configparser.ConfigParser()
    config.read(CONFIG)
    if config.has_section(CONFIG_PATH_SECTION):
        if config.has_option(CONFIG_PATH_SECTION, CONFIG_COMM_DIR_OPTION):
            path = config.get(
                CONFIG_PATH_SECTION, CONFIG_COMM_DIR_OPTION)
            if 'temp' in path.split(os.sep) or 'tmp' in path.split(os.sep):
                # Temp directory. Useless.
                return False
            else:
                return path
    return False


def _get_s_drive_from_user():

    root = tkinter.Tk()
    logger.info("Retrieve directory from user")
    dirname = filedialog.askdirectory(
        parent=root, title='Please select the local location of the S drive')
    root.destroy()
    if len(dirname) > 0:
        logger.info("User selected %s", dirname)
    else:
        logger.warning("User didn't select a directory")
        return False
    s_location = os.path.normpath(dirname)

    if os.path.basename(s_location) != 'edixml':
        s_location = os.path.join(s_location, 'edixml')
    _write_config(s_location)

    return s_location


def _write_config(s_location):

    config = configparser.ConfigParser()
    config.add_section(CONFIG_PATH_SECTION)
    config.set(
        CONFIG_PATH_SECTION, CONFIG_COMM_DIR_OPTION, s_location)

    try:
        os.makedirs(CONFIGDIR)
    except OSError:
        pass
    with open(CONFIG, 'wb') as configfile:
        config.write(configfile)
    logger.info("Writing config to %s", CONFIG)


def create_directories(base_dir):
    try:
        os.makedirs(base_dir)
        logger.info("Creating base edixml (%s) directory", base_dir)
    except OSError:
        logger.debug("Directory (%s) exists so not creating it", base_dir)
    except PermissionError:
        logger.error("Permission denied creating %s", base_dir)
        return False

    bin_dir = os.path.join(base_dir, 'bin')
    try:
        os.makedirs(bin_dir)
        logger.info("Creating bin (%s) directory", bin_dir)
    except OSError:
        logger.debug("Directory %s exists so not creating it", bin_dir)
    except PermissionError:
        logger.error("Permission denied creating %s", bin_dir)
        return False

    return True


def install_binaries(base_dir):
    bin_dir = os.path.join(base_dir, 'bin')
    logger.info("Copying binaries to %s directory", bin_dir)
    copy_error = False
    for dir in ['add_to_pile', 'archive_files', 'reconcile_eschedule']:
        if os.path.isdir(os.path.join(bin_dir, dir)):
            logger.info("Deleting old %s dir", dir)
            shutil.rmtree(
                os.path.join(bin_dir, dir)
            )
        logger.info("Copying %s directory", dir)
        try:
            shutil.copytree(
                os.path.join(BASE_DIR, dir),
                os.path.join(bin_dir, dir))
        except shutil.Error as e:
            logger.warning("Could not copy %s: %s", dir, e)
            copy_error = True

    return not copy_error


def main():
    failures = []
    if not install_wheel():
        failures.append("Wheel not installed")
    if not update_service():
        failures.append("Service not upgraded")
    if not stop_service():
        failures.append("Couldn't stop service")
    if not start_service():
        failures.append("Couldn't start service")
    s_drive = get_s_drive()
    if s_drive:
        if not create_directories(s_drive):
            failures.append("Couldn't create edixml directory")
        if not install_binaries(s_drive):
            failures.append("Couldn't install binaries")
    stop_service()
    start_service()

    if len(failures) > 0:
        win32api.MessageBox(
            0,
            '\n'.join(failures),
            "Update errors encountered"
        )
        return 1
    else:
        return 0


if __name__ == "__main__":
    logger.info("Executing %s", " ".join(sys.argv))
    sys.exit(main())

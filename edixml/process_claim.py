#!/usr/bin/env python3
# vim:fileencoding=utf-8

import argparse
import sys

import yaml

from edixml import edi_requests
from edixml.exceptions.base import InvalidYamlFileError, \
        UnknownMessageTypeError


def create_parser():
    parser = argparse.ArgumentParser(
        description="Send and receive claims from the EDI")
    parser.add_argument('input', type=argparse.FileType('r'),
                        help="Location of input yaml file")
    parser.add_argument('-p', '--print',
                        action='store_true',
                        dest='print_to_screen',
                        help="Print resulting xml to screen")

    return parser


def validate_data(data):
    required_keys = ['MessageType']
    for key in required_keys:
        if key not in data.keys():
            raise InvalidYamlFileError("YAML file is missing '{}'".format(key))

    allowed_message_types = ['Dental Claim']
    message_type = data.get('MessageType', None)
    if message_type is not None:
        if message_type not in allowed_message_types:
            raise UnknownMessageTypeError(
                ("Message type '{}' is not supported\n" +
                 "Must be '{}'.").format(
                    message_type, ','.join(allowed_message_types)))
    else:
        raise UnknownMessageTypeError(
            ("Message type not provided." +
             "Must be '{}'.").format(','.join(allowed_message_types)))

    return True


def get_data_from_yaml_file(filehandle):
    try:
        data = yaml.load(filehandle)
    except yaml.YAMLError as e:
        raise InvalidYamlFileError("Yaml error detected")

    return data


def create_edi_claim(data):
    """Create the correct type of EDI claim from data."""
    return edi_requests.create_claim(data)


def main():
    """Main entry point for processing claims

    """
    parser = create_parser()
    args = parser.parse_args(sys.argv[1:])
    data = get_data_from_yaml_file(args.input)
    claim = create_edi_claim(data)

    if args.print_to_screen:
        print(claim.to_xml())


if __name__ == '__main__':
    sys.exit(main())

#!/usr/bin/env python3
# vim:fileencoding=utf-8

from edixml.models.base import MessageElement


class PostCodeType(MessageElement):
    def __init__(self, text=None):
        self.text_coercers = [
            (self.coerce_to_upper, )
        ]
        self.text_validators = [
            (self.validate_simple_patterns_alt,
                [['^[A-Z]{1,2}[0-9R][0-9A-Z]? [0-9][A-Z]{2}$']])
        ]
        self.text = text


class PostCode(PostCodeType):
    pass


class TelephoneNumberType(MessageElement):
    def __init__(self, text=None):
        self.text_validators = [
            (self.validate_length, 1, 20),
        ]
        self.text = text


class TelephoneNo(TelephoneNumberType):
    class_var_name = 'telephone_number'


class AddressLineType(MessageElement):
    def __init__(self, text=None):
        self.text_validators = [
            (self.validate_length, 1, 35),
        ]
        self.text = text


class AddressLine(AddressLineType):
    class_var_name = 'address_lines'


class AddressLines(MessageElement):
    required_children = ['address_lines']
    child_classes = [AddressLine]

    def __init__(self, address_lines=None):
        if address_lines is None:
            self.address_lines = []
        else:
            self.address_lines = address_lines

    def add_address_line(self, address_line):
        self.address_lines.append(address_line)


class Address(MessageElement):
    """Definition of a typical address."""
    required_children = ['address_lines', 'post_code']
    child_classes = [AddressLines, PostCode, TelephoneNo]

    def __init__(self, address_lines=None, post_code=None,
                 telephone_number=None):
        self.address_lines = address_lines
        self.post_code = post_code
        self.telephone_number = telephone_number

    def bootstrap_from_yaml_specialist(self, payload):
        address_line_key = 'AddressLine'
        if address_line_key in payload:
            self.address_lines = AddressLines(
                [
                    AddressLine(text) for text in
                    payload.get(address_line_key).split('\n')
                    if text
                ]
            )
            payload.pop(address_line_key)
        return self.bootstrap_from_yaml_generic(payload)


class RepresentativeAddress(Address):
    pass

#!/usr/bin/env python3
# vim:fileencoding=utf-8


from edixml.models.base import MessageElement, MessageEnvelope, XmlBoolean
from edixml.models.organisation import (
    RequestingOrganisationDetails,
)
from edixml.models.patient import PatientDetailsDentalStructure
from edixml.models.person import HealthcareProfessionalPractitionerStructure
from edixml.models.transaction_header import TransactionHeader

SCHEMA_VERSION = "1.0"


class RequestingPractitionerDetails(
        HealthcareProfessionalPractitionerStructure):
    pass


class DentalReferenceType(MessageElement):
    def __init__(self, text=None):
        self.text_validators = [
            (self.validate_length, 10),
        ]
        self.text = text


class PatientDetailsRequestReference(DentalReferenceType):
    pass


class InitialSearchReference(DentalReferenceType):
    pass


class SecondarySearch(XmlBoolean):
    pass


class DentalPatientDetailsRequest(MessageElement):
    required_children = [
        'requesting_organisation_details',
        'requesting_practitioner_details',
        'patient_details_request_reference',
        'patient_details'
    ]
    child_classes = [
        RequestingOrganisationDetails,
        RequestingPractitionerDetails,
        PatientDetailsRequestReference,
        SecondarySearch,
        InitialSearchReference,
        PatientDetailsDentalStructure,
    ]

    def __init__(self, requesting_organisation_details=None,
                 requesting_practitioner_details=None,
                 patient_details_request_reference=None,
                 secondary_search=None,
                 initial_search_reference=None,
                 patient_details=None):
        self.requesting_organisation_details = requesting_organisation_details
        self.requesting_practitioner_details = requesting_practitioner_details
        self.patient_details_request_reference = \
            patient_details_request_reference
        self.secondary_search = secondary_search
        self.initial_search_reference = initial_search_reference
        self.patient_details = patient_details
        self.schema_version = SCHEMA_VERSION
        self.signature = True

    @property
    def required_attributes(self):
        return {'SchemaVersion': self.schema_version}


class DentalPatientDetailsAppBodies(MessageElement):
    node_name = 'AppBodies'
    required_children = ['dental_patient_details_request']
    child_classes = [DentalPatientDetailsRequest]

    def __init__(self, dental_patient_details_request=None):
        self.dental_patient_details_request = dental_patient_details_request


class DentalPatientDetailsRequestEnv(MessageEnvelope):
    child_classes = [TransactionHeader, DentalPatientDetailsAppBodies]
    nsmap = {
        None: "http://www.eps.nds.scot.nhs.uk",
        'xsd': "http://www.w3.org/2001/XMLSchema",
        'ds': "http://www.w3.org/2000/09/xmldsig#"
    }

    def __init__(self, transaction_header=None,
                 dental_patient_details_app_bodies=None,
                 schema_version=None):
        self.schema_version = schema_version
        self.transaction_header = transaction_header
        self.dental_patient_details_app_bodies = (
            dental_patient_details_app_bodies
        )
        if self.schema_version is None:
            self.schema_version = SCHEMA_VERSION

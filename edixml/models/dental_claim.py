#!/usr/bin/env python3
# vim:fileencoding=utf-8

import itertools
import logging


from edixml.exceptions.base import (
    XmlValidationError, CsvValidationError, InvalidYamlFileError
)
from edixml.models.address import (
    RepresentativeAddress,
)
from edixml.models.base import MessageElement, XmlBoolean, DateType
from edixml.models.organisation import (
    OrganisationAlternativeStructure,
)
from edixml.models.patient import (
    PatientDetails,
)
from edixml.models.person import (
    BenefitRecipientName,
    PractitionerDetails,
    PractitionerPinNumber,
    RepresentativeName,
)
try:
    itertools.zip_longest
except AttributeError:
    # Python 2
    itertools.zip_longest = itertools.izip_longest

logger = logging.getLogger(__name__)
SCHEMA_VERSION = "1.0"


class DentalClaimRequestEnv(MessageElement):
    """Message envelope for a GP17 General Dental Treatment claim
    request message.

    """
    nsmap = {
        None: 'http://www.eps.nds.scot.nhs.uk',
        'ds': 'http://www.w3.org/2000/09/xmldsig#',
        'xsi': 'http://www.w3.org/2001/XMLSchema-instance'
    }

    def __init__(self, transaction_header=None,
                 app_bodies=None, schema_version=None):
        self.schema_version = schema_version
        self.transaction_header = transaction_header
        self.app_bodies = app_bodies
        self.validators = [self.schema_version_is_valid]
        if self.schema_version is None:
            self.schema_version = SCHEMA_VERSION

    @property
    def required_attributes(self):
        return {'SchemaVersion': self.schema_version}

    @property
    def children(self):
        return [self.transaction_header, self.app_bodies]

    @property
    def schema_version(self):
        return self._schema_version

    @schema_version.setter
    def schema_version(self, schema_version):
        valid = self.validate_schema_version(schema_version)
        if not valid:
            raise XmlValidationError(
                value=schema_version, target='schema_version')
        else:
            self._schema_version = schema_version


class DentalClaimResponse(MessageElement):
    """The message structure for the Dental Claim Response message."""
    required_children = [
        'claimed_form_details',
        'claiming_organisation_details',
        'claiming_practitioner_details',
        'claim_reference_details',
        'claim_response_details',
    ]
    # Child classes:
    # - ClaimReferenceResponse!! Not ClaimReferenceDetails

    def __init__(self, schema_version=None, claimed_form_details=None,
                 claiming_organisation_details=None,
                 claiming_practitioner_details=None,
                 claim_reference_details=None,
                 claim_response_details=None):
        self.schema_version = schema_version
        self.claimed_form_details = claimed_form_details
        self.claiming_organisation_details = claiming_organisation_details
        self.claiming_practitioner_details = claiming_practitioner_details
        self.claim_reference_details = claim_reference_details
        self.claim_response_details = claim_response_details

        self.validators = []

        if self.schema_version is None:
            self.schema_version = SCHEMA_VERSION

    @property
    def required_attributes(self):
        return {'SchemaVersion': self.schema_version}

    @property
    def children(self):
        return [
            self.claimed_form_details,
            self.claiming_organisation_details,
            self.claiming_practitioner_details,
            self.claim_reference_details,
            self.claim_response_details
        ]

    def as_fm_suitable_list(self):
        # PracticeClaimReference, ClaimValidationFlag, ErrorCode,
        # ErrorDescription, ErrorAdditionalInfo
        try:
            practice_claim_reference = (
                self.claim_reference_details.practice_claim_reference.text)
        except AttributeError:
            raise CsvValidationError("Reference required for CSV")
        try:
            claim_validation_flag = (
                self.claim_response_details.claim_validation_flag.text)
        except AttributeError:
            raise CsvValidationError("Validation Flag required for CSV")

        data = [practice_claim_reference, claim_validation_flag]
        if not claim_validation_flag:
            try:
                errors = self.claim_response_details.errors
            except AttributeError:
                raise CsvValidationError("Errors required for CSV")
            try:
                error_code = (
                    errors.error_code.text)
                data.append(error_code)
            except AttributeError:
                raise CsvValidationError("Error code required for CSV")
            try:
                error_description = (
                    errors.error_description.text)
                data.append(error_description)
            except AttributeError:
                raise CsvValidationError("Error code required for CSV")
            try:
                error_additional_info = (
                    errors.error_additional_info.text)
                data.append(error_additional_info)
            except AttributeError:
                pass

        return data


class DentalClaimAppBodies(MessageElement):
    node_name = 'AppBodies'
    required_children = [
        'dental_claim_response'
    ]
    child_classes = [
        DentalClaimResponse,
    ]

    def __init__(self, dental_claim_response=None):
        self.dental_claim_response = dental_claim_response


class ClaimFormType(MessageElement):
    def __init__(self, text=None):
        self.text_validators = [
            (self.validate_value_in_list, ['GP17-1', 'GP17-O']),
        ]
        self.text = text


class FormType(ClaimFormType):
    pass


class VersionType(MessageElement):
    def __init__(self, text=None):
        self.text_coercers = [
            (self.coerce_to_string,)
        ]
        self.text_validators = [
            (self.validate_length, 1, 10),
        ]
        self.text = text


class FormVersion(VersionType):
    yaml_name = 'VersionType'


class ClaimFormSourceType(MessageElement):
    def __init__(self, text=None):
        self.text_validators = [
            (self.validate_value_in_list, ['PMS', 'WEB']),
        ]
        self.text = text


class Source(ClaimFormSourceType):
    yaml_name = 'ClaimFormSourceType'


class FormStructure(MessageElement):
    """Complex type to hold common claim/form details."""
    required_children = ['form_type', 'form_version', 'source']
    child_classes = [FormType, FormVersion, Source]

    def __init__(self, form_type=None, form_version=None, source=None):
        self.form_type = form_type
        self.form_version = form_version
        self.source = source


class FormDetails(FormStructure):
    pass


class ClaimedFormDetails(FormStructure):
    pass


class ClaimingOrganisationDetails(OrganisationAlternativeStructure):
    pass


class PractitionerOrganisationDetails(OrganisationAlternativeStructure):
    def bootstrap_from_yaml_specialist(self, payload):
        prac_org_keys = [
            'PracticeAddressLine',
            'PracticePostCode',
        ]
        for key in prac_org_keys:
            updated_key = key.replace('Practice', '')
            # Remove the old keys if they are there
            try:
                payload.pop(updated_key)
            except KeyError:
                pass
            if key in payload:
                data = payload.pop(key)
                payload[updated_key] = data
        tree = self.bootstrap_from_yaml_generic(payload)
        return tree


class DateAuthorised(DateType):
    """Date of claim"""
    pass


class PmsVersion(MessageElement):
    """For PMS submitted claims, set to the accredited PMS identifier as
    issued by Practitioner Services. For Web Form claims, set to the
    eDental Web Form system version

    """
    def __init__(self, text=None):
        self.text_validators = [
            (self.validate_length, 1, 50),
        ]
        self.text = text
        self.node_name = 'PMSVersion'


class PracticeClaimReferenceType(MessageElement):
    def __init__(self, text=None):
        self.text_validators = [
            (self.validate_length, 6)
        ]
        self.text = text


class PracticeClaimReference(PracticeClaimReferenceType):
    """Use this value as a primary key for FM."""
    pass


class SubmissionCountType(MessageElement):
    def __init__(self, text=None):
        self.text_coercers = [
            (self.coerce_to_integer,)
        ]
        self.text_validators = [
            (self.validate_integer_in_range, 0, 9),
        ]
        self.text = text


class SubmissionClaimCount(SubmissionCountType):
    pass


class ClaimReferenceDetails(MessageElement):
    required_children = [
        'practice_claim_reference',
        'submission_claim_count',
        'practitioner_pin_number',
        'date_authorised',
        'pms_version'
    ]
    child_classes = [
        PracticeClaimReference,
        SubmissionClaimCount,
        PractitionerPinNumber,
        DateAuthorised,
        PmsVersion
    ]

    def __init__(self, practice_claim_reference=None,
                 submission_claim_count=None, practitioner_pin_number=None,
                 date_authorised=None, pms_version=None):
        self.practice_claim_reference = practice_claim_reference
        self.submission_claim_count = submission_claim_count
        self.practitioner_pin_number = practitioner_pin_number
        self.date_authorised = date_authorised
        self.pms_version = pms_version


class DateOfAcceptance(DateType):
    pass


class ClaimReferenceResponse(MessageElement):
    required_children = [
        'practice_claim_reference',
        'submission_claim_count',
        'date_of_acceptance',
    ]
    child_classes = [
        PracticeClaimReference, SubmissionClaimCount, DateOfAcceptance
    ]

    def __init__(self, practice_claim_reference=None,
                 submission_claim_count=None, date_of_acceptance=None):
        self.practice_claim_reference = practice_claim_reference
        self.submission_claim_count = submission_claim_count
        self.date_of_acceptance = date_of_acceptance
        self.node_name = 'ClaimReferenceDetails'


class ClaimValidationFlag(XmlBoolean):
    pass


class NotesType(MessageElement):
    def __init__(self, text=None):
        self.text_validators = [
            (self.validate_length, 1, 1999)
        ]
        self.text = text


class ErrorDescription(NotesType):
    pass


class ErrorCodeType(MessageElement):
    def __init__(self, text=None):
        self.text_validators = [
            (self.validate_length, 7),
        ]
        self.text = text


class ErrorCode(ErrorCodeType):
    pass


class ErrorAdditionalInfo(NotesType):
    pass


class Errors(MessageElement):
    required_children = ['error_code', 'error_description']
    child_classes = [
        ErrorCode, ErrorDescription, ErrorAdditionalInfo
    ]

    def __init__(self, error_code=None, error_description=None,
                 error_additional_info=None):
        self.error_code = error_code
        self.error_description = error_description
        self.error_additional_info = error_additional_info


class ClaimResponseDetails(MessageElement):
    required_children = ['claim_validation_flag']
    child_classes = [
        ClaimValidationFlag, Errors
    ]

    def __init__(self, claim_validation_flag=None, errors=None):
        self.claim_validation_flag = claim_validation_flag
        self.errors = errors


class DateOfCompletion(DateType):
    pass


class TreatmentDateDetails(MessageElement):
    required_children = ['date_of_acceptance']
    child_classes = [DateOfAcceptance, DateOfCompletion]

    def __init__(self, date_of_acceptance=None, date_of_completion=None):
        self.date_of_acceptance = date_of_acceptance
        self.date_of_completion = date_of_completion


class ContinuationPartNumber(MessageElement):
    yaml_name = 'Continuationcasepartnumber'

    def __init__(self, text=None):
        self.text_coercers = [
            (self.coerce_to_integer,)
        ]
        self.text_validators = [
            (self.validate_integer_in_range, 1, 99),
        ]
        self.text = text


class PreviousCaseId(MessageElement):
    def __init__(self, text=None):
        self.node_name = 'PreviousCaseID'
        self.text_validators = [
            (self.validate_length, 12),
        ]
        self.text = text


class ContinuationCaseDetails(MessageElement):
    required_children = ['continuation_part_number']
    child_classes = [ContinuationPartNumber, PreviousCaseId]

    def __init__(self, continuation_part_number=None, previous_case_id=None):
        self.continuation_part_number = continuation_part_number
        self.previous_case_id = previous_case_id

        self.validators = [
            self.continuation_part_number_is_valid,
        ]

    def continuation_part_number_is_valid(self):
        if self.continuation_part_number.text > 1:
            if self.previous_case_id is None:
                raise XmlValidationError(
                    "Previous Case ID required for a follow-up part number")
        return True


class TypeOfClaimCode(MessageElement):
    def __init__(self, text=None):
        self.text_coercers = [
            (self.coerce_to_integer,)
        ]
        self.text_validators = [
            (self.validate_integer_in_range, 1, 5),
        ]
        self.text = text


class GdsClaim(XmlBoolean):
    node_name = 'GDSClaim'


class NonGdsClaim(XmlBoolean):
    node_name = 'NonGDSClaim'


class TextualDescriptionType(MessageElement):
    def __init__(self, text=None):
        self.text_validators = [
            (self.validate_length, 1, 396),
        ]
        self.text = text


class TypeOfClaimDescription(TextualDescriptionType):
    pass


class TypeOfClaimDetails(MessageElement):
    required_children = ['type_of_claim_code']
    child_classes = [TypeOfClaimCode, TypeOfClaimDescription]

    def __init__(
            self, type_of_claim_code=None, type_of_claim_description=None):
        self.type_of_claim_description = type_of_claim_description
        self.type_of_claim_code = type_of_claim_code

    @property
    def type_of_claim_code(self):
        return self._type_of_claim_code

    @type_of_claim_code.setter
    def type_of_claim_code(self, value):
        self._type_of_claim_code = value
        try:
            claim_descriptions = {
                1: "Initial registration/continued registration with dentist",
                2: "Registered with another dentist at this practice",
                3: "Registered with another dentist at another practice",
                4: "Not registered with any dentist",
                5: "Referred patient",
            }
            if self.type_of_claim_description is None:
                self.type_of_claim_description = TypeOfClaimDescription(
                    claim_descriptions[value.text])
        except AttributeError:
            pass


class DateOfApproval(DateType):
    pass


class PriorApprovalReferenceType(MessageElement):
    def __init__(self, text=None):
        self.text_validators = [
            (self.validate_length, 1, 30),
        ]
        self.text = text


class PriorApprovalReference(PriorApprovalReferenceType):
    pass


class PriorApprovalDetails(MessageElement):
    required_children = ['date_of_approval']
    child_classes = [DateOfApproval, PriorApprovalReference]

    def __init__(self, date_of_approval=None, prior_approval_reference=None):
        self.date_of_approval = date_of_approval
        self.prior_approval_reference = prior_approval_reference


class TreatmentTypeStructure(MessageElement):
    required_children = [
        'type_of_claim_details',
        'gds_claim',
        'non_gds_claim'
    ]
    child_classes = [
        TypeOfClaimDetails, GdsClaim, NonGdsClaim,
    ]

    def __init__(self, type_of_claim_details=None, gds_claim=None,
                 non_gds_claim=None):
        self.type_of_claim_details = type_of_claim_details
        self.gds_claim = gds_claim
        self.non_gds_claim = non_gds_claim
        self.validators = [
            self.gds_claim_is_valid
        ]

    def gds_claim_is_valid(self):
        """GDS Claim and Non GDS claim must be boolean opposites of one
        another.

        """
        try:
            gds_claim = self.gds_claim.text
        except AttributeError:
            raise XmlValidationError("gds claim cannot be empty")
        try:
            non_gds_claim = self.non_gds_claim.text
        except AttributeError:
            raise XmlValidationError("non gds claim cannot be empty")

        if gds_claim != non_gds_claim:
            return True
        else:
            raise XmlValidationError("GDS and NonGDS flags must be different")


class TreatmentTypeDetails(TreatmentTypeStructure):
    pass


class ExternalMouthTrauma(XmlBoolean):
    """Whether treatment was necessitated by external trauma of the
    mouth.

    """
    pass


class SpecialNeeds(XmlBoolean):
    """Whether patient has special needs."""
    pass


class ReferralReasonType(MessageElement):
    def __init__(self, text=None):
        self.text_coercers = [
            (self.coerce_to_integer,)
        ]
        self.text_validators = [
            (self.validate_integer_in_range, 1, 3),
        ]
        self.text = text


class ReferralReasonCode(ReferralReasonType):
    # TODO - must be completed if Referrals Claimed is true
    pass


class ReferralReasonDescription(TextualDescriptionType):
    pass


class ReferralReasonDetails(MessageElement):
    required_children = ['referral_reason_code']
    child_classes = [
        ReferralReasonCode, ReferralReasonDescription
    ]

    def __init__(self, referral_reason_code=None,
                 referral_reason_description=None):
        self.referral_reason_description = referral_reason_description
        self.referral_reason_code = referral_reason_code

    @property
    def referral_reason_code(self):
        return self._referral_reason_code

    @referral_reason_code.setter
    def referral_reason_code(self, value):
        self._referral_reason_code = value
        try:
            descriptions = {
                1: "Facilities",
                2: "Experience",
                3: "Expertise",
            }
            if self.referral_reason_description is None:
                self.referral_reason_description = ReferralReasonDescription(
                    descriptions[value.text])
        except AttributeError:
            pass


class ModelsAvailable(XmlBoolean):
    pass


class RadiographsAvailable(XmlBoolean):
    pass


class FreeTextDescriptionType(MessageElement):
    def __init__(self, text=None):
        self.text_validators = [
            (self.validate_length, 1, 400),
        ]
        self.text = text


class NoRadiographsRemarks(FreeTextDescriptionType):
    pass


class Orthodontic(XmlBoolean):
    pass


class TreatmentArrangementsDetails(MessageElement):
    child_classes = [
        ExternalMouthTrauma, SpecialNeeds, ReferralReasonDetails,
        ModelsAvailable, RadiographsAvailable, NoRadiographsRemarks,
        Orthodontic
    ]

    def __init__(self, external_mouth_trauma=None, special_needs=None,
                 referral_reason_details=None, models_available=None,
                 radiographs_available=None,
                 no_radiographs_remarks=None, orthodontic=None):
        self.external_mouth_trauma = external_mouth_trauma
        self.special_needs = special_needs
        self.referral_reason_details = referral_reason_details
        self.models_available = models_available
        self.radiographs_available = radiographs_available
        self.no_radiographs_remarks = no_radiographs_remarks
        self.orthodontic = orthodontic


class TreatmentCode(MessageElement):
    def __init__(self, text=None):
        # TODO Should we check the code is correct?
        # See issue #56
        self.text_validators = [
            (self.validate_length, 4),
            (self.validate_simple_patterns_alt, [['^[0-9][0-9][0-9][0-9]$']]),
        ]
        self.text = text


class TreatmentQualifier(MessageElement):
    def __init__(self, text=None):
        self.text_validators = [
            (self.validate_length, 2),
            (self.validate_simple_patterns_alt, [['^[0-9][0-9]$']]),
        ]
        self.text = text


class MonetaryType(MessageElement):
    def __init__(self, text=None):
        self.text_coercers = [
            (self.coerce_to_float,)
        ]
        self.text_validators = [
            (self.validate_positive_number_or_zero,),
            (self.validate_max_two_decimal_places,),
        ]
        self.text = text

    @property
    def text(self):
        if self._text is None:
            return self._text
        else:
            return "{:.2f}".format(self._text)

    @text.setter
    def text(self, text):
        self._text = text


class TreatmentValue(MonetaryType):
    pass


class ToothNotationType(MessageElement):
    def __init__(self, text=None):
        # TODO - check issue #57
        self.text_coercers = [
            (self.coerce_to_integer,)
        ]
        self.text_validators = [
            (self.validate_integer_in_range, 10, 99),
        ]
        self.text = text


class ToothNotation(ToothNotationType):
    pass


class SupernumeraryFlag(XmlBoolean):
    pass


class CurFlag(XmlBoolean):
    node_name = 'CURFlag'


class CurObservations(NotesType):
    node_name = 'CURObservations'


class Teeth(MessageElement):
    required_children = ['tooth_notation']
    child_classes = [
        ToothNotation, SupernumeraryFlag
    ]

    def __init__(self, tooth_notation=None, supernumerary_flag=None):
        self.tooth_notation = tooth_notation
        self.supernumerary_flag = supernumerary_flag


class Treatment(MessageElement):
    required_children = [
        'treatment_code',
        'treatment_qualifier',
        'treatment_value'
    ]
    child_classes = [
        TreatmentCode, TreatmentQualifier, TreatmentValue, Teeth, CurFlag
    ]

    def __init__(self, treatment_code=None, treatment_qualifier=None,
                 treatment_value=None, teeth=None, cur_flag=None):
        self.treatment_code = treatment_code
        self.treatment_qualifier = treatment_qualifier
        self.treatment_value = treatment_value
        if teeth is None:
            self.teeth = []
        else:
            self.teeth = teeth
        self.cur_flag = cur_flag


class TreatmentDetailsStructure(MessageElement):
    required_children = ['treatment']
    child_classes = [Treatment, CurObservations]

    def __init__(self, treatment=None, cur_observations=None):
        if treatment is None:
            self.treatment = []
        else:
            self.treatment = treatment
        self.cur_observations = cur_observations

    def bootstrap_from_yaml_specialist(self, payload):
        treatments = []
        treatment_key = 'Treatment'
        if treatment_key in payload:
            treatment_items = {}
            for item in payload.get(treatment_key):
                for code, data in item.items():
                    atoms = data.split(':')
                    if len(atoms) < 2:
                        raise InvalidYamlFileError(
                            "Could only find one element in treatment")
                    else:
                        qualifier = atoms[0]
                        value = atoms[1]
                    if len(atoms) > 2:
                        teeth_tokens = atoms[2:]
                        if teeth_tokens[-1].lower() == 'true':
                            # Free replacement
                            code = code + 'CUR'
                            teeth_tokens = teeth_tokens[:-1]
                    else:
                        teeth_tokens = []
                    if code in treatment_items:
                        (old_qual, old_val, old_teeth) = treatment_items[code]
                        qualifier = "{:02}".format(
                            int(qualifier) + int(old_qual)
                        )
                        value = "{:.2f}".format(float(value) + float(old_val))
                        old_teeth.extend(teeth_tokens)
                        teeth_tokens = old_teeth
                    treatment_items[code] = (qualifier, value, teeth_tokens)
            for code, data in treatment_items.items():
                trt = Treatment()
                (qualifier, value, teeth_tokens) = data
                if code.endswith('CUR'):
                    code = code.replace('CUR', '')
                    trt.cur_flag = CurFlag(True)
                trt.treatment_code = TreatmentCode(code)
                trt.treatment_qualifier = TreatmentQualifier(qualifier)
                trt.treatment_value = TreatmentValue(value)
                if teeth_tokens:
                    teeth_list = []
                    for tooth in teeth_tokens:
                        teeth = Teeth()
                        teeth.tooth_notation = ToothNotation(
                            tooth.replace('S', '')
                        )
                        if 'S' in tooth:
                            teeth.supernumerary_flag = SupernumeraryFlag(True)
                        teeth_list.append(teeth)
                    trt.teeth = teeth_list
                treatments.append(trt)
        self.treatment = treatments
        cur_observations_key = 'CURObservations'
        if cur_observations_key in payload:
            obs = payload[cur_observations_key]
            self.cur_observations = CurObservations(obs)
        if self.treatment or self.cur_observations:
            return self
        else:
            return None


class CompletedTreatmentDetails(TreatmentDetailsStructure):
    pass


class AnnotationCodeType(MessageElement):
    def __init__(self, text=None):
        self.text_validators = [
            (self.validate_value_in_list, [
                'M', 'Z', 'R', 'E', 'A', 'D', 'C', 'BR', 'BP', 'F',
                'IN', 'U', 'IM', 'V', 'O',
                ]),
        ]
        self.text = text

    @property
    def text(self):
        if self._text == 'A':
            return 'DT'
        else:
            return self._text

    @text.setter
    def text(self, value):
        self._text = value


class AnnotationCode(AnnotationCodeType):
    pass


class AnnotationDescription(TextualDescriptionType):
    pass


class AnnotationCodeDetails(MessageElement):
    required_children = ['annotation_code']
    child_classes = [AnnotationCode, AnnotationDescription]

    def __init__(self, annotation_code=None, annotation_description=None):
        self.annotation_description = annotation_description
        self.annotation_code = annotation_code

    @property
    def annotation_code(self):
        return self._annotation_code

    @annotation_code.setter
    def annotation_code(self, value):
        self._annotation_code = value
        descriptions = {
            'M': 'Missing tooth',
            'Z': 'Tooth missing and space closed',
            'R': 'Root present',
            'E': 'Tooth to be extracted',
            'D': 'Denture tooth present',
            'C': 'Crown present',
            'BR': 'Bridge retainer present',
            'BP': 'Bridge pontic present',
            'F': 'Filling',
            'IN': 'Gold inlay',
            'U': 'Unerupted tooth',
            'IM': 'Implant',
            'V': 'Porcelain veneer',
            'O': 'Tooth present',
        }
        try:
            if self.annotation_description is None:
                try:
                    self.annotation_description = AnnotationDescription(
                        descriptions[value.text])
                except KeyError:
                    pass
        except AttributeError:
            pass


class ToothSurfaceType(MessageElement):
    def __init__(self, text=None):
        allowed_values = ['M', 'O', 'D', 'B', 'P', 'L', 'I']
        self.text_validators = [
            (self.validate_characters_in_list_no_repeats, allowed_values),
        ]
        self.text = text


class ToothSurfaceCode(ToothSurfaceType):
    pass


class ToothSurfaceDescription(TextualDescriptionType):
    pass


class ToothSurfaceDetails(MessageElement):
    required_children = ['tooth_surface_code']
    child_classes = [ToothSurfaceCode, ToothSurfaceDescription]

    def __init__(self, tooth_surface_code=None,
                 tooth_surface_description=None):
        self.tooth_surface_description = tooth_surface_description
        self.tooth_surface_code = tooth_surface_code

    @property
    def tooth_surface_code(self):
        return self._tooth_surface_code

    @tooth_surface_code.setter
    def tooth_surface_code(self, value):
        self._tooth_surface_code = value
        descriptions = {
            'M': 'Mesial',
            'O': 'Occlusal',
            'D': 'Distal',
            'B': 'Buccal or Labial',
            'P': 'Palatal',
            'L': 'Lingual',
            'I': 'Incisal',
        }
        try:
            if self.tooth_surface_description is None:
                try:
                    self.tooth_surface_description = ToothSurfaceDescription(
                        descriptions[value.text])
                except KeyError:
                    pass
        except AttributeError:
            pass


class MaterialType(MessageElement):
    def __init__(self, text=None):
        self.text_validators = [
            (self.validate_value_in_list, ['A', 'R', 'G']),
        ]
        self.text = text


class MaterialCode(MaterialType):
    pass


class MaterialDescription(TextualDescriptionType):
    pass


class MaterialDetails(MessageElement):
    required_children = ['material_code']
    child_classes = [MaterialCode, MaterialDescription]

    def __init__(self, material_code=None, material_description=None):
        self.material_description = material_description
        self.material_code = material_code

    @property
    def material_code(self):
        return self._material_code

    @material_code.setter
    def material_code(self, value):
        self._material_code = value
        descriptions = {
            'A': 'Amalgam',
            'R': 'Resin',
            'G': 'Glass',
        }
        try:
            if self.material_description is None:
                self.material_description = MaterialDescription(
                    descriptions.get(value.text, None))
        except AttributeError:
            pass


class DentalChartStructure(MessageElement):
    required_children = ['tooth_notation']
    child_classes = [
        ToothNotation, SupernumeraryFlag, AnnotationCodeDetails,
        ToothSurfaceDetails, MaterialDetails
    ]

    def __init__(self, tooth_notation=None, supernumerary_flag=None,
                 annotation_code_details=None,
                 tooth_surface_details=None, material_details=None):
        self.tooth_notation = tooth_notation
        self.supernumerary_flag = supernumerary_flag
        self.annotation_code_details = annotation_code_details
        self.tooth_surface_details = tooth_surface_details
        self.material_details = material_details


class DentalChart(DentalChartStructure):
    pass


class DentalChartDetails(MessageElement):
    required_children = ['dental_chart']
    child_classes = [DentalChart]
    yaml_name = 'Charting'

    def __init__(self, dental_chart=None):
        if dental_chart is None:
            self.dental_chart = []
        else:
            self.dental_chart = dental_chart

    def bootstrap_from_yaml_specialist(self, payload):
        chartings = []
        if self.yaml_name in payload:
            for item in payload.get(self.yaml_name):
                # TODO go from here
                for tooth, data in item.items():
                    atoms = data.split(':')
                    # Pad out atoms to correct length
                    tokens = [
                        'annotation_code', 'surface', 'material', 'superflag',
                    ]
                    atoms = [
                        x for x, y in itertools.zip_longest(
                            atoms, tokens, fillvalue='')
                    ]
                    (annotation, surface, material, superflag) = atoms
                chart = DentalChart(tooth_notation=ToothNotation(tooth))
                if annotation:
                    chart.annotation_code_details = AnnotationCodeDetails(
                        AnnotationCode(annotation)
                    )
                if surface:
                    chart.tooth_surface_details = ToothSurfaceDetails(
                        ToothSurfaceCode(surface)
                    )
                if material:
                    chart.material_details = MaterialDetails(
                        MaterialCode(material)
                    )
                if superflag == 'S':
                    chart.supernumerary_flag = SupernumeraryFlag(True)
                chartings.append(chart)

        self.dental_chart = chartings
        if self.dental_chart:
            return self
        else:
            return None


class BpeScoreType(MessageElement):
    def __init__(self, text=None):
        self.text_coercers = [
            (self.coerce_to_string, )
        ]
        allowed_values = ['0', '1', '2', '3', '4', '-', '*', 'X', 'x']
        self.text_validators = [
            (self.validate_length, 1, 2),
            (self.validate_characters_in_list_no_repeats, allowed_values),
        ]
        self.text = text


class UpperLeft(BpeScoreType):
    pass


class UpperAnterior(BpeScoreType):
    pass


class UpperRight(BpeScoreType):
    pass


class LowerLeft(BpeScoreType):
    pass


class LowerAnterior(BpeScoreType):
    pass


class LowerRight(BpeScoreType):
    pass


class BpeStructure(MessageElement):
    required_children = [
        'upper_left',
        'upper_anterior',
        'upper_right',
        'lower_left',
        'lower_anterior',
        'lower_right',
    ]
    child_classes = [
        UpperLeft, UpperAnterior, UpperRight,
        LowerLeft, LowerAnterior, LowerRight,
    ]

    def __init__(self, upper_left=None, upper_anterior=None,
                 upper_right=None, lower_left=None, lower_anterior=None,
                 lower_right=None):
        self.upper_left = upper_left
        self.upper_anterior = upper_anterior
        self.upper_right = upper_right
        self.lower_left = lower_left
        self.lower_anterior = lower_anterior
        self.lower_right = lower_right


class BpeDetails(BpeStructure):
    node_name = 'BPEDetails'


class TreatmentDetails(MessageElement):
    required_children = [
        'treatment_date_details',
        'treatment_type_details',
    ]
    child_classes = [
        TreatmentDateDetails, PriorApprovalDetails, TreatmentTypeDetails,
        TreatmentArrangementsDetails, CompletedTreatmentDetails,
        DentalChartDetails, BpeDetails
    ]

    def __init__(self, treatment_date_details=None,
                 prior_approval_details=None,
                 treatment_type_details=None,
                 treatment_arrangements_details=None,
                 completed_treatment_details=None,
                 dental_chart_details=None, bpe_details=None):
        self.treatment_date_details = treatment_date_details
        self.prior_approval_details = prior_approval_details
        self.treatment_type_details = treatment_type_details
        self.treatment_arrangements_details = treatment_arrangements_details
        self.completed_treatment_details = completed_treatment_details
        self.dental_chart_details = dental_chart_details
        self.bpe_details = bpe_details


class PatientRefusedTreatment(XmlBoolean):
    pass


class PatientRefusedTreatmentObservations(NotesType):
    pass


class Pftr(XmlBoolean):
    node_name = 'PFTR'


class PftrFeeRequired(XmlBoolean):
    node_name = 'PFTRFeeRequired'


class PftrFeeRequiredObservations(NotesType):
    node_name = 'PFTRFeeRequiredObservations'


class Declaration(XmlBoolean):
    pass


class OneTwoType(MessageElement):
    def __init__(self, text=None):
        if text is not None:
            text = str(text)
        allowed_values = ['1', '2']
        self.text_validators = [
            (self.validate_value_in_list, allowed_values),
        ]
        self.text = text


class PatientOrRepresentativeCode(OneTwoType):
    pass


class PatientOrRepresentativeDescription(TextualDescriptionType):
    pass


class PatientOrRepresentativeSignature(XmlBoolean):
    pass


class PatientOrRepresentativeSignatureDate(DateType):
    pass


class PatientOrRepresentativeStructure(MessageElement):
    required_children = ['patient_or_representative_code']
    child_classes = [
        PatientOrRepresentativeCode,
        PatientOrRepresentativeDescription
    ]

    def __init__(self, patient_or_representative_code=None,
                 patient_or_representative_description=None):
        self.patient_or_representative_description = \
                patient_or_representative_description
        self.patient_or_representative_code = patient_or_representative_code

    @property
    def patient_or_representative_code(self):
        return self._patient_or_representative_code

    @patient_or_representative_code.setter
    def patient_or_representative_code(self, value):
        self._patient_or_representative_code = value
        descriptions = {
            '1': 'patient',
            '2': 'representative',
        }
        try:
            if self.patient_or_representative_description is None:
                self.patient_or_representative_description = \
                        PatientOrRepresentativeDescription(
                            descriptions.get(value.text, None))
        except AttributeError:
            pass


class PatientOrRepresentativeDetails(PatientOrRepresentativeStructure):
    pass


class PatientOrRepresentativeDetailsStructure(MessageElement):
    required_children = [
        'patient_or_representative_details',
        'patient_or_representative_signature',
        'patient_or_representative_signature_date',
    ]
    child_classes = [
        PatientOrRepresentativeDetails,
        PatientOrRepresentativeSignature,
        PatientOrRepresentativeSignatureDate,
        RepresentativeName,
        RepresentativeAddress
    ]

    def __init__(self, patient_or_representative_details=None,
                 patient_or_representative_signature=None,
                 patient_or_representative_signature_date=None,
                 representative_name=None,
                 representative_address=None):
        self.patient_or_representative_details = \
            patient_or_representative_details
        self.patient_or_representative_signature = \
            patient_or_representative_signature
        self.patient_or_representative_signature_date = \
            patient_or_representative_signature_date
        self.representative_name = representative_name
        self.representative_address = \
            representative_address
        self.validators = [
            self.representative_name_is_valid
        ]

    def representative_name_is_valid(self):
        if self.patient_or_representative_details is not None:
            pat_details = self.patient_or_representative_details
            if pat_details.patient_or_representative_code.text == '2':
                if self.representative_name is None:
                    return False
        return True

    def bootstrap_from_yaml_specialist(self, payload):
        """Remove the word PatientOrRepresentative from all keys"""
        pat_repr_keys = [
            'PatientOrRepresentativeTitle',
            'PatientOrRepresentativeBirthFamilyName',
            'PatientOrRepresentativeFamilyName',
            'PatientOrRepresentativeGivenName',
            'PatientOrRepresentativeAddressLine',
            'PatientOrRepresentativePostCode',
        ]
        for key in pat_repr_keys:
            updated_key = key.replace('PatientOrRepresentative', '')
            # Remove the old keys if they are there
            try:
                payload.pop(updated_key)
            except KeyError:
                pass
            if key in payload:
                data = payload.pop(key)
                payload[updated_key] = data
        tree = self.bootstrap_from_yaml_generic(payload)
        return tree


class DeclarationOnAcceptance(PatientOrRepresentativeDetailsStructure):
    pass


class DeclarationOnCompletion(PatientOrRepresentativeDetailsStructure):
    def bootstrap_from_yaml_specialist(self, payload):
        """Remove the word "Finished" from all keys."""
        completed_keys = [
            'PatientOrRepresentativeCodeFinish',
            'PatientOrRepresentativeSignatureFinish',
            'PatientOrRepresentativeSignatureDateFinish',
            'PatientOrRepresentativeTitleFinish',
            'PatientOrRepresentativeBirthFamilyNameFinish',
            'PatientOrRepresentativeFamilyNameFinish',
            'PatientOrRepresentativeGivenNameFinish',
            'PatientOrRepresentativeAddressLineFinish',
            'PatientOrRepresentativePostCodeFinish',
        ]
        for key in completed_keys:
            updated_key = key.replace('Finish', '')
            # Remove the old keys if they are there
            try:
                payload.pop(updated_key)
            except KeyError:
                pass
            if key in payload:
                data = payload.pop(key)
                payload[updated_key] = data
        tree = super(
            DeclarationOnCompletion, self
        ).bootstrap_from_yaml_specialist(payload)
        return tree


class PatientCharge(MonetaryType):
    pass


class AmountClaimed(MonetaryType):
    pass


class TreatmentAmountStructure(MessageElement):
    required_children = ['patient_charge', 'amount_claimed']
    child_classes = [
        PatientCharge,
        AmountClaimed
    ]

    def __init__(self, patient_charge=None, amount_claimed=None):
        self.patient_charge = patient_charge
        self.amount_claimed = amount_claimed


class TreatmentAmountDetails(TreatmentAmountStructure):
    pass


class BenefitCategoryCodeType(MessageElement):
    def __init__(self, text=None):
        if text is not None:
            text = text.upper()
        allowed_values = [
            'H3', '18', 'ED', 'H2', 'PR', 'NM', 'IS', 'JS', 'TC', 'PC',
            'ES', 'UC',
        ]
        self.text_validators = [
            (self.validate_value_in_list, allowed_values),
        ]
        self.text = text


class BenefitCategoryCode(BenefitCategoryCodeType):
    pass


class BenefitCategoryDescription(TextualDescriptionType):
    pass


class BenefitCategoryDetails(MessageElement):
    required_children = ['benefit_category_code']
    child_classes = [
        BenefitCategoryCode, BenefitCategoryDescription
    ]

    def __init__(self, benefit_category_code=None,
                 benefit_category_description=None):
        self.benefit_category_description = benefit_category_description
        self.benefit_category_code = benefit_category_code

    @property
    def benefit_category_code(self):
        return self._benefit_category_code

    @benefit_category_code.setter
    def benefit_category_code(self, value):
        self._benefit_category_code = value
        descriptions = {
            'H3': 'Liable to pay partial NHS charge HC3',
            '18': 'Under 18 years old',
            'ED': 'Aged 18 and in full-time education',
            'H2': 'HC2 Exempt',
            'PR': 'Expecting a baby',
            'NM': 'Mother of a baby < 12 months old',
            'IS': 'Income Support',
            'JS': 'Income-based Job Seekers Allowance',
            'TC': 'Tax Credit',
            'PC': 'Pension Credit',
            'ES': 'Employment Support Allowance',
            'UC': 'Universal Credit',
        }
        try:
            if self.benefit_category_description is None:
                self.benefit_category_description = \
                        BenefitCategoryDescription(
                            descriptions.get(value.text, None))
        except AttributeError:
            pass


class CertificateNumberType(MessageElement):
    def __init__(self, text=None):
        self.text_validators = [
            (self.validate_length, 1, 9),
        ]
        self.text = text


class Hc2CertificateNumber(CertificateNumberType):
    node_name = 'HC2CertificateNumber'


class Hc3CertificateNumber(CertificateNumberType):
    node_name = 'HC3CertificateNumber'


class HcCertificateDetails(MessageElement):
    node_name = 'HCCertificateDetails'
    child_classes = [
        Hc2CertificateNumber, Hc3CertificateNumber,
    ]

    def __init__(self, hc2_certificate_number=None,
                 hc3_certificate_number=None):
        self.hc2_certificate_number = hc2_certificate_number
        self.hc3_certificate_number = hc3_certificate_number
        self.validators = [
            self.only_one_certificate_included,
        ]

    def only_one_certificate_included(self):
        if self.hc2_certificate_number is not None:
            if self.hc3_certificate_number is not None:
                return False
            else:
                return True
        if self.hc3_certificate_number is not None:
            if self.hc2_certificate_number is not None:
                return False
            else:
                return True
        # No certificates supplied
        return False


class BenefitRecipientDateOfBirth(DateType):
    pass


class BenefitRecipientNiNumber(CertificateNumberType):
    node_name = 'BenefitRecipientNINumber'


class EvidenceNotProduced(XmlBoolean):
    pass


class ExemptionOrRemissionStatusChanged(XmlBoolean):
    pass


class BenefitRecipientStructure(MessageElement):
    required_children = ['benefit_category_details', 'evidence_not_produced']
    child_classes = [
        BenefitCategoryDetails,
        HcCertificateDetails,
        BenefitRecipientName,
        BenefitRecipientDateOfBirth,
        BenefitRecipientNiNumber,
        EvidenceNotProduced
    ]

    def __init__(self, benefit_category_details=None,
                 hc_certificate_details=None,
                 benefit_recipient_name=None,
                 benefit_recipient_date_of_birth=None,
                 benefit_recipient_ni_number=None,
                 evidence_not_produced=None):
        self.benefit_category_details = benefit_category_details
        self.hc_certificate_details = hc_certificate_details
        self.benefit_recipient_name = benefit_recipient_name
        self.benefit_recipient_date_of_birth = benefit_recipient_date_of_birth
        self.benefit_recipient_ni_number = benefit_recipient_ni_number
        self.evidence_not_produced = evidence_not_produced
        self.validators = [
            self.h2_code_requires_hc2_cert,
            self.h3_code_requires_hc3_cert,
            self.benefit_recipient_name_required_if_code_appropriate,
        ]

    def benefit_recipient_name_required_if_code_appropriate(self):
        benefit_details = self.benefit_category_details
        if benefit_details is None:
            return True

        benefit_code = benefit_details.benefit_category_code
        if benefit_code is None:
            return True

        code = benefit_code.text
        if code is None:
            return True

        if code in ['ES', 'IS', 'JS', 'PC', 'UC', 'TC']:
            if self.benefit_recipient_name is None:
                logger.error(
                    'Benefit Recipient Name cannot be None when '
                    'exemption code is %s',
                    code
                )
                return False

        return True

    def h2_code_requires_hc2_cert(self):
        benefit_details = self.benefit_category_details
        hc_details = self.hc_certificate_details

        if benefit_details is None:
            return True

        benefit_code = benefit_details.benefit_category_code

        if benefit_code is None:
            return True

        code = benefit_code.text

        if code is None:
            return True

        if code == 'H2':
            if hc_details is None:
                logger.error(
                    'HCDetails cannot be None when exemption code is %s',
                    code
                )
                return False
            if hc_details.hc2_certificate_number is None:
                logger.error(
                    'Hc2CertificateNumber cannot be None when '
                    'exemption code is %s',
                    code
                )
                return False

        return True

    def h3_code_requires_hc3_cert(self):
        benefit_details = self.benefit_category_details
        hc_details = self.hc_certificate_details

        if benefit_details is None:
            return True

        benefit_code = benefit_details.benefit_category_code

        if benefit_code is None:
            return True

        code = benefit_code.text

        if code is None:
            return True

        if code == 'H3':
            if hc_details is None:
                logger.error(
                    'HCDetails cannot be None when exemption code is %s',
                    code
                )
                return False
            if hc_details.hc3_certificate_number is None:
                logger.error(
                    'Hc3CertificateNumber cannot be None when '
                    'exemption code is %s',
                    code
                )
                return False

        return True

    def bootstrap_from_yaml_specialist(self, payload):
        recipient_keys = [
            'BenefitRecipientTitle',
            'BenefitRecipientFamilyName',
            'BenefitRecipientGivenName',
            'BenefitRecipientBirthFamilyName',
        ]
        for key in recipient_keys:
            updated_key = key.replace('BenefitRecipient', '')
            logger.debug('Key is %s', key)
            logger.debug('Updated key is %s', updated_key)
            # Remove the old keys if they are there
            try:
                payload.pop(updated_key)
                logger.debug('Removed %s from payload', updated_key)
            except KeyError:
                pass
            if key in payload:
                data = payload.pop(key)
                logger.debug('Found %s in payload', key)
                payload[updated_key] = data
        tree = self.bootstrap_from_yaml_generic(payload)
        for key in recipient_keys:
            updated_key = key.replace('BenefitRecipient', '')
            try:
                payload.pop(updated_key)
                logger.debug('Removed %s from payload', updated_key)
            except KeyError:
                pass
        return tree


class ExemptionOrRemissionStatusOnAcceptanceDetails(BenefitRecipientStructure):
    def bootstrap_from_yaml_specialist(self, payload):
        acceptance_keys = [
            'BenefitCategoryCodeOnAcceptance',
            'BenefitCategoryDescriptionOnAcceptance',
            'HC2CertificateNumberOnAcceptance',
            'HC3CertificateNumberOnAcceptance',
            'BenefitRecipientTitleOnAcceptance',
            'BenefitRecipientFamilyNameOnAcceptance',
            'BenefitRecipientGivenNameOnAcceptance',
            'BenefitRecipientBirthFamilyNameOnAcceptance',
            'BenefitRecipientDateOfBirthOnAcceptance',
            'BenefitRecipientNINumberOnAcceptance',
            'EvidenceNotProducedOnAcceptance'
        ]
        for key in acceptance_keys:
            updated_key = key.replace('OnAcceptance', '')
            logger.debug('Key is %s', key)
            logger.debug('Updated key is %s', updated_key)
            # Remove the old keys if they are there
            try:
                payload.pop(updated_key)
                logger.debug('Removed %s from payload', updated_key)
            except KeyError:
                pass
            if key in payload:
                # acceptance_data = True
                data = payload.pop(key)
                logger.debug('Found %s in payload', key)
                payload[updated_key] = data
        tree = super(
            ExemptionOrRemissionStatusOnAcceptanceDetails, self
        ).bootstrap_from_yaml_specialist(payload)

        return tree


class ExemptionOrRemissionStatusDetails(BenefitRecipientStructure):
    pass


class ObservationsText(NotesType):
    pass


class ObservationsDetails(MessageElement):
    required_children = ['observations_text']
    child_classes = [ObservationsText]

    def __init__(self, observations_text=None):
        if observations_text is None:
            self.observations_text = []
        else:
            self.observations_text = observations_text

    def add_observations_text(self, observations_text):
        self.observations_text.append(observations_text)


class ExemptionDetails(MessageElement):
    child_classes = [
        ExemptionOrRemissionStatusDetails,
        ExemptionOrRemissionStatusChanged,
        ExemptionOrRemissionStatusOnAcceptanceDetails,
    ]

    def __init__(self, exemption_or_remission_status_details=None,
                 exemption_or_remission_status_changed=None,
                 exemption_or_remission_status_on_acceptance_details=None):
        self.exemption_or_remission_status_details = \
            exemption_or_remission_status_details
        self.exemption_or_remission_status_changed = \
            exemption_or_remission_status_changed
        self.exemption_or_remission_status_on_acceptance_details = \
            exemption_or_remission_status_on_acceptance_details


class PaymentClaimDetails(MessageElement):
    required_children = ['treatment_amount_details']
    child_classes = [
        TreatmentAmountDetails,
        ExemptionDetails,
    ]

    def __init__(self, treatment_amount_details=None, exemption_details=None):
        self.treatment_amount_details = treatment_amount_details
        self.exemption_details = exemption_details


class PatientDeclaration(MessageElement):
    required_children = ['declaration_on_acceptance']
    child_classes = [
        DeclarationOnAcceptance, DeclarationOnCompletion
    ]

    def __init__(self, declaration_on_acceptance=None,
                 declaration_on_completion=None):
        self.declaration_on_acceptance = declaration_on_acceptance
        self.declaration_on_completion = declaration_on_completion


class DentistDeclaration(MessageElement):
    required_children = ['declaration']
    child_classes = [
        PatientRefusedTreatment,
        PatientRefusedTreatmentObservations,
        Pftr, PftrFeeRequired, PftrFeeRequiredObservations,
        Declaration
    ]

    def __init__(self, patient_refused_treatment=None,
                 patient_refused_treatment_observations=None, pftr=None,
                 pftr_fee_required=None,
                 pftr_fee_required_observations=None, declaration=None):
        self.patient_refused_treatment = patient_refused_treatment
        self.patient_refused_treatment_observations = \
            patient_refused_treatment_observations
        self.pftr = pftr
        self.pftr_fee_required = pftr_fee_required
        self.pftr_fee_required_observations = pftr_fee_required_observations
        self.declaration = declaration
        self.validators = [
            self.patient_refused_treatment_is_valid,
            self.pftr_is_valid,
            self.pftr_fee_required_is_valid,
        ]

    def patient_refused_treatment_is_valid(self):
        if self.patient_refused_treatment is not None:
            if self.patient_refused_treatment.text:
                if self.patient_refused_treatment_observations is not None:
                    return True
                else:
                    return False
        return True

    def pftr_is_valid(self):
        if self.pftr is not None:
            if self.pftr.text:
                if self.pftr_fee_required is not None:
                    return True
                else:
                    return False
        return True

    def pftr_fee_required_is_valid(self):
        if self.pftr_fee_required is not None:
            if self.pftr_fee_required.text:
                if self.pftr_fee_required_observations is not None:
                    return True
                else:
                    return False
        return True


class DentistsDeclarationDetails(MessageElement):
    required_children = ['dentist_declaration', 'patient_declaration']
    child_classes = [
        DentistDeclaration, PatientDeclaration
    ]

    def __init__(self, dentist_declaration=None, patient_declaration=None):
        self.dentist_declaration = dentist_declaration
        self.patient_declaration = patient_declaration
        self.validators = [
            self.declaration_on_completion_is_valid,
        ]

    def declaration_on_completion_is_valid(self):
        if self.patient_declaration is None:
            return True

        pat_dec = self.patient_declaration

        if self.dentist_declaration is None:
            return True

        den_dec = self.dentist_declaration

        if pat_dec.declaration_on_completion is None:
            if den_dec.pftr is not None:
                if den_dec.pftr.text:
                    return True
                else:
                    return False
            else:
                return False
        else:
            return True


class DentalClaimRequest(MessageElement):
    """The message structure for GP17 General Dental Treatment Claim
    Request messages.

    """

    nsmap = {
        None: "http://www.eps.nds.scot.nhs.uk",
        'xsd': "http://www.w3.org/2001/XMLSchema",
        'ds': "http://www.w3.org/2000/09/xmldsig#"
    }

    required_children = [
        'form_details', 'claim_reference_details',
        'practitioner_organisation_details', 'practitioner_details',
        'patient_details', 'treatment_details',
        'dentists_declaration_details', 'payment_claim_details'
    ]
    child_classes = [
        FormDetails,
        ClaimReferenceDetails,
        PractitionerOrganisationDetails,
        PractitionerDetails,
        PatientDetails,
        ContinuationCaseDetails,
        TreatmentDetails,
        DentistsDeclarationDetails,
        PaymentClaimDetails,
        ObservationsDetails,
    ]

    def __init__(self, schema_version=None, form_details=None,
                 claim_reference_details=None,
                 practitioner_organisation_details=None,
                 practitioner_details=None, patient_details=None,
                 continuation_case_details=None, treatment_details=None,
                 dentists_declaration_details=None, payment_claim_details=None,
                 observations_details=None):
        self.schema_version = schema_version
        self.form_details = form_details
        self.claim_reference_details = claim_reference_details
        self.practitioner_organisation_details = \
            practitioner_organisation_details
        self.practitioner_details = practitioner_details
        self.patient_details = patient_details
        self.continuation_case_details = continuation_case_details
        self.treatment_details = treatment_details
        self.dentists_declaration_details = dentists_declaration_details
        self.payment_claim_details = payment_claim_details
        self.observations_details = observations_details
        self.signature = True

        if self.schema_version is None:
            self.schema_version = SCHEMA_VERSION

    @property
    def required_attributes(self):
        return {'SchemaVersion': self.schema_version}

#!/usr/bin/env python3
# vim:fileencoding=utf-8

from edixml.models.base import DateType, MessageElement


class PersonGivenNameType(MessageElement):
    def __init__(self, text=None):
        self.text_validators = [
            (self.validate_length, 1, 35),
        ]
        self.text = text


class PersonFamilyNameType(MessageElement):
    def __init__(self, text=None):
        self.text_validators = [
            (self.validate_length, 1, 35),
        ]
        self.text = text


class PersonNameTitleType(MessageElement):
    def __init__(self, text=None):
        self.text_validators = [
            (self.validate_length, 1, 35),
        ]
        self.text = text


class FamilyName(PersonFamilyNameType):
    pass


class BirthFamilyName(PersonFamilyNameType):
    pass


class GivenName(PersonGivenNameType):
    pass


class Title(PersonNameTitleType):
    pass


class NameStructure(MessageElement):
    required_children = ['family_name', 'given_name']
    child_classes = [
        Title, FamilyName, BirthFamilyName, GivenName
    ]

    def __init__(self, family_name=None, given_name=None,
                 birth_family_name=None, title=None):
        self.family_name = family_name
        self.given_name = given_name
        self.birth_family_name = birth_family_name
        self.title = title


class StructuredName(NameStructure):
    pass


class Name(NameStructure):
    pass


class DateOfBirth(DateType):
    pass


class Sex(MessageElement):
    """Note :- The acceptable values for this must include categories
    for patients of indeterminate sex (i), patients whose sex has not
    been recorded (u) , male (m) and  female (f).

    """
    def __init__(self, text=None):
        self.text_coercers = [
            (self.coerce_to_lower,)
        ]
        self.text_validators = [
            (self.validate_value_in_list, ['i', 'u', 'm', 'f']),
        ]
        self.text = text


class RepresentativeName(NameStructure):
    pass


class BenefitRecipientName(NameStructure):
    pass


class ProfessionalCategoryType(MessageElement):
    def __init__(self, text=None):
        self.text_validators = [
            (self.validate_value_in_list,
             ['Dentist', 'DentalHealthCareProfessional', 'Orthodontist']),
        ]
        self.text = text

    def bootstrap_from_yaml_specialist(self, payload):
        if self.yaml_name in payload:
            self.text = payload.get(self.yaml_name)
        else:
            self.text = 'Dentist'
        return self


class ProfType(ProfessionalCategoryType):
    pass


class ProfessionalCodeType(MessageElement):
    def __init__(self, text=None):
        self.text_coercers = [
            (self.coerce_to_string,)
        ]
        self.text_validators = [
            (self.validate_length, 1, 20),
        ]
        self.text = text


class ProfCode(ProfessionalCodeType):
    pass


class IdSchemeType(MessageElement):
    def __init__(self, text=None):
        self.text_validators = [
            (self.validate_length, 1, 255),
        ]
        self.text = text


class ProfCodeScheme(IdSchemeType):
    def bootstrap_from_yaml_specialist(self, payload):
        if self.yaml_name in payload:
            self.text = payload.get(self.yaml_name)
        else:
            self.text = 'Dental List Number'
        return self


class ProfCodeStructure(MessageElement):
    required_children = ['prof_code', 'prof_code_scheme']
    child_classes = [ProfCode, ProfCodeScheme]

    def __init__(self, prof_code=None, prof_code_scheme=None):
        self.prof_code = prof_code
        self.prof_code_scheme = prof_code_scheme

    def bootstrap_from_yaml_specialist(self, payload):
        tree = self.bootrap_from_yaml_generic(payload)
        if 'ProfCode' in payload:
            return tree
        else:
            return None


class HcpId(ProfCodeStructure):
    pass


class AlternativeHcpId(ProfCodeStructure):
    def bootstrap_from_yaml_specialist(self, payload):
        alt_keys = [
            'AlternativeProfCode',
            'AlternativeProfCodeScheme',
        ]
        for key in alt_keys:
            updated_key = key.replace('Alternative', '')
            try:
                payload.pop(updated_key)
            except KeyError:
                pass
            if key in payload:
                data = payload.pop(key)
                payload[updated_key] = data
        tree = self.bootstrap_from_yaml_generic(payload)
        if 'ProfCode' in payload:
            return tree
        else:
            return None


class HcpName(MessageElement):
    required_children = ['structured_name']
    child_classes = [StructuredName]

    def __init__(self, structured_name=None):
        self.structured_name = structured_name

    def bootstrap_from_yaml_specialist(self, payload):
        tree = self.bootstrap_from_yaml_generic(payload)
        family_name_key = 'DentistFamilyName'
        if family_name_key in payload:
            family_name = FamilyName(payload.get(family_name_key))
            if tree is None:
                tree = self
            if tree.structured_name is None:
                tree.structured_name = StructuredName()
            tree.structured_name.family_name = family_name
        given_name_key = 'DentistGivenName'
        if given_name_key in payload:
            given_name = GivenName(payload.get(given_name_key))
            if tree is None:
                tree = self
            if tree.structured_name is None:
                tree.structured_name = StructuredName()
            tree.structured_name.given_name = given_name
        return tree


class HealthcareProfessionalPractitionerStructure(MessageElement):
    required_children = ['hcp_name', 'prof_type', 'hcp_id']
    child_classes = [HcpName, ProfType, HcpId, AlternativeHcpId]

    def __init__(self, hcp_name=None, prof_type=None, hcp_id=None,
                 alternative_hcp_id=None):
        self.hcp_name = hcp_name
        self.prof_type = prof_type
        self.hcp_id = hcp_id
        self.alternative_hcp_id = alternative_hcp_id


class PractitionerDetails(HealthcareProfessionalPractitionerStructure):
    pass


class ClaimingPractitionerDetails(HealthcareProfessionalPractitionerStructure):
    pass


class PractitionerPinNumber(MessageElement):
    """Practitioner Pin Number"""
    def __init__(self, text=None):
        self.text_validators = [
            (self.validate_length, 6),
        ]
        self.text = text

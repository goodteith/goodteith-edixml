#!/usr/bin/env python3
# vim:fileencoding=utf-8


import logging
import itertools
import string

from edixml.exceptions.base import XmlValidationError
from edixml.models.address import (
    Address,
    AddressLines,
    AddressLine,
    PostCode,
)
from edixml.models.base import DateType, MessageElement, XmlBoolean
from edixml.models.person import (
    Title,
    DateOfBirth,
    Sex,
)


logger = logging.getLogger(__name__)


class PatientGivenNameType(MessageElement):
    def __init__(self, text=None):
        self.text_validators = [
            (self.validate_length, 1, 35),
        ]
        self.text = text

    @property
    def text(self):
        if self._text is not None:
            text = self._text
            text = text.replace('-', ' ')
            text = ''.join(
                c for c in text if c not in list(string.punctuation))
            text = ''.join(c for c in text if c not in list(string.digits))
            text = text.split(' ')[0]
            text = text[:14]
            return text
        else:
            return None

    @text.setter
    def text(self, text):
        self._text = text


class PatientFamilyNameType(MessageElement):
    def __init__(self, text=None):
        self.text_validators = [
            (self.validate_length, 1, 35),
        ]
        self.text = text

    @property
    def text(self):
        if self._text is not None:
            text = self._text
            text = text.replace('-', ' ')
            text = ''.join(
                c for c in text if c not in list(string.punctuation))
            text = ''.join(
                c for c in text if c not in list(string.digits))
            # Generate all permutations of upper/lower mac and mc
            macs = map(
                ''.join, itertools.product(*zip('mac'.upper(), 'mac'.lower())))
            mcs = map(
                ''.join, itertools.product(*zip('mc'.upper(), 'mc'.lower())))
            for mc in list(macs) + list(mcs):
                if (mc + ' ') in text:
                    text = text.replace(mc + ' ', mc)
            text = text.split(' ')[-1]
            text = text[:14]
            return text
        else:
            return None

    @text.setter
    def text(self, text):
        self._text = text


class PatientFamilyName(PatientFamilyNameType):
    node_name = 'FamilyName'
    yaml_name = 'PatientFamilyName'


class PatientBirthFamilyName(PatientFamilyNameType):
    node_name = 'BirthFamilyName'
    yaml_name = 'PatientBirthFamilyName'


class PatientGivenName(PatientGivenNameType):
    node_name = 'GivenName'
    yaml_name = 'PatientGivenName'


class PatientName(MessageElement):
    child_classes = [
        Title, PatientFamilyName, PatientBirthFamilyName, PatientGivenName
    ]
    node_name = 'Name'

    def __init__(self, family_name=None, given_name=None,
                 birth_family_name=None, title=None):
        self.patient_family_name = family_name
        self.patient_given_name = given_name
        self.patient_birth_family_name = birth_family_name
        self.title = title


class ChiNumber(MessageElement):
    """Community Health Index Number."""
    def __init__(self, text=None):
        self.text_validators = [
            (self.validate_simple_patterns_alt,
             [['^[0123]{1}\\d{9}$']])
        ]
        self.text = text
        self.node_name = 'CHINumber'


class PatientDetailsRequestFlag(XmlBoolean):
    """A boolean flag to indicate whether a Patient Details check has
    been performed prior to sending the claim

    """
    pass


class PatientDetailsResponseReference(MessageElement):
    """MIDAS generated reference number for the response to the patient
    details request

    """
    def __init__(self, text=None):
        self.text_validators = [
            (self.validate_length, 10),
        ]
        self.text = text


class PatientDetailsRequestDate(DateType):
    """Date the Patient Details Request was made"""
    pass


class PatientDetailsRequestCheck(MessageElement):
    required_children = [
        'patient_details_request_flag'
    ]
    child_classes = [
        PatientDetailsRequestFlag, PatientDetailsResponseReference,
        PatientDetailsRequestDate
    ]

    def __init__(self, patient_details_request_flag=None,
                 patient_details_response_reference=None,
                 patient_details_request_date=None):
        self.patient_details_request_flag = patient_details_request_flag
        self.patient_details_response_reference = \
            patient_details_response_reference
        self.patient_details_request_date = patient_details_request_date


class PatientDetailsDentalStructure(MessageElement):
    required_children = [
        'patient_name', 'address', 'date_of_birth', 'sex'
    ]
    child_classes = [
        ChiNumber, PatientName, Address, DateOfBirth, Sex,
    ]

    def __init__(self, patient_name=None, address=None,
                 date_of_birth=None, sex=None, chi_number=None):
        self.chi_number = chi_number
        self.patient_name = patient_name
        self.address = address
        self.date_of_birth = date_of_birth
        self.sex = sex
        self.node_name = 'PatientDetails'
        self.class_var_name = 'patient_details'

    def bootstrap_from_yaml_specialist(self, payload):
        tree = self.bootstrap_from_yaml_generic(payload)
        address_key = 'PatientAddressLine'
        if address_key in payload:
            if tree is None:
                tree = self
            lines = AddressLines([
                AddressLine(text) for text in
                payload.get(address_key).split('\n')
                if text
            ])
            if tree.address is None:
                tree.address = Address()
            tree.address.address_lines = lines
        postcode_key = 'PatientPostcode'
        if postcode_key in payload:
            if tree is None:
                tree = self
            postcode = PostCode(payload.get(postcode_key))
            if tree.address is None:
                tree.address = Address()
            tree.address.post_code = postcode
        return tree


class PatientDetails(MessageElement):
    required_children = [
        'patient_details',
        'patient_details_request_check',
    ]
    child_classes = [PatientDetailsDentalStructure, PatientDetailsRequestCheck]

    def __init__(self, patient_details=None,
                 patient_details_request_check=None):
        self.patient_details = patient_details
        self.patient_details_request_check = patient_details_request_check

        self.validators = [
            self.chi_number_is_valid,
        ]

    def chi_number_is_valid(self):
        chi = self.patient_details.chi_number
        try:
            ref = (
                self.patient_details_request_check
                .patient_details_response_reference
            )
        except AttributeError:
            ref = None
        if chi is None:
            if ref is None:
                logger.exception('CHI Number is missing in patient,'
                                 'reference must be provided.')
                raise XmlValidationError(
                    "Reference must be provided if CHI number is not")
        return True

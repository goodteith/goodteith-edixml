#!/usr/bin/env python3
# vim:fileencoding=utf-8


from edixml.models.base import MessageElement
from edixml.models.address import Address


class OrganisationCodeType(MessageElement):
    def __init__(self, text=None):
        self.text_validators = [
            (self.validate_length, 1, 10),
        ]
        self.text = text


class IdValue(OrganisationCodeType):
    pass


class OrganisationNameType(MessageElement):
    def __init__(self, text=None):
        self.text_validators = [
            (self.validate_length, 1, 255),
        ]
        self.text = text


class OrganisationName(OrganisationNameType):
    pass


class OrganisationType(MessageElement):
    def __init__(self, text=None):
        self.text_validators = [
            (self.validate_value_in_list,
             ['pharmacy', 'optometrypractice', 'dentalpractice']),
        ]
        self.text = text

    def bootstrap_from_yaml_specialist(self, payload):
        if self.yaml_name in payload:
            self.text = payload.get(self.yaml_name)
        else:
            self.text = 'dentalpractice'
        return self


class OrganisationId(MessageElement):
    """Unique Identifier of the organisation.

    eg.
    - GP practice Code
    - Optometry pratice list number

    """

    required_children = ['id_value']
    child_classes = [IdValue]

    def __init__(self, id_value=None):
        self.id_value = id_value


class OrganisationAlternativeStructure(MessageElement):
    """ePractitioner organisation structure which uses mandatory address."""
    required_children = [
        'organisation_id',
        'organisation_name',
        'organisation_type',
        'address',
    ]
    child_classes = [
        OrganisationId, OrganisationName, OrganisationType, Address
    ]

    def __init__(self, organisation_id=None, organisation_name=None,
                 organisation_type=None, address=None):
        self.organisation_id = organisation_id
        self.organisation_name = organisation_name
        self.organisation_type = organisation_type
        self.address = address


class RequestingOrganisationDetails(OrganisationAlternativeStructure):

    def bootstrap_from_yaml_specialist(self, payload):
        prac_org_keys = [
            'PracticeAddressLine',
            'PracticePostCode',
        ]
        for key in prac_org_keys:
            updated_key = key.replace('Practice', '')
            # Remove the old keys if they are there
            try:
                payload.pop(updated_key)
            except KeyError:
                pass
            if key in payload:
                data = payload.pop(key)
                payload[updated_key] = data
        tree = self.bootstrap_from_yaml_generic(payload)
        return tree

#!/usr/bin/env python3
# vim:fileencoding=utf-8


import logging

from edixml.exceptions.base import XmlValidationError
from edixml.models.base import MessageElement

SCHEMA_VERSION = "1.2"
logger = logging.getLogger(__name__)


class Acknowledge(MessageElement):
    """A generic acknowledgement message.

    It can be used across multiple services to acknowledge the
    success/failure of a message transmission. An acknowledge message is
    returned in reqply to every message transmission.

    """

    required_children = ['code', 'description', 'details']

    def __init__(self, schema_version=None, code=None, description=None,
                 details=None):
        logger.debug('Creating Acknowledge object')
        logger.debug('Adding schema_version: %s', schema_version)
        logger.debug('Adding code: %s', code)
        logger.debug('Adding description: %s', description)
        logger.debug('Adding details: %s', details)
        self.schema_version = schema_version
        self.code = code
        self.description = description
        self.details = details

        self.validators = [self.code_is_valid]

        if self.schema_version is None:
            self.schema_version = SCHEMA_VERSION

    def code_is_valid(self):
        if self.code is not None:
            return self.validate_code(self.code)
        return True

    def validate_code(self, code):
        if code is not None:
            return self.validate_positive_integer(code)
        return True

    @property
    def code(self):
        return self._code

    @code.setter
    def code(self, code):
        valid = self.validate_code(code)
        if not valid:
            raise XmlValidationError(
                value=code, target='code')
        else:
            self._code = code

    @property
    def children(self):
        return [
            ('Code', self.code),
            ('Description', self.description),
            ('Details', self.details),
        ]

    @property
    def schema_version(self):
        return self._schema_version

    @schema_version.setter
    def schema_version(self, schema_version):
        valid = self.validate_schema_version(schema_version)
        if not valid:
            raise XmlValidationError(
                value=schema_version, target='schema_version')
        else:
            self._schema_version = schema_version

    @property
    def required_attributes(self):
        return {'SchemaVersion': self.schema_version}

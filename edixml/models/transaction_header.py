#!/usr/bin/env python3
# vim:fileencoding=utf-8

import datetime
import logging
import uuid

from edixml.exceptions.base import XmlValidationError, XmlMissingChildError
from edixml.models.base import (
    MessageElement, XmlBoolean, DateTimeType
)

logger = logging.getLogger(__name__)

SCHEMA_VERSION = '1.0'


class MsgCategory(MessageElement):
    def __init__(self, text=None):
        allowed_values = ['single', 'fixed', 'multiple']
        self.text_validators = [
            (self.validate_value_in_list, allowed_values),
        ]
        self.text = text


class AppBodyTypeQty(MessageElement):
    def __init__(self, text=None):
        self.text_validators = [
            (self.validate_positive_integer, ),
        ]
        self.text = text


class AppTransDateTime(DateTimeType):
    pass


class AppTransStepRef(MessageElement):
    """The transaction step identifier assigned by the issuing
    application. Will always be a positive integer > 0.

    """
    def __init__(self, text=None):
        self.text_coercers = [
            (self.coerce_to_integer,)
        ]
        self.text_validators = [
            (self.validate_positive_integer,)
        ]
        self.text = text


class GuidType(MessageElement):
    def __init__(self, text=None):
        self.text_validators = [
            (self.validate_uuid,),
        ]
        self.text = text

    def bootstrap_from_yaml_specialist(self, payload):
        if self.yaml_name in payload:
            value = payload.get(self.yaml_name)
            self.text = uuid.UUID(value)
            return self
        return None


class AppLastBlockId(GuidType):
    """Used in Update and Refresh transactions.  In a n update or
    refresh request it is the ID of the last record block (AppBody)
    received in the previous transaction of the sequence.  In a response
    to an update or refresh request it is the ID assigned to the last
    record block  (AppBody) in the response. Used to confirm receipt of
    last response and  identify start block required in next response.

    """
    node_name = 'AppLastBlockID'


class AppSttl(MessageElement):
    """Step-Time-To-Live - The business transaction step lifetime (in
    seconds). Time period available for message management system to
    complete message exchange.

    """
    def __init__(self, text=None):
        self.text_coercers = [
            (self.coerce_to_integer,)
        ]
        self.text_validators = [
            (self.validate_positive_integer,)
        ]
        self.text = text
        self.node_name = 'AppSTTL'


class AppTransId(GuidType):
    """A GUID. The transaction identifier assigned by the issuing
    application.

    """
    node_name = 'AppTransID'


class MsgPriority(MessageElement):
    """Each message type will be assigned a priority which will be used
    to determine the order of sending by the message management system.
    Messages with MsgPriority="immediate" MUST always be processed ahead
    of "normal" messages.

    """
    def __init__(self, text='normal'):
        self.text_validators = [
            (self.validate_value_in_list, ['normal', 'immediate'])
        ]
        self.text = text


class MsgStatus(MessageElement):
    """Indicator of Live or Test status of a message.  Test messages
    MUST NOT be included in operational outcomes.

    """
    def __init__(self, text='test'):
        self.text_validators = [
            (self.validate_value_in_list, ['live', 'test']),
        ]
        self.text = text


class OrganisationName(MessageElement):
    """Text string for holding the name by which an organisation wishes
    to be known or the official name given to an organisation.

    """
    def __init__(self, text=None):
        self.text_validators = [
            (self.validate_length, 1, 255),
        ]
        self.text = text


class OrganisationType(MessageElement):
    """The set of Healthcare Organisations from whom a message can
    originate - e.g. 'Pharmacy' , 'GP Practice', 'optemetrypractice'

    """
    def __init__(self, text=None):
        self.text_validators = [
            (self.validate_value_in_list, [
                'pharmacy', 'GPpractice', 'system', 'dentalpractice',
                'optometrypractice']),
        ]
        self.text = text


class OrganisationId(MessageElement):
    """Text string for holding the unique identifier of the message
    sending organisation.

    """
    def __init__(self, text=None):
        self.text_validators = [
            (self.validate_length, 1, 10),
        ]
        self.text = text
        self.node_name = 'OrganisationID'


class SenderDetails(MessageElement):
    """Details of the organisation from which the message originated."""

    required_children = [
        'organisation_type',
        'organisation_id',
        'organisation_name']
    child_classes = [
        OrganisationType,
        OrganisationId,
        OrganisationName,
    ]

    def __init__(self, organisation_type=None, organisation_id=None,
                 organisation_name=None):
        self.organisation_type = organisation_type
        self.organisation_id = organisation_id
        self.organisation_name = organisation_name


class ProductName(MessageElement):
    """The manufacturers product name for the software used to create
    the message."""
    required_attributes_list = ['ProductVersion']

    def __init__(self, text=None, attributes=None):
        self.text_validators = [
            (self.validate_length, 1, 50),
        ]
        self.text = text
        self.attributes = attributes


class ProductAuthor(MessageElement):
    """The company name of the manufacturer of the  software used to
    create the message.

    """
    def __init__(self, text=None):
        self.text_validators = [
            (self.validate_length, 1, 50),
        ]
        self.text = text


class Software(MessageElement):
    """Structure that holds information about the software product used
    by the originating system."""

    required_children = [
        'product_name',
        'product_author']
    child_classes = [ProductName, ProductAuthor]

    def __init__(self, product_name=None, product_author=None):
        self.product_name = product_name
        self.product_author = product_author


class AppServiceName(MessageElement):
    """Identifies the name of the ePractitioner service context in which
    the message is being used."""

    required_attributes_list = ['AppServiceVersion']

    def __init__(self, text=None, attributes=None):
        self.text_validators = [
            (self.validate_value_in_list, [
                'AMS', 'MAS', 'CMS', 'PRS', 'Dental', 'Ophthalmic']),
        ]
        self.text = text
        self.attribute_validators = {
            'AppServiceVersion': [(self.validate_float,)]
        }
        self.attributes = attributes


class Signing(XmlBoolean):
    pass


class Count(MessageElement):
    def __init__(self, text=None):
        # TODO - count should be calculated
        self.text_validators = [
            (self.validate_positive_integer,),
        ]
        self.text = text


class Position(MessageElement):
    def __init__(self, text=None):
        self.text_validators = [
            (self.validate_positive_integer,),
        ]
        self.text = text


class Name(MessageElement):
    """The name of an application body type included in the message.
    """
    # TODO - what are the values allowed here?
    def __init__(self, text=None):
        self.text_validators = [
            (self.validate_length, 1, 50),
        ]
        self.text = text


class BodyDefinition(MessageElement):
    """Collection describing the content and structure of the message payload.

    A message with MsgCategory= "single" will have one occurence
    (Position=1; Count=1) of one application body type.

    A message with MsgCategory="multiple" will have one or more
    occurences (Position=1; Count>=1) of a single body type.

    MsgCategory="fixed" will have two or more body types with possible
    multiple occurences.

    Type and Position are fixed for any specfic message type.

    """
    child_classes = [
        Name,
        Position,
        Count,
        Signing,
    ]
    required_children = [
        'name', 'position', 'count', 'signing'
    ]

    def __init__(self, name=None, position=None, count=None, signing=None):
        self.name = name
        self.position = position
        self.count = count
        self.signing = signing


class BodyDefinitions(MessageElement):
    required_children = ['body_definition']
    child_classes = [BodyDefinition]

    def __init__(self, body_definition=None):
        if body_definition is None:
            self.body_definition = []
        else:
            self.body_definition = body_definition
        self.update_positions()
        self.update_counts()

    def add_body_definition(self, body_definition):
        self.body_definition.append(body_definition)
        self.update_positions()
        self.update_counts()

    def update_positions(self):
        message_types = []
        position = 0
        for body in self.body_definition:
            name = body.name.text
            if name not in message_types:
                position += 1
                message_types.append(name)
            body.position = Position(position)

    def update_counts(self):
        message_types = {}
        for body in self.body_definition:
            name = body.name.text
            if name in message_types:
                message_types[name] += 1
            else:
                message_types[name] = 1
            body.count = Count(message_types[name])


class TransactionHeader(MessageElement):
    """Definition of data element structure required for the generic
    message transaction header"""

    required_children = [
        'msg_priority',
        'msg_status',
        'sender_details',
        'software',
        'app_service_name',
        'app_sttl',
        'app_trans_id',
        'app_trans_step_ref',
        'app_trans_date_time',
        'msg_category',
        'app_body_type_qty',
        'body_definitions']

    required_attributes_list = ['SchemaVersion']
    child_classes = [
        MsgPriority, MsgStatus, SenderDetails, Software, AppServiceName,
        AppSttl, AppTransId, AppTransStepRef, AppTransDateTime,
        MsgCategory, AppLastBlockId, AppBodyTypeQty, BodyDefinitions
    ]

    def __init__(self, msg_priority=None, msg_status=None,
                 sender_details=None, software=None,
                 app_service_name=None, app_sttl=None,
                 app_trans_id=None, app_trans_step_ref=None,
                 app_trans_date_time=None, app_last_block_id=None,
                 body_definitions=None, attributes=None):
        self.msg_priority = msg_priority
        self.msg_status = msg_status
        self.sender_details = sender_details
        self.software = software
        self.app_service_name = app_service_name
        self.app_sttl = app_sttl
        self.app_trans_id = app_trans_id
        self.app_trans_step_ref = app_trans_step_ref
        self.app_trans_date_time = app_trans_date_time
        self.app_last_block_id = app_last_block_id
        self.body_definitions = body_definitions
        self.attribute_validators = {
            'SchemaVersion': [(
                self.validate_schema_version,)]
        }
        if attributes is None:
            self.attributes = {}
        else:
            self.attributes = attributes
        self.validators = [
            self.app_trans_date_time_is_valid]

        if 'SchemaVersion' not in self.attributes:
            self.attributes['SchemaVersion'] = SCHEMA_VERSION

    def model_is_valid(self):
        super(TransactionHeader, self).model_is_valid()
        if self.msg_category == 'multiple':
            if getattr(self, 'app_last_block_id', None) is None:
                logger.exception(
                    "%s requires %s child", self.__class__.__name__,
                    'app_last_block_id')
                raise XmlMissingChildError(
                    model=self.__class__.__name__, child='app_last_block_id')
        return True

    def app_trans_date_time_is_valid(self):
        cutoff = self.app_trans_date_time.text + datetime.timedelta(
            seconds=self.app_sttl.text)
        if cutoff > datetime.datetime.now():
            return True
        else:
            logger.exception(
                "Message has expired. Please regenerate message")
            raise XmlValidationError(
                "Message has expired. Please regenerate message")

    @property
    def msg_category(self):
        if self.body_definitions is None:
            return None
        length = len(self.body_definitions.body_definition)
        if length == 1:
            msg_category = MsgCategory('single')
        elif length > 1:
            types = set([
                b.name.text for b in self.body_definitions.body_definition
            ])
            if len(types) == 1:
                msg_category = MsgCategory('multiple')
            else:
                msg_category = MsgCategory('fixed')
        else:
            msg_category = None
        return msg_category

    @property
    def app_body_type_qty(self):
        if self.msg_category is not None:
            if self.msg_category.text == 'fixed':
                types = set([
                    b.name.text for b in self.body_definitions.body_definition
                ])
                count = len(types)
                return AppBodyTypeQty(count)
            else:
                return AppBodyTypeQty(1)
        else:
            return None

    def app_last_block_id_is_valid(self):
        try:
            self.app_last_block_id.text
            block_set = True
        except AttributeError:
            block_set = False
        if self.msg_category is 'multiple':
            # app_last_block_id should be populated
            return block_set
        else:
            # app_last_block_id should be empty
            return not block_set

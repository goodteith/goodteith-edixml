#!/usr/bin/env python3
# vim:fileencoding=utf-8

from __future__ import print_function
from future.utils import python_2_unicode_compatible

import datetime
import logging
import re
import sys
import uuid

from lxml import etree

from edixml.exceptions.base import XmlValidationError, XmlMissingChildError
from edixml import utils
from edixml.settings import DEBUG

try:
    unicode
except NameError:
    unicode = str


logger = logging.getLogger(__name__)

# From https://stackoverflow.com/a/1176023
first_cap_re = re.compile('(.)([A-Z][a-z]+)')
all_cap_re = re.compile('([a-z0-9])([A-Z])')


def convert_camel_to_snake(name):
    s1 = first_cap_re.sub(r'\1_\2', name)
    s2 = all_cap_re.sub(r'\1_\2', s1).lower()
    # logger.debug('Calculated %s from %s', s2, name)
    return s2


@python_2_unicode_compatible
class MessageElement(object):
    """Abstract class for message elements.
    """

    nsmap = {
        None: "http://www.eps.nds.scot.nhs.uk",
        'xsd': "http://www.w3.org/2001/XMLSchema",
        'ds': "http://www.w3.org/2000/09/xmldsig#"
    }

    validate_schema_version_patterns = [['^[0-9]\\.[0-9][a-z]?[a-z]?$']]
    validate_guid_type_patterns = [
        ['^[0-9a-fA-F]{8}-'
         '[0-9a-fA-F]{4}-'
         '[0-9a-fA-F]{4}-'
         '[0-9a-fA-F]{4}-'
         '[0-9a-fA-F]{12}$']]
    #: list of validation methods to check attributes
    validators = []
    #: list of the classes that children should be
    child_classes = []
    #: these children will be tested to ensure they are not empty
    required_children = []
    #: Attributes to include in xml node
    attributes = {}
    required_attributes = {}
    required_attribute_list = []
    #: XML node text content
    text = None
    # List of functions to check that text is valid
    text_validators = []
    # List of functions to coerce string to correct type
    text_coercers = []
    attribute_validators = {}

    def __str__(self):
        if self.text is not None:
            return "{}({})".format(self.node_name, self.text)
        else:
            return "{}".format(self.node_name)

    @property
    def node_name(self):
        """Name of XML element represented by this class"""
        try:
            return self._node_name
        except AttributeError:
            return self.__class__.__name__

    @node_name.setter
    def node_name(self, value):
        """Set the XML element represented by this class."""
        self._node_name = value

    @property
    def yaml_name(self):
        """Name of the element when imported from a yaml document."""
        try:
            return self._yaml_name
        except AttributeError:
            return self.node_name

    @yaml_name.setter
    def yaml_name(self, value):
        """Set the yaml name."""
        self._yaml_name = value

    @property
    def class_var_name(self):
        try:
            return self._class_var_name
        except AttributeError:
            return convert_camel_to_snake(self.__class__.__name__)

    @class_var_name.setter
    def class_var_name(self, value):
        self._class_var_name = value

    @property
    def child_names(self):
        return [cls().class_var_name for cls in self.child_classes]

    @property
    def children(self):
        return [self.__getattribute__(name) for name in self.child_names]

    def __setattr__(self, name, value):
        if name == 'text' and value is not None:
            value = self._coerce_text(value)
            self._validate_text(name, value)
        if name == 'attributes' and value is not None:
            self._validate_attributes(value)
        object.__setattr__(self, name, value)

    def _validate_text(self, name, value):
        self._validate_field(name, value, self.text_validators)

    def _validate_attributes(self, attributes):
        """Validate all attributes in attributes.

        This needs to be done each time, since there is no way to
        determine which attribute has been changed.

        """
        for attr_name, attr_value in attributes.items():
            validators = self.attribute_validators.get(attr_name, [])
            self._validate_field(attr_name, attr_value, validators)

    def _validate_field(self, name, value, validators):
        if validators:
            for validator in validators:
                if len(validator) > 0:
                    # Python2 compatible unpack
                    args = validator[1:]
                    validator = validator[0]
                    valid = validator(value, *args)
                else:
                    valid = validator(value)
                if not valid:
                    logger.exception(
                        'Value %s is not a valid value for %s in %s',
                        value, name, self.node_name
                    )
                    raise XmlValidationError(
                        value=value, target=name, node=self.node_name)
        else:
            # There are no validators
            valid = True
        if not valid:
            logger.exception(
                'Value %s is not a valid value for %s in %s',
                value, name, self.node_name
            )
            raise XmlValidationError(value=value, target=name)

    def _coerce_text(self, value):
        if self.text_coercers:
            for coercer in self.text_coercers:
                if len(coercer) > 0:
                    args = coercer[1:]
                    coercer = coercer[0]
                    try:
                        value = coercer(value, *args)
                    except (ValueError, TypeError):
                        continue
                else:
                    try:
                        value = coercer(value)
                    except (ValueError, TypeError):
                        continue
        return value

    def as_etree(self):
        """Create an etree representation of this model and all child models.

        """
        if not self.is_valid():
            self.get_validation_error()
        if self.required_attributes:
            if DEBUG:
                print(
                    "{} should use class attribute instead of required".format(
                        self.node_name
                    )
                )
            attributes = self.required_attributes.copy()
            if self.nsmap:
                attributes['nsmap'] = self.nsmap
            root = etree.Element(self.node_name, **attributes)
        elif self.attributes:
            populated_attributes = {
                k: utils.get_string_repr(v) for k, v
                in self.attributes.items() if v is not None}
            if self.nsmap:
                populated_attributes['nsmap'] = self.nsmap
            root = etree.Element(self.node_name, **populated_attributes)
        else:
            if self.nsmap:
                root = etree.Element(self.node_name, nsmap=self.nsmap)
            else:
                root = etree.Element(self.node_name)
        if self.text is not None:
            root.text = utils.get_string_repr(self.text)
        else:
            for child in self.children:
                if isinstance(child, list):
                    for elem in child:
                        root.append(elem.as_etree())
                elif isinstance(child, tuple):
                    nodename, value = child
                    if DEBUG:
                        print("{} should be replaced with data class".format(
                            nodename), file=sys.stderr)
                    if value is not None:
                        etree.SubElement(
                            root, nodename).text = utils.get_string_repr(value)
                elif child is not None:
                    root.append(child.as_etree())
        return root

    def bootstrap_from_yaml(self, payload):
        try:
            return self.bootstrap_from_yaml_specialist(payload)
        except AttributeError:
            return self.bootstrap_from_yaml_generic(payload)

    def bootstrap_from_yaml_generic(self, payload):
        if self.yaml_name in payload:
            value = payload.get(self.yaml_name)
            if isinstance(value, str):
                logger.warning(
                        "Value for %s in %s needs to be "
                        "handled by boutique", value, self.__class__.__name__
                )
            if isinstance(value, (dict, list)):
                logger.warning(
                    "Value for %s in %s needs to be "
                    "handled by boutique", value, self.__class__.__name__
                )
            else:
                self.text = value
        for attr, cls in zip(self.child_names, self.child_classes):
            result = cls().bootstrap_from_yaml(payload)
            if result is not None:
                self.__setattr__(attr, result)
        if len(self.children) == 0:
            if self.text is not None:
                return self
            else:
                return None
        populated = False
        for child in self.children:
            if child is not None and child != []:
                populated = True

        if populated:
            return self
        else:
            return None

    def is_valid(self):
        """Check that tree and any decendents is valid."""
        for validator in self.validators:
            # TODO Deprecated. Handled by __setattr__
            if not validator():
                return False
        for value in self.required_attributes.values():
            # TODO Deprecated. Handled by __setattr__
            if value is None:
                return False
        if not self.model_is_valid():
            return False
        for child in self.children:
            if isinstance(child, list):
                for elem in child:
                    if elem is not None:
                        if not elem.is_valid():
                            return False
            elif isinstance(child, MessageElement):
                if not child.is_valid():
                    return False
        return True

    def model_is_valid(self):
        """Ensure that the model contains all mandatory children."""
        for child in self.required_children:
            node = getattr(self, child, None)
            if node is None:
                logger.exception(
                    'Model %s is missing %s child.', self.__class__.__name__,
                    child,
                )
                raise XmlMissingChildError(
                    model=self.__class__.__name__, child=child)
            if isinstance(node, list) and len(node) == 0:
                logger.exception(
                    'Model %s is missing %s child.', self.__class__.__name__,
                    child,
                )
                raise XmlMissingChildError(
                    model=self.__class__.__name__, child=child)
        return True

    def get_validation_error(self):
        """Detect where the validation error occurs."""
        for validator in self.validators:
            if not validator():
                logger.exception(
                    'Validator %s returned false for %s',
                    validator, self.__class__.__name__
                )
                raise XmlValidationError("{} returned False".format(validator))
        for key, value in self.required_attributes.items():
            if value is None:
                logger.exception(
                    '%s cannot be None in %s',
                    key, self.__class__.__name__
                )
                raise XmlValidationError("{} cannot be None".format(key))
        self.model_is_valid()
        for child in self.children:
            if isinstance(child, list):
                for elem in child:
                    if elem is not None:
                        elem.get_validation_error()
            elif isinstance(child, MessageElement):
                child.get_validation_error()

    def validate_simple_patterns_alt(self, target, patterns):
        return self.validate_simple_patterns(patterns, target)

    def validate_simple_patterns(self, patterns, target):
        """Validate target against a list of patterns.

        :param list patterns: List of lists of strings/patterns. The
                              outer elements are ANDed, and the inner
                              elements are ORed.
        :param str target: string to test against patterns.
        :return: True if pattern matches, otherwise False
        :rtype: bool

        """
        found1 = True
        for patterns1 in patterns:
            found2 = False
            for patterns2 in patterns1:
                if re.search(patterns2, target) is not None:
                    found2 = True
                    break
            if not found2:
                found1 = False
                break
        return found1

    def validate_positive_integer(self, target):
        """Check target is a positive integer.

        :param obj target: value to test
        :return: True if value is positive integer, otherwise False
        :rtype: bool

        """
        return (isinstance(target, int) and target > 0)

    def validate_positive_number(self, target):
        """Check target is a positive number (float or integer).

        :param obj target: value to test
        :return: True if value is a positive number, otherwise False
        :rtype: bool

        """
        if isinstance(target, bool):
            return False

        return (
            (isinstance(target, int) and target > 0) or
            (isinstance(target, float) and target > 0)
        )

    def validate_positive_number_or_zero(self, target):
        """Check target is a number zero or greater (float or integer).

        :param obj target: value to test
        :return: True if value is a positive number, otherwise False
        :rtype: bool

        """
        if isinstance(target, bool):
            return False

        return (
            (isinstance(target, int) and target >= 0) or
            (isinstance(target, float) and target >= 0)
        )

    def validate_integer_in_range(self, target, minimum, maximum):
        """Check target is an integer inside the given range (inclusive).

        :param obj target: value to test
        :param int minimum: minimum length (inclusive)
        :param int maximum: maximum allowable length (inclusive)
        :return: True if value is inside range, otherwise False.
        :rtype: bool
        """
        return (isinstance(target, int) and target >= minimum and
                target <= maximum)

    def validate_boolean(self, target):
        """Check target is boolean.

        .. warning::
            validate_boolean(False) will return True!

        :param obj target: value to test
        :return: True if target is a bool
        :rtype: bool

        """
        return target is True or target is False

    def validate_max_two_decimal_places(self, target):
        """Check that a float has a maximum of two dp.

        :param obj value: value to test
        :return: True if target is a bool
        :rtype: bool

        """
        pennies = str(target).split('.')[-1]
        if len(str(pennies)) <= 2:
            return True
        else:
            return False

    def validate_float(self, target):
        """Check target is a float.

        :param obj target: value to test
        :return: True if value is a float, otherwise False
        :rtype: bool

        """
        return (isinstance(target, float))

    def validate_uuid(self, value):
        """Check value is a uuid object.

        :param obj value: value to test
        :return: True if value is a uuid, otherwise False
        :rtype: bool

        """
        return (isinstance(value, uuid.UUID))

    def validate_date(self, value):
        """Check value is a date.

        :param obj value: value to test
        :return: True if value is a datetime.date object
        :rtype: bool

        """
        return isinstance(value, datetime.date)

    def validate_datetime(self, value):
        """Check value is a datetime.

        :param obj value: value to test
        :return: True if value is a datetime.datetime object
        :rtype: bool

        """
        return isinstance(value, datetime.datetime)

    def validate_length(self, value, minimum, maximum=None):
        """Ensure string is not not too short and not too long.

        If only minimum is provided, test with check that string is
        precisely minimum length.

        :param str value: string to test
        :param int minimum: minimum length (inclusive)
        :param int maximum: maximum allowable length (inclusive)
        :return: True if value is good length, otherwise False.
        :rtype: bool

        """
        if maximum is None:
            return len(value) == minimum
        else:
            return (len(value) >= minimum and len(value) <= maximum)

    def validate_value_in_list(self, value, possibles):
        """Test that value matches one of the entries in list, or is None.

        :param value: value to search for
        :param list possibles: haystack to search in
        :return: True if value is in list or None, otherwise False
        :rtype: bool

        """
        # TODO - remove test for None. It should not be needed.
        if value is not None:
            if value in possibles:
                return True
            else:
                return False
        return True

    def validate_characters_in_list_no_repeats(self, value, possibles):
        """Test that each character exists in the list of possibles.

        No duplicates are permitted.

        :param value: value to tokenize and match
        :param list possibles: haystack to search in
        :return: True if values are all in list, with no repeats,
                 otherwise False
        :rtype: bool

        """
        # Check that each character is in the possibles first.
        try:
            value_list = list(value)
        except TypeError:
            return False
        for character in value_list:
            valid = self.validate_value_in_list(character, possibles)
            if not valid:
                return False
        if len(list(value)) == len(set(value)):
            return True
        else:
            return False

    def validate_schema_version(self, value):
        """Test that value is correct syntax for a schema version value.

        :param float value: value to test
        :return: True if value is appropriate, otherwise False
        :rtype: bool

        """
        # TODO - remove test for None. It should not be needed.
        if value is not None:
            return self.validate_simple_patterns_alt(
                value, self.validate_schema_version_patterns)
        return True

    def coerce_to_string(self, value):
        if value is not None:
            return str(value)
        return value

    def coerce_to_integer(self, value):
        if isinstance(value, (str, unicode)):
            if value is not None:
                return int(value)
        return value

    def coerce_to_float(self, value):
        if isinstance(value, (str, unicode)):
            if value is not None:
                return float(value)
        return value

    def coerce_to_date(self, value):
        if isinstance(value, (str, unicode)):
            if value is not None:
                return datetime.datetime.strptime(value, '%Y-%m-%d').date()
        return value

    def coerce_to_datetime(self, value):
        possible_formats = [
            '%Y-%m-%d',
            '%Y-%m-%d %H:%M:%S.%f',
            '%Y-%m-%dT%H:%M:%S',
        ]
        if isinstance(value, (str, unicode)):
            if value is not None:
                for fmt in possible_formats:
                    try:
                        return datetime.datetime.strptime(value, fmt)
                    except ValueError:
                        pass
        return value

    def coerce_to_boolean(self, value):
        if value is not None:
            if isinstance(value, (str, unicode)):
                if value.lower() == "true":
                    return True
                elif value.lower() == "false":
                    return False
        return value

    def coerce_to_upper(self, value):
        if value is not None:
            if isinstance(value, (str, unicode)):
                return value.upper()
        return value

    def coerce_to_lower(self, value):
        if value is not None:
            if isinstance(value, (str, unicode)):
                return value.lower()
        return value

    def schema_version_is_valid(self):
        """Ensure that the model's schema_version attribute is of the
        correct form.

        :return: True if schema version is appropriate, otherwise False
        :rtype: bool

        """
        return self.validate_schema_version(self.schema_version)


class XmlBoolean(MessageElement):
    def __init__(self, text=None):
        self.text_coercers = [
            (self.coerce_to_boolean,),
        ]
        self.text_validators = [
            (self.validate_boolean,),
        ]
        self.text = text


class DateType(MessageElement):
    def __init__(self, text=None):
        self.text_coercers = [
            (self.coerce_to_date,)
        ]
        self.text_validators = [
            (self.validate_date,),
        ]
        self.text = text


class DateTimeType(MessageElement):
    def __init__(self, text=None):
        self.text_coercers = [
            (self.coerce_to_datetime,)
        ]
        self.text_validators = [
            (self.validate_datetime,),
        ]
        self.text = text


class MessageEnvelope(MessageElement):
    @property
    def required_attributes(self):
        return {'SchemaVersion': self.schema_version}

    @property
    def schema_version(self):
        return self._schema_version

    @schema_version.setter
    def schema_version(self, schema_version):
        valid = self.validate_schema_version(schema_version)
        if not valid:
            logger.exception(
                '%s is not a valid schema version for %s',
                schema_version, self.__class__.__name__
            )
            raise XmlValidationError(
                value=schema_version, target='schema_version')
        else:
            self._schema_version = schema_version

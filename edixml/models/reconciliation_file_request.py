#!/usr/bin/env python3
# vim:fileencoding=utf-8

import datetime

from edixml.models.base import DateTimeType, MessageElement, MessageEnvelope
from edixml.models.organisation import RequestingOrganisationDetails
from edixml.models.transaction_header import TransactionHeader

SCHEMA_VERSION = "1.0"


class AppLastBlockId(MessageElement):
    def __init__(self, text=None):
        self.text_validators = [
            (self.validate_length, 36),
        ]
        self.text = text


class RequestDateTime(DateTimeType):
    pass


class DentalReconciliationFileRequest(MessageElement):
    nsmap = {
        None: "http://www.eps.nds.scot.nhs.uk",
        'xsd': "http://www.w3.org/2001/XMLSchema",
        'ds': "http://www.w3.org/2000/09/xmldsig#"
    }

    required_children = [
        'app_last_block_id',
        'requesting_organisation_details',
        'request_date_time',
    ]
    child_classes = [
        AppLastBlockId,
        RequestingOrganisationDetails,
        RequestDateTime
    ]

    def __init__(self, app_last_block_id=None,
                 requesting_organisation_details=None):
        # Ignore app last block id
        self.requesting_organisation_details = requesting_organisation_details
        self.request_date_time = RequestDateTime(datetime.datetime.now())
        self.schema_version = SCHEMA_VERSION
        self.signature = True

    @property
    def required_attributes(self):
        return {'SchemaVersion': self.schema_version}

    @property
    def app_last_block_id(self):
        temp_value = '00000000-0000-0000-0000-000000000000'
        return AppLastBlockId(temp_value)


class DentalReconciliationFileAppBodies(MessageElement):
    node_name = 'AppBodies'
    required_children = ['dental_reconciliation_file_request']
    child_classes = [DentalReconciliationFileRequest]

    def __init__(self, dental_reconciliation_file_request=None):
        self.dental_reconciliation_file_request = (
            dental_reconciliation_file_request
        )


class DentalReconciliationFileRequestEnv(MessageEnvelope):
    child_classes = [TransactionHeader, DentalReconciliationFileAppBodies]
    nsmap = {
        None: "http://www.eps.nds.scot.nhs.uk",
        'xsd': "http://www.w3.org/2001/XMLSchema",
        'ds': "http://www.w3.org/2000/09/xmldsig#"
    }

    def __init__(self, transaction_header=None,
                 dental_reconciliation_file_app_bodies=None,
                 schema_version=None):
        self.schema_version = schema_version
        self.transaction_header = transaction_header
        self.dental_reconciliation_file_app_bodies = (
            dental_reconciliation_file_app_bodies
        )
        if self.schema_version is None:
            self.schema_version = SCHEMA_VERSION

#!/usr/bin/env python3
# vim:fileencoding=utf-8

from edixml.models import dental_claim


def create_claim(data):
    """Create the appropriate claim xml structure."""
    if data.get('MessageType', None) == 'Dental Claim':
        claim = create_dental_claim(data=data)

    return claim


def create_dental_claim(data):
    return dental_claim.DentalClaimRequestEnv(data)

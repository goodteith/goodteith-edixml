- Patient details request should be immediate - Add a delay to make sure you get
  the response ✓
- Date of birth not being matched in patient info response ✓
- Patient search reference should be incremented every time ✓
- Change search type back to primary again after secondary completed - remove
  search reference ✓

- Automatically perform a secondary search if no match found? Or ask the user?
- Wipe errors when a new claim is sent ✓

- A acknowledgement typo: Should be *an* ✓
- Record sent datetime in Filemaker, prevent the user from resubmitting the same
  message before sttl expired ✓
- Error text when long is difficult is too ✓
- Don't increment submission count if ttl has expired.
- do a patient search, don't get a response until 90 seconds

- Remove confirmation of secondary search
- Make sure all claims have claim id
- Charting needs to fixed from Filemaker - it generated: ✓
    - 12:BP
    - 12:M

- Send automatically to schedule history unless python script fails validation ✓
- Submission count should not be incremented if claim is not sent ✓

- Return submission-claim-count for claim, and include that in the relationship
  so you can tell which errors belong to which claim, and which claim has been
  validated ✓
- Stop being so persistent about missing chi numbers ✓
- Replace "enter reference" dialog box with "Get a chi number first ✓
- Submission count should relate to errors as well, for claim errors so errors
  can be related to claim ✓
- If dentist changed half-way through, adjust claim reference to suit ✓
- Filemaker: Date of birth shouldn't be in the future. ✓
- Create file for each imported csv; do a straight import and then relate them ✓
- If `pftr`, include `pftr_fee_required` in dentist declaration ✓
- If `pftr_fee_required` is true, include `pftr_fee_required_observations` ✓
- Don't generate subsequent history - temporary records shouldn't be included in
  the history ✓
- Maybe show a warning that they're on their last chance ✓
- After the 10 claims have been said, ask if they want to generate a new claim ✓

- Figure out how to display reconciliation errors if there are no records
  returned
- Associate reconciliation errors with date or something so they can be
  associated with the request
- Don't reconcile responses that you've already received
- Need to ensure treatment has been sent to schedules before checking messages
- Reconciliation imports don't always work first time; stuff not being imported
- Make sure reconciliation imports happen every time.




- Change surname to one to get a single match
- Change surname to multi to get a multi match
- Change surname to multinone to get a single match from the secondary search
- Change first line of address to midas-nack-e000001 to throw an error
- Call someone Brown to get them in the reconciliation file
- For errors in reconciliation response, change 1st line of practice address to
  be: `midas:response=RecError`p


452621


Second round
============

Patient details request; no match found. What do? Automatically do a secondary
request? Tell the user somehow?

Claims

Prevent claim from being resubmitted unless:

- Get an ack
- Get a response
- Timer runs out


Don't send a secondary search if the timer for the primary one has expired.
Resend the primary after the timer has expired.

When dentist is changed during treatment, you need a new
patientdetailsresponsereference - it's tied to the old list number

Reconciliation file - import only errors, if current found count is 0, show all
records


Third round
===========

TTY for patient details requests:

Configure the state before a message is sent, based on three fields:

- PatientDetailsResponseReference
- Secondary Search
- MessageTime

```
if pat_details_response_reference = '' and secondary_search = '' and time_since_last_send < 2:
    # Sent a message very recently, not gotten a response back yet
    halt script
else if pat_details_response_reference = '' and secondary_search = '':
    # Never sent a message before
    set message_time to `now`
else if pat_details_response_reference > 0 and secondary_search = '':
    # Sent a primary message, gotten a response
    set secondary_search to `true`
    set message_time to `now`
else if pat_details_response_reference > 0 and secondary_search = 'true':
    # Sent a secondary message. We're starting the whole cycle again
    set message_time to `now`
    set pat_details_response_reference to ''			# We clear this because we are starting again and trusting the practice board will resend the response reference
    set secondary_search to ''

# Set up the rest of the fields
Send the file


# What happens if it fails validation?
```

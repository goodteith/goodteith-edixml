# Server installation

Using http://www.matthanson.ca/2012/04/sql-server-2012-unattended-installation/

UNZIP

1. Download `epharmacy_installation.zip`
2. Download `edixml_1.*.exe`
3. Unzip `epharmacy_installation.zip`
4. Install SQL Server 2012
    1. Click `New SQL Server stand-alone installation`
	2. Accept the licence
	3. Install all features
	4. Select `Default instance`
	5. Change `SQL Server Database Engine` user to `NETWORK SERVICE`
	6. Click next on Database Engine Configuration
	7. Click next on error reporting screen
	8. Restart if needed
	9. Run `SQL Server Configuration Manager`
		1. Select `SQL Server Network Configuration`
		2. Select `Protocols for MSSQLSERVER`
		3. Enable `Named Pipes`
	10. Run Services
	11. Restart `SQL Server (MSSQLSERVER)`
5. Run `install-tools.bat` **as an administrator**
6. Right-click on `adapter.tar.gz` and select `7-zip->Open archive`
7. Double-click on `adapter.tar` inside 7-zip
8. Drag `adapter` directory onto the desktop.
7. Install certificate manager (for everyone)
8. Check to see if routes are needed
9. Install certificates
10. Install adapter
11. Run `edixml_1.*.exe` **as an administrator**


# If the S drive not plugged into the server
If S drive is on another computer, calculate the location of the s drive using the following code:

1. Open a command prompt
2. Run python
3. Run the following python script:

```python
import win32wnet
print(win32wnet.WNetGetUniversalName('s:', 1))
```
4. Update `C:\ProgramData\goodteith\goodteith-edixml\edixml\edixml.ini` file
   with this output.


# Upgrading python stuff on the server 

1. Download the latest version of `edixml_1.*.exe` from nextcloud
2. Run it as **an administrator**.

# Disabling security warnings for executables

1. Open the start menu, and select run
2. Run `inetcpl.cpl`
3. Select the `Security` tab
4. Click `Local Intranet`
5. Click the `Sites` button
6. Click the `Advanced` button
7. Add `\\servername` (where servername is the name of the server)
8. Click `OK`
9. Click `OK`
10. Click `OK`
11. Click `OK` again probably

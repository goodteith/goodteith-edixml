#!/usr/bin/env python3
# vim:fileencoding=utf-8

# Always prefer setuptools over distutils
from setuptools import find_packages, setup
# To use consistent encoding
from codecs import open
from os import path

from edixml import __version__


here = path.abspath(path.dirname(__file__))


# Get the long description from the README file
with open(path.join(here, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()


setup(
    name='goodteith-edixml',
    version=__version__,
    description='Interface between Filemaker and ePharmacy adaptor',
    long_description=long_description,
    url='https://gitlab.com/goodteith/goodteith-edixml',
    author='Stoo Johnston',
    author_email='stoo@goodteith.scot',
    license='GPL',
    packages=find_packages(exclude=['docs', 'tests']),
    install_requires=[
        'PyYAML',
        'lxml',
        'future',
        'appdirs',
    ],
    entry_points={
        'console_scripts': [
            'archive_files=archive_files:main',
            'add_to_pile=add_to_pile:main',
            'upgrade=upgrade:main',
            'reconcile_eschedule=reconcile_eschedule:main',
            'send_and_receive=send_and_receive:main',
        ]
    },
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Goodteith users',
        'Licence :: OSI Approved :: GPL3 Licence',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.6',
    ],
    python_requires='>=2.7, !=3.0.*, !=3.1.*, !=3.2.*, <4',
    # package_data={'edixml': ['schema/*.xsd']},
    include_package_data=True,
)

# TODO

- Port tuple nodes over to leaf nodes
- Port `required_attribute` elements over to `attributes`
- Update `required_attributes_list` with a list of required attribute names.
- Rename `required_attributes_list` to `required_attributes`


## Leaf Node conversion

This branch is for working on converting the transaction headers elements to leaf nodes.

The road block is due to attributes; the `required_attributes` attribute
contains data, and there is no ability to add optional attributes.

### Proposal

- `required_attributes` should become a list of attribute names, in XML camelcase.
- `attributes` should contain a dictionary with the XML camelcase name and
attribute value.
- `attribute_validators` should contain a dictionary with the XML camelcase name
  and a list of validators to apply to that attribute.
- Base class should iterate over required attributes to check that none of them
  are set to None.

Since `required_attributes` is used for the wrong thing at the moment, I will
use `required_attributes_list` to store the list.

This will then be renamed when all old models have been moved across to proper
leaf nodes.

## Other TODOs

- Document utils
- Prevent generate yaml script from running if S drive is not available

## Reconcile Schedules

Need to read both files in if we can; and parse them both together. Each record
in the schedule needs to be related to adjustments{0,many}


## Response files


- [x] PatientDetailsResponse
- [x] Schedule
- [x] PatientRequest

## Delete files

- [ ] alerter.py
- [ ] edi_requests.py
- [ ] process_claim.py
- [ ] send.py
- [ ] receive.py

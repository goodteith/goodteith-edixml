# Migrate to data classes

- Import new class into tests
- Create new test class
    - Create `self.node_name`
    - Create `self.model`
    - Create `self.text`
    - Create `self.correct_values`
    - Create `self.incorrect_values`
- update `as_etree` in parent tests to create text classes (search for string)
- write text classes
- add text validators
- delete validator from parent
- remove validator from list
- add attribute validators
- update children tuple in parent
- update validator length tests

Note: Don't update text-attribute functions in parent models - they are fine
as-is.


```python
class ErrorCodeTest(unittest.TestCase):
    def setUp(self):
        self.model = ErrorCode()

    def test_class_has_node_name(self):
        self.assertEqual('ErrorCode', self.model.node_name)

    def test_attributes_set_correctly_in_constructor(self):
        code = '1234567'
        model = ErrorCode(code)
        self.assertEqual(code, model.text)

    def test_error_code_allows_correct_values(self):
        test_str = ''.join([random.choice(ALPHANUM_CHARS) for i in range(7)])
        try:
            ErrorCode(test_str)
        except XmlValidationError:
            self.fail("{} should allow correct values".format(
                self.model.node_name
            ))

    def test_error_code_rejects_incorrect_values(self):
        test_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(6)]),
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(8)]),
        ]
        for test_str in test_values:
            with self.assertRaises(XmlValidationError):
                ErrorCode(test_str)

    def test_to_etree_produces_correct_tree(self):
        code = '7654321'
        model = ErrorCode(code)
        reference = E.ErrorCode(code)
        if not etrees_equal(reference, model.as_etree()):
            self.fail("{} did not produce the correct etree".format(
                model.node_name))
```

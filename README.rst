EDI XML
=======

Python scripts for handling the interface between the DPB and Filemaker.

The current plan is to use the EDI adapter as an interim measure. Files
will need to be converted from yaml to xml

Currently this contains a quick file check utility for YAML, checking
that files are correct YAML syntax.

Requirements
============

-  [x] Needs to work with python2 and python3, to remain
   backwards-compatible with existing installations and minimise
   transition disruption
-  [ ] Automatically install EDI adaptor for now
-  [ ] Use configuration file to specify the following:

   -  [ ] Source file
   -  [ ] Output file

-  [x] Read in YAML file and convert to XML
-  [x] Receive XML file from adaptor and convert to csv for FileMaker
-  [x] Schema validation using `Validation with
   lxml <http://lxml.de/validation.html#xmlschema>`__ based on the
   schema version. This needs to be provided by Filemaker?

Tests
=====

To run the tests, ``cd`` to the project directory and run
``python -m unittest``

Design
======

Each element that requires validation has two validation methods:

.. code:: python

    validate_field_name(field_name)

and

.. code:: python

    field_name_is_valid()

-  The first method is provided to ensure that a value is of the correct
   form.
-  The second method takes context into account, meaning it will check
   w.r.t other values in the class.

Installation
============

-  ``add2virtualenv`` top level directory

Building
========

To build a source distribution:

.. code:: bash

    python setup.py sdist

To build a wheel:

.. code:: bash

    python setup.py bdist_wheel --universal

Building on Windows
-------------------

.. code:: bash

    pip install pyinstaller

.. code:: bash

    python setup.py bdist_wheel --universal
    pyinstaller --add-data schema;schema edixml\add_to_pile.py
    pyinstaller edixml\archive_files.py
    pyinstaller --onefile edixml\upgrade.py
    pyinstaller edixml\reconcile_eschedule.py

Entry Points
============

There are multiple entry points, depending on what machine you are using:

Client
------

- add_to_pile: Add a message to the pile to send
- archive_files: move csvs and logs out of the way after they have been imported
  into Filemaker

Server
------

- send_and_receive

Other tools
-----------

- reconcile_eschedule: Can import an eSchedule csv as if it were a ePharmacy
  one.

To reconcile a schedule, visit https://www.bo.scot.nhs.uk/BOE/BI and login.
Select `Items of Service Detail`
Click `Refresh values`
Double-click a single month
CLick `OK`
Click the `Export` button
Click `Export Data to CSV`
Run the reconcile tool and point it at the downloaded csv file.

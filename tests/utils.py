#!/usr/bin/env python3
# vim:fileencoding=utf-8

import string

from lxml import etree
import yaml

from edixml.exceptions.base import XmlValidationError
from edixml.utils import get_string_repr
from edixml.settings import DEBUG


ALPHANUM_CHARS = ''.join([string.ascii_letters, string.digits])
ALPHA_CHARS = ''.join([string.ascii_letters])
NUM_CHARS = ''.join([string.digits])


def etrees_equal(e1, e2):
    # From https://stackoverflow.com/a/24349916
    if e1.tag != e2.tag:
        return False
    if e1.text != e2.text:
        return False
    if e1.tail != e2.tail:
        return False
    if e1.attrib != e2.attrib:
        return False
    if len(e1) != len(e2):
        return False
    return all(etrees_equal(c1, c2) for c1, c2 in zip(e1, e2))


class NodeMixinBase(object):
    def __init__(self, *args, **kwargs):
        self.debug_print = False
        self.yaml = ''
        super(NodeMixinBase, self).__init__(*args, **kwargs)

    def test_class_has_node_name(self):
        self.assertEqual(self.node_name, self.model().node_name)

    def test_to_etree_produces_correct_tree(self):
        if self.debug_print:
            print()
            print("Reference: {}".format(etree.tostring(self.reference)))
            print("Generated: {}".format(
                etree.tostring(self.test_model.as_etree())))
        if not etrees_equal(self.reference, self.test_model.as_etree()):
            self.fail("{} did not produce the correct etree.".format(
                self.model().node_name))


class ComplexNodeMixin(NodeMixinBase):
    def __init__(self, *args, **kwargs):
        self.validator_count = 0
        self.children = []
        self.debug_print = False
        super(ComplexNodeMixin, self).__init__(*args, **kwargs)

    def test_required_children_set_correctly(self):
        self.assertEqual(
            self.required_children, self.model().required_children)

    def test_children_set_correctly(self):
        # TODO - for each value, test the attribute name?
        if len(self.children) > 0:
            if DEBUG:
                print("Create children list for {}".format(self.node_name))
            self.assertEqual(len(self.children), len(self.model().children))


class LeafNodeMixin(NodeMixinBase):
    def __init__(self, *args, **kwargs):
        self.validator_count = 1
        self.expected_text = ''
        self.debug_print = False
        super(LeafNodeMixin, self).__init__(*args, **kwargs)

    def test_attributes_set_correctly_in_constructor(self):
        model = self.model(self.text)
        if self.expected_text:
            self.assertEqual(self.expected_text, model.text)
        else:
            self.assertEqual(self.text, model.text)

    def test_validators_included_in_parser(self):
        self.assertEqual(
            len(self.model().text_validators), self.validator_count)

    def test_model_allows_correct_values(self):
        for test in self.correct_values:
            try:
                self.model(test)
            except XmlValidationError:
                self.fail("{} should allow correct values".format(
                    self.model().node_name
                ))

    def test_model_rejects_incorrect_values(self):
        for test in self.incorrect_values:
            with self.assertRaises(XmlValidationError):
                self.model(test)

    def test_to_etree_produces_correct_tree(self):
        self.test_model = self.model(self.text)
        self.reference = etree.Element(self.node_name)
        if self.expected_text:
            self.reference.text = self.expected_text
        else:
            self.reference.text = get_string_repr(self.text)
        super(LeafNodeMixin, self).test_to_etree_produces_correct_tree()

    def test_yaml_creates_correct_object(self):
        try:
            self.yaml_name
        except AttributeError:
            self.yaml_name = self.node_name
        if self.yaml:
            payload = yaml.load(self.yaml, Loader=yaml.loader.BaseLoader)
            expected_value = payload[self.yaml_name]
        else:
            self.yaml = '{}: {}'.format(self.yaml_name, self.text)
            payload = yaml.load(self.yaml, Loader=yaml.loader.BaseLoader)
            if self.expected_text:
                expected_value = self.expected_text
            else:
                expected_value = self.text
        model = self.model().bootstrap_from_yaml(payload)
        self.assertEqual(model.text, expected_value)


class LeafNodeWithAttributeMixin(LeafNodeMixin):
    def test_model_allows_correct_attributes(self):
        for test in self.correct_attributes:
            try:
                self.model(attributes=test)
            except XmlValidationError:
                self.fail(
                    "{} should allow {} as attributes".format(
                        self.model().node_name, test))

    def test_model_rejects_incorrect_attribute_values(self):
        for test in self.incorrect_attributes:
            with self.assertRaises(XmlValidationError):
                self.model(attributes=test)

    def test_to_etree_produces_correct_tree(self):
        self.text_model = self.model(self.text, self.attributes)
        self.reference = etree.Element(self.node_name, {
            k: get_string_repr(v) for k, v in self.attributes.items()})
        self.reference.text = get_string_repr(self.text)
        super(
            LeafNodeWithAttributeMixin, self
        ).test_to_etree_produces_correct_tree()

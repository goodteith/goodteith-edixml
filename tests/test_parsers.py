#!/usr/bin/env python3
# vim:fileencoding=utf-8


import unittest
try:  # python 3
    from unittest.mock import patch
except ImportError as e:  # python 2
    from mock import patch

from lxml import etree
from lxml.builder import ElementMaker

from edixml.parsers import (
    parse_patient_details_response_to_list,
    parse_reconciliation_response_to_list,
)


class PatientDetailsToListTest(unittest.TestCase):

    def test_all_elements_returned(self):
        E = ElementMaker(
            namespace="http://www.eps.nds.scot.nhs.uk",
            nsmap={
             None: "http://www.eps.nds.scot.nhs.uk",
             "xsd": "http://www.w3.org/2001/XMLSchema",
             "ds": "http://www.w3.org/2000/09/xmldsig#"
            })
        request_reference = '105'
        response_reference = '5027'
        patient_found_flag = 'true'
        error_code = '3333333'
        error_description = 'Something went wrong'
        additional_info = 'Seriously wrong'
        chi_number = '2000000000'
        treatment_item_1 = 'abc'
        treatment_item_2 = 'def'
        treatment_desc_1 = 'ghi'
        treatment_desc_2 = 'jkl'
        tooth_not_1 = '15'
        tooth_not_2 = '20'
        treat_date_1 = '2017-01-01'
        treat_date_2 = '2016-12-05'
        title_1 = 'Ms'
        family_1 = 'Woolfe'
        birth_1 = 'Lamb'
        given_1 = 'Virginia'
        dob_1 = '1900-05-05'
        sex_1 = 'f'
        post_code_1 = 'AB16 5HH'
        title_2 = 'Mr'
        family_2 = 'Jekyl'
        birth_2 = 'Hyde'
        given_2 = 'Eustace'
        dob_2 = '1850-08-02'
        sex_2 = 'M'
        post_code_2 = 'LW1 5HH'
        prof_code = '123456'
        prof_code_scheme = 'This is my list number or sommink'

        ref = E.DentalPatientDetailsResponseEnv(
            E.TransactionHeader(
                E.Foo('bar'),
            ),
            E.AppBodies(
                E.DentalPatientDetailsResponse(
                    E.RequestingOrganisationDetails(),
                    E.RequestingPractitionerDetails(
                        E.HcpId(
                            E.ProfCode(prof_code),
                            E.ProfCodeScheme(prof_code_scheme),
                        )
                    ),
                    E.PatientDetailsRequestReferenceDetails(
                        E.PatientDetailsRequestReference(
                            request_reference
                        ),
                        E.PatientDetailsResponseReference(
                            response_reference
                        ),
                    ),
                    E.ResponseDetails(
                        E.PatientFoundFlag(patient_found_flag),
                        E.Errors(
                            E.ErrorCode(error_code),
                            E.ErrorDescription(error_description),
                            E.ErrorAdditionalInfo(additional_info),
                        ),
                        E.MatchedCHINumber(chi_number),
                        E.TreatmentDetails(
                            E.Items(
                                E.TreatmentItem(treatment_item_1),
                                E.TreatmentDescription(treatment_desc_1),
                                E.ToothNotation(tooth_not_1),
                                E.LastTreatmentClaimDate(treat_date_1),
                            ),
                            E.Items(
                                E.TreatmentItem(treatment_item_2),
                                E.TreatmentDescription(treatment_desc_2),
                                E.ToothNotation(tooth_not_2),
                                E.LastTreatmentClaimDate(treat_date_2),
                            ),
                        ),
                        E.PatientsList(
                            E.PatientDetails(
                                E.Title(title_1),
                                E.FamilyName(family_1),
                                E.BirthFamilyName(birth_1),
                                E.GivenName(given_1),
                                E.DateOfBirth(dob_1),
                                E.Sex(sex_1),
                                E.PostCode(post_code_1),
                            ),
                            E.PatientDetails(
                                E.Title(title_2),
                                E.FamilyName(family_2),
                                E.BirthFamilyName(birth_2),
                                E.GivenName(given_2),
                                E.DateOfBirth(dob_2),
                                E.Sex(sex_2),
                                E.PostCode(post_code_2),
                            ),
                        ),
                    ),
                ),
            ),
        )
        tree = etree.ElementTree(ref)

        result = parse_patient_details_response_to_list(tree)
        expected_result = [
            [
                [request_reference, patient_found_flag,
                 response_reference, chi_number]
            ],
            [
                [request_reference, title_1, family_1, birth_1, given_1,
                 dob_1, sex_1, post_code_1],
                [request_reference, title_2, family_2, birth_2, given_2,
                 dob_2, sex_2, post_code_2],
            ],
            [
                [request_reference, treatment_item_1, treatment_desc_1,
                 tooth_not_1, treat_date_1],
                [request_reference, treatment_item_2, treatment_desc_2,
                 tooth_not_2, treat_date_2],
            ],
            [
                ['DentalPatientDetailsResponseEnv', request_reference,
                 prof_code, error_code, error_description,
                 additional_info],
            ],
        ]

        self.assertEqual(result, expected_result)


class PatientReconcilationResponseToListTest(unittest.TestCase):

    def test_all_elements_returned(self):
        E = ElementMaker(
            namespace="http://www.eps.nds.scot.nhs.uk",
            nsmap={
                None: "http://www.eps.nds.scot.nhs.uk",
                "xsd": "http://www.w3.org/2001/XMLSchema",
                "ds": "http://www.w3.org/2000/09/xmldsig#"
            })

        details_available = 'true'
        error_code = '3333333'
        error_description = 'Something went wrong'
        additional_info = 'Seriously wrong'
        schedule_date = '2017-01-31'
        list_number = '1234567'
        schedule_number = '5'
        ref_number_1 = '123456'
        amount_authed_1 = '17.60'
        charge_authed_1 = '12.01'
        remiss_count_1 = '4.21'
        percent_1 = '0.12'
        chi_number = '1234356798'
        item_1 = 'abc'
        fee_code_1 = '13636'
        adj_text_1 = 'defg'
        dentist_fee_1 = '123'
        tooth_item_1 = '12'
        tooth_fee_code_1 = 'sb'
        tooth_adj_text_1 = 'ghsdg'
        tooth_dentist_fee_1 = '125'
        tooth_item_2 = 'na'
        tooth_fee_code_2 = 'dkjg'
        tooth_adj_text_2 = 'gkjlsg'
        tooth_dentist_fee_2 = '192'
        item_2 = 'gsd'
        fee_code_2 = 'kkjlghsdfg'
        adj_text_2 = 'agas'
        dentist_fee_2 = 'a6326'
        tooth_item_3 = '215'
        tooth_fee_code_3 = '616'
        tooth_adj_text_3 = 'gdsg'
        tooth_dentist_fee_3 = 'khjgds'
        tooth_item_4 = 'hjlkh63'
        tooth_fee_code_4 = 'khgsdih'
        tooth_adj_text_4 = '6632'
        tooth_dentist_fee_4 = 'gjopjp'
        unique = 'abcde161236342'
        unique_2 = 'a0368276426342'

        ref = E.DentalReconciliationFileResponseEnv(
            E.TransactionHeader(
                E.Foo('bar'),
            ),
            E.AppBodies(
                E.DentalReconciliationFileResponse(
                    E.ResponseDetails(
                        E.DetailsAvailableFlag(details_available),
                        E.Errors(
                            E.ErrorCode(error_code),
                            E.ErrorDescription(error_description),
                            E.ErrorAdditionalInfo(additional_info),
                        ),
                        E.ScheduleDate(schedule_date),
                        E.ScheduleDetails(
                            E.ListNumber(list_number),
                            E.ScheduleNumber(schedule_number),
                            E.ItemsOfService(
                                E.ReferenceNumber(ref_number_1),
                                E.AmountAuthorised(amount_authed_1),
                                E.PatientChargeAuthorised(charge_authed_1),
                                E.RemissionAmount(remiss_count_1),
                                E.AdditionalPercentage(percent_1),
                                E.CHINumber(chi_number),
                                E.AdjustmentNarratives(
                                    E.Item(item_1),
                                    E.FeeCode(fee_code_1),
                                    E.AdjustmentText(adj_text_1),
                                    E.DentistFee(dentist_fee_1),
                                    E.ToothSpecificNarratives(
                                        E.Item(tooth_item_1),
                                        E.FeeCode(tooth_fee_code_1),
                                        E.AdjustmentText(tooth_adj_text_1),
                                        E.DentistFee(tooth_dentist_fee_1),
                                    ),
                                    E.ToothSpecificNarratives(
                                        E.Item(tooth_item_2),
                                        E.FeeCode(tooth_fee_code_2),
                                        E.AdjustmentText(tooth_adj_text_2),
                                        E.DentistFee(tooth_dentist_fee_2),
                                    ),
                                ),
                                E.AdjustmentNarratives(
                                    E.Item(item_2),
                                    E.FeeCode(fee_code_2),
                                    E.AdjustmentText(adj_text_2),
                                    E.DentistFee(dentist_fee_2),
                                    E.ToothSpecificNarratives(
                                        E.Item(tooth_item_3),
                                        E.FeeCode(tooth_fee_code_3),
                                        E.AdjustmentText(tooth_adj_text_3),
                                        E.DentistFee(tooth_dentist_fee_3),
                                    ),
                                    E.ToothSpecificNarratives(
                                        E.Item(tooth_item_4),
                                        E.FeeCode(tooth_fee_code_4),
                                        E.AdjustmentText(tooth_adj_text_4),
                                        E.DentistFee(tooth_dentist_fee_4),
                                    ),
                                ),
                            ),
                        ),
                    ),
                ),
            ),
        )

        tree = etree.ElementTree(ref)
        with patch('uuid.uuid4') as mock_uuid:
            mock_uuid.side_effect = [unique, unique_2]
            result = parse_reconciliation_response_to_list(tree)
        expected_result = [
            [   # Response
                [list_number, schedule_number],
            ],
            [   # Items of Service
                [
                    list_number, schedule_number, ref_number_1,
                    amount_authed_1, charge_authed_1, remiss_count_1,
                    percent_1, chi_number
                ],
            ],
            [   # Adjustment Narratives
                [
                    list_number, schedule_number, ref_number_1, unique,
                    item_1, fee_code_1, adj_text_1, dentist_fee_1,
                ],
                [
                    list_number, schedule_number, ref_number_1,
                    unique_2, item_2, fee_code_2, adj_text_2,
                    dentist_fee_2,
                ],
            ],
            [   # Tooth specific narratives
                [
                    list_number, schedule_number, ref_number_1, unique,
                    tooth_item_1, tooth_fee_code_1, tooth_adj_text_1,
                    tooth_dentist_fee_1,
                ],
                [
                    list_number, schedule_number, ref_number_1, unique,
                    tooth_item_2, tooth_fee_code_2, tooth_adj_text_2,
                    tooth_dentist_fee_2,
                ],
                [
                    list_number, schedule_number, ref_number_1,
                    unique_2, tooth_item_3, tooth_fee_code_3,
                    tooth_adj_text_3, tooth_dentist_fee_3,
                ],
                [
                    list_number, schedule_number, ref_number_1,
                    unique_2, tooth_item_4, tooth_fee_code_4,
                    tooth_adj_text_4, tooth_dentist_fee_4,
                ],
            ],
            [   # errors
                ['DentalReconciliationFileResponseEnv', '', '',
                 error_code, error_description, additional_info],
            ],
        ]
        self.assertEqual(result, expected_result)

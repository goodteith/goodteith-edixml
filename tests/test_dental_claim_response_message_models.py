#!/usr/bin/env python3
# vim:fileencoding=utf-8

import datetime
import logging
import random
import unittest

from lxml.builder import E

from edixml.models.address import (
    Address,
    AddressLine,
    AddressLines,
    PostCode,
    TelephoneNo,
)
from edixml.models.dental_claim import (
    ClaimReferenceResponse,
    ClaimResponseDetails,
    ClaimValidationFlag,
    ClaimedFormDetails,
    ClaimingOrganisationDetails,
    DateOfAcceptance,
    DentalClaimResponse,
    ErrorAdditionalInfo,
    ErrorCode,
    ErrorDescription,
    Errors,
    FormType,
    FormVersion,
    PracticeClaimReference,
    Source,
    SubmissionClaimCount,
)
from edixml.models.organisation import (
    IdValue,
    OrganisationId,
    OrganisationName,
    OrganisationType,
)
from edixml.models.person import (
    AlternativeHcpId,
    BirthFamilyName,
    ClaimingPractitionerDetails,
    FamilyName,
    GivenName,
    HcpId,
    HcpName,
    ProfCode,
    ProfCodeScheme,
    ProfType,
    StructuredName,
)
from .utils import etrees_equal, LeafNodeMixin, NUM_CHARS, ALPHANUM_CHARS

SCHEMA_VERSION = "1.0"

# Disable logging so it doesn't clutter up from tests
logging.disable(logging.CRITICAL)


class DentalClaimResponseTest(unittest.TestCase):
    def setUp(self):
        self.model = DentalClaimResponse()

    def test_class_has_node_name(self):
        self.assertEqual('DentalClaimResponse', self.model.node_name)

    def test_to_etree_produces_correct_tree(self):
        form_type = 'GP17-1'
        form_version = '0601'
        source = 'PMS'
        id_value = '12345'
        org_name = 'blahblah'
        org_type = 'dentalpractice'
        addr_line_1 = 'flapple'
        addr_line_2 = 'road'
        post_code = 'AB67 4CD'
        telephone_number = '01234567890'
        family_name = 'flibble'
        given_name = 'wibble'
        prof_type = 'Dentist'
        prof_code = 'abcde'
        prof_code_scheme = 'lah hla la'
        practice_claim_reference = 'abcdef'
        submission_claim_count = 4
        date_of_acceptance = datetime.date.today()
        claim_validation_flag = False
        error_code = '7654321'
        error_description = 'This is another nice thing'
        error_additional_info = 'sidewinder pro'
        errors = Errors(
            ErrorCode(error_code), ErrorDescription(error_description),
            ErrorAdditionalInfo(error_additional_info)
        )
        claim_response_details = ClaimResponseDetails(
            ClaimValidationFlag(claim_validation_flag), errors)
        claim_reference_details = ClaimReferenceResponse(
            PracticeClaimReference(practice_claim_reference),
            SubmissionClaimCount(submission_claim_count),
            DateOfAcceptance(date_of_acceptance))
        structured_name = StructuredName(
            FamilyName(family_name), GivenName(given_name))
        hcp_name = HcpName(structured_name)
        hcp_id = HcpId(
            ProfCode(prof_code), ProfCodeScheme(prof_code_scheme))
        claiming_practitioner_details = ClaimingPractitionerDetails(
            hcp_name, ProfType(prof_type), hcp_id)
        organisation_id = OrganisationId(IdValue(id_value))
        address_lines = AddressLines(
            [
                AddressLine(addr_line_1),
                AddressLine(addr_line_2),
            ]
        )
        address = Address(
            address_lines,
            PostCode(post_code),
            TelephoneNo(telephone_number))
        claiming_organisation_details = ClaimingOrganisationDetails(
            organisation_id,
            OrganisationName(org_name),
            OrganisationType(org_type), address)
        claimed_form_details = ClaimedFormDetails(
            FormType(form_type),
            FormVersion(form_version),
            Source(source))
        dental_claim_response = DentalClaimResponse(
            SCHEMA_VERSION, claimed_form_details,
            claiming_organisation_details, claiming_practitioner_details,
            claim_reference_details, claim_response_details)
        reference = E.DentalClaimResponse(
            E.ClaimedFormDetails(
                E.FormType(form_type),
                E.FormVersion(form_version),
                E.Source(source),
            ),
            E.ClaimingOrganisationDetails(
                E.OrganisationId(
                    E.IdValue(id_value),
                ),
                E.OrganisationName(org_name),
                E.OrganisationType(org_type),
                E.Address(
                    E.AddressLines(
                        E.AddressLine(addr_line_1),
                        E.AddressLine(addr_line_2),
                    ),
                    E.PostCode(post_code),
                    E.TelephoneNo(telephone_number),
                ),
            ),
            E.ClaimingPractitionerDetails(
                E.HcpName(
                    E.StructuredName(
                        E.FamilyName(family_name),
                        E.GivenName(given_name),
                    ),
                ),
                E.ProfType(prof_type),
                E.HcpId(
                    E.ProfCode(prof_code),
                    E.ProfCodeScheme(prof_code_scheme),
                ),
            ),
            E.ClaimReferenceDetails(
                E.PracticeClaimReference(practice_claim_reference),
                E.SubmissionClaimCount(str(submission_claim_count)),
                E.DateOfAcceptance(date_of_acceptance.strftime('%Y-%m-%d')),
            ),
            E.ClaimResponseDetails(
                E.ClaimValidationFlag(
                    '{}'.format(claim_validation_flag).lower()
                ),
                E.Errors(
                    E.ErrorCode(error_code),
                    E.ErrorDescription(error_description),
                    E.ErrorAdditionalInfo(error_additional_info)
                )
            ),
            SchemaVersion=SCHEMA_VERSION,
        )
        if not etrees_equal(reference, dental_claim_response.as_etree()):
            self.fail("{} did not produce the correct etree".format(
                "DentalClaimResponse"))

    def test_has_schemaversion_attribute(self):
        self.assertEqual(SCHEMA_VERSION, self.model.schema_version)

    def test_schemaversion_in_etree(self):
        form_type = 'GP17-1'
        form_version = '0601'
        source = 'PMS'
        id_value = '12345'
        org_name = 'blahblah'
        org_type = 'dentalpractice'
        addr_line_1 = 'flapple'
        addr_line_2 = 'road'
        post_code = 'AB67 4CD'
        telephone_number = '01234567890'
        family_name = 'flibble'
        given_name = 'wibble'
        prof_type = 'Dentist'
        prof_code = 'abcde'
        prof_code_scheme = 'lah hla la'
        practice_claim_reference = 'abcdef'
        submission_claim_count = 4
        date_of_acceptance = datetime.date.today()
        claim_validation_flag = False
        error_code = '7654321'
        error_description = 'This is another nice thing'
        error_additional_info = 'sidewinder pro'
        errors = Errors(
            ErrorCode(error_code), ErrorDescription(error_description),
            ErrorAdditionalInfo(error_additional_info)
        )
        claim_response_details = ClaimResponseDetails(
            ClaimValidationFlag(claim_validation_flag), errors)
        claim_reference_details = ClaimReferenceResponse(
            PracticeClaimReference(practice_claim_reference),
            SubmissionClaimCount(submission_claim_count),
            DateOfAcceptance(date_of_acceptance))
        structured_name = StructuredName(
            FamilyName(family_name), GivenName(given_name))
        hcp_name = HcpName(structured_name)
        hcp_id = HcpId(
            ProfCode(prof_code), ProfCodeScheme(prof_code_scheme))
        claiming_practitioner_details = ClaimingPractitionerDetails(
            hcp_name, ProfType(prof_type), hcp_id)
        organisation_id = OrganisationId(IdValue(id_value))
        address_lines = AddressLines(
            [
                AddressLine(addr_line_1),
                AddressLine(addr_line_2),
            ]
        )
        address = Address(
            address_lines,
            PostCode(post_code),
            TelephoneNo(telephone_number))
        claiming_organisation_details = ClaimingOrganisationDetails(
            organisation_id,
            OrganisationName(org_name),
            OrganisationType(org_type), address)
        claimed_form_details = ClaimedFormDetails(
            FormType(form_type),
            FormVersion(form_version),
            Source(source))
        model = DentalClaimResponse(
            SCHEMA_VERSION, claimed_form_details,
            claiming_organisation_details, claiming_practitioner_details,
            claim_reference_details, claim_response_details)
        self.assertIn('SchemaVersion', model.as_etree().attrib)

    def test_required_children_present(self):
        for member in [
            'claimed_form_details',
            'claiming_organisation_details',
            'claiming_practitioner_details',
            'claim_reference_details',
            'claim_response_details',
        ]:
            self.assertIn(member, self.model.required_children)

    def test_attributes_set_correctly_in_constructor(self):
        schema_version = "1.5"
        claimed_form_details = "jings"
        claiming_organisation_details = "crivvens"
        claiming_practitioner_details = "Help ma boab"
        claim_reference_details = "spam"
        claim_response_details = "ham"
        t = DentalClaimResponse(
            schema_version,
            claimed_form_details,
            claiming_organisation_details,
            claiming_practitioner_details,
            claim_reference_details,
            claim_response_details)

        self.assertEqual(schema_version, t.schema_version)
        self.assertEqual(claimed_form_details, t.claimed_form_details)
        self.assertEqual(
            claiming_organisation_details, t.claiming_organisation_details)
        self.assertEqual(
            claiming_practitioner_details, t.claiming_practitioner_details)
        self.assertEqual(claim_reference_details, t.claim_reference_details)
        self.assertEqual(claim_response_details, t.claim_response_details)

    def test_success_csv_generated_correctly(self):
        practice_claim_reference = '123456'
        submission_claim_count = 1
        date_of_acceptance = datetime.date.today()
        claim_validation_flag = True
        expected_data = [practice_claim_reference, claim_validation_flag]
        model = DentalClaimResponse(
            claim_reference_details=ClaimReferenceResponse(
                PracticeClaimReference(practice_claim_reference),
                SubmissionClaimCount(submission_claim_count),
                DateOfAcceptance(date_of_acceptance),
            ),
            claim_response_details=ClaimResponseDetails(
                ClaimValidationFlag(claim_validation_flag),
            ),
        )
        self.assertEqual(model.as_fm_suitable_list(), expected_data)

    def test_failed_csv_generated_correctly(self):
        practice_claim_reference = '654321'
        submission_claim_count = 2
        date_of_acceptance = datetime.date.today()
        claim_validation_flag = False
        error_code = '1234567'
        error_description = 'Things are all messed up'
        error_additional_info = 'Really stuffed up'
        expected_data = [
            practice_claim_reference,
            claim_validation_flag,
            error_code,
            error_description,
            error_additional_info
        ]
        model = DentalClaimResponse(
            claim_reference_details=ClaimReferenceResponse(
                PracticeClaimReference(practice_claim_reference),
                SubmissionClaimCount(submission_claim_count),
                DateOfAcceptance(date_of_acceptance),
            ),
            claim_response_details=ClaimResponseDetails(
                ClaimValidationFlag(claim_validation_flag),
                Errors(
                    ErrorCode(error_code),
                    ErrorDescription(error_description),
                    ErrorAdditionalInfo(error_additional_info),
                ),
            ),
        )
        self.assertEqual(model.as_fm_suitable_list(), expected_data)


class ClaimedFormDetailsTest(unittest.TestCase):
    def setUp(self):
        self.model = ClaimedFormDetails()

    def test_class_has_node_name(self):
        self.assertEqual('ClaimedFormDetails', self.model.node_name)

    def test_required_children_present(self):
        for member in ['form_type', 'form_version', 'source']:
            self.assertIn(member, self.model.required_children)

    def test_attributes_set_correctly_in_constructor(self):
        form_type = "GP17-1"
        form_version = "0613"
        source = "PMS"
        t = ClaimedFormDetails(form_type, form_version, source)
        self.assertEqual(form_type, t.form_type)
        self.assertEqual(form_version, t.form_version)
        self.assertEqual(source, t.source)

    def test_to_etree_produces_correct_tree(self):
        form_type = "GP17-1"
        form_version = "0613"
        source = "PMS"
        c = ClaimedFormDetails(
            FormType(form_type),
            FormVersion(form_version),
            Source(source))
        reference = E.ClaimedFormDetails(
            E.FormType(form_type),
            E.FormVersion(form_version),
            E.Source(source))

        if not etrees_equal(reference, c.as_etree()):
            self.fail("{} did not produce the correct etree".format(
                "ClaimedFormDetails"))


class FormTypeTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'FormType'
        self.model = FormType
        self.text = 'GP17-1'
        self.correct_values = [
            'GP17-1', 'GP17-O'
        ]
        self.incorrect_values = [
            1, 'abc', 1.5
        ]


class FormVersionTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'FormVersion'
        self.yaml_name = 'VersionType'
        self.model = FormVersion
        self.text = '0613'
        self.correct_values = [
            '0613'
        ]
        self.incorrect_values = [
            '01234567890'
        ]


class SourceTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'Source'
        self.yaml_name = 'ClaimFormSourceType'
        self.model = Source
        self.text = 'PMS'
        self.correct_values = [
            'PMS', 'WEB'
        ]
        self.incorrect_values = [
            'carrier pigeon', 'tcp-morse', 'smoke signals', 'BUG'
        ]


class ClaimingOrganisationDetailsTest(unittest.TestCase):
    def setUp(self):
        self.model = ClaimingOrganisationDetails()

    def test_class_has_node_name(self):
        self.assertEqual('ClaimingOrganisationDetails', self.model.node_name)

    def test_required_children_present(self):
        for member in [
                'organisation_id', 'organisation_name', 'organisation_type',
                'address']:
            self.assertIn(member, self.model.required_children)

    def test_attributes_set_correctly_in_constructor(self):
        o_id = '012345'
        o_name = 'Test practice'
        o_type = 'dentalpractice'
        o_address = '15 West Hollywood Way'
        c = ClaimingOrganisationDetails(o_id, o_name, o_type, o_address)
        self.assertEqual(o_id, c.organisation_id)
        self.assertEqual(o_name, c.organisation_name)
        self.assertEqual(o_type, c.organisation_type)
        self.assertEqual(o_address, c.address)

    def test_validators_included_in_parser(self):
        self.assertEqual(len(self.model.validators), 0)

    def test_to_etree_produces_correct_tree(self):
        id_value = '12345'
        org_name = 'Woopwoop'
        org_type = 'dentalpractice'
        addr_line_1 = 'vague'
        addr_line_2 = 'place'
        post_code = 'AB12 3CD'
        telephone_number = '01234567890'
        i = OrganisationId(IdValue(id_value))
        l = AddressLines([AddressLine(addr_line_1),
                         AddressLine(addr_line_2)])
        a = Address(
            l,
            PostCode(post_code),
            TelephoneNo(telephone_number))
        c = ClaimingOrganisationDetails(
            i,
            OrganisationName(org_name),
            OrganisationType(org_type), a)
        reference = E.ClaimingOrganisationDetails(
            E.OrganisationId(
                E.IdValue(id_value)
            ),
            E.OrganisationName(org_name),
            E.OrganisationType(org_type),
            E.Address(
                E.AddressLines(
                    E.AddressLine(addr_line_1),
                    E.AddressLine(addr_line_2),
                ),
                E.PostCode(post_code),
                E.TelephoneNo(telephone_number),
            )
        )
        if not etrees_equal(reference, c.as_etree()):
            self.fail("{} did not produce the correct etree".format(
                "ClaimingOrganisationDetails"))


class OrganisationIdTest(unittest.TestCase):
    def setUp(self):
        self.model = OrganisationId()

    def test_class_has_node_name(self):
        self.assertEqual('OrganisationId', self.model.node_name)

    def test_required_children_present(self):
        self.assertIn('id_value', self.model.required_children)

    def test_attributes_set_correctly_in_constructor(self):
        o_id_value = '0123456789'
        o = OrganisationId(o_id_value)
        self.assertEqual(o_id_value, o.id_value)

    def test_validators_included_in_parser(self):
        self.assertEqual(len(self.model.validators), 0)

    def test_to_etree_produces_correct_tree(self):
        id_value = ''.join([random.choice(NUM_CHARS) for i in range(10)])
        o = OrganisationId(IdValue(id_value))
        reference = E.OrganisationId(
            E.IdValue(id_value))
        if not etrees_equal(reference, o.as_etree()):
            self.fail("{} did not produce the correct etree".format(
                "OrganisationId"))


class IdValueTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'IdValue'
        self.model = IdValue
        self.text = '0123456789'
        self.correct_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(10)])
        ]
        self.incorrect_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(11)])
        ]


class OrganisationNameTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'OrganisationName'
        self.model = OrganisationName
        self.text = 'Woopwoop'
        self.correct_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(254)])
        ]
        self.incorrect_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(256)])
        ]


class OrganisationTypeTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'OrganisationType'
        self.model = OrganisationType
        self.text = 'dentalpractice'
        self.correct_values = [
            'pharmacy', 'optometrypractice', 'dentalpractice'
        ]
        self.incorrect_values = [
            'this', 'is', 'made45', 'up'
        ]


class AddressTest(unittest.TestCase):
    def setUp(self):
        self.model = Address()

    def test_class_has_node_name(self):
        self.assertEqual('Address', self.model.node_name)

    def test_required_children_present(self):
        for member in ['address_lines', 'post_code']:
            self.assertIn(member, self.model.required_children)

    def test_attributes_set_correctly_in_constructor(self):
        address_lines = AddressLines([AddressLine('1 Boulevard Way')])
        post_code = 'AB10 0FG'
        telephone_number = '01000 000000'
        a = Address(address_lines, post_code, telephone_number)
        self.assertEqual(address_lines, a.address_lines)
        self.assertEqual(post_code, a.post_code)
        self.assertEqual(telephone_number, a.telephone_number)

    def test_validators_included_in_parser(self):
        self.assertEqual(len(self.model.validators), 0)

    def test_to_etree_produces_correct_tree(self):
        addr_line_1 = '1 Hollywood Boulevard'
        addr_line_2 = 'Los Angeles'
        address_lines = AddressLines([
            AddressLine(addr_line_1), AddressLine(addr_line_2)])
        post_code = 'LN1 5WN'
        telephone_number = '55501 012345'
        a = Address(
            address_lines,
            PostCode(post_code),
            TelephoneNo(telephone_number))
        reference = E.Address(
            E.AddressLines(
                E.AddressLine(addr_line_1),
                E.AddressLine(addr_line_2),
            ),
            E.PostCode(post_code),
            E.TelephoneNo(telephone_number))

        if not etrees_equal(reference, a.as_etree()):
            self.fail("{} did not produce the correct etree".format(
                "Address"))


class PostCodeTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'PostCode'
        self.model = PostCode
        self.text = 'AB10 0FG'
        self.correct_values = [
            'HX4 9HH',
            'DD6 9LQ',
            'NR10 3EG',
            'N7 1AN',
            'HU11 4RU',
            'DY12 2EE',
            'PO40 9AS',
            'CV23 0HF',
            'SO20 8EB',
            'SN4 7SY',
            'KW3 6DA',
        ]
        self.incorrect_values = [
            '1234', 'A4501 1bd', 'jings foo', 'abd5 1pg'
        ]

    def test_postcode_auto_converted_to_upper(self):
        postcode = 'ab16 2gg'
        model = self.model(postcode)
        self.assertEqual(model.text, postcode.upper())


class TelephoneNoTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'TelephoneNo'
        self.model = TelephoneNo
        self.text = '01000 000000'
        self.correct_values = [
            ''.join([random.choice(NUM_CHARS) for i in range(20)])
        ]
        self.incorrect_values = [
            ''.join([random.choice(NUM_CHARS) for i in range(21)])
        ]


class AddressLinesTest(unittest.TestCase):
    def setUp(self):
        self.model = AddressLines()

    def test_class_has_node_name(self):
        self.assertEqual('AddressLines', self.model.node_name)

    def test_attributes_set_correctly_in_constructor(self):
        test_list = ['a', 'b', 'c']
        a = AddressLines(test_list)
        self.assertEqual(a.address_lines, test_list)

    def test_add_to_address_line(self):
        a = AddressLines()
        addr_line_1 = AddressLine('15 Bootle Bay')
        addr_line_2 = AddressLine('Twickenweed')
        addr_line_3 = AddressLine('Hooplesloop')

        a.add_address_line(addr_line_1)
        self.assertEqual(len(a.address_lines), 1)
        self.assertEqual(a.address_lines, [addr_line_1])
        a.add_address_line(addr_line_2)
        self.assertEqual(len(a.address_lines), 2)
        self.assertEqual(a.address_lines, [addr_line_1, addr_line_2])
        a.add_address_line(addr_line_3)
        self.assertEqual(len(a.address_lines), 3)
        self.assertEqual(a.address_lines, [addr_line_1, addr_line_2,
                                           addr_line_3])

    def test_to_etree_produces_correct_tree(self):
        addr_line_1 = '15 Bootle Bay'
        addr_line_2 = 'Twickenweed'
        addr_line_3 = 'Hooplesloop'
        a = AddressLines(
            [
                AddressLine(addr_line_1),
                AddressLine(addr_line_2),
                AddressLine(addr_line_3)
            ]
        )
        reference = E.AddressLines(
            E.AddressLine(addr_line_1),
            E.AddressLine(addr_line_2),
            E.AddressLine(addr_line_3),
        )
        if not etrees_equal(reference, a.as_etree()):
            self.fail("{} did not produce the correct etree".format(
                "AddressLines"))


class AddressLineTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'AddressLine'
        self.model = AddressLine
        self.text = '1 Bouleway Place'
        self.correct_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(35)])
        ]
        self.incorrect_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(36)])
        ]


class ClaimingPractitionerDetailsTest(unittest.TestCase):
    def setUp(self):
        self.model = ClaimingPractitionerDetails()

    def test_class_has_node_name(self):
        self.assertEqual('ClaimingPractitionerDetails', self.model.node_name)

    def test_required_children_present(self):
        for member in ['hcp_name', 'prof_type', 'hcp_id']:
            self.assertIn(member, self.model.required_children)

    def test_attributes_set_correctly_in_constructor(self):
        hcp_name = 'foo'
        prof_type = 'Dentist'
        hcp_id = 'spam'
        c = ClaimingPractitionerDetails(hcp_name, prof_type, hcp_id)
        self.assertEqual(hcp_name, c.hcp_name)
        self.assertEqual(prof_type, c.prof_type)
        self.assertEqual(hcp_id, c.hcp_id)

    def test_validators_included_in_parser(self):
        self.assertEqual(len(self.model.validators), 0)

    def test_to_etree_produces_correct_tree(self):
        family_name = 'flibble'
        given_name = 'wibble'
        prof_type = 'Dentist'
        prof_code = '123456'
        prof_code_scheme = 'This is my list number or sommink'
        s = StructuredName(
            FamilyName(family_name), GivenName(given_name))
        h = HcpName(s)
        i = HcpId(
            ProfCode(prof_code), ProfCodeScheme(prof_code_scheme))
        c = ClaimingPractitionerDetails(h, ProfType(prof_type), i)
        reference = E.ClaimingPractitionerDetails(
            E.HcpName(
                E.StructuredName(
                    E.FamilyName(family_name),
                    E.GivenName(given_name),
                ),
            ),
            E.ProfType(prof_type),
            E.HcpId(
                E.ProfCode(prof_code),
                E.ProfCodeScheme(prof_code_scheme),
            )
        )
        if not etrees_equal(reference, c.as_etree()):
            self.fail("{} did not produce the correct etree".format(
                "ClaimingPractitionerDetails"))

    def test_to_etree_produces_correct_tree_extended(self):
        family_name = 'flibble'
        given_name = 'wibble'
        prof_type = 'Dentist'
        prof_code = '123456'
        prof_code_scheme = 'This is my list number or sommink'
        alt_prof_code = '765342'
        alt_prof_code_scheme = "I've been so sad"
        s = StructuredName(
            FamilyName(family_name), GivenName(given_name))
        h = HcpName(s)
        i = HcpId(
            ProfCode(prof_code), ProfCodeScheme(prof_code_scheme))
        a = AlternativeHcpId(
            ProfCode(alt_prof_code), ProfCodeScheme(alt_prof_code_scheme))
        c = ClaimingPractitionerDetails(h, ProfType(prof_type), i, a)
        reference = E.ClaimingPractitionerDetails(
            E.HcpName(
                E.StructuredName(
                    E.FamilyName(family_name),
                    E.GivenName(given_name),
                ),
            ),
            E.ProfType(prof_type),
            E.HcpId(
                E.ProfCode(prof_code),
                E.ProfCodeScheme(prof_code_scheme),
            ),
            E.AlternativeHcpId(
                E.ProfCode(alt_prof_code),
                E.ProfCodeScheme(alt_prof_code_scheme),
            )
        )
        if not etrees_equal(reference, c.as_etree()):
            self.fail("{} did not produce the correct etree".format(
                "ClaimingPractitionerDetails (extended)"))


class HcpNameTest(unittest.TestCase):
    def setUp(self):
        self.model = HcpName()

    def test_class_has_node_name(self):
        self.assertEqual('HcpName', self.model.node_name)

    def test_required_children_present(self):
        self.assertIn('structured_name', self.model.required_children)

    def test_to_etree_produces_correct_tree(self):
        family_name = 'flibble'
        given_name = 'wibble'
        s = StructuredName(
            FamilyName(family_name), GivenName(given_name))
        h = HcpName(s)
        reference = E.HcpName(
            E.StructuredName(
                E.FamilyName(family_name),
                E.GivenName(given_name),
            )
        )
        if not etrees_equal(reference, h.as_etree()):
            self.fail("{} did not produce the correct etree".format(
                "HcpId"))


class StructuredNameTest(unittest.TestCase):
    def setUp(self):
        self.model = StructuredName()

    def test_class_has_node_name(self):
        self.assertEqual('StructuredName', self.model.node_name)

    def test_required_children_present(self):
        for member in [
                'family_name', 'given_name']:
            self.assertIn(member, self.model.required_children)

    def test_attributes_set_correctly_in_constructor(self):
        family_name = 'Sinatra'
        given_name = 'Frank'
        s = StructuredName(family_name, given_name)
        self.assertEqual(family_name, s.family_name)
        self.assertEqual(given_name, s.given_name)

    def validators_included_in_parser(self):
        self.assertEqual(len(self.model.validators), 0)

    def test_to_etree_produces_correct_tree(self):
        family_name = 'Ham'
        given_name = 'Spam'
        s = StructuredName(
            FamilyName(family_name), GivenName(given_name))
        reference = E.StructuredName(
            E.FamilyName(family_name),
            E.GivenName(given_name),
        )
        if not etrees_equal(reference, s.as_etree()):
            self.fail("{} did not produce the correct etree".format(
                "StructuredName"))

    def test_to_etree_produces_correct_tree_extended(self):
        family_name = 'Ham'
        given_name = 'Spam'
        birth_family_name = 'Foo'
        s = StructuredName(
            FamilyName(family_name), GivenName(given_name),
            BirthFamilyName(birth_family_name))
        reference = E.StructuredName(
            E.FamilyName(family_name),
            E.BirthFamilyName(birth_family_name),
            E.GivenName(given_name),
        )
        if not etrees_equal(reference, s.as_etree()):
            self.fail("{} did not produce the correct etree".format(
                "StructuredName (extended)"))


class FamilyNameTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'FamilyName'
        self.model = FamilyName
        self.text = 'Cole'
        self.correct_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(1)]),
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(35)]),
        ]
        self.incorrect_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(36)])
        ]


class GivenNameTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'GivenName'
        self.model = GivenName
        self.text = 'Frank'
        self.correct_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(1)]),
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(35)]),
        ]
        self.incorrect_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(36)])
        ]


class BirthFamilyNameTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'BirthFamilyName'
        self.model = BirthFamilyName
        self.text = 'Sinatra'
        self.correct_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(1)]),
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(35)]),
        ]
        self.incorrect_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(36)])
        ]


class ProfTypeTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'ProfType'
        self.model = ProfType
        self.text = 'Dentist'
        self.correct_values = [
            'Dentist', 'DentalHealthCareProfessional', 'Orthodontist'
        ]
        self.incorrect_values = [
            'Dantist', 'DuntalHealthCareProfessional', 'Orthodantist'
        ]
        self.yaml = 'ProfType:   Dentist'


class HcpIdTest(unittest.TestCase):
    def setUp(self):
        self.model = HcpId()

    def test_class_has_node_name(self):
        self.assertEqual('HcpId', self.model.node_name)

    def test_required_children_present(self):
        for member in ['prof_code', 'prof_code_scheme']:
            self.assertIn(member, self.model.required_children)

    def test_attributes_set_correctly_in_constructor(self):
        prof_code = '1234567'
        prof_code_scheme = 'this is a list number'
        a = HcpId(prof_code, prof_code_scheme)
        self.assertEqual(prof_code, a.prof_code)
        self.assertEqual(prof_code_scheme, a.prof_code_scheme)

    def test_to_etree_produces_correct_tree(self):
        prof_code = '0715421'
        prof_code_scheme = 'this is the story'
        a = HcpId(
            ProfCode(prof_code), ProfCodeScheme(prof_code_scheme))
        reference = E.HcpId(
            E.ProfCode(prof_code),
            E.ProfCodeScheme(prof_code_scheme),
        )
        if not etrees_equal(reference, a.as_etree()):
            self.fail("{} did not produce the correct etree".format(
                "HcpId"))


class AlternativeHcpIdTest(unittest.TestCase):
    def setUp(self):
        self.model = AlternativeHcpId()

    def test_class_has_node_name(self):
        self.assertEqual('AlternativeHcpId', self.model.node_name)

    def test_required_children_present(self):
        for member in ['prof_code', 'prof_code_scheme']:
            self.assertIn(member, self.model.required_children)

    def test_attributes_set_correctly_in_constructor(self):
        prof_code = '1234567'
        prof_code_scheme = 'this is a list number'
        a = AlternativeHcpId(prof_code, prof_code_scheme)
        self.assertEqual(prof_code, a.prof_code)
        self.assertEqual(prof_code_scheme, a.prof_code_scheme)

    def test_to_etree_produces_correct_tree(self):
        prof_code = '0715421'
        prof_code_scheme = 'this is the story'
        a = AlternativeHcpId(
            ProfCode(prof_code), ProfCodeScheme(prof_code_scheme))
        reference = E.AlternativeHcpId(
            E.ProfCode(prof_code),
            E.ProfCodeScheme(prof_code_scheme),
        )
        if not etrees_equal(reference, a.as_etree()):
            self.fail("{} did not produce the correct etree".format(
                "AlternativeHcpId"))


class ProfCodeTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'ProfCode'
        self.model = ProfCode
        self.text = '1234567'
        self.correct_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(1)]),
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(20)]),
        ]
        self.incorrect_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(21)])
        ]


class ProfCodeSchemeTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'ProfCodeScheme'
        self.model = ProfCodeScheme
        self.text = 'this is a list number'
        self.correct_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(1)]),
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(255)]),
        ]
        self.incorrect_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(256)])
        ]


class ClaimReferenceResponseTest(unittest.TestCase):
    def setUp(self):
        self.model = ClaimReferenceResponse()

    def test_class_has_node_name(self):
        self.assertEqual('ClaimReferenceDetails', self.model.node_name)

    def test_required_children_present(self):
        for member in [
                'practice_claim_reference',
                'submission_claim_count',
                'date_of_acceptance']:
            self.assertIn(member, self.model.required_children)

    def test_attributes_set_correctly_in_constructor(self):
        practice_claim_reference = 'abcdef'
        submission_claim_count = 5
        date_of_acceptance = datetime.date.today()
        c = ClaimReferenceResponse(
            practice_claim_reference,
            submission_claim_count,
            date_of_acceptance)
        self.assertEqual(practice_claim_reference, c.practice_claim_reference)
        self.assertEqual(submission_claim_count, c.submission_claim_count)
        self.assertEqual(date_of_acceptance, c.date_of_acceptance)

    def test_to_etree_produces_correct_tree(self):
        practice_claim_reference = 'abcdef'
        submission_claim_count = 4
        date_of_acceptance = datetime.date.today()
        c = ClaimReferenceResponse(
            PracticeClaimReference(practice_claim_reference),
            SubmissionClaimCount(submission_claim_count),
            DateOfAcceptance(date_of_acceptance))
        reference = E.ClaimReferenceDetails(
            E.PracticeClaimReference(practice_claim_reference),
            E.SubmissionClaimCount(str(submission_claim_count)),
            E.DateOfAcceptance(date_of_acceptance.strftime('%Y-%m-%d')),
        )
        if not etrees_equal(reference, c.as_etree()):
            self.fail("{} did not produce the correct etree".format(
                "ClaimReferenceResponse"))


class PracticeClaimReferenceTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'PracticeClaimReference'
        self.model = PracticeClaimReference
        self.text = 'akjgsd'
        self.correct_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(6)])
        ]
        self.incorrect_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(5)]),
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(7)]),
        ]


class SubmissionClaimCountTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'SubmissionClaimCount'
        self.model = SubmissionClaimCount
        self.text = 4
        self.correct_values = range(10)
        self.incorrect_values = [-1, 10]


class DateOfAcceptanceTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'DateOfAcceptance'
        self.model = DateOfAcceptance
        self.text = datetime.date.today()
        self.correct_values = [
            datetime.date(2000, 1, 1),
            datetime.date(2017, 12, 15)
        ]
        self.incorrect_values = ['abc', 123, '01-05-2017']


class ClaimResponseDetailsTest(unittest.TestCase):
    def setUp(self):
        self.model = ClaimResponseDetails()

    def test_class_has_node_name(self):
        self.assertEqual('ClaimResponseDetails', self.model.node_name)

    def test_required_children_present(self):
        self.assertIn('claim_validation_flag', self.model.required_children)

    def test_attributes_set_correctly_in_constructor(self):
        claim_validation_flag = True
        c = ClaimResponseDetails(claim_validation_flag)
        self.assertEqual(claim_validation_flag, c.claim_validation_flag)

    def test_errors_is_optional(self):
        c = ClaimResponseDetails(True)
        self.assertTrue(c.model_is_valid())

    def test_errors_included_in_children(self):
        self.assertEqual(len(self.model.children), 2)

    def test_to_etree_produces_correct_tree(self):
        claim_validation_flag = True
        c = ClaimResponseDetails(ClaimValidationFlag(claim_validation_flag))
        reference = E.ClaimResponseDetails(
            E.ClaimValidationFlag('{}'.format(claim_validation_flag).lower())
        )
        if not etrees_equal(reference, c.as_etree()):
            self.fail("{} did not produce the correct etree".format(
                "ClaimResponseDetails"))

    def test_to_etree_produces_correct_tree_extended(self):
        claim_validation_flag = False
        error_code = '7654321'
        error_description = 'This is another nice thing'
        error_additional_info = 'sidewinder pro'
        e = Errors(
            ErrorCode(error_code), ErrorDescription(error_description),
            ErrorAdditionalInfo(error_additional_info)
        )
        c = ClaimResponseDetails(ClaimValidationFlag(claim_validation_flag), e)

        reference = E.ClaimResponseDetails(
            E.ClaimValidationFlag('{}'.format(claim_validation_flag).lower()),
            E.Errors(
                E.ErrorCode(error_code),
                E.ErrorDescription(error_description),
                E.ErrorAdditionalInfo(error_additional_info)
            )
        )
        if not etrees_equal(reference, c.as_etree()):
            self.fail("{} did not produce the correct etree".format(
                "ClaimResponseDetails"))


class ClaimValidationFlagTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'ClaimValidationFlag'
        self.model = ClaimValidationFlag
        self.text = True
        self.correct_values = [True, False]
        self.incorrect_values = ['abc', 123, 0x56]


class ErrorsTest(unittest.TestCase):
    def setUp(self):
        self.model = Errors()

    def test_class_has_node_name(self):
        self.assertEqual('Errors', self.model.node_name)

    def test_required_children_present(self):
        for member in [
                'error_code', 'error_description']:
            self.assertIn(member, self.model.required_children)

    def test_attributes_set_correctly_in_constructor(self):
        error_code = 'abcdefg'
        error_description = 'a strange error'
        e = Errors(error_code, error_description)
        self.assertEqual(error_code, e.error_code)
        self.assertEqual(error_description, e.error_description)

    def test_optional_attributes(self):
        error_additional_info = 'flipple floo'
        e = Errors(
            error_additional_info=error_additional_info)
        self.assertEqual(
            error_additional_info, e.error_additional_info)

    def test_additional_information_is_optional(self):
        e = Errors('1234567', 'abcdre')
        self.assertTrue(e.model_is_valid())

    def test_to_etree_produces_correct_tree(self):
        error_code = '1234567'
        error_description = 'This is a nice thing'
        e = Errors(ErrorCode(error_code), ErrorDescription(error_description))
        reference = E.Errors(
            E.ErrorCode(error_code),
            E.ErrorDescription(error_description),
        )
        if not etrees_equal(reference, e.as_etree()):
            self.fail("{} did not produce the correct etree".format(
                "Errors"))

    def test_to_etree_produces_correct_tree_extended(self):
        error_code = '7654321'
        error_description = 'This is another nice thing'
        error_additional_info = 'sidewinder pro'
        e = Errors(
            ErrorCode(error_code), ErrorDescription(error_description),
            ErrorAdditionalInfo(error_additional_info))
        reference = E.Errors(
            E.ErrorCode(error_code),
            E.ErrorDescription(error_description),
            E.ErrorAdditionalInfo(error_additional_info)
        )

        if not etrees_equal(reference, e.as_etree()):
            self.fail("{} did not produce the correct etree".format(
                "Errors (extended)"))


class ErrorDescriptionTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'ErrorDescription'
        self.model = ErrorDescription
        self.text = 'a strange error'
        self.correct_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(1)]),
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(1999)]),
        ]
        self.incorrect_values = [
            '',
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(2000)]),
        ]


class ErrorCodeTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'ErrorCode'
        self.model = ErrorCode
        self.text = '1234567'
        self.correct_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(7)])
        ]
        self.incorrect_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(6)]),
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(8)]),
        ]


class ErrorAdditionalInfoTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'ErrorAdditionalInfo'
        self.model = ErrorAdditionalInfo
        self.text = 'what is going on here then?'
        self.correct_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(1)]),
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(1999)]),
        ]
        self.incorrect_values = [
            '',
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(2000)]),
        ]

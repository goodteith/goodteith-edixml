#!/usr/bin/env python3
# vim:fileencoding=utf-8

import glob
import unittest


def create_test_suite():
    test_file_strings = glob.glob('tests/test_*.py')
    module_strings = ['tests.' + s[6:len(s)-3] for s in test_file_strings]
    suites = [
        unittest.defaultTestLoader.loadTestsFromName(name)
        for name in module_strings
    ]
    testSuite = unittest.TestSuite(suites)
    return testSuite

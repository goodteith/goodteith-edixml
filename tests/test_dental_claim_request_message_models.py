#!/usr/bin/env python3
# vim:fileencoding=utf-8


import datetime
import logging
import random
import string
import unittest

from lxml.builder import E

from edixml.exceptions.base import XmlValidationError
from edixml.models.address import (
    Address,
    AddressLine,
    AddressLines,
    PostCode,
    TelephoneNo,
)
from edixml.models.dental_claim import (
    AmountClaimed,
    AnnotationCode,
    AnnotationCodeDetails,
    AnnotationDescription,
    BenefitCategoryCode,
    BenefitCategoryDescription,
    BenefitCategoryDetails,
    BenefitRecipientDateOfBirth,
    BenefitRecipientName,
    BenefitRecipientNiNumber,
    BpeDetails,
    ClaimReferenceDetails,
    CompletedTreatmentDetails,
    ContinuationCaseDetails,
    ContinuationPartNumber,
    CurFlag,
    CurObservations,
    DateAuthorised,
    DateOfAcceptance,
    DateOfApproval,
    DateOfCompletion,
    Declaration,
    DeclarationOnAcceptance,
    DeclarationOnCompletion,
    DentalChart,
    DentalChartDetails,
    DentalClaimRequest,
    DentistDeclaration,
    DentistsDeclarationDetails,
    EvidenceNotProduced,
    ExemptionDetails,
    ExemptionOrRemissionStatusChanged,
    ExemptionOrRemissionStatusDetails,
    ExemptionOrRemissionStatusOnAcceptanceDetails,
    ExternalMouthTrauma,
    GdsClaim,
    Hc2CertificateNumber,
    Hc3CertificateNumber,
    HcCertificateDetails,
    LowerAnterior,
    LowerLeft,
    LowerRight,
    MaterialCode,
    MaterialDescription,
    MaterialDetails,
    ModelsAvailable,
    NoRadiographsRemarks,
    NonGdsClaim,
    ObservationsDetails,
    ObservationsText,
    Orthodontic,
    PatientCharge,
    PatientDeclaration,
    PatientOrRepresentativeCode,
    PatientOrRepresentativeDescription,
    PatientOrRepresentativeDetails,
    PatientOrRepresentativeSignature,
    PatientOrRepresentativeSignatureDate,
    PatientRefusedTreatment,
    PatientRefusedTreatmentObservations,
    PaymentClaimDetails,
    Pftr,
    PftrFeeRequired,
    PftrFeeRequiredObservations,
    PmsVersion,
    PracticeClaimReference,
    PractitionerDetails,
    PractitionerOrganisationDetails,
    PractitionerPinNumber,
    PreviousCaseId,
    PriorApprovalDetails,
    PriorApprovalReference,
    RadiographsAvailable,
    ReferralReasonCode,
    ReferralReasonDescription,
    ReferralReasonDetails,
    RepresentativeAddress,
    RepresentativeName,
    SpecialNeeds,
    SubmissionClaimCount,
    SupernumeraryFlag,
    Teeth,
    ToothNotation,
    ToothSurfaceCode,
    ToothSurfaceDescription,
    ToothSurfaceDetails,
    Treatment,
    TreatmentAmountDetails,
    TreatmentArrangementsDetails,
    TreatmentCode,
    TreatmentDateDetails,
    TreatmentDetails,
    TreatmentQualifier,
    TreatmentTypeDetails,
    TreatmentValue,
    TypeOfClaimCode,
    TypeOfClaimDescription,
    TypeOfClaimDetails,
    UpperAnterior,
    UpperLeft,
    UpperRight,
)
from edixml.models.organisation import (
    IdValue,
    OrganisationId,
    OrganisationName,
    OrganisationType,
)
from edixml.models.patient import (
    ChiNumber,
    PatientBirthFamilyName,
    PatientDetails,
    PatientDetailsDentalStructure,
    PatientDetailsRequestCheck,
    PatientDetailsRequestDate,
    PatientDetailsRequestFlag,
    PatientDetailsResponseReference,
    PatientFamilyName,
    PatientGivenName,
    PatientName,
)

from edixml.models.person import (
    BirthFamilyName,
    DateOfBirth,
    FamilyName,
    GivenName,
    HcpId,
    HcpName,
    Name,
    ProfCode,
    ProfCodeScheme,
    ProfType,
    Sex,
    StructuredName,
    Title,
)
from edixml import utils

from .utils import (
    ComplexNodeMixin, LeafNodeMixin, NUM_CHARS, ALPHANUM_CHARS, ALPHA_CHARS
)


# Disable logging so it doesn't clutter up from tests
logging.disable(logging.CRITICAL)


class ClaimReferenceDetailsTest(ComplexNodeMixin, unittest.TestCase):
    def setUp(self):
        self.model = ClaimReferenceDetails
        self.node_name = 'ClaimReferenceDetails'
        self.required_children = [
            'practice_claim_reference',
            'submission_claim_count',
            'practitioner_pin_number',
            'date_authorised',
            'pms_version'
        ]
        self.test_model = self.model(
            PracticeClaimReference('123456'),
            SubmissionClaimCount(5),
            PractitionerPinNumber('012345'),
            DateAuthorised(datetime.date.today()),
            PmsVersion('abcde'))
        self.reference = E.ClaimReferenceDetails(
            E.PracticeClaimReference('123456'),
            E.SubmissionClaimCount('5'),
            E.PractitionerPinNumber('012345'),
            E.DateAuthorised(datetime.date.today().strftime('%Y-%m-%d')),
            E.PMSVersion('abcde'))


class PractitionerPinNumberTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'PractitionerPinNumber'
        self.model = PractitionerPinNumber
        self.text = '000000'
        self.correct_values = [
            ''.join([random.choice(NUM_CHARS) for i in range(6)])
        ]
        self.incorrect_values = [
            ''.join([random.choice(NUM_CHARS) for i in range(5)]),
            ''.join([random.choice(NUM_CHARS) for i in range(7)])
        ]


class DateAuthorisedTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'DateAuthorised'
        self.model = DateAuthorised
        self.text = datetime.date.today()
        self.correct_values = [
            datetime.date(2000, 5, 12),
            datetime.date(3004, 6, 1),
        ]
        self.incorrect_values = [
            'now',
            '2017/04/01',
        ]


class PmsVersionTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'PMSVersion'
        self.model = PmsVersion
        self.text = 'abcde'
        self.correct_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(1)]),
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(50)]),
        ]
        self.incorrect_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(51)]),
        ]


class PractitionerOrganisationDetailsTest(ComplexNodeMixin, unittest.TestCase):
    def setUp(self):
        self.model = PractitionerOrganisationDetails
        self.node_name = 'PractitionerOrganisationDetails'
        self.required_children = [
            'organisation_id',
            'organisation_name',
            'organisation_type',
            'address',
        ]
        id_value = '12345'
        org_name = 'Woopwoop'
        org_type = 'dentalpractice'
        addr_line_1 = 'vague'
        addr_line_2 = 'place'
        post_code = 'AB12 3CD'
        telephone_number = '01234567890'
        self.test_model = self.model(
            OrganisationId(IdValue(id_value)),
            OrganisationName(org_name),
            OrganisationType(org_type),
            Address(
                AddressLines([
                    AddressLine(addr_line_1),
                    AddressLine(addr_line_2),
                ]),
                PostCode(post_code),
                TelephoneNo(telephone_number))
        )
        self.reference = E.PractitionerOrganisationDetails(
            E.OrganisationId(
                E.IdValue(id_value)
            ),
            E.OrganisationName(org_name),
            E.OrganisationType(org_type),
            E.Address(
                E.AddressLines(
                    E.AddressLine(addr_line_1),
                    E.AddressLine(addr_line_2),
                ),
                E.PostCode(post_code),
                E.TelephoneNo(telephone_number),
            )
        )


class PractitionerDetailsTest(ComplexNodeMixin, unittest.TestCase):
    def setUp(self):
        self.model = PractitionerDetails
        self.node_name = 'PractitionerDetails'
        self.required_children = [
            'hcp_name',
            'prof_type',
            'hcp_id'
        ]
        family_name = 'flibble'
        given_name = 'wibble'
        prof_type = 'Dentist'
        prof_code = '123456'
        prof_code_scheme = 'This is my list number or sommink'
        self.test_model = self.model(
            HcpName(
                StructuredName(
                    FamilyName(family_name),
                    GivenName(given_name),
                ),
            ),
            ProfType(prof_type),
            HcpId(
                ProfCode(prof_code),
                ProfCodeScheme(prof_code_scheme),
            )
        )
        self.reference = E.PractitionerDetails(
            E.HcpName(
                E.StructuredName(
                    E.FamilyName(family_name),
                    E.GivenName(given_name),
                ),
            ),
            E.ProfType(prof_type),
            E.HcpId(
                E.ProfCode(prof_code),
                E.ProfCodeScheme(prof_code_scheme),
            )
        )


class ChiNumberTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'CHINumber'
        self.model = ChiNumber
        self.text = '3000000000'
        self.correct_values = [
            '0' + ''.join([random.choice(NUM_CHARS) for i in range(9)]),
            '1' + ''.join([random.choice(NUM_CHARS) for i in range(9)]),
            '2' + ''.join([random.choice(NUM_CHARS) for i in range(9)]),
            '3' + ''.join([random.choice(NUM_CHARS) for i in range(9)]),
        ]
        self.incorrect_values = [
            '4' + ''.join([random.choice(NUM_CHARS) for i in range(9)]),
            ''.join([random.choice(NUM_CHARS) for i in range(9)]),
            ''.join([random.choice(NUM_CHARS) for i in range(11)]),
        ]


class NameTest(ComplexNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'Name'
        self.model = Name
        self.required_children = [
            'family_name', 'given_name'
        ]
        family_name = 'Jenkins'
        given_name = 'Joshua'
        self.test_model = self.model(
            PatientFamilyName(family_name),
            PatientGivenName(given_name),
        )
        self.reference = E.Name(
            E.FamilyName(family_name),
            E.GivenName(given_name),
        )

    def test_optional_creates_correct_tree(self):
        family_name = 'LangKent'
        given_name = 'Lana'
        birth_family_name = 'Lang'
        self.test_model = self.model(
            PatientFamilyName(family_name),
            PatientGivenName(given_name),
            PatientBirthFamilyName(birth_family_name),
        )
        self.reference = E.Name(
            E.FamilyName(family_name),
            E.BirthFamilyName(birth_family_name),
            E.GivenName(given_name),
        )
        super(NameTest,
              self).test_to_etree_produces_correct_tree()


class PatientDetailsRequestFlagTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'PatientDetailsRequestFlag'
        self.model = PatientDetailsRequestFlag
        self.text = True
        self.correct_values = [
            True, False
        ]
        self.incorrect_values = [
            'a', 1, 0, 9
        ]


class PatientDetailsResponseReferenceTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'PatientDetailsResponseReference'
        self.model = PatientDetailsResponseReference
        self.text = 'aaaaaaaaaa'
        self.correct_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(10)]),
        ]
        self.incorrect_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(9)]),
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(11)]),
        ]


class PatientDetailsRequestDateTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'PatientDetailsRequestDate'
        self.model = PatientDetailsRequestDate
        self.text = datetime.date.today()
        self.correct_values = [
            datetime.date(2015, 12, 10),
        ]
        self.incorrect_values = [
            'now',
            '2015/12/10',
        ]


class DateOfBirthTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'DateOfBirth'
        self.model = DateOfBirth
        self.text = datetime.date(2000, 1, 1)
        self.correct_values = [
            datetime.date(2015, 12, 10),
        ]
        self.incorrect_values = [
            'now',
            '2015/12/10',
        ]


class SexTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'Sex'
        self.model = Sex
        self.text = 'f'
        self.correct_values = [
            'i', 'u', 'm', 'f'
        ]
        self.incorrect_values = [
            'b', 'a', 1
        ]


class PatientDetailsDentalStructureTest(ComplexNodeMixin, unittest.TestCase):
    def setUp(self):
        self.model = PatientDetailsDentalStructure
        self.node_name = 'PatientDetails'
        self.required_children = [
            'patient_name',
            'address',
            'date_of_birth',
            'sex',
        ]
        family_name = 'spam'
        given_name = 'ham'
        addr_line_1 = '15 Bootle Bay'
        post_code = 'LN1 5WN'
        date_of_birth = datetime.date(1980, 1, 1)
        sex = 'f'
        self.test_model = self.model(
            patient_name=PatientName(
                PatientFamilyName(family_name),
                PatientGivenName(given_name),
            ),
            address=Address(
                AddressLines(
                    AddressLine(addr_line_1),
                ),
                PostCode(post_code),
            ),
            date_of_birth=DateOfBirth(date_of_birth),
            sex=Sex(sex),
        )
        self.reference = E.PatientDetails(
            E.Name(
                E.FamilyName(family_name),
                E.GivenName(given_name),
            ),
            E.Address(
                E.AddressLines(
                    E.AddressLine(addr_line_1),
                ),
                E.PostCode(post_code),
            ),
            E.DateOfBirth(utils.get_string_repr(date_of_birth)),
            E.Sex(sex),
        )

    def test_optional_creates_correct_etree(self):
        chi_number = '0111111111'
        family_name = 'spam'
        given_name = 'ham'
        addr_line_1 = '15 Bootle Bay'
        post_code = 'LN1 5WN'
        date_of_birth = datetime.date(1980, 1, 1)
        sex = 'f'
        self.test_model = self.model(
            Name(
                PatientFamilyName(family_name),
                PatientGivenName(given_name),
            ),
            Address(
                AddressLines(
                    AddressLine(addr_line_1),
                ),
                PostCode(post_code),
            ),
            DateOfBirth(date_of_birth),
            Sex(sex),
            ChiNumber(chi_number),
        )
        self.reference = E.PatientDetails(
            E.CHINumber(chi_number),
            E.Name(
                E.FamilyName(family_name),
                E.GivenName(given_name),
            ),
            E.Address(
                E.AddressLines(
                    E.AddressLine(addr_line_1),
                ),
                E.PostCode(post_code),
            ),
            E.DateOfBirth(utils.get_string_repr(date_of_birth)),
            E.Sex(sex),
        )
        super(PatientDetailsDentalStructureTest,
              self).test_to_etree_produces_correct_tree()


class PatientFamilyNameTest(unittest.TestCase):
    def test_name_truncated_to_fourteen_chars(self):
        """Patient name cannot be more than 14 characters long."""
        str_name = ''.join([random.choice(ALPHA_CHARS) for i in range(20)])
        test = PatientFamilyName(str_name)
        self.assertEqual(test.text, str_name[:14])

    def test_mc_space_is_removed(self):
        name = 'Mc Sweeny'
        expected_result = 'McSweeny'
        model = PatientFamilyName(name)
        self.assertEqual(model.text, expected_result)

    def test_mac_space_is_removed(self):
        name = 'Mac Donald'
        expected_result = 'MacDonald'
        model = PatientFamilyName(name)
        self.assertEqual(model.text, expected_result)

    def test_mc_in_name_not_removed(self):
        name = 'Damcy'
        expected_result = 'Damcy'
        model = PatientFamilyName(name)
        self.assertEqual(model.text, expected_result)

    def test_mac_in_name_not_removed(self):
        name = 'Damacy'
        expected_result = 'Damacy'
        model = PatientFamilyName(name)
        self.assertEqual(model.text, expected_result)

    def test_mac_space_is_removed_and_name_truncated(self):
        name = 'Mac DonaldWheresYerTroosers'
        expected_result = 'MacDonaldWhere'
        model = PatientFamilyName(name)
        self.assertEqual(model.text, expected_result)

    def test_mc_space_is_removed_and_name_truncated(self):
        name = 'Mc DonaldWheresYerTroosers'
        expected_result = 'McDonaldWheres'
        model = PatientFamilyName(name)
        self.assertEqual(model.text, expected_result)

    def test_punctuation_is_removed(self):
        name = "O'Brian"
        expected_result = 'OBrian'
        model = PatientFamilyName(name)
        self.assertEqual(model.text, expected_result)

    def test_other_punctuation_is_removed(self):
        punctuation = string.punctuation.replace('-', '')
        name = 'S' + punctuation + 'J'
        expected_result = 'SJ'
        model = PatientFamilyName(name)
        self.assertEqual(model.text, expected_result)

    def test_double_barrelled_hyphen_retains_second_part(self):
        name = 'Smyth-McMullen'
        expected_result = 'McMullen'
        model = PatientFamilyName(name)
        self.assertEqual(model.text, expected_result)

    def test_double_barrelled_space_retains_second_part(self):
        name = 'Smyth McMullen'
        expected_result = 'McMullen'
        model = PatientFamilyName(name)
        self.assertEqual(model.text, expected_result)

    def test_double_barrelled_space_hyphen_retains_second_part(self):
        expected_result = 'McMullen'
        for name in [
            'Smyth - McMullen',
            'Smyth -McMullen',
            'Smyth- McMullen',
            'Smyth - Mc Mullen',
            'Smyth -Mc Mullen',
            'Smyth- Mc Mullen'
        ]:
            model = PatientFamilyName(name)
            self.assertEqual(model.text, expected_result)

    def test_doubled_barrelled_long_name_retains_last_part(self):
        expected_result = 'Jenkins'
        for name in [
            'Smyth-McMullen-Hayles-Jenkins',
            'Smyth - McMullen - Hayles - Jenkins',
        ]:
            model = PatientFamilyName(name)
            self.assertEqual(model.text, expected_result)

    def test_mac_not_removed_when_not_start_of_word(self):
        name = 'Mac Mackie'
        expected_result = 'MacMackie'
        model = PatientFamilyName(name)
        self.assertEqual(model.text, expected_result)

    def test_numbers_are_removed(self):
        name = 'John15st1254on'
        expected_result = 'Johnston'
        model = PatientFamilyName(name)
        self.assertEqual(model.text, expected_result)


class PatientGivenNameTest(unittest.TestCase):
    def test_name_truncated_to_fourteen_chars(self):
        """Patient name cannot be more than 14 characters long."""
        str_name = ''.join([random.choice(ALPHA_CHARS) for i in range(20)])
        test = PatientGivenName(str_name)
        self.assertEqual(test.text, str_name[:14])

    def test_punctuation_is_removed(self):
        name = "O'Brian"
        expected_result = 'OBrian'
        model = PatientGivenName(name)
        self.assertEqual(model.text, expected_result)

    def test_other_punctuation_is_removed(self):
        punctuation = string.punctuation.replace('-', '')
        name = 'S' + punctuation + 'J'
        expected_result = 'SJ'
        model = PatientGivenName(name)
        self.assertEqual(model.text, expected_result)

    def test_double_barrelled_hyphen_retains_first_part(self):
        name = 'Smyth-McMullen'
        expected_result = 'Smyth'
        model = PatientGivenName(name)
        self.assertEqual(model.text, expected_result)

    def test_double_barrelled_space_retains_first_part(self):
        name = 'Smyth McMullen'
        expected_result = 'Smyth'
        model = PatientGivenName(name)
        self.assertEqual(model.text, expected_result)

    def test_double_barrelled_space_hyphen_retains_first_part(self):
        expected_result = 'Smyth'
        for name in [
            'Smyth - McMullen',
            'Smyth -McMullen',
            'Smyth- McMullen',
            'Smyth - Mc Mullen',
            'Smyth -Mc Mullen',
            'Smyth- Mc Mullen'
        ]:
            model = PatientGivenName(name)
            self.assertEqual(model.text, expected_result)

    def test_doubled_barrelled_long_name_retains_first_part(self):
        expected_result = 'Smyth'
        for name in [
            'Smyth-McMullen-Hayles-Jenkins',
            'Smyth - McMullen - Hayles - Jenkins',
        ]:
            model = PatientGivenName(name)
            self.assertEqual(model.text, expected_result)

    def test_numbers_are_removed(self):
        name = 'John15st1254on'
        expected_result = 'Johnston'
        model = PatientGivenName(name)
        self.assertEqual(model.text, expected_result)


class PatientDetailsRequestCheckTest(ComplexNodeMixin, unittest.TestCase):
    def setUp(self):
        self.model = PatientDetailsRequestCheck
        self.node_name = 'PatientDetailsRequestCheck'
        self.required_children = [
            'patient_details_request_flag',
        ]
        flag = True
        self.test_model = self.model(
            patient_details_request_flag=PatientDetailsRequestFlag(flag)
        )
        self.reference = E.PatientDetailsRequestCheck(
            E.PatientDetailsRequestFlag(utils.get_string_repr(flag)))

    def test_optional_creates_correct_etree(self):
        flag = False
        reference = '4444444444'
        date = datetime.date.today()
        self.test_model = self.model(
            patient_details_request_flag=PatientDetailsRequestFlag(flag),
            patient_details_response_reference=(
                PatientDetailsResponseReference(reference)),
            patient_details_request_date=PatientDetailsRequestDate(date)
        )
        self.reference = E.PatientDetailsRequestCheck(
            E.PatientDetailsRequestFlag(utils.get_string_repr(flag)),
            E.PatientDetailsResponseReference(reference),
            E.PatientDetailsRequestDate(date.strftime('%Y-%m-%d'))
        )
        super(PatientDetailsRequestCheckTest,
              self).test_to_etree_produces_correct_tree()


class PatientDetailsTest(ComplexNodeMixin, unittest.TestCase):
    def setUp(self):
        self.model = PatientDetails
        self.node_name = 'PatientDetails'
        self.required_children = [
            'patient_details',
            'patient_details_request_check',
        ]
        family_name = 'Spam'
        given_name = 'Ham'
        addr_line_1 = '124 bouley way'
        post_code = 'AB15 8AB'
        date_of_birth = datetime.date(2000, 1, 1)
        sex = 'u'
        flag = True
        ref = '5555555555'
        self.test_model = self.model(
            PatientDetailsDentalStructure(
                patient_name=PatientName(
                    PatientFamilyName(family_name),
                    PatientGivenName(given_name),
                ),
                address=Address(
                    AddressLines(
                        AddressLine(addr_line_1),
                    ),
                    PostCode(post_code),
                ),
                date_of_birth=DateOfBirth(date_of_birth),
                sex=Sex(sex),
            ),
            PatientDetailsRequestCheck(
                PatientDetailsRequestFlag(flag),
                PatientDetailsResponseReference(ref),
            )
        )
        self.reference = E.PatientDetails(
            E.PatientDetails(
                E.Name(
                    E.FamilyName(family_name),
                    E.GivenName(given_name),
                ),
                E.Address(
                    E.AddressLines(
                        E.AddressLine(addr_line_1),
                    ),
                    E.PostCode(post_code),
                ),
                E.DateOfBirth(date_of_birth.strftime('%Y-%m-%d')),
                E.Sex(sex),
            ),
            E.PatientDetailsRequestCheck(
                E.PatientDetailsRequestFlag(utils.get_string_repr(flag)),
                E.PatientDetailsResponseReference(ref),
            ),
        )

    def test_response_ref_must_be_set_if_chi_empty(self):
        family_name = 'Spam'
        given_name = 'Ham'
        addr_line_1 = '124 bouley way'
        post_code = 'AB15 8AB'
        date_of_birth = datetime.date(2000, 1, 1)
        sex = 'u'
        flag = True
        model = self.model(
            PatientDetailsDentalStructure(
                patient_name=Name(
                    FamilyName(family_name),
                    GivenName(given_name),
                ),
                address=Address(
                    AddressLines(
                        AddressLine(addr_line_1),
                    ),
                    PostCode(post_code),
                ),
                date_of_birth=DateOfBirth(date_of_birth),
                sex=Sex(sex),
            ),
            PatientDetailsRequestCheck(
                PatientDetailsRequestFlag(flag)
            )
        )
        with self.assertRaises(XmlValidationError):
            model.chi_number_is_valid()


class ContinuationCaseDetailsTest(ComplexNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'ContinuationCaseDetails'
        self.model = ContinuationCaseDetails
        self.required_children = [
            'continuation_part_number'
        ]
        number = 1
        self.test_model = self.model(
            ContinuationPartNumber(number),
        )
        self.reference = E.ContinuationCaseDetails(
            E.ContinuationPartNumber(utils.get_string_repr(number)),
        )

    def test_optional_creates_correct_tree(self):
        number = 2
        case_id = '555555555555'
        self.test_model = self.model(
            ContinuationPartNumber(number),
            PreviousCaseId(case_id),
        )
        self.reference = E.ContinuationCaseDetails(
            E.ContinuationPartNumber(utils.get_string_repr(number)),
            E.PreviousCaseID(case_id),
        )
        super(ContinuationCaseDetailsTest,
              self).test_to_etree_produces_correct_tree()

    def test_part_number_over_one_requires_previous_case(self):
        number = 3
        model = self.model(
            ContinuationPartNumber(number)
        )
        with self.assertRaises(XmlValidationError):
            model.is_valid()


class ContinuationPartNumbertTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'ContinuationPartNumber'
        self.model = ContinuationPartNumber
        self.text = 1
        self.yaml_name = 'Continuationcasepartnumber'
        self.correct_values = range(1, 100)
        self.incorrect_values = [0, 100, -5]


class PreviousCaseIdTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'PreviousCaseID'
        self.model = PreviousCaseId
        # TODO - check this is actually the format for a case id
        self.text = '555555555555'
        self.correct_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(12)]),
        ]
        self.incorrect_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(11)]),
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(13)]),
        ]


class TreatmentDetailsTest(ComplexNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'TreatmentDetails'
        self.model = TreatmentDetails
        self.required_children = [
            'treatment_date_details',
            'treatment_type_details',
        ]
        date = datetime.date.today()
        claim_code = 1
        gds_claim = True
        non_gds_claim = False
        self.test_model = self.model(
            treatment_date_details=TreatmentDateDetails(
                DateOfAcceptance(date)
            ),
            treatment_type_details=TreatmentTypeDetails(
                TypeOfClaimDetails(
                    TypeOfClaimCode(claim_code),
                ),
                GdsClaim(gds_claim),
                NonGdsClaim(non_gds_claim),
            )
        )
        self.reference = E.TreatmentDetails(
            E.TreatmentDateDetails(
                E.DateOfAcceptance(utils.get_string_repr(date)),
            ),
            E.TreatmentTypeDetails(
                E.TypeOfClaimDetails(
                    E.TypeOfClaimCode(utils.get_string_repr(claim_code)),
                    E.TypeOfClaimDescription(
                        "Initial registration/continued "
                        "registration with dentist"),
                ),
                E.GDSClaim(utils.get_string_repr(gds_claim)),
                E.NonGDSClaim(utils.get_string_repr(non_gds_claim)),
            )
        )

    def test_optional_creates_correct_tree(self):
        date_of_acceptance = datetime.date(2017, 5, 5)
        date_of_completion = datetime.date.today()
        date_of_approval = datetime.date(2017, 5, 4)
        prior_approval_reference = 'abcdefg'
        claim_code = 2
        claim_description = 'Someone else\'s patient'
        gds_claim = False
        non_gds_claim = True
        trauma = True
        special_needs = False
        referral_code = 3
        referral_description = 'I\'m good at this'
        models = True
        radiographs = False
        remarks = 'Didn\'t want to irradiate the patient'
        orthodontic = False
        treatment_code = '0142'
        qual = '04'
        value = 15.21
        tooth = 11
        flag = False
        obs = 'Patient is friendly'
        annotation_code = 'M'
        surface_code = 'MOD'
        surface_desc = 'Messy side'
        score = 3
        material_type = 'R'
        material_desc = 'A nice resin'
        self.test_model = self.model(
            TreatmentDateDetails(
                DateOfAcceptance(date_of_acceptance),
                DateOfCompletion(date_of_completion),
            ),
            PriorApprovalDetails(
                DateOfApproval(date_of_approval),
                PriorApprovalReference(prior_approval_reference),
            ),
            TreatmentTypeDetails(
                TypeOfClaimDetails(
                    TypeOfClaimCode(claim_code),
                    TypeOfClaimDescription(claim_description),
                ),
                GdsClaim(gds_claim),
                NonGdsClaim(non_gds_claim),
            ),
            TreatmentArrangementsDetails(
                ExternalMouthTrauma(trauma),
                SpecialNeeds(special_needs),
                ReferralReasonDetails(
                    ReferralReasonCode(referral_code),
                    ReferralReasonDescription(referral_description),
                ),
                ModelsAvailable(models),
                RadiographsAvailable(radiographs),
                NoRadiographsRemarks(remarks),
                Orthodontic(orthodontic),
            ),
            CompletedTreatmentDetails(
                treatment=[
                    Treatment(
                        TreatmentCode(treatment_code),
                        TreatmentQualifier(qual),
                        TreatmentValue(value),
                        teeth=[
                            Teeth(
                                ToothNotation(tooth),
                                SupernumeraryFlag(flag),
                            ),
                            Teeth(
                                ToothNotation(tooth + 1),
                                SupernumeraryFlag(flag),
                            ),
                        ],
                    ),
                    Treatment(
                        TreatmentCode(treatment_code),
                        TreatmentQualifier(qual),
                        TreatmentValue(value),
                        teeth=[
                            Teeth(
                                ToothNotation(tooth),
                                SupernumeraryFlag(flag),
                            ),
                            Teeth(
                                ToothNotation(tooth + 1),
                                SupernumeraryFlag(flag),
                            ),
                        ],
                        cur_flag=CurFlag(flag),
                    ),
                ],
                cur_observations=CurObservations(obs),
            ),
            DentalChartDetails(
                dental_chart=[
                    DentalChart(
                        ToothNotation(tooth),
                        SupernumeraryFlag(flag),
                        AnnotationCodeDetails(
                            AnnotationCode(annotation_code),
                            AnnotationDescription(obs),
                        ),
                        ToothSurfaceDetails(
                            ToothSurfaceCode(surface_code),
                            ToothSurfaceDescription(surface_desc),
                        ),
                        MaterialDetails(
                            MaterialCode(material_type),
                            MaterialDescription(material_desc),
                        ),
                    ),
                    DentalChart(
                        ToothNotation(tooth + 1),
                        SupernumeraryFlag(flag),
                        AnnotationCodeDetails(
                            AnnotationCode(annotation_code),
                            AnnotationDescription(obs),
                        ),
                        ToothSurfaceDetails(
                            ToothSurfaceCode(surface_code),
                            ToothSurfaceDescription(surface_desc),
                        ),
                        MaterialDetails(
                            MaterialCode(material_type),
                            MaterialDescription(material_desc),
                        ),
                    ),
                ],
            ),
            BpeDetails(
                UpperLeft(score),
                UpperAnterior(score),
                UpperRight(score),
                LowerLeft(score),
                LowerAnterior(score),
                LowerRight(score),
            ),
        )
        self.reference = E.TreatmentDetails(
            E.TreatmentDateDetails(
                E.DateOfAcceptance(utils.get_string_repr(date_of_acceptance)),
                E.DateOfCompletion(utils.get_string_repr(date_of_completion)),
            ),
            E.PriorApprovalDetails(
                E.DateOfApproval(utils.get_string_repr(date_of_approval)),
                E.PriorApprovalReference(prior_approval_reference),
            ),
            E.TreatmentTypeDetails(
                E.TypeOfClaimDetails(
                    E.TypeOfClaimCode(utils.get_string_repr(claim_code)),
                    E.TypeOfClaimDescription(claim_description),
                ),
                E.GDSClaim(utils.get_string_repr(gds_claim)),
                E.NonGDSClaim(utils.get_string_repr(non_gds_claim)),
            ),
            E.TreatmentArrangementsDetails(
                E.ExternalMouthTrauma(utils.get_string_repr(trauma)),
                E.SpecialNeeds(utils.get_string_repr(special_needs)),
                E.ReferralReasonDetails(
                    E.ReferralReasonCode(utils.get_string_repr(referral_code)),
                    E.ReferralReasonDescription(referral_description),
                ),
                E.ModelsAvailable(utils.get_string_repr(models)),
                E.RadiographsAvailable(utils.get_string_repr(radiographs)),
                E.NoRadiographsRemarks(remarks),
                E.Orthodontic(utils.get_string_repr(orthodontic)),
            ),
            E.CompletedTreatmentDetails(
                E.Treatment(
                    E.TreatmentCode(treatment_code),
                    E.TreatmentQualifier(qual),
                    E.TreatmentValue('15.21'),
                    E.Teeth(
                        E.ToothNotation(utils.get_string_repr(tooth)),
                        E.SupernumeraryFlag(utils.get_string_repr(flag)),
                    ),
                    E.Teeth(
                        E.ToothNotation(utils.get_string_repr(tooth + 1)),
                        E.SupernumeraryFlag(utils.get_string_repr(flag)),
                    ),
                ),
                E.Treatment(
                    E.TreatmentCode(treatment_code),
                    E.TreatmentQualifier(qual),
                    E.TreatmentValue('15.21'),
                    E.Teeth(
                        E.ToothNotation(utils.get_string_repr(tooth)),
                        E.SupernumeraryFlag(utils.get_string_repr(flag)),
                    ),
                    E.Teeth(
                        E.ToothNotation(utils.get_string_repr(tooth + 1)),
                        E.SupernumeraryFlag(utils.get_string_repr(flag)),
                    ),
                    E.CURFlag(utils.get_string_repr(flag)),
                ),
                E.CURObservations(obs),
            ),
            E.DentalChartDetails(
                E.DentalChart(
                    E.ToothNotation(utils.get_string_repr(tooth)),
                    E.SupernumeraryFlag(utils.get_string_repr(flag)),
                    E.AnnotationCodeDetails(
                        E.AnnotationCode(annotation_code),
                        E.AnnotationDescription(obs),
                    ),
                    E.ToothSurfaceDetails(
                        E.ToothSurfaceCode(surface_code),
                        E.ToothSurfaceDescription(surface_desc),
                    ),
                    E.MaterialDetails(
                        E.MaterialCode(material_type),
                        E.MaterialDescription(material_desc),
                    ),
                ),
                E.DentalChart(
                    E.ToothNotation(utils.get_string_repr(tooth + 1)),
                    E.SupernumeraryFlag(utils.get_string_repr(flag)),
                    E.AnnotationCodeDetails(
                        E.AnnotationCode(annotation_code),
                        E.AnnotationDescription(obs),
                    ),
                    E.ToothSurfaceDetails(
                        E.ToothSurfaceCode(surface_code),
                        E.ToothSurfaceDescription(surface_desc),
                    ),
                    E.MaterialDetails(
                        E.MaterialCode(material_type),
                        E.MaterialDescription(material_desc),
                    ),
                ),
            ),
            E.BPEDetails(
                E.UpperLeft(utils.get_string_repr(score)),
                E.UpperAnterior(utils.get_string_repr(score)),
                E.UpperRight(utils.get_string_repr(score)),
                E.LowerLeft(utils.get_string_repr(score)),
                E.LowerAnterior(utils.get_string_repr(score)),
                E.LowerRight(utils.get_string_repr(score)),
            ),
        )
        super(TreatmentDetailsTest,
              self).test_to_etree_produces_correct_tree()


class TreatmentDateDetailsTest(ComplexNodeMixin, unittest.TestCase):
    def setUp(self):
        self.model = TreatmentDateDetails
        self.node_name = 'TreatmentDateDetails'
        self.required_children = [
            'date_of_acceptance',
        ]
        date_of_acceptance = datetime.date.today()
        self.test_model = self.model(
            DateOfAcceptance(date_of_acceptance)
        )
        self.reference = E.TreatmentDateDetails(
            E.DateOfAcceptance(date_of_acceptance.strftime('%Y-%m-%d'))
        )

    def test_optional_creates_correct_etree(self):
        date_of_acceptance = datetime.date(2017, 5, 12)
        date_of_completion = datetime.date.today()
        self.test_model = self.model(
            DateOfAcceptance(date_of_acceptance),
            DateOfCompletion(date_of_completion),
        )
        self.reference = E.TreatmentDateDetails(
            E.DateOfAcceptance(date_of_acceptance.strftime('%Y-%m-%d')),
            E.DateOfCompletion(date_of_completion.strftime('%Y-%m-%d')),
        )


class DateOfAcceptanceTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'DateOfAcceptance'
        self.model = DateOfAcceptance
        self.text = datetime.date.today()
        self.correct_values = [
            datetime.date(2015, 12, 10),
        ]
        self.incorrect_values = [
            'now',
            '2015/12/10',
        ]


class DateOfCompletionTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'DateOfCompletion'
        self.model = DateOfCompletion
        self.text = datetime.date.today()
        self.correct_values = [
            datetime.date(2015, 12, 10),
        ]
        self.incorrect_values = [
            'now',
            '2015/12/10',
        ]


class PriorApprovalDetailsTest(ComplexNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'PriorApprovalDetails'
        self.model = PriorApprovalDetails
        self.required_children = ['date_of_approval']
        date_of_approval = datetime.date.today()
        self.test_model = self.model(
            DateOfApproval(date_of_approval),
        )
        self.reference = E.PriorApprovalDetails(
            E.DateOfApproval(utils.get_string_repr(date_of_approval)),
        )

    def test_optional_attrs_create_correct_etree(self):
        date_of_approval = datetime.date.today()
        prior_approval_reference = 'abcde'
        self.test_model = self.model(
            DateOfApproval(date_of_approval),
            PriorApprovalReference(prior_approval_reference),
        )
        self.reference = E.PriorApprovalDetails(
            E.DateOfApproval(utils.get_string_repr(date_of_approval)),
            E.PriorApprovalReference(prior_approval_reference),
        )
        super(PriorApprovalDetailsTest,
              self).test_to_etree_produces_correct_tree()


class DateOfApprovalTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'DateOfApproval'
        self.model = DateOfApproval
        self.text = datetime.date.today()
        self.correct_values = [
            datetime.date(2015, 12, 10),
        ]
        self.incorrect_values = [
            'now',
            '2015/12/10',
        ]


class PriorApprovalReferenceTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'PriorApprovalReference'
        self.model = PriorApprovalReference
        self.text = ''.join([random.choice(ALPHANUM_CHARS) for i in range(25)])
        self.correct_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(1)]),
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(30)]),
        ]
        self.incorrect_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(31)]),
        ]


class TreatmentTypeDetailsTest(ComplexNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'TreatmentTypeDetails'
        self.model = TreatmentTypeDetails
        self.required_children = [
            'type_of_claim_details',
            'gds_claim',
            'non_gds_claim',
        ]
        code = 1
        desc = "Initial registration/continued registration with dentist"
        gds_claim = True
        non_gds_claim = False
        self.test_model = self.model(
            TypeOfClaimDetails(
                TypeOfClaimCode(code),
            ),
            GdsClaim(gds_claim),
            NonGdsClaim(non_gds_claim),
        )
        self.reference = E.TreatmentTypeDetails(
            E.TypeOfClaimDetails(
                E.TypeOfClaimCode(utils.get_string_repr(code)),
                E.TypeOfClaimDescription(desc),
            ),
            E.GDSClaim(utils.get_string_repr(gds_claim)),
            E.NonGDSClaim(utils.get_string_repr(non_gds_claim)),
        )

    def test_gds_claim_bools_mutually_exclusive_fails_correctly(self):
        for result in [True, False]:
            model = self.model(
                gds_claim=GdsClaim(result),
                non_gds_claim=NonGdsClaim(result),
            )
            with self.assertRaises(XmlValidationError):
                model.gds_claim_is_valid()

    def test_gds_claim_bools_mutually_exclusive_succeeds_correctly(self):
        for result in [True, False]:
            model = self.model(
                gds_claim=GdsClaim(result),
                non_gds_claim=NonGdsClaim(not result),
            )
            self.assertTrue(model.gds_claim_is_valid())


class TypeOfClaimDetailsTest(ComplexNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'TypeOfClaimDetails'
        self.model = TypeOfClaimDetails
        self.required_children = [
            'type_of_claim_code',
        ]
        code = 1
        desc = "Initial registration/continued registration with dentist"
        self.test_model = self.model(
            TypeOfClaimCode(code),
        )
        self.reference = E.TypeOfClaimDetails(
            E.TypeOfClaimCode(utils.get_string_repr(code)),
            E.TypeOfClaimDescription(desc)
        )

    def test_claim_description_set_automatically_if_empty(self):
        descriptions = [
            "Initial registration/continued registration with dentist",
            "Registered with another dentist at this practice",
            "Registered with another dentist at another practice",
            "Not registered with any dentist",
            "Referred patient"
        ]
        for i, text in enumerate(descriptions, 1):
            model = self.model(TypeOfClaimCode(i))
            self.assertEqual(
                model.type_of_claim_description.text,
                text)


class TypeOfClaimDescriptionTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'TypeOfClaimDescription'
        self.model = TypeOfClaimDescription
        self.text = 'abcdef'
        self.correct_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(1)]),
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(396)]),
        ]
        self.incorrect_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(397)]),
        ]


class TypeOfClaimCodeTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'TypeOfClaimCode'
        self.model = TypeOfClaimCode
        self.text = 5
        self.correct_values = [
            1, 2, 3, 4, 5
        ]
        self.incorrect_values = [
            0, 6, 'flibble'
        ]


class GdsClaimTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'GDSClaim'
        self.model = GdsClaim
        self.text = True
        self.correct_values = [
            True, False
        ]
        self.incorrect_values = [
            'a', 1, 0, 9
        ]


class NonGdsClaimTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'NonGDSClaim'
        self.model = NonGdsClaim
        self.text = True
        self.correct_values = [
            True, False
        ]
        self.incorrect_values = [
            'a', 1, 0, 9
        ]


class TreatmentArrangementsDetailsTest(ComplexNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'TreatmentArrangementsDetails'
        self.model = TreatmentArrangementsDetails
        self.required_children = []
        self.test_model = self.model()
        self.reference = E.TreatmentArrangementsDetails()

    def test_optional_values_create_correct_tree(self):
        trauma = True
        special_needs = True
        code = 2
        models_available = True
        radiographs_available = False
        remarks = "Could not x-ray the patient"
        orthodontic = False
        self.test_model = self.model(
            ExternalMouthTrauma(trauma),
            SpecialNeeds(special_needs),
            ReferralReasonDetails(
                ReferralReasonCode(code),
            ),
            ModelsAvailable(models_available),
            RadiographsAvailable(radiographs_available),
            NoRadiographsRemarks(remarks),
            Orthodontic(orthodontic),
        )
        self.reference = E.TreatmentArrangementsDetails(
            E.ExternalMouthTrauma(utils.get_string_repr(trauma)),
            E.SpecialNeeds(utils.get_string_repr(special_needs)),
            E.ReferralReasonDetails(
                E.ReferralReasonCode(utils.get_string_repr(code)),
                E.ReferralReasonDescription("Experience"),
            ),
            E.ModelsAvailable(utils.get_string_repr(models_available)),
            E.RadiographsAvailable(
                utils.get_string_repr(radiographs_available)),
            E.NoRadiographsRemarks(remarks),
            E.Orthodontic(utils.get_string_repr(orthodontic)),
        )
        super(TreatmentArrangementsDetailsTest,
              self).test_to_etree_produces_correct_tree()


class ExternalMouthTraumaTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'ExternalMouthTrauma'
        self.model = ExternalMouthTrauma
        self.text = True
        self.correct_values = [
            True, False
        ]
        self.incorrect_values = [
            'a', 1, 0, 9
        ]


class SpecialNeedsTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'SpecialNeeds'
        self.model = SpecialNeeds
        self.text = True
        self.correct_values = [
            True, False
        ]
        self.incorrect_values = [
            'a', 1, 0, 9
        ]


class ReferralReasonDetailTest(ComplexNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'ReferralReasonDetails'
        self.model = ReferralReasonDetails
        self.required_children = [
            'referral_reason_code'
        ]
        code = 1
        desc = 'Facilities'
        self.test_model = self.model(ReferralReasonCode(code))
        self.reference = E.ReferralReasonDetails(
            E.ReferralReasonCode(utils.get_string_repr(code)),
            E.ReferralReasonDescription(utils.get_string_repr(desc)),
        )

    def test_reason_description_set_automatically_if_empty(self):
        descriptions = [
            "Facilities",
            "Experience",
            "Expertise",
        ]
        for i, text in enumerate(descriptions, 1):
            model = self.model(ReferralReasonCode(i))
            self.assertEqual(
                model.referral_reason_description.text,
                text)


class ReferralReasonCodeTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'ReferralReasonCode'
        self.model = ReferralReasonCode
        self.text = 1
        self.correct_values = [
            1, 2, 3]
        self.incorrect_values = [
            'a', 0, 4, 9
        ]


class ReferralReasonDescriptionTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'ReferralReasonDescription'
        self.model = ReferralReasonDescription
        self.text = "Foo bar baz"
        self.correct_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(1)]),
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(396)]),
        ]
        self.incorrect_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(397)]),
        ]


class ModelsAvailableTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'ModelsAvailable'
        self.model = ModelsAvailable
        self.text = True
        self.correct_values = [
            True, False
        ]
        self.incorrect_values = [
            'a', 1, 0, 9
        ]


class RadiographsAvailableTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'RadiographsAvailable'
        self.model = RadiographsAvailable
        self.text = True
        self.correct_values = [
            True, False
        ]
        self.incorrect_values = [
            'a', 1, 0, 9
        ]


class NoRadiographsRemarksTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'NoRadiographsRemarks'
        self.model = NoRadiographsRemarks
        self.text = "Foo bar baz"
        self.correct_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(1)]),
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(400)]),
        ]
        self.incorrect_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(401)]),
        ]


class OrthodonticTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'Orthodontic'
        self.model = Orthodontic
        self.text = True
        self.correct_values = [
            True, False
        ]
        self.incorrect_values = [
            'a', 1, 0, 9
        ]


class CompletedTreatmentDetailsTest(ComplexNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'CompletedTreatmentDetails'
        self.model = CompletedTreatmentDetails
        self.required_children = ['treatment']
        code = '1234'
        qualifier = '04'
        value = 15.21
        self.test_model = self.model(
            Treatment(
                TreatmentCode(code),
                TreatmentQualifier(qualifier),
                TreatmentValue(value),
            )
        )
        self.reference = E.CompletedTreatmentDetails(
            E.Treatment(
                E.TreatmentCode(code),
                E.TreatmentQualifier(qualifier),
                E.TreatmentValue('15.21'),
            )
        )

    def model_can_have_many_treaments(self):
        code1 = '1234'
        qualifier1 = '04'
        value1 = 15.21
        code2 = '5421'
        qualifier2 = '24'
        value2 = 20
        self.test_model = self.model(
            Treatment(
                TreatmentCode(code1),
                TreatmentQualifier(qualifier1),
                TreatmentValue(value1),
            ),
            Treatment(
                TreatmentCode(code2),
                TreatmentQualifier(qualifier2),
                TreatmentValue(value2),
            ),
        )
        self.reference = E.CompletedTreatmentDetails(
            E.Treatment(
                E.TreatmentCode(code1),
                E.TreatmentQualifier(qualifier1),
                E.TreatmentValue('15.21'),
            ),
            E.Treatment(
                E.TreatmentCode(code2),
                E.TreatmentQualifier(qualifier2),
                E.TreatmentValue('20.00'),
            ),
        )
        super(CompletedTreatmentDetailsTest,
              self).test_to_etree_produces_correct_tree()


class TreatmentTest(ComplexNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'Treatment'
        self.model = Treatment
        self.required_children = [
            'treatment_code',
            'treatment_qualifier',
            'treatment_value',
        ]
        code = '1234'
        qualifier = '04'
        value = 15.21
        self.test_model = self.model(
            TreatmentCode(code),
            TreatmentQualifier(qualifier),
            TreatmentValue(value),
        )
        self.reference = E.Treatment(
            E.TreatmentCode(code),
            E.TreatmentQualifier(qualifier),
            E.TreatmentValue('15.21'),
        )

    def test_optional_creates_correct_etree(self):
        code = '5432'
        qualifier = '15'
        value = 21
        tooth = 31
        super_flag = True
        cur_flag = True
        self.test_model = self.model(
            TreatmentCode(code),
            TreatmentQualifier(qualifier),
            TreatmentValue(value),
            Teeth(
                ToothNotation(tooth),
                SupernumeraryFlag(super_flag),
            ),
            CurFlag(cur_flag),
        )
        self.reference = E.Treatment(
            E.TreatmentCode(code),
            E.TreatmentQualifier(qualifier),
            E.TreatmentValue('21.00'),
            E.Teeth(
                E.ToothNotation(utils.get_string_repr(tooth)),
                E.SupernumeraryFlag(utils.get_string_repr(super_flag)),
            ),
            E.CURFlag(utils.get_string_repr(cur_flag)),
        )
        super(TreatmentTest,
              self).test_to_etree_produces_correct_tree()


class TreatmentCodeTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'TreatmentCode'
        self.model = TreatmentCode
        self.text = '1280'
        self.validator_count = 2
        self.correct_values = [
            ''.join([random.choice(NUM_CHARS) for i in range(4)]),
        ]
        self.incorrect_values = [
            ''.join([random.choice(ALPHA_CHARS) for i in range(4)]),
            ''.join([random.choice(NUM_CHARS) for i in range(3)]),
            ''.join([random.choice(NUM_CHARS) for i in range(5)]),
        ]


class TreatmentQualifierTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'TreatmentQualifier'
        self.model = TreatmentQualifier
        self.validator_count = 2
        self.text = '80'
        self.correct_values = [
            ''.join([random.choice(NUM_CHARS) for i in range(2)]),
        ]
        self.incorrect_values = [
            'aa'
            ''.join([random.choice(NUM_CHARS) for i in range(1)]),
            ''.join([random.choice(NUM_CHARS) for i in range(3)]),
        ]


class TreatmentValueTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'TreatmentValue'
        self.model = TreatmentValue
        self.validator_count = 2
        self.text = 80.20
        self.expected_text = '80.20'
        self.correct_values = [
            5, 15.1, 1000.15,
        ]
        self.incorrect_values = [
            10.005,
            'ab',
            -1,
        ]

    def test_text_returns_formatted_version(self):
        values = [
            (80.20, '80.20'),
            (15, '15.00'),
            (1, '1.00'),
            (19.1, '19.10'),
        ]
        for value, expected_result in values:
            model = self.model(value)
            self.assertEqual(model.text, expected_result)


class TeethTest(ComplexNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'Teeth'
        self.model = Teeth
        self.required_children = ['tooth_notation']
        tooth = 15
        self.test_model = self.model(ToothNotation(tooth))
        self.reference = E.Teeth(
            E.ToothNotation(utils.get_string_repr(tooth))
        )

    def test_optional_creates_correct_tree(self):
        tooth = 31
        flag = True
        self.test_model = self.model(
            ToothNotation(tooth),
            SupernumeraryFlag(flag),
        )
        self.reference = E.Teeth(
            E.ToothNotation(utils.get_string_repr(tooth)),
            E.SupernumeraryFlag(utils.get_string_repr(flag)),
        )
        super(TeethTest,
              self).test_to_etree_produces_correct_tree()


class ToothNotationTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'ToothNotation'
        self.model = ToothNotation
        self.text = 10
        self.correct_values = range(10, 100)
        self.incorrect_values = [
            9, 100
        ]


class SupernumeraryFlagTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'SupernumeraryFlag'
        self.model = SupernumeraryFlag
        self.text = True
        self.correct_values = [
            True, False
        ]
        self.incorrect_values = [
            'a', 1, 0, 9
        ]


class CurFlagTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'CURFlag'
        self.model = CurFlag
        self.text = True
        self.correct_values = [
            True, False
        ]
        self.incorrect_values = [
            'a', 1, 0, 9
        ]


class CurObservationsTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'CURObservations'
        self.model = CurObservations
        self.text = 'Mighty interesting'
        self.correct_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(1)]),
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(1999)]),
        ]
        self.incorrect_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(2000)]),
        ]


class DentalChartDetailsTest(ComplexNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'DentalChartDetails'
        self.model = DentalChartDetails
        self.required_children = ['dental_chart']
        tooth = 21
        self.test_model = self.model(
            DentalChart(
                ToothNotation(tooth)
            )
        )
        self.reference = E.DentalChartDetails(
            E.DentalChart(
                E.ToothNotation(utils.get_string_repr(tooth))
            )
        )


class DentalChartTest(ComplexNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'DentalChart'
        self.model = DentalChart
        self.required_children = [
            'tooth_notation',
        ]
        tooth = 21
        self.test_model = self.model(
            ToothNotation(tooth),
        )
        self.reference = E.DentalChart(
            E.ToothNotation(utils.get_string_repr(tooth)),
        )

    def test_optional_creates_correct_tree(self):
        tooth = 15
        super_flag = True
        annotation_code = 'M'
        annotation_description = 'a nice wee missing tooth'
        tooth_code = 'O'
        tooth_surface_description = 'things are looking up'
        material_code = 'A'
        material_description = 'A material of excellence'
        self.test_model = self.model(
            ToothNotation(tooth),
            SupernumeraryFlag(super_flag),
            AnnotationCodeDetails(
                AnnotationCode(annotation_code),
                AnnotationDescription(annotation_description)
            ),
            ToothSurfaceDetails(
                ToothSurfaceCode(tooth_code),
                ToothSurfaceDescription(tooth_surface_description),
            ),
            MaterialDetails(
                MaterialCode(material_code),
                MaterialDescription(material_description),
            ),
        )
        self.reference = E.DentalChart(
            E.ToothNotation(utils.get_string_repr(tooth)),
            E.SupernumeraryFlag(utils.get_string_repr(super_flag)),
            E.AnnotationCodeDetails(
                E.AnnotationCode(annotation_code),
                E.AnnotationDescription(annotation_description),
            ),
            E.ToothSurfaceDetails(
                E.ToothSurfaceCode(tooth_code),
                E.ToothSurfaceDescription(tooth_surface_description),
            ),
            E.MaterialDetails(
                E.MaterialCode(material_code),
                E.MaterialDescription(material_description),
            ),
        )
        super(DentalChartTest,
              self).test_to_etree_produces_correct_tree()


class AnnotationCodeDetailsTest(ComplexNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'AnnotationCodeDetails'
        self.model = AnnotationCodeDetails
        self.required_children = ['annotation_code']
        code = 'Z'
        desc = 'Tooth missing and space closed'
        self.test_model = self.model(AnnotationCode(code))
        self.reference = E.AnnotationCodeDetails(
            E.AnnotationCode(code),
            E.AnnotationDescription(desc),
        )

    def test_annotation_description_not_included_if_missing_from_dict(self):
        model = self.model(
            AnnotationCode('A')
        )
        self.assertIsNone(model.annotation_description)


class AnnotationCodeTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'AnnotationCode'
        self.model = AnnotationCode
        self.text = 'M'
        self.correct_values = [
            'M', 'Z', 'R', 'E', 'A', 'D', 'C', 'BR', 'BP', 'F', 'IN', 'U',
            'IM', 'V', 'O',
        ]
        self.incorrect_values = [
            'abe', 5,
        ]

    def test_a_gets_replaced_with_dt(self):
        model = self.model('A')
        tree = model.as_etree()
        self.assertEqual(tree.text, 'DT')


class AnnotationDescriptionTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'AnnotationDescription'
        self.model = AnnotationDescription
        self.text = 'Bridge retainer present'
        self.correct_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(1)]),
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(396)]),
        ]
        self.incorrect_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(397)]),
        ]


class ToothSurfaceDetailsTest(ComplexNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'ToothSurfaceDetails'
        self.model = ToothSurfaceDetails
        self.required_children = [
            'tooth_surface_code',
        ]
        code = 'M'
        desc = 'Mesial'
        self.test_model = self.model(ToothSurfaceCode(code))
        self.reference = E.ToothSurfaceDetails(
            E.ToothSurfaceCode(code),
            E.ToothSurfaceDescription(desc),
        )


class ToothSurfaceCodeTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'ToothSurfaceCode'
        self.model = ToothSurfaceCode
        self.text = 'MOD'
        self.correct_values = [
            'M', 'O', 'D', 'B', 'P', 'L', 'I', 'MOD', 'BP', 'LI',
        ]
        self.incorrect_values = [
            'MM', 'A', 5, 'AD',
        ]


class ToothSurfaceDescriptionTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'ToothSurfaceDescription'
        self.model = ToothSurfaceDescription
        self.text = 'A bit of tooth'
        self.correct_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(1)]),
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(396)]),
        ]
        self.incorrect_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(397)]),
        ]


class MaterialDetailsTest(ComplexNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'MaterialDetails'
        self.model = MaterialDetails
        self.required_children = ['material_code']
        code = 'A'
        desc = 'Amalgam'
        self.test_model = self.model(MaterialCode(code))
        self.reference = E.MaterialDetails(
            E.MaterialCode(code),
            E.MaterialDescription(desc),
        )

    def test_optional_creates_correct_tree(self):
        code = 'A'
        desc = 'A wee amalgam'
        self.test_model = self.model(
            MaterialCode(code),
            MaterialDescription(desc),
        )
        self.reference = E.MaterialDetails(
            E.MaterialCode(code),
            E.MaterialDescription(desc),
        )
        super(MaterialDetailsTest,
              self).test_to_etree_produces_correct_tree()


class MaterialCodeTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'MaterialCode'
        self.model = MaterialCode
        self.text = 'R'
        self.correct_values = [
            'A', 'R', 'G',
        ]
        self.incorrect_values = [
            'AR', 5, 'b', 'a'
        ]


class MaterialDescriptionTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'MaterialDescription'
        self.model = MaterialDescription
        self.text = 'A nice piece of pie'
        self.correct_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(1)]),
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(396)]),
        ]
        self.incorrect_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(397)]),
        ]


class BpeDetailsTest(ComplexNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'BPEDetails'
        self.model = BpeDetails
        self.required_children = [
            'upper_left',
            'upper_anterior',
            'upper_right',
            'lower_left',
            'lower_anterior',
            'lower_right',
        ]
        score = '4'
        self.test_model = self.model(
            UpperLeft(score),
            UpperAnterior(score),
            UpperRight(score),
            LowerLeft(score),
            LowerAnterior(score),
            LowerRight(score),
        )
        self.reference = E.BPEDetails(
            E.UpperLeft(score),
            E.UpperAnterior(score),
            E.UpperRight(score),
            E.LowerLeft(score),
            E.LowerAnterior(score),
            E.LowerRight(score),
        )


class UpperLeftTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'UpperLeft'
        self.model = UpperLeft
        self.validator_count = 2
        self.text = '2'
        self.correct_values = [
            '0', '1', '2', '3', '4',
            '0*', '1*', '2*', '3*', '4*',
            '-', 'X', 1, 2, 3, 4,
        ]
        self.incorrect_values = [
            '5', '5*', 'abc'
        ]


class UpperAnteriorTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'UpperAnterior'
        self.model = UpperAnterior
        self.validator_count = 2
        self.text = '2'
        self.correct_values = [
            '0', '1', '2', '3', '4',
            '0*', '1*', '2*', '3*', '4*',
            '-', 'X', 1, 2, 3, 4,
        ]
        self.incorrect_values = [
            '5', '5*', 'abc'
        ]


class UpperRightTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'UpperRight'
        self.model = UpperRight
        self.validator_count = 2
        self.text = '2'
        self.correct_values = [
            '0', '1', '2', '3', '4',
            '0*', '1*', '2*', '3*', '4*',
            '-', 'X', 1, 2, 3, 4,
        ]
        self.incorrect_values = [
            '5', '5*', 'abc'
        ]


class LowerLeftTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'LowerLeft'
        self.model = LowerLeft
        self.validator_count = 2
        self.text = '2'
        self.correct_values = [
            '0', '1', '2', '3', '4',
            '0*', '1*', '2*', '3*', '4*',
            '-', 'X', 1, 2, 3, 4,
        ]
        self.incorrect_values = [
            '5', '5*', 'abc'
        ]


class LowerAnteriorTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'LowerAnterior'
        self.model = LowerAnterior
        self.validator_count = 2
        self.text = '2'
        self.correct_values = [
            '0', '1', '2', '3', '4',
            '0*', '1*', '2*', '3*', '4*',
            '-', 'X', 1, 2, 3, 4,
        ]
        self.incorrect_values = [
            '5', '5*', 'abc'
        ]


class LowerRightTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'LowerRight'
        self.model = LowerRight
        self.validator_count = 2
        self.text = '2'
        self.correct_values = [
            '0', '1', '2', '3', '4',
            '0*', '1*', '2*', '3*', '4*',
            '-', 'X', 1, 2, 3, 4,
        ]
        self.incorrect_values = [
            '5', '5*', 'abc'
        ]


class DentistsDeclarationDetailsTest(ComplexNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'DentistsDeclarationDetails'
        self.model = DentistsDeclarationDetails
        self.children = [
            'dentist_declaration',
            'patient_declaration'
        ]
        self.required_children = [
            'dentist_declaration',
            'patient_declaration'
        ]
        declaration = True
        signature = True
        ftr = True
        fee_required = False
        code = '1'
        description = 'patient'
        date = datetime.date.today()
        self.test_model = self.model(
            DentistDeclaration(
                pftr=Pftr(ftr),
                pftr_fee_required=PftrFeeRequired(fee_required),
                declaration=Declaration(declaration),
            ),
            PatientDeclaration(
                DeclarationOnAcceptance(
                    PatientOrRepresentativeDetails(
                        PatientOrRepresentativeCode(code),
                    ),
                    PatientOrRepresentativeSignature(signature),
                    PatientOrRepresentativeSignatureDate(date),
                ),
            ),
        )
        self.reference = E.DentistsDeclarationDetails(
            E.DentistDeclaration(
                E.PFTR(
                    utils.get_string_repr(ftr),
                ),
                E.PFTRFeeRequired(
                    utils.get_string_repr(fee_required),
                ),
                E.Declaration(
                    utils.get_string_repr(declaration),
                ),
            ),
            E.PatientDeclaration(
                E.DeclarationOnAcceptance(
                    E.PatientOrRepresentativeDetails(
                        E.PatientOrRepresentativeCode(code),
                        E.PatientOrRepresentativeDescription(description),
                    ),
                    E.PatientOrRepresentativeSignature(
                        utils.get_string_repr(signature)
                    ),
                    E.PatientOrRepresentativeSignatureDate(
                        utils.get_string_repr(date)
                    ),
                )
            )
        )

    def test_cannot_skip_completion_unless_patient_fta(self):
        declaration = True
        signature = True
        code = '1'
        date = datetime.date.today()
        model = self.model(
            DentistDeclaration(
                declaration=Declaration(declaration),
            ),
            PatientDeclaration(
                DeclarationOnAcceptance(
                    PatientOrRepresentativeDetails(
                        PatientOrRepresentativeCode(code),
                    ),
                    PatientOrRepresentativeSignature(signature),
                    PatientOrRepresentativeSignatureDate(date),
                ),
            ),
        )
        self.assertFalse(model.declaration_on_completion_is_valid())
        model.dentist_declaration = DentistDeclaration(
            pftr=Pftr(True)
        )
        self.assertTrue(model.declaration_on_completion_is_valid())


class DentistDeclarationTest(ComplexNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'DentistDeclaration'
        self.model = DentistDeclaration
        self.children = [
            'patient_refused_treatment',
            'patient_refused_treatment_observations',
            'pftr',
            'pftr_fee_required',
            'pftr_fee_required_observations',
            'declaration',
        ]
        self.required_children = ['declaration']
        declaration = True
        self.test_model = self.model(
            declaration=Declaration(declaration)
        )
        self.reference = E.DentistDeclaration(
            E.Declaration(
                utils.get_string_repr(declaration)
            )
        )

    def test_cannot_skip_observations_when_patient_refused_treatment(self):
        model = self.model(
            patient_refused_treatment=PatientRefusedTreatment(True),
        )
        self.assertFalse(model.patient_refused_treatment_is_valid())
        model.patient_refused_treatment_observations = \
            PatientRefusedTreatmentObservations('Fooey')
        self.assertTrue(model.patient_refused_treatment_is_valid())

    def test_cannot_skip_fee_flag_when_ftr(self):
        model = self.model(pftr=Pftr(True))
        self.assertFalse(model.pftr_is_valid())
        model.pftr_fee_required = PftrFeeRequired(True)
        self.assertTrue(model.pftr_is_valid())

    def test_cannot_skip_observations_if_ftr_fee_paid(self):
        model = self.model(pftr_fee_required=PftrFeeRequired(True))
        self.assertFalse(model.pftr_fee_required_is_valid())
        model.pftr_fee_required_observations = PftrFeeRequiredObservations('a')
        self.assertTrue(model.pftr_fee_required_is_valid())


class PatientRefusedTreatmentTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'PatientRefusedTreatment'
        self.model = PatientRefusedTreatment
        self.text = True
        self.correct_values = [
            True, False
        ]
        self.incorrect_values = [
            'a', 1, 0, 9
        ]


class PatientRefusedTreatmentObservationsTest(LeafNodeMixin,
                                              unittest.TestCase):
    def setUp(self):
        self.node_name = 'PatientRefusedTreatmentObservations'
        self.model = PatientRefusedTreatmentObservations
        self.text = 'Mighty interesting'
        self.correct_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(1)]),
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(1999)]),
        ]
        self.incorrect_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(2000)]),
        ]


class PftrTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'PFTR'
        self.model = Pftr
        self.text = True
        self.correct_values = [
            True, False
        ]
        self.incorrect_values = [
            'a', 1, 0, 9
        ]


class PftrFeeRequiredTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'PFTRFeeRequired'
        self.model = PftrFeeRequired
        self.text = True
        self.correct_values = [
            True, False
        ]
        self.incorrect_values = [
            'a', 1, 0, 9
        ]


class PftrFeeRequiredObservationsTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'PFTRFeeRequiredObservations'
        self.model = PftrFeeRequiredObservations
        self.text = 'Mighty interesting'
        self.correct_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(1)]),
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(1999)]),
        ]
        self.incorrect_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(2000)]),
        ]


class DeclarationTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'Declaration'
        self.model = Declaration
        self.text = True
        self.correct_values = [
            True, False
        ]
        self.incorrect_values = [
            'a', 1, 0, 9
        ]


class PatientDeclarationTest(ComplexNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'PatientDeclaration'
        self.model = PatientDeclaration
        self.children = [
            'declaration_on_acceptance',
            'declaration_on_completion'
        ]
        self.required_children = [
            'declaration_on_acceptance',
        ]
        code = '1'
        signature = True
        description = 'patient'
        date = datetime.date.today()
        self.test_model = self.model(
            DeclarationOnAcceptance(
                PatientOrRepresentativeDetails(
                    PatientOrRepresentativeCode(code),
                ),
                PatientOrRepresentativeSignature(signature),
                PatientOrRepresentativeSignatureDate(date),
            )
        )
        self.reference = E.PatientDeclaration(
            E.DeclarationOnAcceptance(
                E.PatientOrRepresentativeDetails(
                    E.PatientOrRepresentativeCode(code),
                    E.PatientOrRepresentativeDescription(description),
                ),
                E.PatientOrRepresentativeSignature(
                    utils.get_string_repr(signature)
                ),
                E.PatientOrRepresentativeSignatureDate(
                    utils.get_string_repr(date)
                ),
            ),
        )


class DeclarationOnAcceptanceTest(ComplexNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'DeclarationOnAcceptance'
        self.model = DeclarationOnAcceptance
        self.children = [
            'patient_or_representative_details',
            'patient_or_representative_signature',
            'patient_or_representative_signature_date',
            'representative_name',
            'representative_address',
        ]
        self.required_children = [
            'patient_or_representative_details',
            'patient_or_representative_signature',
            'patient_or_representative_signature_date',
        ]
        signature = True
        code = '1'
        date = datetime.date.today()
        description = 'patient'
        self.test_model = self.model(
            PatientOrRepresentativeDetails(
                PatientOrRepresentativeCode(code),
            ),
            PatientOrRepresentativeSignature(signature),
            PatientOrRepresentativeSignatureDate(date),
        )
        self.reference = E.DeclarationOnAcceptance(
            E.PatientOrRepresentativeDetails(
                E.PatientOrRepresentativeCode(code),
                E.PatientOrRepresentativeDescription(description),
            ),
            E.PatientOrRepresentativeSignature(
                utils.get_string_repr(signature)
            ),
            E.PatientOrRepresentativeSignatureDate(
                utils.get_string_repr(date)
            ),
        )

    def test_representative_name_required_when_code_is_two(self):
        dec = DeclarationOnAcceptance(
            PatientOrRepresentativeDetails(
                PatientOrRepresentativeCode(2),
            )
        )
        self.assertFalse(dec.representative_name_is_valid())
        representative = RepresentativeName(
            family_name='Spam',
            given_name='Ham')
        dec.representative_name = representative
        self.assertTrue(dec.representative_name_is_valid())


class PatientOrRepresentativeDetailsTest(ComplexNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'PatientOrRepresentativeDetails'
        self.model = PatientOrRepresentativeDetails
        self.children = [
            'patient_or_representative_code',
            'patient_or_representative_description',
        ]
        self.required_children = [
            'patient_or_representative_code',
        ]
        code = '2'
        description = 'representative'
        self.test_model = self.model(PatientOrRepresentativeCode(code))
        self.reference = E.PatientOrRepresentativeDetails(
            E.PatientOrRepresentativeCode(code),
            E.PatientOrRepresentativeDescription(description)
        )


class PatientOrRepresentativeCodeTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'PatientOrRepresentativeCode'
        self.model = PatientOrRepresentativeCode
        self.text = '1'
        self.correct_values = [
            1, 2, '1', '2',
        ]
        self.incorrect_values = [
            'a', '3', 3, 'abc', 0.1
        ]


class PatientOrRepresentativeDescriptionTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'PatientOrRepresentativeDescription'
        self.model = PatientOrRepresentativeDescription
        self.text = 'Someone\'s uncle'
        self.correct_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(1)]),
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(396)]),
        ]
        self.incorrect_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(397)]),
        ]


class PatientOrRepresentativeSignatureTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'PatientOrRepresentativeSignature'
        self.model = PatientOrRepresentativeSignature
        self.text = True
        self.correct_values = [
            True, False
        ]
        self.incorrect_values = [
            'a', 1, 0, 9
        ]


class PatientOrRepresentativeSignatureDateTest(LeafNodeMixin,
                                               unittest.TestCase):
    def setUp(self):
        self.node_name = 'PatientOrRepresentativeSignatureDate'
        self.model = PatientOrRepresentativeSignatureDate
        self.text = datetime.date.today()
        self.correct_values = [
            datetime.date(2015, 12, 10),
        ]
        self.incorrect_values = [
            'now',
            '2015/12/10',
        ]


class TitleTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'Title'
        self.model = Title
        self.text = 'Ms'
        self.correct_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(1)]),
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(35)]),
        ]
        self.incorrect_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(36)]),
        ]


class DeclarationOnCompletionTest(ComplexNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'DeclarationOnCompletion'
        self.model = DeclarationOnCompletion
        self.children = [
            'patient_or_representative_details',
            'patient_or_representative_signature',
            'patient_or_representative_signature_date',
            'representative_name',
            'representative_address',
        ]
        self.required_children = [
            'patient_or_representative_details',
            'patient_or_representative_signature',
            'patient_or_representative_signature_date',
        ]
        code = '1'
        description = 'patient'
        date = datetime.date.today()
        signature = True
        self.test_model = self.model(
            PatientOrRepresentativeDetails(
                PatientOrRepresentativeCode(code)
            ),
            PatientOrRepresentativeSignature(signature),
            PatientOrRepresentativeSignatureDate(date)
        )
        self.reference = E.DeclarationOnCompletion(
            E.PatientOrRepresentativeDetails(
                E.PatientOrRepresentativeCode(code),
                E.PatientOrRepresentativeDescription(description),
            ),
            E.PatientOrRepresentativeSignature(
                utils.get_string_repr(signature)
            ),
            E.PatientOrRepresentativeSignatureDate(
                utils.get_string_repr(date)
            ),
        )

    def test_optional_values_create_correct_tree(self):
        code = 2
        description = 'mum'
        signature = True
        date = datetime.date.today()
        family_name = 'Ham'
        birth_family_name = 'Spam'
        given_name = 'tasty'
        addr_line_1 = 'P Sherman'
        addr_line_2 = '42 Wallaby Way'
        addr_line_3 = 'Sydney'
        post_code = 'AB16 5HH'
        self.test_model = self.model(
            PatientOrRepresentativeDetails(
                PatientOrRepresentativeCode(code),
                PatientOrRepresentativeDescription(description),
            ),
            PatientOrRepresentativeSignature(signature),
            PatientOrRepresentativeSignatureDate(date),
            RepresentativeName(
                FamilyName(family_name),
                GivenName(given_name),
                BirthFamilyName(birth_family_name),
            ),
            RepresentativeAddress(
                AddressLines(
                    [
                        AddressLine(addr_line_1),
                        AddressLine(addr_line_2),
                        AddressLine(addr_line_3),
                    ]
                ),
                PostCode(post_code),
            )
        )
        self.reference = E.DeclarationOnCompletion(
            E.PatientOrRepresentativeDetails(
                E.PatientOrRepresentativeCode(utils.get_string_repr(code)),
                E.PatientOrRepresentativeDescription(description),
            ),
            E.PatientOrRepresentativeSignature(
                utils.get_string_repr(signature)),
            E.PatientOrRepresentativeSignatureDate(
                utils.get_string_repr(date)),
            E.RepresentativeName(
                E.FamilyName(family_name),
                E.BirthFamilyName(birth_family_name),
                E.GivenName(given_name),
            ),
            E.RepresentativeAddress(
                E.AddressLines(
                    E.AddressLine(addr_line_1),
                    E.AddressLine(addr_line_2),
                    E.AddressLine(addr_line_3),
                ),
                E.PostCode(post_code),
            ),
        )

        super(DeclarationOnCompletionTest,
              self).test_to_etree_produces_correct_tree()


class PaymentClaimDetailsTest(ComplexNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'PaymentClaimDetails'
        self.model = PaymentClaimDetails
        self.children = [
            'treatment_amount_details',
            'exemption_details',
        ]
        self.required_children = ['treatment_amount_details']
        self.test_model = self.model(
            TreatmentAmountDetails(
                PatientCharge(50),
                AmountClaimed(40),
            )
        )
        self.reference = E.PaymentClaimDetails(
            E.TreatmentAmountDetails(
                E.PatientCharge('50.00'),
                E.AmountClaimed('40.00'),
            )
        )


class TreatmentAmountDetailsTest(ComplexNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'TreatmentAmountDetails'
        self.model = TreatmentAmountDetails
        self.children = [
            'patient_charge',
            'amount_claimed',
        ]
        self.required_children = [
            'patient_charge',
            'amount_claimed',
        ]
        charge = 10.20
        charge_text = '10.20'
        amount = 10.50
        amount_text = '10.50'
        self.test_model = self.model(
            PatientCharge(charge),
            AmountClaimed(amount)
        )
        self.reference = E.TreatmentAmountDetails(
            E.PatientCharge(charge_text),
            E.AmountClaimed(amount_text),
        )


class PatientChargeTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'PatientCharge'
        self.model = PatientCharge
        self.validator_count = 2
        self.text = 80.20
        self.expected_text = '80.20'
        self.correct_values = [
            5, 15.1, 1000.15,
        ]
        self.incorrect_values = [
            10.005,
            'ab',
            -1,
        ]

    def test_text_returns_formatted_version(self):
        values = [
            (80.20, '80.20'),
            (15, '15.00'),
            (1, '1.00'),
            (19.1, '19.10'),
        ]
        for value, expected_result in values:
            model = self.model(value)
            self.assertEqual(model.text, expected_result)


class AmountClaimedTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'AmountClaimed'
        self.model = AmountClaimed
        self.validator_count = 2
        self.text = 80.20
        self.expected_text = '80.20'
        self.correct_values = [
            5, 15.1, 1000.15,
        ]
        self.incorrect_values = [
            10.005,
            'ab',
            -1,
        ]

    def test_text_returns_formatted_version(self):
        values = [
            (80.20, '80.20'),
            (15, '15.00'),
            (1, '1.00'),
            (19.1, '19.10'),
        ]
        for value, expected_result in values:
            model = self.model(value)
            self.assertEqual(model.text, expected_result)


class ExemptionDetailsTest(ComplexNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'ExemptionDetails'
        self.model = ExemptionDetails
        self.children = [
            'exemption_or_remission_status_details',
            'exemption_or_remission_status_changed',
            'exemption_or_remission_status_on_acceptance_details'
        ]
        self.required_children = []
        self.test_model = self.model()
        self.reference = E.ExemptionDetails()


class ExemptionOrRemissionStatusDetailsTest(
        ComplexNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'ExemptionOrRemissionStatusDetails'
        self.model = ExemptionOrRemissionStatusDetails
        self.children = [
            'benefit_category_details',
            'hc_certificate_details',
            'benefit_recipient_name',
            'benefit_recipient_date_of_birth',
            'benefit_recipient_ni_number',
            'evidence_not_produced'
        ]
        self.required_children = [
            'benefit_category_details',
            'evidence_not_produced'
        ]
        code = '18'
        description = 'Under 18 years old'
        evidence = False
        self.test_model = self.model(
            benefit_category_details=BenefitCategoryDetails(
                BenefitCategoryCode(code),
            ),
            evidence_not_produced=EvidenceNotProduced(evidence)
        )
        self.reference = E.ExemptionOrRemissionStatusDetails(
            E.BenefitCategoryDetails(
                E.BenefitCategoryCode(code),
                E.BenefitCategoryDescription(description),
            ),
            E.EvidenceNotProduced(utils.get_string_repr(evidence)),
        )

    def test_h2_code_requires_hc2_cert(self):
        model = self.model(
            BenefitCategoryDetails(
                BenefitCategoryCode('H2')
            ),
        )
        self.assertFalse(model.h2_code_requires_hc2_cert())
        model.hc_certificate_details = HcCertificateDetails(
            hc2_certificate_number=Hc2CertificateNumber('aaaaa')
        )
        self.assertTrue(model.h2_code_requires_hc2_cert())

    def test_h3_code_requires_hc3_cert(self):
        model = self.model(
            BenefitCategoryDetails(
                BenefitCategoryCode('H3')
            ),
        )
        self.assertFalse(model.h3_code_requires_hc3_cert())
        model.hc_certificate_details = HcCertificateDetails(
            hc3_certificate_number=Hc3CertificateNumber('aaaaa')
        )
        self.assertTrue(model.h3_code_requires_hc3_cert())

    def test_optional_creates_correct_tree(self):
        code = 'H3'
        description = 'Credit from Tax'
        number = 'abcdef'
        family_name = 'Spam'
        given_name = 'Ham'
        title = 'Ms'
        birth_family_name = 'Flan'
        date = datetime.date.today()
        nat_insurance = 'abcdef'
        evidence = False
        self.test_model = self.model(
            BenefitCategoryDetails(
                BenefitCategoryCode(code),
                BenefitCategoryDescription(description),
            ),
            HcCertificateDetails(
                hc3_certificate_number=Hc3CertificateNumber(number)
            ),
            BenefitRecipientName(
                family_name=FamilyName(family_name),
                given_name=GivenName(given_name),
                title=Title(title),
                birth_family_name=BirthFamilyName(birth_family_name)
            ),
            BenefitRecipientDateOfBirth(date),
            BenefitRecipientNiNumber(nat_insurance),
            EvidenceNotProduced(evidence),
        )
        self.reference = E.ExemptionOrRemissionStatusDetails(
            E.BenefitCategoryDetails(
                E.BenefitCategoryCode(code),
                E.BenefitCategoryDescription(description),
            ),
            E.HCCertificateDetails(
                E.HC3CertificateNumber(number),
            ),
            E.BenefitRecipientName(
                E.Title(title),
                E.FamilyName(family_name),
                E.BirthFamilyName(birth_family_name),
                E.GivenName(given_name),
            ),
            E.BenefitRecipientDateOfBirth(utils.get_string_repr(date)),
            E.BenefitRecipientNINumber(nat_insurance),
            E.EvidenceNotProduced(utils.get_string_repr(evidence)),
        )
        super(ExemptionOrRemissionStatusDetailsTest,
              self).test_to_etree_produces_correct_tree()


class BenefitCategoryDetailsTest(ComplexNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'BenefitCategoryDetails'
        self.model = BenefitCategoryDetails
        self.children = [
            'benefit_category_code',
            'benefit_category_description',
        ]
        self.required_children = [
            'benefit_category_code',
        ]
        code = '18'
        description = 'Under 18 years old'
        self.test_model = self.model(BenefitCategoryCode(code))
        self.reference = E.BenefitCategoryDetails(
            E.BenefitCategoryCode(code),
            E.BenefitCategoryDescription(description),
        )


class BenefitCategoryCodeTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'BenefitCategoryCode'
        self.model = BenefitCategoryCode
        self.text = 'ED'
        self.correct_values = [
            'H3', '18', 'ED', 'H2', 'PR', 'NM', 'IS', 'JS', 'TC', 'PC',
            'ES', 'UC', 'ed', 'pr', 'h3', 'js',
        ]
        self.incorrect_values = [
            'H4', 'abc', 'H318'
        ]

    def test_text_converted_to_upper(self):
        values = [
            ('h3', 'H3'),
            ('es', 'ES'),
        ]
        for value, expected_result in values:
            model = self.model(value)
            self.assertEqual(model.text, expected_result)


class BenefitCategoryDescriptionTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'BenefitCategoryDescription'
        self.model = BenefitCategoryDescription
        self.text = 'A wee student'
        self.correct_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(1)]),
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(396)]),
        ]
        self.incorrect_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(397)]),
        ]


class HcCertificateDetailsTest(ComplexNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'HCCertificateDetails'
        self.model = HcCertificateDetails
        self.children = [
            'hc2_certificate_number',
            'hc3_certificate_number'
        ]
        self.required_children = []
        number = 'sssssssss'
        self.test_model = self.model(
            hc2_certificate_number=Hc2CertificateNumber(number),
        )
        self.reference = E.HCCertificateDetails(
            E.HC2CertificateNumber(number)
        )

    def test_multiple_certs_not_valid(self):
        number = 'sssssssss'
        model = self.model(
            hc2_certificate_number=Hc2CertificateNumber(number),
            hc3_certificate_number=Hc3CertificateNumber(number),
        )
        self.assertFalse(model.only_one_certificate_included())

    def test_single_certs_are_valid(self):
        number = 'sssssssss'
        model = self.model(
            hc2_certificate_number=Hc2CertificateNumber(number),
        )
        self.assertTrue(model.only_one_certificate_included())
        model = self.model(
            hc3_certificate_number=Hc3CertificateNumber(number),
        )
        self.assertTrue(model.only_one_certificate_included())


class Hc2CertificateNumberTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'HC2CertificateNumber'
        self.model = Hc2CertificateNumber
        self.text = 'aaaaa'
        self.correct_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(1)]),
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(9)]),
        ]
        self.incorrect_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(10)]),
        ]


class Hc3CertificateNumberTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'HC3CertificateNumber'
        self.model = Hc3CertificateNumber
        self.text = 'aaaaa'
        self.correct_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(1)]),
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(9)]),
        ]
        self.incorrect_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(10)]),
        ]


class BenefitRecipientNameTest(ComplexNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'BenefitRecipientName'
        self.model = BenefitRecipientName
        self.required_children = [
            'family_name',
            'given_name'
        ]
        self.children = [
            'title',
            'family_name',
            'birth_family_name',
            'given_name'
        ]
        family_name = 'Spam'
        given_name = 'Ham'
        self.test_model = self.model(
            family_name=FamilyName(family_name),
            given_name=GivenName(given_name),
        )
        self.reference = E.BenefitRecipientName(
            E.FamilyName(family_name),
            E.GivenName(given_name),
        )

    def test_optional_creates_correct_tree(self):
        family_name = 'Spam'
        given_name = 'Ham'
        title = 'Ms'
        birth_family_name = 'Flan'
        self.test_model = self.model(
            family_name=FamilyName(family_name),
            given_name=GivenName(given_name),
            title=Title(title),
            birth_family_name=BirthFamilyName(birth_family_name)
        )
        self.reference = E.BenefitRecipientName(
            E.Title(title),
            E.FamilyName(family_name),
            E.BirthFamilyName(birth_family_name),
            E.GivenName(given_name),
        )
        super(BenefitRecipientNameTest,
              self).test_to_etree_produces_correct_tree()


class BenefitRecipientDateOfBirthTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'BenefitRecipientDateOfBirth'
        self.model = BenefitRecipientDateOfBirth
        self.text = datetime.date.today()
        self.correct_values = [
            datetime.date(2000, 5, 5),
        ]
        self.incorrect_values = [
            'now',
            '2015/12/10',
        ]


class BenefitRecipientNiNumberTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'BenefitRecipientNINumber'
        self.model = BenefitRecipientNiNumber
        self.text = 'aaaaa'
        self.correct_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(1)]),
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(9)]),
        ]
        self.incorrect_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(10)]),
        ]


class EvidenceNotProducedTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'EvidenceNotProduced'
        self.model = EvidenceNotProduced
        self.text = True
        self.correct_values = [
            True, False
        ]
        self.incorrect_values = [
            'a', 1, 0, 9
        ]


class ExemptionOrRemissionStatusChangedTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'ExemptionOrRemissionStatusChanged'
        self.model = ExemptionOrRemissionStatusChanged
        self.text = True
        self.correct_values = [
            True, False
        ]
        self.incorrect_values = [
            'a', 1, 0, 9
        ]


class ExemptionOrRemissionStatusOnAcceptanceDetailsTest(
        ComplexNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'ExemptionOrRemissionStatusOnAcceptanceDetails'
        self.model = ExemptionOrRemissionStatusOnAcceptanceDetails
        self.children = [
            'benefit_category_details',
            'hc_certificate_details',
            'benefit_recipient_name',
            'benefit_recipient_date_of_birth',
            'benefit_recipient_ni_number',
            'evidence_not_produced'
        ]
        self.required_children = [
            'benefit_category_details',
            'evidence_not_produced'
        ]
        code = 'ED'
        description = 'Aged 18 and in full-time education'
        evidence = False
        self.test_model = self.model(
            benefit_category_details=BenefitCategoryDetails(
                BenefitCategoryCode(code),
            ),
            evidence_not_produced=EvidenceNotProduced(evidence)
        )
        self.reference = E.ExemptionOrRemissionStatusOnAcceptanceDetails(
            E.BenefitCategoryDetails(
                E.BenefitCategoryCode(code),
                E.BenefitCategoryDescription(description),
            ),
            E.EvidenceNotProduced(utils.get_string_repr(evidence)),
        )

    def test_h2_code_requires_hc2_cert(self):
        model = self.model(
            BenefitCategoryDetails(
                BenefitCategoryCode('H2')
            ),
        )
        self.assertFalse(model.h2_code_requires_hc2_cert())
        model.hc_certificate_details = HcCertificateDetails(
            hc2_certificate_number=Hc2CertificateNumber('aaaaa')
        )
        self.assertTrue(model.h2_code_requires_hc2_cert())

    def test_h3_code_requires_hc3_cert(self):
        model = self.model(
            BenefitCategoryDetails(
                BenefitCategoryCode('H3')
            ),
        )
        self.assertFalse(model.h3_code_requires_hc3_cert())
        model.hc_certificate_details = HcCertificateDetails(
            hc3_certificate_number=Hc3CertificateNumber('aaaaa')
        )
        self.assertTrue(model.h3_code_requires_hc3_cert())

    def test_recipient_name_required_if_code_es_is_js_pc_uc_tc(self):
        for code in ['ES', 'IS', 'JS', 'PC', 'UC', 'TC']:
            model = self.model(
                BenefitCategoryDetails(
                    BenefitCategoryCode(code)
                )
            )
            self.assertFalse(
                model.benefit_recipient_name_required_if_code_appropriate()
            )
            model.benefit_recipient_name = BenefitRecipientName(
                family_name='Foo', given_name='Bar')
            self.assertTrue(
                model.benefit_recipient_name_required_if_code_appropriate()
            )

    def test_optional_creates_correct_tree(self):
        code = 'H3'
        description = 'Credit from Tax'
        number = 'abcdef'
        family_name = 'Spam'
        given_name = 'Ham'
        title = 'Ms'
        birth_family_name = 'Flan'
        date = datetime.date.today()
        nat_insurance = 'abcdef'
        evidence = False
        self.test_model = self.model(
            BenefitCategoryDetails(
                BenefitCategoryCode(code),
                BenefitCategoryDescription(description),
            ),
            HcCertificateDetails(
                hc3_certificate_number=Hc3CertificateNumber(number)
            ),
            BenefitRecipientName(
                family_name=FamilyName(family_name),
                given_name=GivenName(given_name),
                title=Title(title),
                birth_family_name=BirthFamilyName(birth_family_name)
            ),
            BenefitRecipientDateOfBirth(date),
            BenefitRecipientNiNumber(nat_insurance),
            EvidenceNotProduced(evidence),
        )
        self.reference = E.ExemptionOrRemissionStatusOnAcceptanceDetails(
            E.BenefitCategoryDetails(
                E.BenefitCategoryCode(code),
                E.BenefitCategoryDescription(description),
            ),
            E.HCCertificateDetails(
                E.HC3CertificateNumber(number),
            ),
            E.BenefitRecipientName(
                E.Title(title),
                E.FamilyName(family_name),
                E.BirthFamilyName(birth_family_name),
                E.GivenName(given_name),
            ),
            E.BenefitRecipientDateOfBirth(utils.get_string_repr(date)),
            E.BenefitRecipientNINumber(nat_insurance),
            E.EvidenceNotProduced(utils.get_string_repr(evidence)),
        )
        super(ExemptionOrRemissionStatusOnAcceptanceDetailsTest,
              self).test_to_etree_produces_correct_tree()


class ObservationsDetailsTest(ComplexNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'ObservationsDetails'
        self.model = ObservationsDetails
        self.children = ['observations_text']
        self.required_children = ['observations_text']
        text = "I've seen things you people wouldn't believe"
        self.test_model = self.model(
            ObservationsText(text)
        )
        self.reference = E.ObservationsDetails(
            E.ObservationsText(text)
        )

    def test_add_to_observations(self):
        model = self.model()
        obs_1 = ObservationsText('abc')
        obs_2 = ObservationsText('def')
        obs_3 = ObservationsText('ghi')
        model.add_observations_text(obs_1)
        self.assertEqual(len(model.observations_text), 1)
        self.assertEqual(model.observations_text, [obs_1])
        model.add_observations_text(obs_2)
        self.assertEqual(len(model.observations_text), 2)
        self.assertEqual(model.observations_text, [obs_1, obs_2])
        model.add_observations_text(obs_3)
        self.assertEqual(len(model.observations_text), 3)
        self.assertEqual(model.observations_text, [obs_1, obs_2, obs_3])


class ObservationsTextTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'ObservationsText'
        self.model = ObservationsText
        self.text = 'Mighty interesting'
        self.correct_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(1)]),
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(1999)]),
        ]
        self.incorrect_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(2000)]),
        ]


class DentalClaimRequestChildrenTest(unittest.TestCase):
    def test_check_child_names_set_correctly(self):
        reference = [
            'form_details',
            'claim_reference_details',
            'practitioner_organisation_details',
            'practitioner_details',
            'patient_details',
            'continuation_case_details',
            'treatment_details',
            'dentists_declaration_details',
            'payment_claim_details',
            'observations_details',
        ]
        self.assertListEqual(DentalClaimRequest().child_names, reference)


class ObservationsDetailsChildrenTest(unittest.TestCase):
    def test_check_children_retrieved_correectly(self):
        text = 'This is interesting'
        observations_text = [ObservationsText(text)]
        model = ObservationsDetails(observations_text)
        self.assertListEqual(model.children, [observations_text])

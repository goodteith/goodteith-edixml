#!/usr/bin/env python3
# vim:fileencoding=utf-8

import datetime
import logging
import uuid
import unittest
try:  # python 3
    from unittest.mock import patch, Mock
except ImportError as e:  # python 2
    from mock import patch, Mock

from edixml.interfaces import (
    _create_app_service_name,
    _create_app_sttl,
    _create_app_trans_date_time,
    _create_app_trans_id,
    _create_app_trans_step_ref,
    _create_body_definitions,
    _create_msg_priority,
    _create_msg_status,
    _create_sender_details,
    _create_software,
    build_transaction_header,
    MESSAGE_CLASS_DICT,
)

from edixml.models.transaction_header import (
    AppBodyTypeQty,
    AppServiceName,
    AppSttl,
    AppTransDateTime,
    AppTransId,
    AppTransStepRef,
    BodyDefinitions,
    MsgCategory,
    OrganisationId,
    OrganisationName,
    OrganisationType,
    SenderDetails,
    Software,
    TransactionHeader,
)
from edixml.models.dental_claim import (
    DentalClaimRequest,
    PractitionerOrganisationDetails,
)
from edixml.models.organisation import (
    IdValue,
    OrganisationId as OrgId,
    OrganisationName as OrgName,
    OrganisationType as OrgType,
)
from edixml.settings import APPSTTL_VALUES


# Disable logging so it doesn't clutter up from tests
logging.disable(logging.CRITICAL)


class DentalClaimRequestTransactionHeader(unittest.TestCase):
    def setUp(self):
        self.tree = DentalClaimRequest(
            practitioner_organisation_details=PractitionerOrganisationDetails(
                organisation_type=OrgType('dentalpractice'),
                organisation_name=OrgName('CallDent'),
                organisation_id=OrgId(
                    IdValue('04810'),
                ),
            ),
        )

    def test_builder_generates_transaction_header(self):
        cls = build_transaction_header(self.tree)
        self.assertIsInstance(cls, TransactionHeader)

    def test_builder_has_msg_priority(self):
        cls = build_transaction_header(self.tree)
        self.assertEqual(cls.msg_priority.text, 'normal')

    def test_builder_has_msg_status(self):
        cls = build_transaction_header(self.tree)
        self.assertIn(cls.msg_status.text, ['live', 'test'])

    def test_builder_has_sender_details(self):
        cls = build_transaction_header(self.tree)
        self.assertIsInstance(cls.sender_details, SenderDetails)

    def test_sender_details_contains_correct_elements(self):
        cls = build_transaction_header(self.tree)
        self.assertIsInstance(
            cls.sender_details.organisation_type,
            OrganisationType
        )
        self.assertIsInstance(
            cls.sender_details.organisation_id,
            OrganisationId,
        )
        self.assertIsInstance(
            cls.sender_details.organisation_name,
            OrganisationName,
        )

    def test_builder_creates_software_details(self):
        cls = build_transaction_header(self.tree)
        self.assertIsInstance(cls.software, Software)

    def test_builder_creates_app_service_name(self):
        cls = build_transaction_header(self.tree)
        self.assertIsInstance(cls.app_service_name, AppServiceName)

    def test_builder_creates_app_sttl(self):
        cls = build_transaction_header(self.tree)
        self.assertIsInstance(cls.app_sttl, AppSttl)

    def test_builder_creates_app_trans_id(self):
        cls = build_transaction_header(self.tree)
        self.assertIsInstance(cls.app_trans_id, AppTransId)

    def test_builder_creates_app_trans_step_ref(self):
        cls = build_transaction_header(self.tree)
        self.assertIsInstance(cls.app_trans_step_ref, AppTransStepRef)

    def test_builder_creates_app_trans_date_time(self):
        cls = build_transaction_header(self.tree)
        self.assertIsInstance(cls.app_trans_date_time, AppTransDateTime)

    @unittest.skip("Skip until body definitions completed")
    def test_builder_includes_msg_category(self):
        cls = build_transaction_header(self.tree)
        self.assertIsInstance(cls.msg_category, MsgCategory)

    def test_build_does_not_include_app_last_block_id(self):
        cls = build_transaction_header(self.tree)
        self.assertIsNone(cls.app_last_block_id)

    @unittest.skip("Skip until body definitions completed")
    def test_builder_sets_app_body_type_qty_correctly(self):
        cls = build_transaction_header(self.tree)
        self.assertIsInstance(cls.app_body_type_qty, AppBodyTypeQty)

    @unittest.skip("Skip until the adaptor is set up")
    def test_builder_creates_body_definitions(self):
        cls = build_transaction_header(self.tree)
        self.assertIsInstance(cls.body_definitions, BodyDefinitions)


class MsgPriorityTest(unittest.TestCase):
    def test_msg_priority_for_claim_request_is_normal(self):
        tree = DentalClaimRequest()
        cls = _create_msg_priority(tree)
        self.assertEqual(cls.text, 'normal')

    @patch('edixml.interfaces.IMMEDIATE_MESSAGES', ('spam'))
    def test_msg_priority_for_immediate_messages_is_immediate(self):
        tree = Mock()
        tree.node_name = 'spam'
        cls = _create_msg_priority(tree)
        self.assertEqual(cls.text, 'immediate')


class MsgStatusTest(unittest.TestCase):
    def test_msg_status_is_set(self):
        tree = Mock()
        cls = _create_msg_status(tree)
        self.assertIn(cls.text, ['live', 'test'])

    @patch('edixml.interfaces.MSG_STATUS', 'test')
    def test_msg_status_is_test(self):
        tree = Mock()
        cls = _create_msg_status(tree)
        self.assertEqual(cls.text, 'test')


class SenderDetailsTest(unittest.TestCase):
    def test_sender_details_is_created(self):
        organisation_type = Mock(text='dentalpractice')
        organisation_name = Mock(text='A dental practice')
        id_value = Mock(text='04810')
        organisation_id = Mock(id_value=id_value)
        prac_org_details = Mock(
            organisation_type=organisation_type,
            organisation_name=organisation_name,
            organisation_id=organisation_id,
        )
        tree = Mock(
            practitioner_organisation_details=prac_org_details,
            node_name='DentalClaimRequest',
        )
        cls = _create_sender_details(tree)
        self.assertIsInstance(cls, SenderDetails)

    def test_sender_details_organisation_type_is_created(self):
        organisation_type = Mock(text='dentalpractice')
        organisation_name = Mock(text='A dental practice')
        id_value = Mock(text='04810')
        organisation_id = Mock(id_value=id_value)
        prac_org_details = Mock(
            organisation_type=organisation_type,
            organisation_name=organisation_name,
            organisation_id=organisation_id,
        )
        tree = Mock(
            practitioner_organisation_details=prac_org_details,
            node_name='DentalClaimRequest',
        )
        cls = _create_sender_details(tree)
        self.assertIsInstance(cls.organisation_type, OrganisationType)

    def test_sender_details_organisation_id_is_created(self):
        organisation_type = Mock(text='dentalpractice')
        organisation_name = Mock(text='A dental practice')
        id_value = Mock(text='04810')
        organisation_id = Mock(id_value=id_value)
        prac_org_details = Mock(
            organisation_type=organisation_type,
            organisation_name=organisation_name,
            organisation_id=organisation_id,
        )
        tree = Mock(
            practitioner_organisation_details=prac_org_details,
            node_name='DentalClaimRequest',
        )
        cls = _create_sender_details(tree)
        self.assertIsInstance(cls.organisation_id, OrganisationId)

    def test_sender_details_organisation_name_is_created(self):
        organisation_type = Mock(text='dentalpractice')
        organisation_name = Mock(text='A dental practice')
        id_value = Mock(text='04810')
        organisation_id = Mock(id_value=id_value)
        prac_org_details = Mock(
            organisation_type=organisation_type,
            organisation_name=organisation_name,
            organisation_id=organisation_id,
        )
        tree = Mock(
            practitioner_organisation_details=prac_org_details,
            node_name='DentalClaimRequest',
        )
        cls = _create_sender_details(tree)
        self.assertIsInstance(cls.organisation_name, OrganisationName)

    def test_reconciliation_sender_details_are_created(self):
        organisation_type = Mock(text='dentalpractice')
        organisation_name = Mock(text='A dental practice')
        id_value = Mock(text='04810')
        organisation_id = Mock(id_value=id_value)
        prac_org_details = Mock(
            organisation_type=organisation_type,
            organisation_name=organisation_name,
            organisation_id=organisation_id,
        )
        tree = Mock(
            requesting_organisation_details=prac_org_details,
            node_name='DentalReconciliationFileRequest',
        )
        cls = _create_sender_details(tree)
        self.assertIsInstance(cls, SenderDetails)


class SoftwareTest(unittest.TestCase):
    def test_software_details_are_created(self):
        cls = _create_software()
        self.assertIsInstance(cls, Software)

    @patch('edixml.interfaces.SOFTWARE_NAME', 'Smash and Grab')
    def test_software_name_is_taken_from_settings(self):
        cls = _create_software()
        self.assertEqual(cls.product_name.text, 'Smash and Grab')

    @patch('edixml.interfaces.SOFTWARE_AUTHOR', 'The Professionals')
    def test_software_author_is_taken_from_settings(self):
        cls = _create_software()
        self.assertEqual(cls.product_author.text, 'The Professionals')

    def test_softare_version_is_included(self):
        cls = _create_software()
        self.assertIn('ProductVersion', cls.product_name.attributes)


class AppServiceNameTest(unittest.TestCase):
    def test_app_service_name_is_created(self):
        cls = _create_app_service_name()
        self.assertIsInstance(cls, AppServiceName)

    def test_app_service_name_has_correct_content(self):
        cls = _create_app_service_name()
        self.assertEqual(cls.text, 'Dental')

    def test_app_service_name_has_correct_attributes(self):
        cls = _create_app_service_name()
        self.assertIn('ServiceVersion', cls.attributes)


class AppSttlTest(unittest.TestCase):
    def test_app_sttl_set_correctly(self):
        for name, time in APPSTTL_VALUES.items():
            tree = Mock(node_name=name)
            cls = _create_app_sttl(tree)
            self.assertEqual(cls.text, time)


class AppTransIdTest(unittest.TestCase):
    def test_app_trans_id_set_correctly(self):
        cls = _create_app_trans_id()
        self.assertIsInstance(cls, AppTransId)
        self.assertIsInstance(cls.text, uuid.UUID)


class AppTransStepRefTest(unittest.TestCase):
    def test_app_trans_step_ref_created(self):
        cls = _create_app_trans_step_ref()
        self.assertIsInstance(cls, AppTransStepRef)
        self.assertEqual(cls.text, 1)


class AppTransDateTimeTest(unittest.TestCase):
    def test_app_trans_date_time_created(self):
        cls = _create_app_trans_date_time()
        self.assertIsInstance(cls, AppTransDateTime)
        self.assertIsInstance(cls.text, datetime.datetime)


class BodyDefinitionsTest(unittest.TestCase):
    def test_body_definition_set_correctly(self):
        tree = Mock(node_name='DentalClaimRequest')
        cls = _create_body_definitions(tree)
        self.assertIsInstance(cls, BodyDefinitions)

    def test_body_definition_list_set_correctly(self):
        tree = Mock(node_name='DentalClaimRequest')
        cls = _create_body_definitions(tree)
        self.assertEqual(len(cls.body_definition), 1)

    def test_body_definition_name_set_correctly(self):
        for name, _ in MESSAGE_CLASS_DICT.items():
            tree = Mock(node_name=name)
            cls = _create_body_definitions(tree)
            defn = cls.body_definition[0]
            self.assertEqual(defn.name.text, name)

    def test_body_definition_position_set_correctly(self):
        tree = Mock(node_name='DentalClaimRequest')
        cls = _create_body_definitions(tree)
        defn = cls.body_definition[0]
        self.assertEqual(defn.position.text, 1)

    def test_body_definition_count_set_correctly(self):
        tree = Mock(node_name='DentalClaimRequest')
        cls = _create_body_definitions(tree)
        defn = cls.body_definition[0]
        self.assertEqual(defn.count.text, 1)

    def test_body_definition_signing_set_if_tree_contains_sig(self):
        tree = Mock(node_name='DentalClaimRequest', signature='abc')
        cls = _create_body_definitions(tree)
        defn = cls.body_definition[0]
        self.assertEqual(defn.signing.text, True)

    def test_body_definition_signing_set_if_tree_doesnt_contains_sig(self):
        tree = Mock(node_name='DentalClaimRequest', signature=None)
        cls = _create_body_definitions(tree)
        defn = cls.body_definition[0]
        self.assertEqual(defn.signing.text, False)

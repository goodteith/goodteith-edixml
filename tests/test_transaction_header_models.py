#!/usr/bin/env python3
# vim:fileencoding=utf-8

import datetime
import logging
import random
import unittest
try:  # python 3
    from unittest.mock import patch, PropertyMock
except ImportError as e:  # python 2
    from mock import patch, PropertyMock
import uuid

from lxml.builder import E

from edixml.exceptions.base import XmlValidationError, XmlMissingChildError
from edixml.models.transaction_header import (
    AppLastBlockId,
    AppServiceName,
    AppSttl,
    AppTransDateTime,
    AppTransId,
    AppTransStepRef,
    BodyDefinition,
    BodyDefinitions,
    Count,
    MsgPriority,
    MsgStatus,
    Name,
    OrganisationId,
    OrganisationName,
    OrganisationType,
    Position,
    ProductAuthor,
    ProductName,
    SenderDetails,
    Signing,
    Software,
    TransactionHeader
)
from edixml import utils
from .utils import (
    etrees_equal, LeafNodeWithAttributeMixin, LeafNodeMixin, ALPHANUM_CHARS,
    ComplexNodeMixin,

)

# Disable logging so it doesn't clutter up from tests
logging.disable(logging.CRITICAL)


class AppTransStepRefTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'AppTransStepRef'
        self.model = AppTransStepRef
        self.text = 5
        self.correct_values = [
            1, 5, 100
        ]
        self.incorrect_values = [
            'a', -1, 12.1
        ]


class AppTransDateTimeTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'AppTransDateTime'
        self.model = AppTransDateTime
        self.text = datetime.datetime.now()
        self.correct_values = [
            datetime.datetime(2000, 12, 1),
            datetime.datetime(2020, 1, 5, 15, 12),
        ]
        self.incorrect_values = [
            '2000121',
            '15th October',
            12
        ]


class AppSttlTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'AppSTTL'
        self.model = AppSttl
        self.text = 5
        self.correct_values = [
            1, 10, 500, 10000
        ]
        self.incorrect_values = [
            1.0, 'test', 1j
        ]


class AppLastBlockIdTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'AppLastBlockID'
        self.model = AppLastBlockId
        self.text = uuid.uuid4()
        self.correct_values = [
            uuid.uuid4(),
        ]
        self.incorrect_values = [
            'this is not a uuid',
            15
        ]


class AppTransIdTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'AppTransID'
        self.model = AppTransId
        self.text = uuid.uuid4()
        self.correct_values = [
            uuid.uuid4(),
        ]
        self.incorrect_values = [
            'this is not a uuid',
            15
        ]


class MsgPriorityTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'MsgPriority'
        self.model = MsgPriority
        self.text = 'immediate'
        self.correct_values = [
            'normal',
            'immediate',
        ]
        self.incorrect_values = [
            'narmal',
            'fast',
            'right now immediately',
        ]

    def test_msg_priority_defaults_to_normal(self):
        self.assertEqual(self.model().text, 'normal')


class MsgStatusTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'MsgStatus'
        self.model = MsgStatus
        self.text = 'live'
        self.correct_values = [
            'live',
            'test',
        ]
        self.incorrect_values = [
            'anything',
            'else',
            'Live',
            'Test',
        ]

    def test_msg_status_defaults_to_test(self):
        self.assertEqual(self.model().text, 'test')


class OrganisationNameTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'OrganisationName'
        self.model = OrganisationName
        self.text = 'Callander Dental Practice'
        self.correct_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(1)]),
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(255)])
        ]
        self.incorrect_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(256)])
        ]


class OrganisationIdTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'OrganisationID'
        self.model = OrganisationId
        self.text = 'abcde'
        self.correct_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(1)]),
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(10)]),
        ]
        self.incorrect_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(11)]),
        ]


class OrganisationTypeTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'OrganisationType'
        self.model = OrganisationType
        self.text = 'dentalpractice'
        self.correct_values = [
            'dentalpractice', 'pharmacy', 'GPpractice', 'system',
            'optometrypractice'
        ]
        self.incorrect_values = [
            'dontalpractice', 'phurmacy', 'GpPractice', 'syStem',
            'optomatryproctice'
        ]


class ProductNameTest(LeafNodeWithAttributeMixin, unittest.TestCase):

    def setUp(self):
        self.node_name = 'ProductName'
        self.model = ProductName
        self.text = 'Ham'
        self.attributes = {'ProductVersion': '1.5'}
        self.correct_values = ['Any', 'value', 'will', 'do']
        self.incorrect_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(51)])
        ]
        self.correct_attributes = [{'ProductVersion': '1.7'}]
        self.incorrect_attributes = []


class ProductAuthorTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'ProductAuthor'
        self.model = ProductAuthor
        self.text = "Goodteith Software"
        self.correct_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(1)]),
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(50)])
        ]
        self.incorrect_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(51)])
        ]


class AppServiceNameTest(LeafNodeWithAttributeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'AppServiceName'
        self.model = AppServiceName
        self.text = 'Dental'
        self.attributes = {'AppServiceVersion': 1.4}
        self.correct_values = [
            'AMS', 'MAS', 'CMS', 'PRS', 'Dental', 'Ophthalmic'
        ]
        self.incorrect_values = [
            'EMS', 'MSA', 'CMT', 'PRN', 'Dootal', 'Opthalmic'
        ]
        self.correct_attributes = [
            {'AppServiceVersion': 0.1},
            {'AppServiceVersion': 9.2},
            {'AppServiceVersion': 100.1},
            {'AppServiceVersion': 8.0},
        ]
        self.incorrect_attributes = [
            {'AppServiceVersion': 'v1'},
            {'AppServiceVersion': 3},
            {'AppServiceVersion': 'alpha'},
        ]


class SoftwareTest(unittest.TestCase):

    def setUp(self):
        self.model = Software()

    def test_class_has_node_name(self):
        self.assertEqual('Software', self.model.node_name)

    def test_to_etree_returns_correct_tree(self):
        product_name = 'Spam'
        product_version = '1.0'
        product_author = 'Ham'
        model = Software(
            product_name=ProductName(
                product_name, attributes={'ProductVersion': product_version}),
            product_author=ProductAuthor(product_author))
        reference = E.Software(
            E.ProductName(product_name, ProductVersion=product_version),
            E.ProductAuthor(product_author))
        if not etrees_equal(reference, model.as_etree()):
            self.fail("{} did not produce the correct etree.".format(
                model.node_name))


class SenderDetailsTest(unittest.TestCase):

    def setUp(self):
        self.model = SenderDetails()

    def test_class_has_node_name(self):
        self.assertEqual('SenderDetails', self.model.node_name)

    def test_to_etree_returns_correct_tree(self):
        o_type = 'dentalpractice'
        o_id = 'calldent1'
        o_name = 'Callander Dental Practice'
        s = SenderDetails(
            organisation_type=OrganisationType(o_type),
            organisation_id=OrganisationId(o_id),
            organisation_name=OrganisationName(o_name))
        reference = E.SenderDetails(
            E.OrganisationType(o_type),
            E.OrganisationID(o_id),
            E.OrganisationName(o_name))
        if not etrees_equal(reference, s.as_etree()):
            self.fail("{} did not produce correct etree".format(
                'SenderDetails'))


class BodyDefinitionsTest(unittest.TestCase):

    def setUp(self):
        self.model = BodyDefinitions()

    def test_body_definition_is_list(self):
        self.assertIsInstance(self.model.body_definition, list)

    def test_class_has_node_name(self):
        self.assertEqual('BodyDefinitions', self.model.node_name)

    def test_to_etree_returns_correct_tree(self):
        name1 = 'pear'
        position1 = 1
        count1 = 1
        signing1 = Signing(False)
        name2 = 'oval'
        position2 = 2
        count2 = 1
        signing2 = Signing(False)
        b1 = BodyDefinition(name=Name(name1), position=Position(position1),
                            count=Count(count1), signing=signing1)
        b2 = BodyDefinition(name=Name(name2), position=Position(position2),
                            count=Count(count2), signing=signing2)
        self.model.add_body_definition(b1)
        self.model.add_body_definition(b2)
        reference = E.BodyDefinitions(
            E.BodyDefinition(
                E.Name(name1),
                E.Position(str(position1)),
                E.Count(str(count1)),
                E.Signing('false')),
            E.BodyDefinition(
                E.Name(name2),
                E.Position(str(position2)),
                E.Count(str(count2)),
                E.Signing('false')),
        )
        if not etrees_equal(reference, self.model.as_etree()):
            self.fail("{} did not produce correct etree".format(
                'BodyDefinitions'))

    def test_add_body_defintion_appends_to_list(self):
        orig_len = len(self.model.body_definition)
        self.model.add_body_definition(BodyDefinition(name=Name('foo')))
        new_len = len(self.model.body_definition)
        self.assertGreater(new_len, orig_len)

    def test_position_is_updated_automatically(self):
        name1 = 'pear'
        signing1 = Signing(False)
        name2 = 'oval'
        signing2 = Signing(False)
        name3 = 'stick'
        signing3 = Signing(False)
        b1 = BodyDefinition(name=Name(name1), signing=signing1)
        b2 = BodyDefinition(name=Name(name2), signing=signing2)
        b3 = BodyDefinition(name=Name(name3), signing=signing3)
        for body in [b1, b2, b3]:
            self.model.add_body_definition(body)
        retrieved_bodies = self.model.body_definition
        for index, body in enumerate(retrieved_bodies, 1):
            self.assertEqual(body.position.text, index)


class BodyDefinitionTest(ComplexNodeMixin, unittest.TestCase):

    def setUp(self):
        self.model = BodyDefinition
        self.node_name = 'BodyDefinition'
        self.required_children = [
            'name', 'position', 'count', 'signing'
        ]
        name = "DentalClaimRequest"
        position = 1
        count = 1
        signing = False
        self.test_model = self.model(
            name=Name(name),
            position=Position(position),
            count=Count(count),
            signing=Signing(signing),
        )
        self.reference = E.BodyDefinition(
            E.Name(name),
            E.Position(utils.get_string_repr(position)),
            E.Count(utils.get_string_repr(count)),
            E.Signing(utils.get_string_repr(signing)),
        )


class TransactionHeaderTest(unittest.TestCase):

    def setUp(self):
        self.model = TransactionHeader()

    def test_class_has_node_name(self):
        self.assertEqual('TransactionHeader', self.model.node_name)

    def test_cutoff_date_triggers_validation_failure(self):
        app_trans_date_time = datetime.datetime.now() - datetime.timedelta(
            seconds=11)
        app_sttl = 10
        model = TransactionHeader(
            app_trans_date_time=AppTransDateTime(app_trans_date_time),
            app_sttl=AppSttl(app_sttl)
        )
        with self.assertRaises(XmlValidationError):
            model.app_trans_date_time_is_valid()

    def test_to_etree_returns_correct_tree(self):
        schema_version = '1.0'
        attributes = {'SchemaVersion': schema_version}
        msg_priority = 'normal'
        msg_status = 'test'
        organisation_type = 'dentalpractice'
        organisation_id = 'calldent01'
        organisation_name = 'Callander Dental Practice'
        product_name = 'Goodteith'
        product_version = '1.0'
        product_author = 'Goodteith Software'
        app_service_name = 'Dental'
        app_service_version = 1.0
        app_service_name_obj = AppServiceName(
            app_service_name,
            attributes={'AppServiceVersion': app_service_version})
        app_sttl = 3600
        app_trans_id = uuid.uuid4()
        app_trans_step_ref = 1
        app_trans_date_time = datetime.datetime.now()
        msg_category = 'single'
        name = 'Dental Claim'
        position = 1
        count = 1
        signing = Signing(False)
        sender_details = SenderDetails(
            organisation_type=OrganisationType(organisation_type),
            organisation_id=OrganisationId(organisation_id),
            organisation_name=OrganisationName(organisation_name))
        software = Software(
            product_name=ProductName(
                product_name, attributes={"ProductVersion": product_version}),
            product_author=ProductAuthor(product_author))
        body_definition = BodyDefinition(
            name=Name(name),
            position=Position(position),
            count=Count(count),
            signing=signing)
        body_definitions = BodyDefinitions(
            body_definition=[body_definition])
        transaction_header = TransactionHeader(
            body_definitions=body_definitions,
            msg_priority=MsgPriority(msg_priority),
            msg_status=MsgStatus(msg_status),
            sender_details=sender_details,
            software=software,
            app_service_name=app_service_name_obj,
            app_sttl=AppSttl(app_sttl),
            app_trans_id=AppTransId(app_trans_id),
            app_trans_step_ref=AppTransStepRef(app_trans_step_ref),
            app_trans_date_time=AppTransDateTime(app_trans_date_time),
            attributes=attributes,
            )

        reference = E.TransactionHeader(
            E.MsgPriority(msg_priority),
            E.MsgStatus(msg_status),
            E.SenderDetails(
                E.OrganisationType(organisation_type),
                E.OrganisationID(organisation_id),
                E.OrganisationName(organisation_name)),
            E.Software(
                E.ProductName(
                    product_name, {"ProductVersion": product_version}),
                E.ProductAuthor(product_author)),
            E.AppServiceName(
                app_service_name, {
                    "AppServiceVersion": utils.get_string_repr(
                        app_service_version)
                }),
            E.AppSTTL(str(app_sttl)),
            E.AppTransID(str(app_trans_id)),
            E.AppTransStepRef(str(app_trans_step_ref)),
            E.AppTransDateTime(
                utils.datetime_as_iso_string(app_trans_date_time)),
            E.MsgCategory(msg_category),
            E.AppBodyTypeQty(str(1)),
            E.BodyDefinitions(
                E.BodyDefinition(
                    E.Name(name),
                    E.Position(str(position)),
                    E.Count(str(count)),
                    E.Signing('false'))),
            {"SchemaVersion": utils.get_string_repr(schema_version)}
        )

        if not etrees_equal(reference, transaction_header.as_etree()):
            self.fail("{} did not produce correct etree".format(
                'TransactionHeader'))

    def test_sender_details_required(self):
        schema_version = '1.0'
        attributes = {'SchemaVersion': schema_version}
        msg_priority = 'normal'
        msg_status = 'test'
        organisation_type = 'dentalpractice'
        organisation_id = 'calldent01'
        organisation_name = 'Callander Dental Practice'
        product_name = 'Goodteith'
        product_version = '1.0'
        product_author = 'Goodteith Software'
        app_service_name = 'Dental'
        app_service_version = 1.0
        app_service_name_obj = AppServiceName(
            app_service_name,
            attributes={'AppServiceVersion': app_service_version})
        app_sttl = 3600
        app_trans_id = uuid.uuid4()
        app_trans_step_ref = 1
        app_trans_date_time = datetime.datetime.now()
        name = 'Dental Claim'
        position = 1
        count = 1
        signing = Signing(False)
        sender_details = SenderDetails(
            organisation_type=OrganisationType(organisation_type),
            organisation_id=OrganisationId(organisation_id),
            organisation_name=OrganisationName(organisation_name))
        software = Software(
            product_name=ProductName(
                product_name, attributes={"ProductVersion": product_version}),
            product_author=ProductAuthor(product_author))
        body_definition = BodyDefinition(
            name=Name(name),
            position=Position(position),
            count=Count(count),
            signing=signing)
        body_definitions = BodyDefinitions(
            body_definition=[body_definition])
        transaction_header = TransactionHeader(
            body_definitions=body_definitions,
            msg_priority=MsgPriority(msg_priority),
            msg_status=MsgStatus(msg_status),
            software=software,
            app_service_name=app_service_name_obj,
            app_sttl=AppSttl(app_sttl),
            app_trans_id=AppTransId(app_trans_id),
            app_trans_step_ref=AppTransStepRef(app_trans_step_ref),
            app_trans_date_time=AppTransDateTime(app_trans_date_time),
            attributes=attributes,
            )
        with self.assertRaises(XmlMissingChildError):
            transaction_header.model_is_valid()
        transaction_header.sender_details = sender_details
        self.assertTrue(transaction_header.model_is_valid())

    def test_msg_status_required(self):
        schema_version = '1.0'
        attributes = {'SchemaVersion': schema_version}
        msg_priority = 'normal'
        msg_status = 'test'
        organisation_type = 'dentalpractice'
        organisation_id = 'calldent01'
        organisation_name = 'Callander Dental Practice'
        product_name = 'Goodteith'
        product_version = '1.0'
        product_author = 'Goodteith Software'
        app_service_name = 'Dental'
        app_service_version = 1.0
        app_service_name_obj = AppServiceName(
            app_service_name,
            attributes={'AppServiceVersion': app_service_version})
        app_sttl = 3600
        app_trans_id = uuid.uuid4()
        app_trans_step_ref = 1
        app_trans_date_time = datetime.datetime.now()
        name = 'Dental Claim'
        position = 1
        count = 1
        signing = Signing(False)
        sender_details = SenderDetails(
            organisation_type=OrganisationType(organisation_type),
            organisation_id=OrganisationId(organisation_id),
            organisation_name=OrganisationName(organisation_name))
        software = Software(
            product_name=ProductName(
                product_name, attributes={"ProductVersion": product_version}),
            product_author=ProductAuthor(product_author))
        body_definition = BodyDefinition(
            name=Name(name),
            position=Position(position),
            count=Count(count),
            signing=signing)
        body_definitions = BodyDefinitions(
            body_definition=[body_definition])
        transaction_header = TransactionHeader(
            body_definitions=body_definitions,
            msg_priority=MsgPriority(msg_priority),
            sender_details=sender_details,
            software=software,
            app_service_name=app_service_name_obj,
            app_sttl=AppSttl(app_sttl),
            app_trans_id=AppTransId(app_trans_id),
            app_trans_step_ref=AppTransStepRef(app_trans_step_ref),
            app_trans_date_time=AppTransDateTime(app_trans_date_time),
            attributes=attributes,
        )
        with self.assertRaises(XmlMissingChildError):
            transaction_header.model_is_valid()
        transaction_header.msg_status = MsgStatus(msg_status)
        self.assertTrue(transaction_header.model_is_valid())

    def test_app_body_type_qty_calculates_correctly(self):
        body_definition = BodyDefinition(
            name=Name('Dental Claim'),
            position=Position(1),
            count=Count(1),
            signing=Signing(False))
        body_definitions = BodyDefinitions(
            body_definition=[body_definition])
        transaction_header = TransactionHeader(
            body_definitions=body_definitions)
        self.assertEqual(transaction_header.app_body_type_qty.text, 1)
        body_definition_2 = BodyDefinition(
            name=Name('Dental Prior Approval'),
            position=Position(2),
            count=Count(1),
            signing=Signing(False))
        body_definitions.add_body_definition(body_definition_2)
        self.assertEqual(transaction_header.app_body_type_qty.text, 2)

    def test_msg_category_calculates_correctly(self):
        b1 = BodyDefinition(name=Name('DentalClaimRequest'))
        b2 = BodyDefinition(name=Name('DentalClaimRequest'))
        b3 = BodyDefinition(name=Name('DentalClaimRequest'))
        b4 = BodyDefinition(name=Name('DentalPriorApprovalRequest'))
        body_definitions = BodyDefinitions(body_definition=[b1])
        transaction_header = TransactionHeader(
            body_definitions=body_definitions)
        self.assertEqual(transaction_header.msg_category.text, 'single')
        body_definitions.add_body_definition(b2)
        body_definitions.add_body_definition(b3)
        self.assertEqual(transaction_header.msg_category.text, 'multiple')
        body_definitions.add_body_definition(b4)
        self.assertEqual(transaction_header.msg_category.text, 'fixed')

    @patch('edixml.models.transaction_header.TransactionHeader.msg_category',
           new_callable=PropertyMock, return_value='multiple')
    def test_app_last_block_id_validation_fails_empty(self, mock_msg_category):
        transaction_header = TransactionHeader()
        self.assertFalse(transaction_header.app_last_block_id_is_valid())

    @patch('edixml.models.transaction_header.TransactionHeader.msg_category',
           new_callable=PropertyMock, return_value='multiple')
    def test_app_last_block_id_validation_succeeds(self, mock_msg_category):
        transaction_header = TransactionHeader(
            app_last_block_id=AppLastBlockId(uuid.uuid4()))
        self.assertTrue(transaction_header.app_last_block_id_is_valid())

    @patch('edixml.models.transaction_header.TransactionHeader.msg_category',
           new_callable=PropertyMock, return_value='single')
    def test_app_last_block_id_empty_valid_with_single(self, _):
        transaction_header = TransactionHeader()
        self.assertTrue(transaction_header.app_last_block_id_is_valid())

    @patch('edixml.models.transaction_header.TransactionHeader.msg_category',
           new_callable=PropertyMock, return_value='single')
    def test_app_last_block_id_set_invalid_with_single(self, _):
        transaction_header = TransactionHeader(
            app_last_block_id=AppLastBlockId(uuid.uuid4()))
        self.assertFalse(transaction_header.app_last_block_id_is_valid())


if __name__ == '__main__':
    unittest.main()

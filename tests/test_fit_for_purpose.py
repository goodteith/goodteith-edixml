#!/usr/bin/env python3
# vim:fileencoding=utf-8


import datetime
import logging
import os
import tempfile
import unittest


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FIXTURES_DIR = os.path.join(BASE_DIR, 'tests', 'fixtures')
# Disable logging so it doesn't clutter up from tests
logging.disable(logging.CRITICAL)


class Scenario(unittest.TestCase):

    yaml_selections = {}

    def build_yaml_file(self):
        message_type = self.yaml_selections['message_type']
        dentist_number = self.yaml_selections['dentist']
        patient_number = self.yaml_selections['patient']
        data_filename = self.yaml_selections['data_filename']
        request_date = self.yaml_selections.get(
            'request_datetime',
            datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S')
        )

        practice_info = os.path.join(
            FIXTURES_DIR, 'dental_practice_details.yaml'
        )
        dentist_info = os.path.join(
            FIXTURES_DIR, 'dentist_{:01}_details.yaml'.format(dentist_number)
        )
        patient_info = os.path.join(
            FIXTURES_DIR, 'patient_{:02}_details.yaml'.format(patient_number)
        )
        data_info = os.path.join(
            FIXTURES_DIR, '{}.yaml'.format(data_filename)
        )
        self.input_file = tempfile.NamedTemporaryFile(mode='w', delete=False)
        self.input_file.write('RequestDateTime: {}\n'.format(request_date))
        self.input_file.write('MessageType: {}\n'.format(message_type))
        files_to_read = [
            practice_info, dentist_info, patient_info, data_info
        ]
        for filename in files_to_read:
            with open(filename) as f:
                data = f.read()
                self.input_file.write(data)
                self.input_file.write('\n')

        self.input_file.close()

    def tearDown(self):
        os.unlink(self.input_file.name)


class ClaimScenariosTest(Scenario):

    yaml_selections = {
        'message_type': 'DentalClaimRequest',
        'dentist': 1,
        'patient': 1,
        'data_filename': 'claim_valid',
    }


# #####################   CLAIM TEST SCENARIOS   #######################
# 2.  Creation of a claim record followed by submission of a claim with
#     errors; edit and submit reclaim with no errors
# 3.  Creation of a claim record followed by submission of a claim with
#     errors: edit and save for later submission
# 4.  Creation of a claim record followed by submission of a claim with
#     errors; abandon claim
# 5.  Creation of a claim record followed by submission of a claim with
#     errors; edit and submit reclaim, again with errors, edit and
#     submit reclaim with no errors.
# 6.  Creation and save of valid claim record
# 7.  Creation and save of claim record which has client system
#     validation errors.
# 8.  Selection of previously created valid claim record, followed by:
#     - Submission of claim (no errors)
# 9.  Selection of previously created valid claim record, followed by:
#     - edit of claim record
#     - submission of claim (no errors)
# 10. Selection of previously created claim record (data entry in
#     progress), followed by:
#     - edit of claim record
#     - submission of claim (no errors)
# 11. Selection of previously created valid claim record, followed by:
#     - submission of claim (with errors)
#     - edit of claim record
#     - submission of reclaim (no errors)
# 12. Selection of previously submitted valid claim record, followed by:
#     - display of claim record
# 13. Selection of previously submitted invalid claim record, followed
#     by:
#     - edit of claim record
#     - submission of reclaim (no errors)
# 14. Selection of previously submitted invalid claim record, followed
#     by:
#     - edit of claim record
#     - submission of reclaim (with errors)
#     - edit of claim record
#     - submission of reclaim (no errors)
# 15. Selection of claim record where the timer has expired, followed
#     by:
#     - resubmission of same claim (no errors)
# 16. Selection of claim record where the timer has expired, followed
#     by:
#     - edit of claim record
#     - submission of reclaim (no errors)
# 17. Creation of claim record followed by submission of a claim where
#     an Exception (Ack) response is received, followed by:
#     - resubmission of same claim (which is in effect a reclaim) (no
#       errors)
# 18. Creation of claim record followed by submission of a claim where
#     an Exception (Ack) response is received, followed by:
#     - resubmission of same claim (which is in effect a reclaim) (with
#       errors)
#     - edit of claim record
#     - submission of reclaim (no errors)
# 19. Creation of claim record followed by submission of a claim where
#     an Exception (Ack) response is received, followed by:
#     - selection of claim record
#     - resubmission of same claim (which is in effect a reclaim) (no
#       errors)
# 20. Creation of a claim record followed by submission of a claim with
#     errors multiple times, up to the claim submission count limit
# 21. Creation and save of claim record, followed by:
#     - selection of claim record
#     - abandonment of claim
# 22. 'Batch' selection and submission of a set of claim records which
#      are 'ready for claim'
# 23. 'Batch' selection and submission of a set of claim records where
#     the timer has expired.
# 24. 'Batch' selection and submission of a set of claim records where a
#     system error has been received.
# 25. Creation of a claim record followed by attempted submission of
#     claim with validation errors.
# 26. Administrative users - creation/save of claim record but do not
#     have ability to submit claim
# ######################################################################

# ##############   Reconciliation File Test Scenarios   ################

# 27. Submission of a Reconciliation File request message; receipt and
#     processing of a Reconciliation File response message containing
#     reconciliation records
# 28. Submission of a Reconciliation File request message; receipt and
#     processing of a Reconciliation File response message containing no
#     reconciliation records
# 29. Submission of a Reconciliation File request message; receipt and
#     processing of a Reconciliation File response message containing
#     reconciliation records which have already been returned.
# 30. Submission of a Reconciliation File request message; receipt and
#     processing of a Reconciliation File response message where an
#     Exception (Ack) response is received
# 31. Submission of a Reconciliation File request message; receipt and
#     processing of a Reconciliation File response message containing
#     errors returned from the payment system

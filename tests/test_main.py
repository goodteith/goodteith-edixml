#!/usr/bin/env python3
# vim:fileencoding=utf-8

try:
    import io
except ImportError:
    import StringIO as io
import logging
import os
import tempfile
import unittest
import xml.parsers.expat

from lxml import etree
from lxml.builder import ElementMaker

from .context import edixml
from .utils import etrees_equal


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SCHEMA_DIR = os.path.join(BASE_DIR, 'schema')
# Disable logging so it doesn't clutter up from tests
logging.disable(logging.CRITICAL)


class ArgumentsTest(unittest.TestCase):

    def setUp(self):
        self.parser = edixml.process_claim.create_parser()
        self.valid_input = tempfile.NamedTemporaryFile()
        self.invalid_input = tempfile.NamedTemporaryFile()

    def tearDown(self):
        self.valid_input.close()
        self.invalid_input.close()

    def test_help_argument_supported(self):
        usage_string = self.parser.format_usage()
        self.assertIn('-h', usage_string)

    @unittest.skip('Need to redesign')
    def test_short_config_argument_supported(self):
        with tempfile.NamedTemporaryFile() as temp:
            parser = self.parser.parse_args([
                '-c', temp.name, self.valid_input.name])
        self.assertTrue(parser.config)

    @unittest.skip('Need to redesign')
    def test_long_config_argument_supported(self):
        with tempfile.NamedTemporaryFile() as temp:
            parser = self.parser.parse_args([
                '--config', temp.name, self.valid_input.name])
        self.assertTrue(parser.config)

    @unittest.skip('Need to redesign')
    def test_short_print_argument_supported(self):
        parser = self.parser.parse_args(['-p', self.valid_input.name])
        parser.input.close()
        self.assertTrue(parser.print_to_screen)

    @unittest.skip('Need to redesign')
    def test_long_print_argument_supported(self):
        parser = self.parser.parse_args(['--print', self.valid_input.name])
        parser.input.close()
        self.assertTrue(parser.print_to_screen)

    @unittest.skip('Need to redesign')
    def test_input_arg_read_in(self):
        with tempfile.NamedTemporaryFile(delete=False) as temp:
            parser = self.parser.parse_args([temp.name])
        parser.input.close()
        self.assertEqual(temp.name, parser.input.name)


class ValidateDataTest(unittest.TestCase):
    def test_valid_input_parses_correctly(self):
        test_data = {'MessageType': 'Dental Claim'}
        result = edixml.process_claim.validate_data(test_data)
        self.assertTrue(result)

    def test_invalid_input_raises_error(self):
        test_data = {'Messagetype': 'Blethers'}
        with self.assertRaises(edixml.process_claim.InvalidYamlFileError):
            edixml.process_claim.validate_data(test_data)

    def test_invalid_message_type_raises_exception(self):
        test_data = {'MessageType': 'Spam Ham'}
        with self.assertRaises(edixml.process_claim.UnknownMessageTypeError):
            edixml.process_claim.validate_data(test_data)

    def test_missing_message_type_raises_exception(self):
        test_data = {'MessageType': None}
        with self.assertRaises(edixml.process_claim.UnknownMessageTypeError):
            edixml.process_claim.validate_data(test_data)


class YamlToDictTest(unittest.TestCase):

    def setUp(self):
        self.VALID_YAML = """MessageType: 'Dental Claim'"""
        self.INVALID_YAML = """Spam: Ham: Eggs"""

    def test_valid_yaml_converted_correctly(self):
        source = io.StringIO(edixml.utils.to_unicode(self.VALID_YAML))
        self.assertTrue(edixml.process_claim.get_data_from_yaml_file(source))

    def test_invalid_yaml_raises_exception(self):
        source = io.StringIO(edixml.utils.to_unicode(self.INVALID_YAML))
        with self.assertRaises(edixml.process_claim.InvalidYamlFileError):
            edixml.process_claim.get_data_from_yaml_file(source)


class OpenEnvelopeTest(unittest.TestCase):

    def test_no_envelope_returns_original_tree(self):
        E = ElementMaker(
            namespace="http://www.eps.nds.scot.nhs.uk",
            nsmap={
                None: "http://www.eps.nds.scot.nhs.uk",
                "xsd": "http://www.w3.org/2001/XMLSchema",
                "ds": "http://www.w3.org/2000/09/xmldsig#"
            })
        ref = E.DentalReconciliationFileResponse(
            E.Foo('bar'),
        )
        tree = etree.ElementTree(ref)
        result = edixml.connection.open_envelope(tree)
        self.assertTrue(etrees_equal(ref, result),
                        "No envelope did not return same message")

    def test_patient_details_request_env_returns_correct_tree(self):
        E = ElementMaker(
            namespace="http://www.eps.nds.scot.nhs.uk",
            nsmap={
             None: "http://www.eps.nds.scot.nhs.uk",
             "xsd": "http://www.w3.org/2001/XMLSchema",
             "ds": "http://www.w3.org/2000/09/xmldsig#"
            })
        ref = (
            E.DentalPatientDetailsResponseEnv(
                E.TransactionHeader(
                    E.Foo('bar'),
                ),
                E.AppBodies(
                    E.DentalPatientDetailsResponse(
                        E.Spam('ham'),
                    )
                )
            )
        )
        tree = etree.ElementTree(ref)
        expected_result = E.DentalPatientDetailsResponse(
            E.Spam('ham'),
        )
        result = edixml.connection.open_envelope(tree)
        self.assertTrue(
            etrees_equal(expected_result, result),
            "Open envelope did not return patient details response")

    def test_claim_response_env_returns_correct_tree(self):
        E = ElementMaker(
            namespace="http://www.eps.nds.scot.nhs.uk",
            nsmap={
             None: "http://www.eps.nds.scot.nhs.uk",
             "xsd": "http://www.w3.org/2001/XMLSchema",
             "ds": "http://www.w3.org/2000/09/xmldsig#"
            })
        ref = E.DentalClaimResponseEnv(
            E.TransactionHeader(
                E.Foo('bar'),
            ),
            E.AppBodies(
                E.DentalClaimResponse(
                    E.Spam('ham'),
                )
            ),
        )
        tree = etree.ElementTree(ref)
        expected_result = E.DentalClaimResponse(
            E.Spam('ham'),
        )
        result = edixml.connection.open_envelope(tree)
        self.assertTrue(
            etrees_equal(expected_result, result),
            "Open envelope did not return claim response")

    def test_reconciliation_response_env_returns_correct_tree(self):
        E = ElementMaker(
            namespace="http://www.eps.nds.scot.nhs.uk",
            nsmap={
                None: "http://www.eps.nds.scot.nhs.uk",
                "xsd": "http://www.w3.org/2001/XMLSchema",
                "ds": "http://www.w3.org/2000/09/xmldsig#"
            })
        ref = E.DentalReconciliationFileResponseEnv(
            E.TransactionHeader(
                E.Foo('bar'),
            ),
            E.AppBodies(
                E.DentalReconciliationFileResponse(
                    E.Spam('ham'),
                )
            ),
        )
        tree = etree.ElementTree(ref)
        expected_result = E.DentalReconciliationFileResponse(
            E.Spam('ham'),
        )
        result = edixml.connection.open_envelope(tree)
        self.assertTrue(
            etrees_equal(expected_result, result),
            "Open envelope did not return reconciliation response")


@unittest.skip('Need to redesign')
class EdiClaimFactoryTest(unittest.TestCase):

    def test_dental_claim_creates_dental_claim_class(self):
        test_data = {'MessageType': 'Dental Claim'}
        result = edixml.edi_requests.create_claim(test_data)
        self.assertIsInstance(
            result, edixml.models.dental_claim.DentalClaimRequestEnv)


@unittest.skip('Need to redesign')
class DentalClaimRequestTest(unittest.TestCase):

    def setUp(self):
        self.claim = edixml.models.dental_claim.DentalClaimRequestEnv()

    def test_to_xml_method_exists(self):
        self.assertTrue(hasattr(self.claim, 'to_xml'))

    def test_to_xml_returns_xml(self):
        self.assertIn("xml", edixml.utils.to_str(self.claim.to_xml()))

    def test_to_xml_returns_valid_xml(self):
        parser = xml.parsers.expat.ParserCreate()
        try:
            parser.Parse(self.claim.to_xml())
        except xml.parsers.expat.ExpatError:
            self.fail("DentalClaimRequest did not return valid xml")

    def test_to_xml_uses_correct_namespace(self):
        nsmap = {
            None: 'http://www.eps.nds.scot.nhs.uk',
            'ds': 'http://www.w3.org/2000/09/xmldsig#',
            'xsi': 'http://www.w3.org/2001/XMLSchema-instance'}
        tree = self.claim.root
        self.assertDictEqual(nsmap, tree.nsmap)

    def test_create_tree_skeleton_creates_root(self):
        self.assertTrue(hasattr(self.claim, 'root'))

    @unittest.skip('Need to redesign')
    def test_tree_has_transaction_element(self):
        self.assertIsNotNone(self.claim.root.find('TransactionHeader'),
                             "TransactionHeader does not exist")

    @unittest.skip('Need to redesign')
    def test_tree_has_appbodies_element(self):
        # TODO: Refactor this into separate test. Needs to be created
        # separately.
        self.assertIsNotNone(self.claim.root.find('AppBodies'),
                             "AppBodies does not exist")

    @unittest.skip('Need to redesign')
    def test_tree_has_dentalclaimrequest_element(self):
        # TODO: Refactor this into separate test. Needs to be created
        # separately.
        self.assertIsNotNone(self.claim.root.find('AppBodies').find(
            'DentalClaimRequest'),
            "DentalClaimRequest does not exist")

    @unittest.skip('Need to redesign')
    def test_tree_has_dentalclaimrequest_attribute(self):
        # TODO: What is this attribute for?
        self.assertTrue(hasattr(self.claim, 'dentalclaimrequest'))

    @unittest.skip('Need to redesign')
    def test_env_validates_against_schema(self):
        print(self.claim.to_xml())
        # print(etree.tostring(self.claim.root))
        schema_file = 'DentalClaimRequestEnv-v1-0.xsd'
        xmlschema_doc = etree.parse(os.path.join(SCHEMA_DIR, schema_file))
        xmlschema = etree.XMLSchema(xmlschema_doc)
        self.assertTrue(xmlschema.assertValid(self.claim.root))

    @unittest.skip('Need to redesign')
    def test_dentalclaimrequest_validates_against_schema(self):
        schema_files = [
            'DentalClaimRequest-v1-0.xsd',
            'ePractitionerCommon-v1-0.xsd',
            'ePractitionerXMLSignature-v1-0.xsd',
        ]
        for schema in schema_files:
            xmlschema_doc = etree.parse(os.path.join(SCHEMA_DIR, schema))
            xmlschema = etree.XMLSchema(xmlschema_doc)
            self.assertTrue(
                xmlschema.assertValid(self.claim.dentalclaimrequest))


if __name__ == '__main__':
    unittest.main()

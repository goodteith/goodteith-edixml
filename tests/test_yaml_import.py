#!/usr/bin/env python3
# vim:fileencoding=utf-8

import datetime
import logging
import os
import tempfile
import unittest

from lxml import etree

from edixml.exceptions.base import InvalidYamlFileError, XmlValidationError
from edixml.interfaces import load_yaml_document, yaml_to_class
from edixml.models.address import (
    AddressLine,
    AddressLines,
    PostCode,
)
from edixml.models.dental_claim import (
    AmountClaimed,
    BenefitCategoryCode,
    BenefitRecipientDateOfBirth,
    ContinuationPartNumber,
    CurObservations,
    DateAuthorised,
    DateOfAcceptance,
    DateOfCompletion,
    Declaration,
    DentalChart,
    DentalChartDetails,
    DentalClaimRequest,
    FormDetails,
    FormType,
    FormVersion,
    GdsClaim,
    LowerAnterior,
    LowerLeft,
    LowerRight,
    MaterialCode,
    MaterialDetails,
    NonGdsClaim,
    PatientCharge,
    PatientOrRepresentativeCode,
    PatientOrRepresentativeSignature,
    PatientOrRepresentativeSignatureDate,
    PmsVersion,
    PracticeClaimReference,
    PractitionerPinNumber,
    Source,
    SubmissionClaimCount,
    SupernumeraryFlag,
    ToothSurfaceCode,
    ToothSurfaceDetails,
    Treatment,
    TypeOfClaimCode,
    UpperAnterior,
    UpperLeft,
    UpperRight,
)
from edixml.models.patient_details_request import (
    DentalPatientDetailsRequest,
    InitialSearchReference,
    PatientDetailsRequestReference,
    SecondarySearch,
)
from edixml.models.organisation import (
    IdValue,
    OrganisationName,
    OrganisationType,
)
from edixml.models.patient import (
    ChiNumber,
    PatientDetailsRequestFlag,
    PatientFamilyName,
    PatientGivenName,
)
from edixml.models.person import (
    DateOfBirth,
    BirthFamilyName,
    FamilyName,
    GivenName,
    ProfCode,
    ProfCodeScheme,
    ProfType,
    Sex,
    Title,
)
from edixml.models.reconciliation_file_request import (
    DentalReconciliationFileRequest
)

# Disable logging so it doesn't clutter up from tests
logging.disable(logging.CRITICAL)


class DentalClaimClassHeirarchyFromYaml(unittest.TestCase):

    def setUp(self):
        self.data = 'MessageType: DentalClaimRequest\n'

    def test_yaml_to_class_fails_if_messagetype_misssing(self):
        data = {
            'NotUseful': 'rubbish',
            'FormType':   'GP17-1'
        }
        with self.assertRaises(InvalidYamlFileError):
            yaml_to_class(data)

    def test_yaml_to_class_fails_if_messagetype_unknown(self):
        data = {
            'MessageType': 'SpanishInquisition',
            'FormType':   'GP17-1'
        }
        with self.assertRaises(InvalidYamlFileError):
            yaml_to_class(data)

    def test_dental_claim_request_creates_class(self):
        data = {
            'MessageType': 'DentalClaimRequest',
            'FormType':   'GP17-1'
        }
        cls = yaml_to_class(data)
        self.assertIsInstance(cls, DentalClaimRequest)

    def test_dental_claim_request_creates_form_details(self):
        data = self.data + 'FormType:   GP17-1'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            self.assertIsInstance(cls.form_details, FormDetails)

    def test_dental_claim_request_sets_form_type(self):
        data = self.data + 'FormType:   GP17-1'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            self.assertIsInstance(cls.form_details.form_type, FormType)

    def test_dental_claim_request_sets_form_type_text(self):
        data = self.data + 'FormType:   GP17-1'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            self.assertEqual(cls.form_details.form_type.text, 'GP17-1')

    def test_version_type_is_generated_correctly(self):
        data = self.data + 'VersionType:   0613'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            self.assertIsInstance(cls.form_details.form_version, FormVersion)

    def test_version_type_has_correct_content(self):
        data = self.data + 'VersionType:   0613'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            self.assertEqual(cls.form_details.form_version.text, '0613')

    def test_claim_form_source_generated_correctly(self):
        data = self.data + 'ClaimFormSourceType:   PMS'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            self.assertIsInstance(cls.form_details.source, Source)

    def test_claim_form_source_has_correct_content(self):
        data = self.data + 'ClaimFormSourceType:   PMS'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            self.assertEqual(cls.form_details.source.text, 'PMS')

    def test_practice_claim_reference_generated_correctly(self):
        data = self.data + 'PracticeClaimReference:   001204'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.claim_reference_details.practice_claim_reference
            self.assertIsInstance(ref, PracticeClaimReference)

    def test_practice_claim_reference_has_correct_content(self):
        data = self.data + 'PracticeClaimReference:   001204'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.claim_reference_details.practice_claim_reference
            self.assertEqual(ref.text, '001204')

    def test_submission_claim_count_generated_correctly(self):
        data = self.data + 'SubmissionClaimCount:   1'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.claim_reference_details.submission_claim_count
            self.assertIsInstance(ref, SubmissionClaimCount)

    def test_submission_claim_count_has_correct_content(self):
        data = self.data + 'SubmissionClaimCount:   1'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.claim_reference_details.submission_claim_count
            self.assertEqual(ref.text, 1)

    def test_practitioner_pin_number_generated_correctly(self):
        data = self.data + 'PractitionerPinNumber:   123456'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.claim_reference_details.practitioner_pin_number
            self.assertIsInstance(ref, PractitionerPinNumber)

    def test_practitioner_pin_number_has_correct_content(self):
        data = self.data + 'PractitionerPinNumber:   654321'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.claim_reference_details.practitioner_pin_number
            self.assertEqual(ref.text, '654321')

    def test_date_authorised_generated_correctly(self):
        data = self.data + 'DateAuthorised:   2017-08-23'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.claim_reference_details.date_authorised
            self.assertIsInstance(ref, DateAuthorised)

    def test_date_authorised_has_correct_content(self):
        data = self.data + 'DateAuthorised:   2017-08-23'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.claim_reference_details.date_authorised
            self.assertEqual(ref.text, datetime.date(2017, 8, 23))

    def test_pms_version_generated_correctly(self):
        data = self.data + 'PMSVersion:   CALLDENT:2'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.claim_reference_details.pms_version
            self.assertIsInstance(ref, PmsVersion)

    def test_pms_version_has_correct_content(self):
        data = self.data + 'PMSVersion:   CALLDENT:2'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.claim_reference_details.pms_version
            self.assertEqual(ref.text, 'CALLDENT:2')

    def test_id_value_generated_correctly(self):
        data = self.data + 'IdValue:   04818'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.practitioner_organisation_details
                .organisation_id.id_value
            )
            self.assertIsInstance(ref, IdValue)

    def test_id_value_has_correct_content(self):
        data = self.data + 'IdValue:   04818'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.practitioner_organisation_details
                .organisation_id.id_value
            )
            self.assertEqual(ref.text, '04818')

    def test_organisation_name_generated_correctly(self):
        data = self.data + 'OrganisationName:   CALLANDER DENTAL PRACTICE'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.practitioner_organisation_details.organisation_name
            self.assertIsInstance(ref, OrganisationName)

    def test_organisation_name_has_correct_content(self):
        data = self.data + 'OrganisationName:   CALLANDER DENTAL PRACTICE'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.practitioner_organisation_details.organisation_name
            self.assertEqual(ref.text, 'CALLANDER DENTAL PRACTICE')

    def test_organisation_type_set_by_default(self):
        data = self.data + 'OrganisationName:   CALLANDER DENTAL PRACTICE'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.practitioner_organisation_details.organisation_type
            self.assertIsInstance(ref, OrganisationType)

    def test_organisation_type_has_correct_default_value(self):
        data = self.data + 'OrganisationName:   CALLANDER DENTAL PRACTICE'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.practitioner_organisation_details.organisation_type
            self.assertEqual(ref.text, 'dentalpractice')

    def test_organisation_type_generated_correctly(self):
        data = self.data + 'OrganisationType:   pharmacy'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.practitioner_organisation_details.organisation_type
            self.assertIsInstance(ref, OrganisationType)

    def test_organisation_type_has_correct_value(self):
        data = self.data + 'OrganisationType:   pharmacy'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.practitioner_organisation_details.organisation_type
            self.assertEqual(ref.text, 'pharmacy')

    def test_practice_address_lines_generated_correctly(self):
        data = self.data + (
            'PracticeAddressLine:  |\n'
            '    171 Main Street\n'
            '    Callander'
        )
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.practitioner_organisation_details.address.address_lines
            self.assertIsInstance(ref, AddressLines)

    def test_practice_address_lines_has_correct_values(self):
        data = self.data + (
            'PracticeAddressLine:  |\n'
            '    171 Main Street\n'
            '    Callander'
        )
        reference_lines = ['171 Main Street', 'Callander']
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.practitioner_organisation_details.address.address_lines
            for reference, line in zip(reference_lines, ref.address_lines):
                self.assertEqual(reference, line.text)

    def test_practice_post_code_generated_correctly(self):
        data = self.data + 'PracticePostCode:   FK17 8BJ'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.practitioner_organisation_details.address.post_code
            self.assertIsInstance(ref, PostCode)

    def test_practice_post_code_has_correct_values(self):
        data = self.data + 'PracticePostCode:   FK17 8BJ'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.practitioner_organisation_details.address.post_code
            self.assertEqual(ref.text, 'FK17 8BJ')

    def test_dentist_family_name_generated_correctly(self):
        data = self.data + 'DentistFamilyName:   Jones'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.practitioner_details.hcp_name.structured_name.family_name
            self.assertIsInstance(ref, FamilyName)

    def test_dentist_family_name_has_correct_value(self):
        data = self.data + 'DentistFamilyName:   Jones'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.practitioner_details.hcp_name.structured_name.family_name
            self.assertEqual(ref.text, 'Jones')

    def test_empty_dentist_name_doesnt_throw_exception(self):
        data = self.data + 'DentistFamilyName:   '
        payload = load_yaml_document(data)
        data = list(payload)[0]
        try:
            yaml_to_class(data)
        except XmlValidationError:
            self.fail("Empty dentist name crashed yaml_to_class")

    def test_dentist_given_name_generated_correctly(self):
        data = self.data + 'DentistGivenName:   Suzie'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.practitioner_details.hcp_name.structured_name.given_name
            self.assertIsInstance(ref, GivenName)

    def test_dentist_given_name_has_correct_value(self):
        data = self.data + 'DentistGivenName:   Suzie'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.practitioner_details.hcp_name.structured_name.given_name
            self.assertEqual(ref.text, 'Suzie')

    def test_prof_type_generated_correctly(self):
        data = self.data + 'ProfType:   DentalHealthCareProfessional'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.practitioner_details.prof_type
            self.assertIsInstance(ref, ProfType)

    def test_prof_type_has_correct_value(self):
        data = self.data + 'ProfType:   DentalHealthCareProfessional'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.practitioner_details.prof_type
            self.assertEqual(ref.text, 'DentalHealthCareProfessional')

    def test_prof_type_set_by_default(self):
        data = self.data + 'DentistGivenName:   Suzie'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.practitioner_details.prof_type
            self.assertIsInstance(ref, ProfType)

    def test_prof_type_has_correct_default_value(self):
        data = self.data + 'DentistGivenName:   Suzie'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.practitioner_details.prof_type
            self.assertEqual(ref.text, 'Dentist')

    def test_prof_code_generated_correctly(self):
        data = self.data + 'ProfCode:   15151'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.practitioner_details.hcp_id.prof_code
            self.assertIsInstance(ref, ProfCode)

    def test_prof_code_has_correct_value(self):
        data = self.data + 'ProfCode:   15151'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.practitioner_details.hcp_id.prof_code
            self.assertEqual(ref.text, '15151')

    def test_prof_code_scheme_generated_correctly(self):
        data = self.data + 'ProfCodeScheme:   Dental List Number'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.practitioner_details.hcp_id.prof_code_scheme
            self.assertIsInstance(ref, ProfCodeScheme)

    def test_prof_code_scheme_has_correct_value(self):
        data = self.data + 'ProfCodeScheme:   Dental List Number'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.practitioner_details.hcp_id.prof_code_scheme
            self.assertEqual(ref.text, 'Dental List Number')

    def test_prof_code_set_by_default(self):
        data = self.data + 'ProfCode:   15151'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.practitioner_details.hcp_id.prof_code_scheme
            self.assertIsInstance(ref, ProfCodeScheme)

    def test_prof_code_has_correct_default_value(self):
        data = self.data + 'ProfCode:   15151'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.practitioner_details.hcp_id.prof_code_scheme
            self.assertEqual(ref.text, 'Dental List Number')

    def test_chi_number_generated_correctly(self):
        data = self.data + 'CHINumber:   2123123123'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.patient_details.patient_details.chi_number
            self.assertIsInstance(ref, ChiNumber)

    def test_chi_number_set_correctly(self):
        data = self.data + 'CHINumber:   2123123123'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.patient_details.patient_details.chi_number
            self.assertEqual(ref.text, '2123123123')

    def test_patient_family_name_generated_correctly(self):
        data = self.data + 'PatientFamilyName:   mangrove'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            pat_name = cls.patient_details.patient_details.patient_name
            ref = pat_name.patient_family_name
            self.assertIsInstance(ref, PatientFamilyName)

    def test_patient_family_name_set_correctly(self):
        data = self.data + 'PatientFamilyName:   mangrove'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            pat_name = cls.patient_details.patient_details.patient_name
            ref = pat_name.patient_family_name
            self.assertEqual(ref.text, 'mangrove')

    def test_patient_given_name_generated_correctly(self):
        data = self.data + 'PatientGivenName:   Roger'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            pat_name = cls.patient_details.patient_details.patient_name
            ref = pat_name.patient_given_name
            self.assertIsInstance(ref, PatientGivenName)

    def test_patient_given_name_set_correctly(self):
        data = self.data + 'PatientGivenName:   Roger'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            pat_name = cls.patient_details.patient_details.patient_name
            ref = pat_name.patient_given_name
            self.assertEqual(ref.text, 'Roger')

    def test_patient_address_generated_correctly(self):
        data = self.data + (
            'PatientAddressLine:   | \n'
            '   BILMORY COTTAGE\n'
            '   BRIG O\' MONK\n'
            '   PERTHSHIRE'
        )
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.patient_details.patient_details.address.address_lines
            self.assertIsInstance(ref, AddressLines)

    def test_patient_address_has_correct_values(self):
        data = self.data + (
            'PatientAddressLine:   | \n'
            '   BILMORY COTTAGE\n'
            '   BRIG O\' MONK\n'
            '   PERTHSHIRE'
        )
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.patient_details.patient_details.address.address_lines
            reference_lines = [
                'BILMORY COTTAGE', 'BRIG O\' MONK', 'PERTHSHIRE'
            ]
            for reference, line in zip(reference_lines, ref.address_lines):
                self.assertEqual(reference, line.text)

    def test_patient_post_code_generated_correctly(self):
        data = self.data + 'PatientPostcode:   FK17 8HT'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.patient_details.patient_details.address.post_code
            self.assertIsInstance(ref, PostCode)

    def test_patient_post_code_set_correctly(self):
        data = self.data + 'PatientPostcode:   FK17 8HT'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.patient_details.patient_details.address.post_code
            self.assertEqual(ref.text, 'FK17 8HT')

    def test_patient_date_of_birth_generated_correctly(self):
        data = self.data + 'DateOfBirth:   1980-05-27'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.patient_details.patient_details.date_of_birth
            self.assertIsInstance(ref, DateOfBirth)

    def test_patient_date_of_birth_set_correctly(self):
        data = self.data + 'DateOfBirth:   1980-05-27'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.patient_details.patient_details.date_of_birth
            self.assertEqual(ref.text, datetime.date(1980, 5, 27))

    def test_patient_sex_generated_correctly(self):
        data = self.data + 'Sex:   M'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.patient_details.patient_details.sex
            self.assertIsInstance(ref, Sex)

    def test_patient_sex_set_correctly(self):
        data = self.data + 'Sex:   M'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.patient_details.patient_details.sex
            self.assertEqual(ref.text, 'm')

    def test_continuation_case_part_number_generated_correctly(self):
        data = self.data + 'Continuationcasepartnumber:   1'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.continuation_case_details.continuation_part_number
            self.assertIsInstance(ref, ContinuationPartNumber)

    def test_continuation_case_part_number_set_correctly(self):
        data = self.data + 'Continuationcasepartnumber:   1'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.continuation_case_details.continuation_part_number
            self.assertEqual(ref.text, 1)

    def test_date_of_acceptance_generated_correctly(self):
        data = self.data + 'DateOfAcceptance:   2017-08-21'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (cls.treatment_details
                   .treatment_date_details.date_of_acceptance)
            self.assertIsInstance(ref, DateOfAcceptance)

    def test_date_of_acceptance_set_correctly(self):
        data = self.data + 'DateOfAcceptance:   2017-08-21'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (cls.treatment_details
                   .treatment_date_details.date_of_acceptance)
            self.assertEqual(ref.text, datetime.date(2017, 8, 21))

    def test_cur_observations_generated_correctly(self):
        data = self.data + 'CURObservations:    Here is a fine thing'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.treatment_details.completed_treatment_details
                .cur_observations
            )
            self.assertIsInstance(ref, CurObservations)

    def test_cur_observations_set_correctly(self):
        data = self.data + 'CURObservations:    Here is a fine thing'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.treatment_details.completed_treatment_details
                .cur_observations
            )
            self.assertEqual(ref.text, 'Here is a fine thing')

    def test_date_of_completion_generated_correctly(self):
        data = self.data + 'DateOfCompletion:   2017-08-23'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (cls.treatment_details
                   .treatment_date_details.date_of_completion)
            self.assertIsInstance(ref, DateOfCompletion)

    def test_date_of_completion_set_correctly(self):
        data = self.data + 'DateOfCompletion:   2017-08-23'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.treatment_details.treatment_date_details
                .date_of_completion
            )
            self.assertEqual(ref.text, datetime.date(2017, 8, 23))

    def test_type_of_claim_code_generated_correctly(self):
        data = self.data + 'TypeOfClaimCode:   1'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            treat_details = cls.treatment_details.treatment_type_details
            ref = treat_details.type_of_claim_details.type_of_claim_code
            self.assertIsInstance(ref, TypeOfClaimCode)

    def test_type_of_claim_code_set_correctly(self):
        data = self.data + 'TypeOfClaimCode:   1'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            treat_details = cls.treatment_details.treatment_type_details
            ref = treat_details.type_of_claim_details.type_of_claim_code
            self.assertEqual(ref.text, 1)

    def test_gds_claim_generated_correctly(self):
        data = self.data + 'GDSClaim:   True'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            treat_details = cls.treatment_details.treatment_type_details
            ref = treat_details.gds_claim
            self.assertIsInstance(ref, GdsClaim)

    def test_gds_claim_set_correctly(self):
        data = self.data + 'GDSClaim:   True'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            treat_details = cls.treatment_details.treatment_type_details
            ref = treat_details.gds_claim
            self.assertEqual(ref.text, True)

    def test_non_gds_claim_generated_correctly(self):
        data = self.data + 'NonGDSClaim:   True'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            treat_details = cls.treatment_details.treatment_type_details
            ref = treat_details.non_gds_claim
            self.assertIsInstance(ref, NonGdsClaim)

    def test_non_gds_claim_set_correctly(self):
        data = self.data + 'NonGDSClaim:   True'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            treat_details = cls.treatment_details.treatment_type_details
            ref = treat_details.non_gds_claim
            self.assertEqual(ref.text, True)

    def test_gds_and_non_claim_are_both_set(self):
        data = self.data + (
            'GDSClaim:    True\n'
            'NonGDSClaim:  False'
        )
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.treatment_details.treatment_type_details
            self.assertIsInstance(ref.gds_claim, GdsClaim)
            self.assertIsInstance(ref.non_gds_claim, NonGdsClaim)
            self.assertEqual(ref.gds_claim.text, True)
            self.assertEqual(ref.non_gds_claim.text, False)

    def test_treatment_generated_correctly(self):
        data = self.data + (
            "Treatment:\n"
            "    - 0101:    '01:8.70'\n"
            "    - 1001:    '01:13.60'\n"
            "    - 1421:    '01:17.45:43'"
        )
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            treat_details = cls.treatment_details.completed_treatment_details
            ref = treat_details.treatment
            for item in ref:
                self.assertIsInstance(item, Treatment)

    def test_treatment_tokenized_correctly(self):
        data = self.data + (
            "Treatment:\n"
            "    - 0101:    '01:8.70'\n"
            "    - 1001:    '01:13.60'\n"
            "    - 1421:    '01:17.45:43'\n"
            "\n"
        )
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            treat_details = cls.treatment_details.completed_treatment_details
            ref = treat_details.treatment
            for item in ref:
                self.assertIsInstance(item, Treatment)

    def test_treatment_set_correctly(self):
        data = self.data + (
            "Treatment:\n"
            "    - 0101:    '01:8.70'\n"
            "    - 1001:    '01:13.60'\n"
        )
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            treat_details = cls.treatment_details.completed_treatment_details
            ref = treat_details.treatment
            (first_item, second_item) = sorted(
                ref, key=lambda i: i.treatment_code.text
            )
            self.assertEqual(first_item.treatment_code.text, '0101')
            self.assertEqual(first_item.treatment_qualifier.text, '01')
            self.assertEqual(first_item.treatment_value.text, '8.70')
            self.assertEqual(second_item.treatment_code.text, '1001')
            self.assertEqual(second_item.treatment_qualifier.text, '01')
            self.assertEqual(second_item.treatment_value.text, '13.60')

    def test_treatment_teeth_set_correctly(self):
        data = self.data + (
            "Treatment:\n"
            "    - 1421:    '01:17.45:43'"
        )
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            treat_details = cls.treatment_details.completed_treatment_details
            ref = treat_details.treatment[0].teeth[0]
            self.assertEqual(ref.tooth_notation.text, 43)

    def test_treatment_muliple_teeth_set_correctly(self):
        data = self.data + (
            "Treatment:\n"
            "    - 1421:    '01:17.45:43:44:45'"
        )
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            treat_details = cls.treatment_details.completed_treatment_details
            ref = treat_details.treatment[0].teeth
            self.assertEqual(len(ref), 3)
            for i, t in enumerate([43, 44, 45]):
                self.assertEqual(ref[i].tooth_notation.text, t)

    def test_treatment_supernumerary_detected(self):
        data = self.data + (
            "Treatment:\n"
            "    - 1421:    '01:17.45:43S:44:45S'"
        )
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.treatment_details.completed_treatment_details
                .treatment[0].teeth
            )
            super1 = ref[0]
            super2 = ref[2]
            self.assertTrue(super1.supernumerary_flag.text)
            self.assertTrue(super2.supernumerary_flag.text)

    def test_treatment_free_replacements_not_merged(self):
        data = self.data + (
            'Treatment:\n'
            "    - 1774:    '01:49.30:13'\n"
            "    - 1774:    '01:49.30:12:true'"
        )
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.treatment_details.completed_treatment_details.treatment
            )
            self.assertEqual(len(ref), 2)
            codes = [
                item.treatment_code.text for item in ref
            ]
            qualifiers = [
                item.treatment_qualifier.text for item in ref
            ]
            values = [
                item.treatment_value.text for item in ref
            ]
            teeth = [
                item.teeth[0].tooth_notation.text for item in ref
            ]

            self.assertEqual(['1774', '1774'], codes)
            self.assertEqual(['01', '01'], qualifiers)
            self.assertEqual(['49.30', '49.30'], values)
            self.assertIn(12, teeth)
            self.assertIn(13, teeth)
            super_position = teeth.index(12)

            self.assertEqual(ref[super_position].cur_flag.text, True)

    def test_treatment_multiple_free_replacements_merged(self):
        data = self.data + (
            'Treatment:\n'
            "    - 1774:    '01:49.30:13'\n"
            "    - 1774:    '01:49.30:12:true'\n"
            "    - 1774:    '01:49.30:11:true'\n"
            "    - 1774:    '01:49.30:10'"
        )
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.treatment_details.completed_treatment_details.treatment
            )
            self.assertEqual(len(ref), 2)
            for item in ref:
                self.assertEqual(item.treatment_code.text, '1774')
                self.assertEqual(item.treatment_qualifier.text, '02')
                self.assertEqual(item.treatment_value.text, '98.60')

            teeth = [item.teeth for item in ref]
            teeth_flattened = [y for x in teeth for y in x]
            teeth_codes = [
                tooth.tooth_notation.text for tooth in teeth_flattened
            ]

            for i in range(10, 14):
                self.assertIn(i, teeth_codes)

    def test_treatment_duplicate_codes_merged_to_one_item(self):
        data = self.data + (
            'Treatment:\n'
            "    - 1774:    '01:49.30:13'\n"
            "    - 1774:    '01:49.30:12'"
        )
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.treatment_details.completed_treatment_details.treatment
            )
            self.assertEqual(len(ref), 1)
            item = ref[0]
            self.assertEqual(item.treatment_code.text, '1774')
            self.assertEqual(item.treatment_qualifier.text, '02')
            self.assertEqual(item.treatment_value.text, '98.60')
            teeth = item.teeth
            for i, t in enumerate([13, 12]):
                self.assertEqual(teeth[i].tooth_notation.text, t)

    def test_charting_generated_correctly(self):
        data = self.data + (
            "Charting:\n"
            "    - 18: 'M'\n"
            "    - 14: 'M'\n"
            "    - 28: 'M'\n"
            "    - 38: 'M'\n"
            "    - 36: 'M'\n"
            "    - 42: 'M'\n"
            # Tooth notation: AnnotationCode:Surface:Material:SupernumeraryFlag
            "    - 43: 'F:M:R'\n"
            "    - 46: 'M'"
        )
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.treatment_details.dental_chart_details
            self.assertIsInstance(ref, DentalChartDetails)

    def test_charting_tooth_annotation_set_correctly(self):
        data = self.data + (
            "Charting:\n"
            "    - 18: 'M'"
        )
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.treatment_details.dental_chart_details.dental_chart[0]
            self.assertIsInstance(ref, DentalChart)
            self.assertEqual(ref.tooth_notation.text, 18)
            self.assertEqual(
                ref.annotation_code_details.annotation_code.text, 'M'
            )

    def test_charting_tooth_surface_set_correctly(self):
        data = self.data + (
            "Charting:\n"
            "    - 43: 'F:MOD'"
        )
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.treatment_details.dental_chart_details.dental_chart[0]
            self.assertIsInstance(
                ref.tooth_surface_details, ToothSurfaceDetails
            )
            code = ref.tooth_surface_details.tooth_surface_code
            self.assertIsInstance(code, ToothSurfaceCode)
            self.assertEqual(code.text, 'MOD')

    def test_charting_material_details_set_correctly(self):
        data = self.data + (
            "Charting:\n"
            "    - 43: 'F:MOD:R'"
        )
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.treatment_details.dental_chart_details.dental_chart[0]
            self.assertIsInstance(ref.material_details, MaterialDetails)
            code = ref.material_details.material_code
            self.assertIsInstance(code, MaterialCode)
            self.assertEqual(code.text, 'R')

    def test_charting_super_set_correctly(self):
        data = self.data + (
            "Charting:\n"
            "    - 43: 'F:MOD:R:S'"
        )
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.treatment_details.dental_chart_details.dental_chart[0]
            self.assertIsInstance(ref.supernumerary_flag, SupernumeraryFlag)
            self.assertTrue(ref.supernumerary_flag.text)

    def test_charting_super_set_if_mixed_details(self):
        codes = [
            'F:M:R:S',
            ':M:G:S',
            'F::A:S',
            'F:B::S',
            ':::S',
        ]
        for code in codes:
            data = self.data + (
                "Charting:\n"
                "    - 43: '{}'".format(code)
            )
            payload = load_yaml_document(data)
            for doc in payload:
                cls = yaml_to_class(doc)
                ref = (
                    cls.treatment_details.dental_chart_details
                    .dental_chart[0]
                )
                self.assertIsInstance(
                    ref.supernumerary_flag, SupernumeraryFlag
                )
                self.assertTrue(ref.supernumerary_flag.text)

    @unittest.skip("This should not be needed any more")
    def test_charting_duplicate_codes_merged_to_one_item(self):
        data = self.data + (
            "Charting:\n"
            "   - 27: ':O::'\n"
            "   - 27: 'F::R:'"
        )
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.treatment_details.dental_chart_details.dental_chart[0]
            self.assertEqual(ref.tooth_notation.text, 27)
            self.assertEqual(
                ref.annotation_code_details.annotation_code.text, 'F'
            )
            self.assertEqual(
                ref.tooth_surface_details.tooth_surface_code.text, 'O')
            self.assertEqual(
                ref.material_details.material_code.text, 'R')

    def test_charting_duplicate_teeth_multi_annotation_split_two_rows(self):
        data = self.data + (
            "Charting:\n"
            "   - 27: 'BP:::'\n"
            "   - 27: 'M:::'"
        )
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            self.assertEqual(
                len(cls.treatment_details.dental_chart_details.dental_chart),
                2
            )

    def test_charting_duplicate_teeth_multi_annotation_set_correctly(self):
        data = self.data + (
            "Charting:\n"
            "   - 27: 'BP:::'\n"
            "   - 27: 'M:::'"
        )
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            chart = cls.treatment_details.dental_chart_details.dental_chart
            codes = [
                charting.annotation_code_details.annotation_code.text
                for charting in chart
            ]
            self.assertIn('BP', codes)
            self.assertIn('M', codes)

    def test_charting_duplicate_teeth_multi_material_split_two_rows(self):
        data = self.data + (
            "Charting:\n"
            "   - 37: 'F:B:R:'\n"
            "   - 37: 'F:MO:A:'"
        )
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            self.assertEqual(
                len(cls.treatment_details.dental_chart_details.dental_chart),
                2
            )

    def test_charting_duplicate_teeth_multi_material_set_correctly(self):
        data = self.data + (
            "Charting:\n"
            "   - 37: 'F:B:R:'\n"
            "   - 37: 'F:MO:A:'"
        )
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            chart = cls.treatment_details.dental_chart_details.dental_chart
            materials = [
                charting.material_details.material_code.text
                for charting in chart
            ]
            self.assertIn('R', materials)
            self.assertIn('A', materials)

    def test_charting_duplicate_teeth_multi_material_sufraces_correct(self):
        data = self.data + (
            "Charting:\n"
            "   - 37: 'F:B:R:'\n"
            "   - 37: 'F:MO:A:'"
        )
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            chart = cls.treatment_details.dental_chart_details.dental_chart
            surfaces = [
                charting.tooth_surface_details.tooth_surface_code.text
                for charting in chart
            ]
            self.assertIn('B', surfaces)
            self.assertIn('MO', surfaces)

    def test_charting_duplicate_teeth_multi_material_sets_code_correctly(self):
        data = self.data + (
            "Charting:\n"
            "   - 37: 'F:B:R:'\n"
            "   - 37: 'F:MO:A:'"
        )
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            chart = cls.treatment_details.dental_chart_details.dental_chart
            codes = [
                charting.annotation_code_details.annotation_code.text
                for charting in chart
            ]
            for code in codes:
                self.assertEqual(code, 'F')


    def test_charting_duplicate_value_not_repeated(self):
        data = self.data + (
            "Charting:\n"
            "   - 27: 'F:O::'\n"
            "   - 27: 'F::R:'"
        )
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.treatment_details.dental_chart_details.dental_chart[0]
            self.assertEqual(
                ref.annotation_code_details.annotation_code.text, 'F')

    def test_upper_left_generated_correctly(self):
        data = self.data + 'UpperLeft:      2'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.treatment_details.bpe_details.upper_left
            self.assertIsInstance(ref, UpperLeft)

    def test_upper_left_set_correctly(self):
        data = self.data + 'UpperLeft:      2'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.treatment_details.bpe_details.upper_left
            self.assertEqual(ref.text, '2')

    def test_upper_anterior_generated_correctly(self):
        data = self.data + 'UpperAnterior:      2'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.treatment_details.bpe_details.upper_anterior
            self.assertIsInstance(ref, UpperAnterior)

    def test_upper_anterior_set_correctly(self):
        data = self.data + 'UpperAnterior:      2'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.treatment_details.bpe_details.upper_anterior
            self.assertEqual(ref.text, '2')

    def test_upper_right_generated_correctly(self):
        data = self.data + 'UpperRight:      2'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.treatment_details.bpe_details.upper_right
            self.assertIsInstance(ref, UpperRight)

    def test_upper_right_set_correctly(self):
        data = self.data + 'UpperRight:      2'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.treatment_details.bpe_details.upper_right
            self.assertEqual(ref.text, '2')

    def test_lower_left_generated_correctly(self):
        data = self.data + 'LowerLeft:      2'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.treatment_details.bpe_details.lower_left
            self.assertIsInstance(ref, LowerLeft)

    def test_lower_left_set_correctly(self):
        data = self.data + 'LowerLeft:      2'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.treatment_details.bpe_details.lower_left
            self.assertEqual(ref.text, '2')

    def test_lower_anterior_generated_correctly(self):
        data = self.data + 'LowerAnterior:      2'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.treatment_details.bpe_details.lower_anterior
            self.assertIsInstance(ref, LowerAnterior)

    def test_lower_anterior_set_correctly(self):
        data = self.data + 'LowerAnterior:      2'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.treatment_details.bpe_details.lower_anterior
            self.assertEqual(ref.text, '2')

    def test_lower_right_generated_correctly(self):
        data = self.data + 'LowerRight:      2'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.treatment_details.bpe_details.lower_right
            self.assertIsInstance(ref, LowerRight)

    def test_lower_right_set_correctly(self):
        data = self.data + 'LowerRight:      2'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.treatment_details.bpe_details.lower_right
            self.assertEqual(ref.text, '2')

    def test_dentist_declaration_generated_correctly(self):
        data = self.data + 'Declaration:       True'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.dentists_declaration_details
                .dentist_declaration.declaration
            )
            self.assertIsInstance(ref, Declaration)

    def test_dentist_declaration_set_correctly(self):
        data = self.data + 'Declaration:       True'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.dentists_declaration_details
                .dentist_declaration.declaration
            )
            self.assertEqual(ref.text, True)

    def test_patient_or_representative_code_generated_correctly(self):
        data = self.data + 'PatientOrRepresentativeCode:    1'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.dentists_declaration_details.patient_declaration
                .declaration_on_acceptance.patient_or_representative_details
                .patient_or_representative_code
            )
            self.assertIsInstance(ref, PatientOrRepresentativeCode)

    def test_patient_or_representative_code_set_correctly(self):
        data = self.data + 'PatientOrRepresentativeCode:    2'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.dentists_declaration_details.patient_declaration
                .declaration_on_acceptance.patient_or_representative_details
                .patient_or_representative_code
            )
            self.assertEqual(ref.text, '2')

    def test_patient_or_representative_signature_generated_correctly(self):
        data = self.data + 'PatientOrRepresentativeSignature:    True'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.dentists_declaration_details.patient_declaration
                .declaration_on_acceptance.patient_or_representative_signature
            )
            self.assertIsInstance(ref, PatientOrRepresentativeSignature)

    def test_patient_or_representative_signature_set_correctly(self):
        data = self.data + 'PatientOrRepresentativeSignature:    True'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.dentists_declaration_details.patient_declaration
                .declaration_on_acceptance.patient_or_representative_signature
            )
            self.assertEqual(ref.text, True)

    def test_pat_or_representative_signature_date_generated_correctly(self):
        data = self.data + \
                'PatientOrRepresentativeSignatureDate:    2017-08-21'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.dentists_declaration_details.patient_declaration
                .declaration_on_acceptance
                .patient_or_representative_signature_date
            )
            self.assertIsInstance(ref, PatientOrRepresentativeSignatureDate)

    def test_patient_or_representative_signature_date_set_correctly(self):
        data = self.data + \
                'PatientOrRepresentativeSignatureDate:    2017-08-21'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.dentists_declaration_details.patient_declaration
                .declaration_on_acceptance
                .patient_or_representative_signature_date
            )
            self.assertEqual(ref.text, datetime.date(2017, 8, 21))

    def test_patient_or_representative_code_finish_generated_correctly(self):
        data = self.data + 'PatientOrRepresentativeCodeFinish:    1'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.dentists_declaration_details.patient_declaration
                .declaration_on_completion.patient_or_representative_details
                .patient_or_representative_code
            )
            self.assertIsInstance(ref, PatientOrRepresentativeCode)

    def test_patient_or_representative_code_finish_set_correctly(self):
        data = self.data + 'PatientOrRepresentativeCodeFinish:    2'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.dentists_declaration_details.patient_declaration
                .declaration_on_completion.patient_or_representative_details
                .patient_or_representative_code
            )
            self.assertEqual(ref.text, '2')

    def test_patient_signature_finish_generated_correctly(self):
        data = self.data + 'PatientOrRepresentativeSignatureFinish:    True'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.dentists_declaration_details.patient_declaration
                .declaration_on_completion.patient_or_representative_signature
            )
            self.assertIsInstance(ref, PatientOrRepresentativeSignature)

    def test_patient_signature_finish_set_correctly(self):
        data = self.data + 'PatientOrRepresentativeSignatureFinish:    True'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.dentists_declaration_details.patient_declaration
                .declaration_on_completion.patient_or_representative_signature
            )
            self.assertEqual(ref.text, True)

    def test_patient_signature_date_finish_generated_correctly(self):
        data = self.data + \
                'PatientOrRepresentativeSignatureDateFinish:    2017-08-21'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.dentists_declaration_details.patient_declaration
                .declaration_on_completion
                .patient_or_representative_signature_date
            )
            self.assertIsInstance(ref, PatientOrRepresentativeSignatureDate)

    def test_patient_signature_date_finish_set_correctly(self):
        data = self.data + \
                'PatientOrRepresentativeSignatureDateFinish:    2017-08-21'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.dentists_declaration_details.patient_declaration
                .declaration_on_completion
                .patient_or_representative_signature_date
            )
            self.assertEqual(ref.text, datetime.date(2017, 8, 21))

    def test_patient_charge_generated_correctly(self):
        data = self.data + 'PatientCharge:    24.84'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.payment_claim_details
                .treatment_amount_details.patient_charge
            )
            self.assertIsInstance(ref, PatientCharge)

    def test_patient_charge_can_be_zero(self):
        data = self.data + 'PatientCharge:     0.00'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.payment_claim_details
                .treatment_amount_details.patient_charge
            )
            self.assertEqual(ref.text, '0.00')

    def test_patient_charge_set_correctly(self):
        data = self.data + 'PatientCharge:    24.84'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.payment_claim_details
                .treatment_amount_details.patient_charge
            )
            self.assertEqual(ref.text, '24.84')

    def test_amount_claimed_generated_correctly(self):
        data = self.data + 'AmountClaimed:    39.75'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.payment_claim_details
                .treatment_amount_details.amount_claimed
            )
            self.assertIsInstance(ref, AmountClaimed)

    def test_amount_claimed_set_correctly(self):
        data = self.data + 'AmountClaimed:    39.75'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.payment_claim_details
                .treatment_amount_details.amount_claimed
            )
            self.assertEqual(ref.text, '39.75')

    def test_patient_details_request_flag_generated_correctly(self):
        data = self.data + 'PatientDetailsRequestFlag:    False'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.patient_details.patient_details_request_check
                .patient_details_request_flag
            )
            self.assertIsInstance(ref, PatientDetailsRequestFlag)

    def test_patient_details_request_flag_set_correctl(self):
        data = self.data + 'PatientDetailsRequestFlag:    False'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.patient_details.patient_details_request_check
                .patient_details_request_flag
            )
            self.assertEqual(ref.text, False)

    def test_patient_rep_title_generated_correctly(self):
        data = self.data + 'PatientOrRepresentativeTitle:    Mr.'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.dentists_declaration_details.patient_declaration
                .declaration_on_acceptance.representative_name.title
            )
            self.assertIsInstance(ref, Title)

    def test_patient_rep_title_set_correctly(self):
        data = self.data + 'PatientOrRepresentativeTitle:    Mr.'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.dentists_declaration_details.patient_declaration
                .declaration_on_acceptance.representative_name.title
            )
            self.assertEqual(ref.text, 'Mr.')

    def test_patient_rep_family_name_generated_correctly(self):
        data = self.data + 'PatientOrRepresentativeFamilyName:    MCTAVISH'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.dentists_declaration_details.patient_declaration
                .declaration_on_acceptance.representative_name.family_name
            )
            self.assertIsInstance(ref, FamilyName)

    def test_patient_rep_family_name_set_correctly(self):
        data = self.data + 'PatientOrRepresentativeFamilyName:    MCTAVISH'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.dentists_declaration_details.patient_declaration
                .declaration_on_acceptance.representative_name.family_name
            )
            self.assertEqual(ref.text, 'MCTAVISH')

    def test_patient_rep_given_name_generated_correctly(self):
        data = self.data + 'PatientOrRepresentativeGivenName:    Brian'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.dentists_declaration_details.patient_declaration
                .declaration_on_acceptance.representative_name.given_name
            )
            self.assertIsInstance(ref, GivenName)

    def test_patient_rep_given_name_set_correctly(self):
        data = self.data + 'PatientOrRepresentativeGivenName:    Brian'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.dentists_declaration_details.patient_declaration
                .declaration_on_acceptance.representative_name.given_name
            )
            self.assertEqual(ref.text, 'Brian')

    def test_patient_rep_birth_family_name_generated_correctly(self):
        data = self.data + 'PatientOrRepresentativeBirthFamilyName:    Brian'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.dentists_declaration_details.patient_declaration
                .declaration_on_acceptance
                .representative_name.birth_family_name
            )
            self.assertIsInstance(ref, BirthFamilyName)

    def test_patient_rep_birth_family_name_set_correctly(self):
        data = self.data + 'PatientOrRepresentativeBirthFamilyName:    Brian'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.dentists_declaration_details.patient_declaration
                .declaration_on_acceptance
                .representative_name.birth_family_name
            )
            self.assertEqual(ref.text, 'Brian')

    def test_patient_rep_address_set_correctly(self):
        data = self.data + (
            'PatientOrRepresentativeAddressLine:     |\n'
            '    the lots\n'
            '    callander'
        )
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.dentists_declaration_details.patient_declaration
                .declaration_on_acceptance.representative_address.address_lines
            )
            self.assertIsInstance(ref, AddressLines)
            reference_lines = ['the lots', 'callander']
            for reference, line in zip(reference_lines, ref.address_lines):
                self.assertIsInstance(line, AddressLine)
                self.assertEqual(line.text, reference)

    def test_patient_rep_postcode_generated_correctly(self):
        data = self.data + 'PatientOrRepresentativePostCode: FK17 8JJ'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.dentists_declaration_details.patient_declaration
                .declaration_on_acceptance.representative_address.post_code
            )
            self.assertIsInstance(ref, PostCode)

    def test_patient_rep_postcode_set_correctly(self):
        data = self.data + 'PatientOrRepresentativePostCode: FK17 8JJ'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.dentists_declaration_details.patient_declaration
                .declaration_on_acceptance.representative_address.post_code
            )
            self.assertEqual(ref.text, 'FK17 8JJ')

    def test_patient_rep_title_finish_generated_correctly(self):
        data = self.data + 'PatientOrRepresentativeTitleFinish:    Mr.'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.dentists_declaration_details.patient_declaration
                .declaration_on_completion.representative_name.title
            )
            self.assertIsInstance(ref, Title)

    def test_patient_rep_title_finish_set_correctly(self):
        data = self.data + 'PatientOrRepresentativeTitleFinish:    Mr.'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.dentists_declaration_details.patient_declaration
                .declaration_on_completion.representative_name.title
            )
            self.assertEqual(ref.text, 'Mr.')

    def test_patient_rep_family_name_finish_generated_correctly(self):
        data = self.data + (
            'PatientOrRepresentativeFamilyNameFinish:    MCTAVISH'
        )
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.dentists_declaration_details.patient_declaration
                .declaration_on_completion.representative_name.family_name
            )
            self.assertIsInstance(ref, FamilyName)

    def test_patient_rep_family_name_finish_set_correctly(self):
        data = self.data + (
            'PatientOrRepresentativeFamilyNameFinish:    MCTAVISH'
        )
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.dentists_declaration_details.patient_declaration
                .declaration_on_completion.representative_name.family_name
            )
            self.assertEqual(ref.text, 'MCTAVISH')

    def test_patient_rep_given_name_finish_generated_correctly(self):
        data = self.data + 'PatientOrRepresentativeGivenNameFinish:    Brian'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.dentists_declaration_details.patient_declaration
                .declaration_on_completion.representative_name.given_name
            )
            self.assertIsInstance(ref, GivenName)

    def test_patient_rep_given_name_finish_set_correctly(self):
        data = self.data + 'PatientOrRepresentativeGivenNameFinish:    Brian'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.dentists_declaration_details.patient_declaration
                .declaration_on_completion.representative_name.given_name
            )
            self.assertEqual(ref.text, 'Brian')

    def test_patient_rep_birth_family_name_finish_generated_correctly(self):
        data = self.data + (
            'PatientOrRepresentativeBirthFamilyNameFinish:    Brian'
        )
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.dentists_declaration_details.patient_declaration
                .declaration_on_completion
                .representative_name.birth_family_name
            )
            self.assertIsInstance(ref, BirthFamilyName)

    def test_patient_rep_birth_family_name_finish_set_correctly(self):
        data = self.data + (
            'PatientOrRepresentativeBirthFamilyNameFinish:    Brian'
        )
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.dentists_declaration_details.patient_declaration
                .declaration_on_completion
                .representative_name.birth_family_name
            )
            self.assertEqual(ref.text, 'Brian')

    def test_patient_rep_address_finish_set_correctly(self):
        data = self.data + (
            'PatientOrRepresentativeAddressLineFinish:     |\n'
            '    the lots\n'
            '    callander'
        )
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.dentists_declaration_details.patient_declaration
                .declaration_on_completion.representative_address.address_lines
            )
            self.assertIsInstance(ref, AddressLines)
            reference_lines = ['the lots', 'callander']
            for reference, line in zip(reference_lines, ref.address_lines):
                self.assertIsInstance(line, AddressLine)
                self.assertEqual(line.text, reference)

    def test_patient_rep_postcode_finish_generated_correctly(self):
        data = self.data + 'PatientOrRepresentativePostCodeFinish: FK17 8JJ'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.dentists_declaration_details.patient_declaration
                .declaration_on_completion.representative_address.post_code
            )
            self.assertIsInstance(ref, PostCode)

    def test_patient_rep_postcode_finish_set_correctly(self):
        data = self.data + 'PatientOrRepresentativePostCodeFinish: FK17 8JJ'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.dentists_declaration_details.patient_declaration
                .declaration_on_completion.representative_address.post_code
            )
            self.assertEqual(ref.text, 'FK17 8JJ')

    def test_benefit_category_code_generated_correctly(self):
        data = self.data + 'BenefitCategoryCode:    18'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.payment_claim_details.exemption_details
                .exemption_or_remission_status_details.benefit_category_details
                .benefit_category_code
            )
            self.assertIsInstance(ref, BenefitCategoryCode)

    def test_benefit_category_code_set_correctly(self):
        data = self.data + 'BenefitCategoryCode:    18'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.payment_claim_details.exemption_details
                .exemption_or_remission_status_details.benefit_category_details
                .benefit_category_code
            )
            self.assertEqual(ref.text, '18')

    def test_representative_family_name_generated_correctly(self):
        data = self.data + 'BenefitRecipientFamilyName: Spam'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.payment_claim_details.exemption_details
                .exemption_or_remission_status_details.benefit_recipient_name
                .family_name
            )
            self.assertIsInstance(ref, FamilyName)

    def test_representative_family_name_set_correctly(self):
        data = self.data + 'BenefitRecipientFamilyName: Spam'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.payment_claim_details.exemption_details
                .exemption_or_remission_status_details.benefit_recipient_name
                .family_name
            )
            self.assertEqual(ref.text, 'Spam')

    def test_representative_title_generated_correctly(self):
        data = self.data + 'BenefitRecipientTitle: Spam'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.payment_claim_details.exemption_details
                .exemption_or_remission_status_details.benefit_recipient_name
                .title
            )
            self.assertIsInstance(ref, Title)

    def test_representative_title_set_correctly(self):
        data = self.data + 'BenefitRecipientTitle: Spam'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.payment_claim_details.exemption_details
                .exemption_or_remission_status_details.benefit_recipient_name
                .title
            )
            self.assertEqual(ref.text, 'Spam')

    def test_representative_birth_family_name_generated_correctly(self):
        data = self.data + 'BenefitRecipientBirthFamilyName: Spam'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.payment_claim_details.exemption_details
                .exemption_or_remission_status_details.benefit_recipient_name
                .birth_family_name
            )
            self.assertIsInstance(ref, BirthFamilyName)

    def test_representative_birth_family_name_set_correctly(self):
        data = self.data + 'BenefitRecipientBirthFamilyName: Spam'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.payment_claim_details.exemption_details
                .exemption_or_remission_status_details.benefit_recipient_name
                .birth_family_name
            )
            self.assertEqual(ref.text, 'Spam')

    def test_representative_given_name_generated_correctly(self):
        data = self.data + 'BenefitRecipientGivenName: Spam'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.payment_claim_details.exemption_details
                .exemption_or_remission_status_details.benefit_recipient_name
                .given_name
            )
            self.assertIsInstance(ref, GivenName)

    def test_representative_given_name_set_correctly(self):
        data = self.data + 'BenefitRecipientGivenName: Spam'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.payment_claim_details.exemption_details
                .exemption_or_remission_status_details.benefit_recipient_name
                .given_name
            )
            self.assertEqual(ref.text, 'Spam')

    def test_representative_date_of_birth_generated_correctly(self):
        data = self.data + 'BenefitRecipientDateOfBirth: 2000-05-05'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.payment_claim_details.exemption_details
                .exemption_or_remission_status_details
                .benefit_recipient_date_of_birth
            )
            self.assertIsInstance(ref, BenefitRecipientDateOfBirth)

    def test_representative_date_of_birth_set_correctly(self):
        data = self.data + 'BenefitRecipientDateOfBirth: 2000-05-05'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.payment_claim_details.exemption_details
                .exemption_or_remission_status_details
                .benefit_recipient_date_of_birth
            )
            self.assertEqual(ref.text, datetime.date(2000, 5, 5))

    def test_representative_family_name_not_in_acceptance_section(self):
        data = self.data + 'BenefitRecipientFamilyName: Spam'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            self.assertIsNone(
                cls.payment_claim_details.exemption_details
                .exemption_or_remission_status_on_acceptance_details
            )

    def test_representative_family_name_generated_correctly_accept(self):
        data = self.data + 'BenefitRecipientFamilyNameOnAcceptance: Spam'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.payment_claim_details.exemption_details
                .exemption_or_remission_status_on_acceptance_details
                .benefit_recipient_name.family_name
            )
            self.assertIsInstance(ref, FamilyName)

    def test_representative_family_name_set_correctly_accept(self):
        data = self.data + 'BenefitRecipientFamilyNameOnAcceptance: Spam'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.payment_claim_details.exemption_details
                .exemption_or_remission_status_on_acceptance_details
                .benefit_recipient_name.family_name
            )
            self.assertEqual(ref.text, 'Spam')

    def test_representative_title_generated_correctly_accept(self):
        data = self.data + 'BenefitRecipientTitleOnAcceptance: Spam'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.payment_claim_details.exemption_details
                .exemption_or_remission_status_on_acceptance_details
                .benefit_recipient_name.title
            )
            self.assertIsInstance(ref, Title)

    def test_representative_title_set_correctly_accept(self):
        data = self.data + 'BenefitRecipientTitleOnAcceptance: Spam'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.payment_claim_details.exemption_details
                .exemption_or_remission_status_on_acceptance_details
                .benefit_recipient_name.title
            )
            self.assertEqual(ref.text, 'Spam')

    def test_representative_birth_family_name_generated_correctly_accept(self):
        data = self.data + 'BenefitRecipientBirthFamilyNameOnAcceptance: Spam'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.payment_claim_details.exemption_details
                .exemption_or_remission_status_on_acceptance_details
                .benefit_recipient_name.birth_family_name
            )
            self.assertIsInstance(ref, BirthFamilyName)

    def test_representative_birth_family_name_set_correctly_accept(self):
        data = self.data + 'BenefitRecipientBirthFamilyNameOnAcceptance: Spam'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.payment_claim_details.exemption_details
                .exemption_or_remission_status_on_acceptance_details
                .benefit_recipient_name.birth_family_name
            )
            self.assertEqual(ref.text, 'Spam')

    def test_representative_given_name_generated_correctly_accept(self):
        data = self.data + 'BenefitRecipientGivenNameOnAcceptance: Spam'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.payment_claim_details.exemption_details
                .exemption_or_remission_status_on_acceptance_details
                .benefit_recipient_name.given_name
            )
            self.assertIsInstance(ref, GivenName)

    def test_representative_given_name_set_correctly_accept(self):
        data = self.data + 'BenefitRecipientGivenNameOnAcceptance: Spam'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.payment_claim_details.exemption_details
                .exemption_or_remission_status_on_acceptance_details
                .benefit_recipient_name.given_name
            )
            self.assertEqual(ref.text, 'Spam')

    def test_representative_date_of_birth_generated_correctly_accept(self):
        data = (
            self.data + 'BenefitRecipientDateOfBirthOnAcceptance: 2000-05-05'
        )
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.payment_claim_details.exemption_details
                .exemption_or_remission_status_on_acceptance_details
                .benefit_recipient_date_of_birth
            )
            self.assertIsInstance(ref, BenefitRecipientDateOfBirth)

    def test_representative_date_of_birth_set_correctly_accept(self):
        data = (
            self.data + 'BenefitRecipientDateOfBirthOnAcceptance: 2000-05-05'
        )
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.payment_claim_details.exemption_details
                .exemption_or_remission_status_on_acceptance_details
                .benefit_recipient_date_of_birth
            )
            self.assertEqual(ref.text, datetime.date(2000, 5, 5))


class ClaimReconciliationRequestClassHeirarchyFromYaml(unittest.TestCase):
    def setUp(self):
        self.data = 'MessageType:       DentalReconciliationFileRequest\n'

    def test_reconciliation_file_request_generated(self):
        payload = load_yaml_document(self.data)
        data = list(payload)[0]
        cls = yaml_to_class(data)
        self.assertIsInstance(cls, DentalReconciliationFileRequest)

    def test_id_value_generated_correctly(self):
        data = self.data + 'IdValue:   01212'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.requesting_organisation_details.organisation_id.id_value
            self.assertIsInstance(ref, IdValue)

    def test_id_value_set_correctly(self):
        data = self.data + 'IdValue:   01212'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.requesting_organisation_details.organisation_id.id_value
            self.assertEqual(ref.text, '01212')

    def test_organisation_name_generated_correctly(self):
        data = self.data + 'OrganisationName: CALLANDER DENTAL PRACTICE'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.requesting_organisation_details.organisation_name
            self.assertIsInstance(ref, OrganisationName)

    def test_organisation_name_set_correctly(self):
        data = self.data + 'OrganisationName: CALLANDER DENTAL PRACTICE'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.requesting_organisation_details.organisation_name
            self.assertEqual(ref.text, 'CALLANDER DENTAL PRACTICE')

    def test_organisation_type_generated_correctly(self):
        data = self.data + 'OrganisationType:   dentalpractice'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.requesting_organisation_details.organisation_type
            self.assertIsInstance(ref, OrganisationType)

    def test_organisation_type_set_correctly(self):
        data = self.data + 'OrganisationType:   dentalpractice'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.requesting_organisation_details.organisation_type
            self.assertEqual(ref.text, 'dentalpractice')

    def test_practice_address_generated_correctly(self):
        data = self.data + (
            'PracticeAddressLine:   |\n'
            '    171 Main Street\n'
            '    Callander\n'
        )
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.requesting_organisation_details.address.address_lines
            self.assertIsInstance(ref, AddressLines)

    def test_practice_address_set_correctly(self):
        data = self.data + (
            'PracticeAddressLine:   |\n'
            '    171 Main Street\n'
            '    Callander\n'
        )
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.requesting_organisation_details.address.address_lines
            reference_lines = ['171 Main Street', 'Callander']
            for reference, line in zip(reference_lines, ref.address_lines):
                self.assertEqual(reference, line.text)

    def test_practice_post_code_generated_correctly(self):
        data = self.data + 'PracticePostCode:    FK17 8BJ'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.requesting_organisation_details.address.post_code
            self.assertIsInstance(ref, PostCode)

    def test_practice_post_code_set_correctly(self):
        data = self.data + 'PracticePostCode:    FK17 8BJ'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.requesting_organisation_details.address.post_code
            self.assertEqual(ref.text, 'FK17 8BJ')


class PatientDetailsRequestFromYamlTest(unittest.TestCase):
    def setUp(self):
        self.data = 'MessageType:       DentalPatientDetailsRequest\n'

    def test_request_file_generates_class(self):
        payload = load_yaml_document(self.data)
        cls = yaml_to_class(list(payload)[0])
        self.assertIsInstance(cls, DentalPatientDetailsRequest)

    def test_id_value_generated_correctly(self):
        data = self.data + 'IdValue:   01212'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.requesting_organisation_details.organisation_id.id_value
            self.assertIsInstance(ref, IdValue)

    def test_id_value_set_correctly(self):
        data = self.data + 'IdValue:   01212'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.requesting_organisation_details.organisation_id.id_value
            self.assertEqual(ref.text, '01212')

    def test_organisation_name_generated_correctly(self):
        data = self.data + 'OrganisationName: CALLANDER DENTAL PRACTICE'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.requesting_organisation_details.organisation_name
            self.assertIsInstance(ref, OrganisationName)

    def test_organisation_name_set_correctly(self):
        data = self.data + 'OrganisationName: CALLANDER DENTAL PRACTICE'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.requesting_organisation_details.organisation_name
            self.assertEqual(ref.text, 'CALLANDER DENTAL PRACTICE')

    def test_organisation_type_generated_correctly(self):
        data = self.data + 'OrganisationType:   dentalpractice'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.requesting_organisation_details.organisation_type
            self.assertIsInstance(ref, OrganisationType)

    def test_organisation_type_set_correctly(self):
        data = self.data + 'OrganisationType:   dentalpractice'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.requesting_organisation_details.organisation_type
            self.assertEqual(ref.text, 'dentalpractice')

    def test_practice_address_generated_correctly(self):
        data = self.data + (
            'PracticeAddressLine:   |\n'
            '    171 Main Street\n'
            '    Callander\n'
        )
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.requesting_organisation_details.address.address_lines
            self.assertIsInstance(ref, AddressLines)

    def test_practice_address_set_correctly(self):
        data = self.data + (
            'PracticeAddressLine:   |\n'
            '    171 Main Street\n'
            '    Callander\n'
        )
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.requesting_organisation_details.address.address_lines
            reference_lines = ['171 Main Street', 'Callander']
            for reference, line in zip(reference_lines, ref.address_lines):
                self.assertEqual(reference, line.text)

    def test_practice_post_code_generated_correctly(self):
        data = self.data + 'PracticePostCode:    FK17 8BJ'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.requesting_organisation_details.address.post_code
            self.assertIsInstance(ref, PostCode)

    def test_practice_post_code_set_correctly(self):
        data = self.data + 'PracticePostCode:    FK17 8BJ'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.requesting_organisation_details.address.post_code
            self.assertEqual(ref.text, 'FK17 8BJ')

    def test_dentist_family_name_generated_correctly(self):
        data = self.data + 'DentistFamilyName:   Jones'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.requesting_practitioner_details.hcp_name
                .structured_name.family_name
            )
            self.assertIsInstance(ref, FamilyName)

    def test_dentist_family_name_has_correct_value(self):
        data = self.data + 'DentistFamilyName:   Jones'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.requesting_practitioner_details
                .hcp_name.structured_name.family_name
            )
            self.assertEqual(ref.text, 'Jones')

    def test_dentist_given_name_generated_correctly(self):
        data = self.data + 'DentistGivenName:   Suzie'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.requesting_practitioner_details
                .hcp_name.structured_name.given_name
            )
            self.assertIsInstance(ref, GivenName)

    def test_dentist_given_name_has_correct_value(self):
        data = self.data + 'DentistGivenName:   Suzie'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = (
                cls.requesting_practitioner_details
                .hcp_name.structured_name.given_name
            )
            self.assertEqual(ref.text, 'Suzie')

    def test_prof_type_generated_correctly(self):
        data = self.data + 'ProfType:   DentalHealthCareProfessional'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.requesting_practitioner_details.prof_type
            self.assertIsInstance(ref, ProfType)

    def test_prof_type_has_correct_value(self):
        data = self.data + 'ProfType:   DentalHealthCareProfessional'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.requesting_practitioner_details.prof_type
        self.assertEqual(ref.text, 'DentalHealthCareProfessional')

    def test_prof_type_set_by_default(self):
        data = self.data + 'DentistGivenName:   Suzie'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.requesting_practitioner_details.prof_type
            self.assertIsInstance(ref, ProfType)

    def test_prof_type_has_correct_default_value(self):
        data = self.data + 'DentistGivenName:   Suzie'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.requesting_practitioner_details.prof_type
            self.assertEqual(ref.text, 'Dentist')

    def test_prof_code_generated_correctly(self):
        data = self.data + 'ProfCode:   15151'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.requesting_practitioner_details.hcp_id.prof_code
            self.assertIsInstance(ref, ProfCode)

    def test_prof_code_has_correct_value(self):
        data = self.data + 'ProfCode:   15151'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.requesting_practitioner_details.hcp_id.prof_code
            self.assertEqual(ref.text, '15151')

    def test_prof_code_scheme_generated_correctly(self):
        data = self.data + 'ProfCodeScheme:   Dental List Number'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.requesting_practitioner_details.hcp_id.prof_code_scheme
            self.assertIsInstance(ref, ProfCodeScheme)

    def test_prof_code_scheme_has_correct_value(self):
        data = self.data + 'ProfCodeScheme:   Dental List Number'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.requesting_practitioner_details.hcp_id.prof_code_scheme
            self.assertEqual(ref.text, 'Dental List Number')

    def test_prof_code_set_by_default(self):
        data = self.data + 'ProfCode:   15151'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.requesting_practitioner_details.hcp_id.prof_code_scheme
            self.assertIsInstance(ref, ProfCodeScheme)

    def test_prof_code_has_correct_default_value(self):
        data = self.data + 'ProfCode:   15151'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.requesting_practitioner_details.hcp_id.prof_code_scheme
            self.assertEqual(ref.text, 'Dental List Number')

    def test_patient_details_request_reference_generated_correctly(self):
        data = self.data + 'PatientDetailsRequestReference: 0000000006'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.patient_details_request_reference
            self.assertIsInstance(ref, PatientDetailsRequestReference)

    def test_patient_details_request_reference_set_correctly(self):
        data = self.data + 'PatientDetailsRequestReference: 0000000006'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.patient_details_request_reference
            self.assertEqual(ref.text, '0000000006')

    def test_secondary_search_generated_correctly(self):
        data = self.data + 'SecondarySearch: True'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.secondary_search
            self.assertIsInstance(ref, SecondarySearch)

    def test_secondary_search_set_correctly(self):
        data = self.data + 'SecondarySearch: True'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.secondary_search
            self.assertEqual(ref.text, True)

    def test_initial_search_reference_generated_correctly(self):
        data = self.data + 'InitialSearchReference: 0000000006'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.initial_search_reference
            self.assertIsInstance(ref, InitialSearchReference)

    def test_initial_search_reference_set_correctly(self):
        data = self.data + 'InitialSearchReference: 0000000006'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.initial_search_reference
            self.assertEqual(ref.text, '0000000006')

    def test_chi_number_generated_correctly(self):
        data = self.data + 'CHINumber:   2123123123'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.patient_details.chi_number
            self.assertIsInstance(ref, ChiNumber)

    def test_chi_number_set_correctly(self):
        data = self.data + 'CHINumber:   2123123123'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.patient_details.chi_number
            self.assertEqual(ref.text, '2123123123')

    def test_patient_family_name_generated_correctly(self):
        data = self.data + 'PatientFamilyName:   mangrove'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            pat_name = cls.patient_details.patient_name
            ref = pat_name.patient_family_name
            self.assertIsInstance(ref, PatientFamilyName)

    def test_patient_family_name_set_correctly(self):
        data = self.data + 'PatientFamilyName:   mangrove'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            pat_name = cls.patient_details.patient_name
            ref = pat_name.patient_family_name
            self.assertEqual(ref.text, 'mangrove')

    def test_patient_given_name_generated_correctly(self):
        data = self.data + 'PatientGivenName:   Roger'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            pat_name = cls.patient_details.patient_name
            ref = pat_name.patient_given_name
            self.assertIsInstance(ref, PatientGivenName)

    def test_patient_given_name_set_correctly(self):
        data = self.data + 'PatientGivenName:   Roger'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            pat_name = cls.patient_details.patient_name
            ref = pat_name.patient_given_name
            self.assertEqual(ref.text, 'Roger')

    def test_patient_address_generated_correctly(self):
        data = self.data + (
            'PatientAddressLine:   | \n'
            '   BILMORY COTTAGE\n'
            '   BRIG O\' MONK\n'
            '   PERTHSHIRE'
        )
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.patient_details.address.address_lines
            self.assertIsInstance(ref, AddressLines)

    def test_patient_address_has_correct_values(self):
        data = self.data + (
            'PatientAddressLine:   | \n'
            '   BILMORY COTTAGE\n'
            '   BRIG O\' MONK\n'
            '   PERTHSHIRE'
        )
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.patient_details.address.address_lines
            reference_lines = [
                'BILMORY COTTAGE', 'BRIG O\' MONK', 'PERTHSHIRE'
            ]
            for reference, line in zip(reference_lines, ref.address_lines):
                self.assertEqual(reference, line.text)

    def test_patient_post_code_generated_correctly(self):
        data = self.data + 'PatientPostcode:   FK17 8HT'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.patient_details.address.post_code
            self.assertIsInstance(ref, PostCode)

    def test_patient_post_code_set_correctly(self):
        data = self.data + 'PatientPostcode:   FK17 8HT'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.patient_details.address.post_code
            self.assertEqual(ref.text, 'FK17 8HT')

    def test_patient_date_of_birth_generated_correctly(self):
        data = self.data + 'DateOfBirth:   1980-05-27'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.patient_details.date_of_birth
            self.assertIsInstance(ref, DateOfBirth)

    def test_patient_date_of_birth_set_correctly(self):
        data = self.data + 'DateOfBirth:   1980-05-27'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.patient_details.date_of_birth
            self.assertEqual(ref.text, datetime.date(1980, 5, 27))

    def test_patient_sex_generated_correctly(self):
        data = self.data + 'Sex:   M'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.patient_details.sex
            self.assertIsInstance(ref, Sex)

    def test_patient_sex_set_correctly(self):
        data = self.data + 'Sex:   M'
        payload = load_yaml_document(data)
        for doc in payload:
            cls = yaml_to_class(doc)
            ref = cls.patient_details.sex
            self.assertEqual(ref.text, 'm')


class DentalClaimRequestFromFile(unittest.TestCase):
    filenames = [
        'claim-1.yaml',
        'claim-2.yaml',
        'claim-3.yaml',
        'claim-4.yaml',
        'claim-5.yaml',
        'claim-6.yaml',
        'claim-7.yaml',
    ]

    def test_basic_claim_created(self):
        for filename in self.filenames:
            source = os.path.join('examples', filename)
            with open(source) as f:
                data = f.read()

            payload = load_yaml_document(data)
            for doc in payload:
                cls = yaml_to_class(doc)
                cls.as_etree()

    def test_basic_claim_is_valid_xml(self):
        schema = os.path.join('schema', 'DentalClaimRequest-v1-0.xsd')
        for filename in self.filenames:
            source = os.path.join('examples', filename)

            with open(schema) as f:
                xmlschema_doc = etree.parse(f)
            xmlschema = etree.XMLSchema(xmlschema_doc)

            with open(source) as f:
                data = f.read()

            payload = load_yaml_document(data)
            for doc in payload:
                cls = yaml_to_class(doc)
                tree = cls.as_etree()
                root = etree.ElementTree(tree)

                f = tempfile.NamedTemporaryFile('wb', delete=False)
                root.write(
                    f, pretty_print=True, encoding='utf-8',
                    xml_declaration=True
                )
                f.close()

                with open(f.name) as f:
                    data = etree.parse(f)

                os.unlink(f.name)

                self.assertTrue(xmlschema.validate(data))


class ReconcileRequestFromFileTest(unittest.TestCase):
    filenames = [
        'reconcile-1.yaml',
        'reconcile-2.yaml',
    ]

    def test_basic_request_created(self):
        for filename in self.filenames:
            source = os.path.join('examples', filename)
            with open(source) as f:
                data = f.read()

            payload = load_yaml_document(data)
            for doc in payload:
                cls = yaml_to_class(doc)
                cls.as_etree()

    def test_basic_request_is_valid_xml(self):
        schema = os.path.join(
            'schema', 'DentalReconciliationFileRequest-v1-0.xsd'
        )
        for filename in self.filenames:
            source = os.path.join('examples', filename)

            with open(schema) as f:
                xmlschema_doc = etree.parse(f)
            xmlschema = etree.XMLSchema(xmlschema_doc)

            with open(source) as f:
                data = f.read()

            payload = load_yaml_document(data)
            for doc in payload:
                cls = yaml_to_class(doc)
                tree = cls.as_etree()
                root = etree.ElementTree(tree)

                f = tempfile.NamedTemporaryFile('wb', delete=False)
                root.write(
                    f, pretty_print=True, encoding='utf-8',
                    xml_declaration=True
                )
                f.close()

                with open(f.name) as f:
                    data = etree.parse(f)

                os.unlink(f.name)

                self.assertTrue(xmlschema.validate(data))


class PatientDetailsRequestFromFileTest(unittest.TestCase):
    filenames = [
        'detail-request-1.yaml',
        'detail-request-2.yaml',
        'detail-request-3.yaml',
    ]

    def test_basic_claim_created(self):
        for filename in self.filenames:
            source = os.path.join('examples', filename)
            with open(source) as f:
                data = f.read()

            payload = load_yaml_document(data)
            for doc in payload:
                cls = yaml_to_class(doc)
                cls.as_etree()

    def test_basic_claim_is_valid_xml(self):
        schema = os.path.join('schema', 'DentalPatientDetailsRequest-v1-0.xsd')
        for filename in self.filenames:
            source = os.path.join('examples', filename)

            with open(schema) as f:
                xmlschema_doc = etree.parse(f)
            xmlschema = etree.XMLSchema(xmlschema_doc)

            with open(source) as f:
                data = f.read()

            payload = load_yaml_document(data)
            for doc in payload:
                cls = yaml_to_class(doc)
                tree = cls.as_etree()
                root = etree.ElementTree(tree)

                f = tempfile.NamedTemporaryFile('wb', delete=False)
                root.write(
                    f, pretty_print=True, encoding='utf-8',
                    xml_declaration=True
                )
                f.close()

                with open(f.name) as f:
                    data = etree.parse(f)

                os.unlink(f.name)

                self.assertTrue(xmlschema.validate(data))

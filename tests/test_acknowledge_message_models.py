#!/usr/bin/env python3
# vim:fileencoding=utf-8

import logging
import unittest

from lxml.builder import E

from edixml.exceptions.base import XmlMissingChildError
from edixml.models.acknowledge import Acknowledge

from .utils import etrees_equal


SCHEMA_VERSION = "1.2"
# Disable logging so it doesn't clutter up from tests
logging.disable(logging.CRITICAL)


class AcknowledgeTest(unittest.TestCase):

    def setUp(self):
        self.model = Acknowledge()

    def test_class_has_node_name(self):
        self.assertEqual('Acknowledge', self.model.node_name)

    def test_to_etree_produces_correct_tree(self):
        code = 1000
        description = 'This is a message'
        details = 'There are no details here'
        a = Acknowledge(
            code=code,
            description=description,
            details=details)
        reference = E.Acknowledge(
            E.Code(str(code)),
            E.Description(description),
            E.Details(details),
            SchemaVersion=SCHEMA_VERSION,
        )

        if not etrees_equal(reference, a.as_etree()):
            self.fail("{} did not produce correct etree".format(
                "Acknowledge"))

    def test_has_schemaversion_attribute(self):
        self.assertEqual(SCHEMA_VERSION, self.model.schema_version)

    def test_schemaversion_in_etree(self):
        model = Acknowledge(code=1, description='foo', details='bar')
        self.assertIn('SchemaVersion', model.as_etree().attrib)

    def test_code_allows_integer(self):
        code = 1
        result = self.model.validate_code(code)
        self.assertTrue(result)

    def test_code_does_not_allow_string(self):
        code = 'a'
        result = self.model.validate_code(code)
        self.assertFalse(result)

    def test_required_children_present(self):
        for member in ['code', 'description', 'details']:
            self.assertIn(member, self.model.required_children)

    def test_code_required(self):
        a = Acknowledge(description="foo", details="bar")
        with self.assertRaises(XmlMissingChildError):
            a.model_is_valid()
        a.code = 1000
        self.assertTrue(a.model_is_valid())

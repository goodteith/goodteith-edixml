#!/usr/bin/env python3
# vim:fileencoding=utf-8

import logging
import os
import subprocess
import unittest

from .context import edixml

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# TEST_DATA = {
#     'AddressLine': 'Sprig House\nCallander Way\nBrig o\' Turk',
#     'CHINumber': 2701021314,
#     'Charting': {
#         12: 'M',
#         21: 'F:MI:R',
#         22: 'M',
#         23: 'C::',
#         44: 'F:MOD:A',
#         45: 'F:MOD:A'
#     },
#     'ClaimFormSourceType': 'PMS',
#     'Claimformtype': 'GP17-1',
#     'Continuationcasepartnumber': 1,
#     'DateAuthorised': '-0-0',
#     'DateOfAcceptance': datetime.date(2017, 5, 24),
#     'DateOfBirth': datetime.date(1993, 6, 17),
#     'DateOfCompletion': datetime.date(2017, 6, 8),
#     'Declaration': True,
#     'DentistFamilyName': 'Bruehmann',

#     'FamilyName': 'Shepherd',
#     'GDSClaim': True,
#     'GivenName': 'Gin',
#     'IdValue': '04818',
#     'NonGDSClaim': False,
#     'OrganisationName': 'CALLANDER DENTAL PRACTICE',
#     'OrganisationType': 'dentalpractice',
#     'PMSVersion': 'CALLDENT:1',
#     'PatientAddressLine': 'Teithside House\nRiverview\nCallander\n',
#     'PatientFamilyName': 'Vas Normandy',
#     'PatientGivenName': 'Tali',
#     'PatientOrRepresentativeCode': 2,
#     'PatientOrRepresentativeDescription': 'Carer',
#     'PatientOrRepresentativeSignature': True,
#     'PatientOrRepresentativeSignatureDate': datetime.date(2017, 5, 24),
#     'PatientPostcode': 'FK17 8AS',
#     'PostCode': 'FK17 7HB',
#     'PracticeAddressLine': '171 Main Street\nCallander\n',
#     'PracticeClaimReference': 470,
#     'PracticePostCode': 'FK17 8BJ',
#     'PractitionerPinNumber': 558462,
#     'ProfCode': 80925,
#     'ProfCodeScheme': 'Dental List Number',
#     'ProfType': 'Dentist',
#     'Sex': 'F',
#     'SubmissionClaimCount': 1,
#     'Title': 'Mrs',
#     'Treatment': {
#         1001: '01:13.60',
#         1404: '02:47.50:44:45',
#         1421: '01:17.45:21',
#         1422: '01:5.65:21',
#         1600: '01:8.55',
#         1601: '01:110.40:14',
#         1700: '01:8.55',
#         1716: '01:90.00:23',
#         2101: '01:8.55:28',
#         2121: '01:6.95',
#         6666: '55:'
#     },
#     'TypeOfClaimCode': None,
#     'VersionType': 395}


# Disable logging so it doesn't clutter up from tests
logging.disable(logging.CRITICAL)


class TestSubmitClaims(unittest.TestCase):
    """Test claim submission."""

    process_claim_file = os.path.join(BASE_DIR, 'edixml', 'process_claim.py')

    def test_help_text_shown_when_help_arg_passed(self):
        # Jill wants to know how to use the tool
        # She runs the program with the help flag
        output_gnu = edixml.utils.to_str(subprocess.check_output(
            ['python', self.process_claim_file, '--help']))
        self.assertIn("usage", output_gnu)
        # She runs the same command with a UNIX flag to see if there are
        # any easter eggs
        output_unix = edixml.utils.to_str(subprocess.check_output(
            ['python', self.process_claim_file, '-h']))
        self.assertIn("usage", output_unix)
        self.assertEqual(output_gnu, output_unix)
        # The usage message indicates that an input file is required
        self.assertIn("input", output_unix)

    def test_file_exists(self):
        # Alistair wants to know if the tool is installed correctly.
        self.assertTrue(os.path.isfile(self.process_claim_file))


if __name__ == '__main__':
    unittest.main()

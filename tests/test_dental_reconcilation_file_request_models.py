#!/usr/bin/env python3
# vim:fileencoding=utf-8


import datetime
import logging
import random
import unittest

from lxml.builder import ElementMaker

from edixml.models.address import (
    Address,
    AddressLine,
    AddressLines,
    PostCode,
)
from edixml.models.organisation import (
    IdValue,
    OrganisationId,
    OrganisationName,
    OrganisationType,
    RequestingOrganisationDetails,
)
from edixml.models.reconciliation_file_request import (
    AppLastBlockId,
    DentalReconciliationFileRequest,
    RequestDateTime,
)

from edixml import utils

from .utils import (
    ALPHANUM_CHARS,
    ComplexNodeMixin,
    LeafNodeMixin,
)
"""
DentalReconciliationFileRequest
    AppLastBlockId
    RequestingOrganisationDetails
    RequestDateTime
"""

# Disable logging so it doesn't clutter up from tests
logging.disable(logging.CRITICAL)


class AppLastBlockIdTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'AppLastBlockId'
        self.model = AppLastBlockId
        self.text = '00000000-0000-0000-0000-000000000000'
        self.correct_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(36)])
        ]
        self.incorrect_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(35)]),
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(37)])
        ]


class RequestDateTimeTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'RequestDateTime'
        self.model = RequestDateTime
        self.text = datetime.datetime.now()
        self.correct_values = [
            datetime.datetime.now(),
            datetime.datetime(2000, 1, 1)
        ]
        self.incorrect_values = [
            'now',
            '15st street',
        ]


class DentalReconciliationFileRequestTest(ComplexNodeMixin, unittest.TestCase):
    def setUp(self):
        self.model = DentalReconciliationFileRequest
        self.node_name = 'DentalReconciliationFileRequest'
        self.children = [
            'app_last_block_id',
            'requesting_organisation_details',
            'request_date_time'
        ]
        self.required_children = [
            'app_last_block_id',
            'requesting_organisation_details',
            'request_date_time'
        ]
        id_value = '1234567'
        org_name = 'Callander Dental Practice'
        org_type = 'dentalpractice'
        org_addr_line_1 = '156 Main Street'
        org_addr_line_2 = 'Callander'
        org_post_code = 'FK17 8BG'
        self.test_model = self.model(
            requesting_organisation_details=RequestingOrganisationDetails(
                organisation_id=OrganisationId(
                    id_value=IdValue(id_value),
                ),
                organisation_name=OrganisationName(org_name),
                organisation_type=OrganisationType(org_type),
                address=Address(
                    address_lines=AddressLines(
                        [
                            AddressLine(org_addr_line_1),
                            AddressLine(org_addr_line_2),
                        ]
                    ),
                    post_code=PostCode(org_post_code),
                ),
            ),
        )
        request_datetime = self.test_model.request_date_time.text
        nsmap = {
            None: "http://www.eps.nds.scot.nhs.uk",
            'xsd': "http://www.w3.org/2001/XMLSchema",
            'ds': "http://www.w3.org/2000/09/xmldsig#"
        }
        E = ElementMaker(nsmap=nsmap)
        self.reference = E.DentalReconciliationFileRequest(
            E.AppLastBlockId('00000000-0000-0000-0000-000000000000'),
            E.RequestingOrganisationDetails(
                E.OrganisationId(
                    E.IdValue(id_value),
                ),
                E.OrganisationName(org_name),
                E.OrganisationType(org_type),
                E.Address(
                    E.AddressLines(
                        E.AddressLine(org_addr_line_1),
                        E.AddressLine(org_addr_line_2),
                    ),
                    E.PostCode(org_post_code),
                ),
            ),
            E.RequestDateTime(utils.get_string_repr(request_datetime)),
            SchemaVersion="1.0",
        )

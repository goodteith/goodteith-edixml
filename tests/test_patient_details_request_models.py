#!/usr/bin/env python3
# vim:fileencoding=utf-8

import datetime
import logging
import random
import unittest

from lxml.builder import E

from edixml.models.address import (
    Address,
    AddressLine,
    AddressLines,
    PostCode,
)
from edixml.models.organisation import (
    IdValue,
    OrganisationId,
    OrganisationName,
    OrganisationType,
)
from edixml.models.patient import (
    ChiNumber,
    DateOfBirth,
    PatientBirthFamilyName,
    PatientDetailsDentalStructure,
    PatientFamilyName,
    PatientGivenName,
    PatientName,
    Sex,
)
from edixml.models.patient_details_request import (
    DentalPatientDetailsRequest,
    InitialSearchReference,
    PatientDetailsRequestReference,
    RequestingOrganisationDetails,
    RequestingPractitionerDetails,
    SecondarySearch,
)
from edixml.models.person import (
    BirthFamilyName,
    FamilyName,
    GivenName,
    HcpId,
    HcpName,
    ProfCode,
    ProfCodeScheme,
    ProfType,
    StructuredName,
    Title,
)
from edixml import utils
from .utils import ComplexNodeMixin, LeafNodeMixin, ALPHANUM_CHARS

# Disable logging so it doesn't clutter up from tests
logging.disable(logging.CRITICAL)


class DentalPatientDetailsRequestTest(ComplexNodeMixin, unittest.TestCase):
    def setUp(self):
        self.model = DentalPatientDetailsRequest
        self.node_name = 'DentalPatientDetailsRequest'
        self.children = [
            'requesting_organisation_details',
            'requesting_practitioner_details',
            'patient_details_request_reference',
            'secondary_search',
            'initial_search_reference',
            'patient_details',
        ]
        self.required_children = [
            'requesting_organisation_details',
            'requesting_practitioner_details',
            'patient_details_request_reference',
            'patient_details',
        ]
        reference = 'abcdefghij'
        secondary = True
        old_ref = '1234567890'
        sex = 'm'
        chi_number = '0987654321'
        date_of_birth = datetime.date(2000, 1, 1)
        id_value = '1234567'
        org_name = 'Callander Dental Practice'
        org_type = 'dentalpractice'
        org_addr_line_1 = '156 Main Street'
        org_addr_line_2 = 'Callander'
        org_post_code = 'FK17 8BG'
        hcp_family_name = 'Van Cleef'
        hcp_birth_family_name = 'Clifford'
        hcp_given_name = 'Lee'
        prof_type = 'Dentist'
        prof_code = '123456'
        prof_code_scheme = 'Dental List Number'
        title = 'Ms'
        pat_family_name = 'Fonda'
        pat_birth_family_name = 'Fondue'
        pat_given_name = 'Janet'
        pat_addr_line_1 = '1 wolleyway way'
        pat_addr_line_2 = 'WoopWoop'
        pat_post_code = 'AB16 5HH'
        self.test_model = self.model(
            requesting_organisation_details=RequestingOrganisationDetails(
                organisation_id=OrganisationId(
                    id_value=IdValue(id_value),
                ),
                organisation_name=OrganisationName(org_name),
                organisation_type=OrganisationType(org_type),
                address=Address(
                    address_lines=AddressLines(
                        [
                            AddressLine(org_addr_line_1),
                            AddressLine(org_addr_line_2),
                        ]
                    ),
                    post_code=PostCode(org_post_code),
                ),
            ),
            requesting_practitioner_details=RequestingPractitionerDetails(
                hcp_name=HcpName(
                    structured_name=StructuredName(
                        family_name=FamilyName(hcp_family_name),
                        birth_family_name=BirthFamilyName(
                            hcp_birth_family_name
                        ),
                        given_name=GivenName(hcp_given_name),
                    ),
                ),
                prof_type=ProfType(prof_type),
                hcp_id=HcpId(
                    prof_code=ProfCode(prof_code),
                    prof_code_scheme=ProfCodeScheme(prof_code_scheme),
                ),
            ),
            patient_details_request_reference=PatientDetailsRequestReference(
                reference
            ),
            secondary_search=SecondarySearch(secondary),
            initial_search_reference=InitialSearchReference(old_ref),
            patient_details=PatientDetailsDentalStructure(
                chi_number=ChiNumber(chi_number),
                patient_name=PatientName(
                    title=Title(title),
                    family_name=PatientFamilyName(pat_family_name),
                    birth_family_name=PatientBirthFamilyName(
                        pat_birth_family_name
                    ),
                    given_name=PatientGivenName(pat_given_name),
                ),
                address=Address(
                    address_lines=AddressLines(
                        [
                            AddressLine(pat_addr_line_1),
                            AddressLine(pat_addr_line_2),
                        ]
                    ),
                    post_code=PostCode(pat_post_code),
                ),
                date_of_birth=DateOfBirth(date_of_birth),
                sex=Sex(sex),
            ),
        )
        self.reference = E.DentalPatientDetailsRequest(
            E.RequestingOrganisationDetails(
                E.OrganisationId(
                    E.IdValue(id_value),
                ),
                E.OrganisationName(org_name),
                E.OrganisationType(org_type),
                E.Address(
                    E.AddressLines(
                        E.AddressLine(org_addr_line_1),
                        E.AddressLine(org_addr_line_2),
                    ),
                    E.PostCode(org_post_code),
                ),
            ),
            E.RequestingPractitionerDetails(
                E.HcpName(
                    E.StructuredName(
                        E.FamilyName(hcp_family_name),
                        E.BirthFamilyName(hcp_birth_family_name),
                        E.GivenName(hcp_given_name),
                    ),
                ),
                E.ProfType(prof_type),
                E.HcpId(
                    E.ProfCode(prof_code),
                    E.ProfCodeScheme(prof_code_scheme),
                ),
            ),
            E.PatientDetailsRequestReference(reference),
            E.SecondarySearch(utils.get_string_repr(secondary)),
            E.InitialSearchReference(old_ref),
            E.PatientDetails(
                E.CHINumber(chi_number),
                E.Name(
                    E.Title(title),
                    E.FamilyName(pat_family_name),
                    E.BirthFamilyName(pat_birth_family_name),
                    E.GivenName(pat_given_name),
                ),
                E.Address(
                    E.AddressLines(
                        E.AddressLine(pat_addr_line_1),
                        E.AddressLine(pat_addr_line_2),
                    ),
                    E.PostCode(pat_post_code),
                ),
                E.DateOfBirth(utils.get_string_repr(date_of_birth)),
                E.Sex(sex),
            ),
            SchemaVersion="1.0",
        )


class PatientDetailsRequestReferenceTest(LeafNodeMixin, unittest.TestCase):
    def setUp(self):
        self.node_name = 'PatientDetailsRequestReference'
        self.model = PatientDetailsRequestReference
        self.text = '0123456789'
        self.correct_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(10)])
        ]
        self.incorrect_values = [
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(9)]),
            ''.join([random.choice(ALPHANUM_CHARS) for i in range(11)])
        ]

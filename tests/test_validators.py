#!/usr/bin/env python3
# vim:fileencoding=utf-8


import logging
import unittest


from edixml.models.base import MessageElement

# Disable logging so it doesn't clutter up from tests
logging.disable(logging.CRITICAL)


class ValidatePositiveNumberTest(unittest.TestCase):

    func = MessageElement().validate_positive_number

    def test_accepts_correct_values(self):
        values = [
            1, 100, 1000, 10000, 0.1, 3.14, 3.1415, 0.01, 0.1
        ]
        for value in values:
            self.assertTrue(self.func(value))

    def test_rejects_incorrect_values(self):
        values = [
            'abc', -5, -0.1, -0.000000001, True
        ]
        for value in values:
            self.assertFalse(self.func(value))


class ValidateMaxTwoDecimalPlaces(unittest.TestCase):

    func = MessageElement().validate_max_two_decimal_places

    def test_accepts_correct_values(self):
        values = [
            1, 1.0, 1.00, 2.5, 2.55
        ]
        for value in values:
            self.assertTrue(self.func(value))

    def test_rejects_incorrect_values(self):
        values = [
            0.001, 1.001, 3.141592
        ]
        for value in values:
            self.assertFalse(self.func(value))


class ValidateValueCombinationInListNoRepeats(unittest.TestCase):

    func = MessageElement().validate_characters_in_list_no_repeats

    def test_accepts_correct_values(self):
        allowed_values = ['a', 'b', 'c']
        values = [
            'a',
            'b',
            'c',
            'ab',
            'ac',
            'ba',
            'bc',
            'ca',
            'cb',
            'abc',
        ]
        for value in values:
            self.assertTrue(self.func(value, allowed_values))

    def test_rejects_incorrect_values(self):
        allowed_values = ['a', 'b', 'c']
        values = [
            'aa', 'bb', 'cc', 'aaa', 'aba', 'caa', 'd', 'da', 'ad'
        ]
        for value in values:
            self.assertFalse(self.func(value, allowed_values))

pushd %~dp0

SET NEXTCLOUDDIR=%HOMEDRIVE%%HOMEPATH%\nextcloud

rd /s /q dist
rd /s /q build

python setup.py bdist_wheel --universal
pyinstaller --noconfirm --add-data schema;schema edixml\add_to_pile.py
pyinstaller --noconfirm edixml\archive_files.py
pyinstaller --noconfirm --onefile edixml\upgrade.py
pyinstaller --noconfirm edixml\reconcile_eschedule.py

IF exist %NEXTCLOUDDIR%\goodteith ( set SHAREDIR=%NEXTCLOUDDIR%\goodteith) ELSE ( set SHAREDIR=%NEXTCLOUDDIR%\documents)

SET DEPLOYDIR=%SHAREDIR%\goodteith-edixml

REM Work out the version number

for /F "delims=" %%a in ('findstr /L "__version__ = " "edixml\__init__.py" ') do set "VERSIONLINE=%%a"

FOR /f "tokens=2 delims==" %%a IN ("%VERSIONLINE%") DO SET "VERSION=%%a"

SET VERSION=%VERSION: =%
SET VERSION=%VERSION:'=%

rd /s %DEPLOYDIR%\edixml_%VERSION%
ROBOCOPY /e dist %DEPLOYDIR%\edixml_%VERSION%

cd %DEPLOYDIR%\edixml_%VERSION%
del ..\edixml_%VERSION%.7z
..\tools\7zr.exe a ..\edixml_%VERSION%.7z *
cd ..
del edixml_%VERSION%.exe
copy /b tools\7zSD.sfx + tools\config.txt + edixml_%VERSION%.7z edixml_%VERSION%.exe

popd

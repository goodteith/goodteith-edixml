.. _contents:

====================================
Goodteith EDI XML interface contents
====================================

.. toctree::
   :hidden:

   index

.. toctree::
   :maxdepth: 3

   models
   ref/index
   glossary

Indices, glossary, and tables
=============================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`glossary`
* :ref:`search`

===================
Base Message Object
===================

.. currentmodule:: edixml.models.base

.. autoclass:: MessageElement

   .. autoattribute:: node_name
   .. autoattribute:: validators
   .. autoattribute:: children
   .. autoattribute:: required_children
   .. autoattribute:: required_attributes
   .. autoattribute:: text

   .. automethod:: as_etree
   .. automethod:: is_valid
   .. automethod:: model_is_valid
   .. automethod:: get_validation_error
   .. automethod:: validate_simple_patterns
   .. automethod:: validate_positive_integer
   .. automethod:: validate_boolean
   .. automethod:: validate_length
   .. automethod:: validate_schema_version
   .. automethod:: schema_version_is_valid

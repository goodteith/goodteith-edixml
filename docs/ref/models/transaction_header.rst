==================
Transaction Header
==================

.. currentmodule:: edixml.models.transaction_header

.. class:: TransactionHeader(schema_version=None, msg_priority=None, msg_status=None, sender_details=None, software=None, app_service_name=None, app_service_version=None, app_sttl=None, app_trans_id=None, app_trans_step_ref=None, app_trans_date_time=None, msg_category=None, app_last_block_id=None, body_definitions=None)

   ``TransactionHeader`` provides metadata for any message sent or received.
   

   .. attribute:: schema_version

      A fixed value, indicating the version of the schema currently being used.
      This value is 1.0, and is a element attribute.

   .. attribute:: msg_priority
     Message priority is fixed for a given message type.

     The allowed values are:

     `immediate`
         highest priority, process on receipt

     `normal`
         normal priority, process after 'immediate' messages.

     A response message has the same priority as the associated request.

     This attribute is mandatory.

   .. attribute:: msg_status

     Message status allows the use of test messages in the live
     environment.

     The allowed values are:

     `live`
         a message issued with full :term:`EPS` significance.

     `test`
         a message which has no :term:`EPS` significance.

     Test messages will be processed but not stored in live instances
     of the ePractitioner message store and will not be forwared to
     backend systems, e.g. MIDAS.

     a response message has the same status as the associated
     request.

     This attribute is mandatory.

   .. attribute:: sender_details

      Sender details are used for authenticating the message sender. The details
      identify the sending organisation at the contractual level.

      A response message has the organisation details of the message store.

      This attribute is mandatory, and should be an instance of
      :class:`SenderDetails`

   .. attribute:: software

      The ``software`` element holds the product description and ``version`` of
      the application used to create the message. Typically, this is the system
      software product identifier. It is mandatory that the version information
      is accurate.

      A response message will have the ``software`` details of the message
      store.

      This attribute is mandatory and should be an instance of
      :class:`Software`.

   .. attribute:: app_service_name

      The ``app_service_name`` indicates the ePractitioner service to which the
      message applies.

      This attribute is mandatory and should be an instance of
      :class:`AppServiceName`.

   .. attribute:: app_service_version

      The ``app_service_version`` indictates the ePractitioner version to which
      the message applies.

   .. attribute:: app_sttl

      The ``app_sttl`` is the transation *step Time-to-Live* for the message
      type used in the subject transaction. ``app_sttl`` is expressed in seconds
      - it is the period for which a requesting system may continue to attempt
      to send a message before the transaction step is abandoned.

      The ``app_sttl`` value used in the request is populated into the response
      message but is not used in the response part of the message exhcnage.

      A response message has the same ``sttl`` value as the associated request.

      This attribute is mandatory.

   .. attribute:: app_trans_id

      ``app_trans_id``, along with :attr:`app_trans_step_ref`, hold the
      unique *identifcation* of the current transaction step. The transaction
      identifier remains fixed for all message exhange attempts during a
      transaction i.e. during ``sttl``. A new transaction identifier must be
      used if an application re-attempts a transaction step when a rprevious
      transaction completes without success.

      A response message has the same transaction ``identifier`` as the
      associated request.

      This attribute is mandatory.

   .. attribute:: app_trans_step_ref

      See :attr:`app_trans_id`

   .. attribute:: app_trans_date_time

      ``app_trans_date_time`` is the UTC date and time the transaction step is
      initiated. This value will remain fixed for all message exhange attempts
      during a transaction, i.e. during ``sttl``.

      A response message has the same transaction *start time* information as
      the associated request.

      This attribute is mandatory.

   .. attribute:: msg_category

      The ``msg_category`` provides summary message structure information
      relating to the disposition of transaction bodies within the message.

      - **single** category messages have a single transaction body type and are
        the simplest form of message. All request messages are this category.
      - **multiple** category messages have a single transaction body type and
        always have a variable number of instances of the type. Multiple
        category messages are used for batched delivery of specific transaction
        body types. This category is only used for response messages.
      - **fixed** category messages have more than one transaction body type in a
        fixed relationship; there may be more than one instance of a transaction
        body type. This category is only used for response messages.

      This attribute is mandatory.

   .. attribute:: app_last_block_id

      The last block ID is used in response messages of category "multiple" it
      indicate the message store internal referenec of the last included
      transaction body. The next request of the same type will pass this
      reference back to the message store (in its transaction body, not the
      transaction header) to confirm receipt of the previous message and act as
      a pointer to the next set of bodies to be returned.

      A GUID is used in the block ID to ensure the receipt confirmation is
      unique. A last block ID can be re-used to recall previous responses,
      however response data is only retained on the message store for a limited
      period.

      This attribute is optional.

   .. attribute:: app_body_type_qty

      Indicates the number of message body types in the message.

      This attribute is mandatory.

   .. attribute:: body_definitions=None

      The body definition explicitly describes the structure of the message in
      terms of the included body types. The definition is fixed for each type of
      message as described elsewhere.

      This attribute is mandatory.

.. class:: SenderDetails(organisation_type=None, organisation_id=None, organisation_name=None)

   .. attribute:: organization_type

      Should be set to *dentalpractice*.

      According to the spec, this value can be one of the following attributes:

      - pharmacy
      - GPpractice
      - system
      - dentalpractice
      - optometrypractice

   .. attribute:: organization_id

      The practice site code, probably.

   .. attribute:: organization_name

      The name of the practice, presumably.

.. class:: Software(self, product_name=None, product_author=None)

   .. attribute:: product_name

      An instance of :class:ProductName:

   .. attribute:: product_author

      The name of the company responsible for the practice management software.

.. class:: ProductName(product_name=None, product_version=None)

   .. attribute:: product_name

      The name of the practice management software.

   .. attribute:: product_version

      The current version of the software.

.. class:: AppServiceName(app_service_name=None, app_service_version=None)

   .. attribute:: app_service_name

      The name of the ePractitioner service. In our case, this value is
      ``Dental``.

      Possible values are:

      - AMS
      - MAS
      - CMS
      - PRS
      - Dental
      - Ophthalmic


   .. attribute:: app_service_version

      The version number of the app service.

      .. todo:: Figure out what this should be set to.

.. class:: BodyDefinitions(body_definitions=None)

   .. attribute:: body_definitions

      A collection of :class:`BodyDefinition` instances.

.. class:: BodyDefinition(name=None, position=None, count=None, signing=None)

   .. attribute:: name

      The name (type) of the associated message body type.

      This could be:

      - ``Acknowledge``
      - ``DentalClaimRequest``
      - ``DentalClaimResponse``
      - ``DentalPriorApprovalRequest``
      - ``DentalPriorApprovalResponse``
      - ``DentalPatientDetailsRequest``
      - ``DentalPatientDetailsResponse``
      - ``DentalReconciliationFileRequest``
      - ``DentalReconciliationFileResponse``
      - ``DentalPriorApprovalUpdatesRequest``
      - ``DentalPriorApprovalUpdatesResponse``

      .. todo:: Link these to the appropriate classes

   .. attribute:: position

      The position of the message in the structure, which element is the
      corresponding message?

   .. attribute:: count

      The number of instances of this body type.

      If the :attr:`TransactionHeader.msg_category` is 'single', this value should be 1.
      Otherwise, it should be programmatically calculated.

   .. attribute:: signing

      A boolean value indicating whether or not the message is signed.

      True if signed, False if not.

.. _glossary:

========
Glossary
========



.. glossary::

   ...Env
      Suffix for an evelope schema name; a schema which describes an :term:`EPS`
      message (Transaction header and Transaction Bodies)

   API
      Application Programming Interface

   Client
      A system that uses ePractitioner services.

   CMI
      Correlation Message Identifier - Request :term:`UMI` used in Response

   EPOC
      ePractitioner Point of Contact. An EPOC is assied an EPOC reference.

   EPS
      ePractitioner Services

   ETP
      Electronic Transmission of Prescriptions

   GUID
      128-bit Globally Unique Identifier

   MEP
      Message Exchange Pattern

   MTTL
      Message-Time-To-Live - :term:`MEP` session lifetime

   MIDAS
      NSS Practitioner & Counter Fraud Services Dental Payment Engine

   Release
      Formal issue of a set of items with specific functionality.

   SOAP
      Simple Object Access Protocol

   SSL
      Secure Sockets Layer

   STTL
      Step-Time-To-Live - Business Transaction step lifetime

   SWAN
      Scottish Wide Area NEtwork - the NHS Scotland national data network.

   UMI
      Unique Message Identifier

   XML
      eXtensible Markup Language

Goodteith EDI
=============

Tasks
-----

-  Data models
-  Yaml parsing
-  Adapter interface
-  CSV interface
-  FileMaker parsing
-  Automated installation

Data models
~~~~~~~~~~~

The xml schema need to be converted to python data classes. Each xml tag
represents a python class that will contain data and the ability to
generate an etree of itself.

This work has been partially completed for outgoing messages and the
general transaction header, but incoming messages and other elements
need to be completed. I estimate that the models will take two to three
weeks of work, judging by the progress so far.

XML signing
^^^^^^^^^^^

Crucially, the xml signing part has not been attempted yet. This will
only take a day, but further research is needed to find out exactly how
to do it.

Yaml parsing
~~~~~~~~~~~~

This section will involve the most testing, as there are a lot of corner
cases that I expect FileMaker to expose.

Adapter interface
~~~~~~~~~~~~~~~~~

This is somewhat of an unknown quantity. The code seems to be pretty
straight-forward to do, but the python com interface will take a wee
while as I've not used it in years.

Windows installation
^^^^^^^^^^^^^^^^^^^^

I need to buy and install windows in a VM to get the adapter interface
configured.

Installing Windows will probably take 1 day.

CSV interface
~~~~~~~~~~~~~

This will involve dad a lot; we need to figure out a spec for
communicating between FileMaker and Python. CSV seems like the way to
go. It should only take a few days to get nailed down on the python
side, but the FileMaker side may take more time.

FileMaker parsing
~~~~~~~~~~~~~~~~~

This can only happen after we've decided on the communication spec. I'm
not sure how long this will take to be honest.

Automated installation
~~~~~~~~~~~~~~~~~~~~~~

Installation is going to be a significant part of the job, but does not
need to be completed before November.

Requirements
------------

Data models
~~~~~~~~~~~

-  None

Yaml parsing
~~~~~~~~~~~~

-  FileMaker yaml export to be completed.
-  More examples
-  Data models to be complete.

Adapter interface
~~~~~~~~~~~~~~~~~

-  A working windows installation
-  The adapter software

CSV interface
~~~~~~~~~~~~~

-  Data models roughly sketched out
-  Response data models completed.
-  Discussion with dad about the information that is required and what
   form that information will take.

FileMaker parsing
~~~~~~~~~~~~~~~~~

-  This is a task for dad; reading in the csv and parsing it.

Automated installation
~~~~~~~~~~~~~~~~~~~~~~

-  Some windows VMs in various states of mess.

Priorities
----------

At the moment, there are blockers that are preventing dad from doing a
lot. He is slowly working on the YAML export stuff, but he will need to
start working on the CSV importing pretty soon.

1. Response data models
2. CSV interface -> Will unblock FileMaker parsing
3. Other data models
4. YAML interface
5. Adapter interface
6. Automated installation

Automated installation Installation is going to be a significant part of
the job, but does not need to be completed before November.

Timeline
--------

Python
~~~~~~

Response data models
   1.5 weeks (1st September)

CSV interface
   2 days (19th September)

Request data models
   1.5 weeks (29th September)

XML signing
   3 days (4th October)

YAML interface
   3 days (9th October)

Adapter interface
   3 days (12th October)
   
Automated installation
   2 weeks

FileMaker
~~~~~~~~~

-  CSV interface
-  YAML interface

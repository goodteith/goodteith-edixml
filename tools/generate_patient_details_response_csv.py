#!/usr/bin/env python3
# vim:fileencoding=utf-8

import csv
import sys

if sys.platform.startswith('win32'):
    MODE = 'ab'
elif sys.platform.startswith('linux'):
    MODE = 'a'


def write_to_csv(target, header, results):
    with open(target, MODE) as csvfile:
        results_writer = csv.writer(csvfile)
        results_writer.writerow(header)
        for row in results:
            results_writer.writerow(row)


def generate_found_patient():
    target = 'found_patient.csv'
    results = [
        ['0000000001', True, '1234567890' '2704511632'],
        ['0000000002', False, '1234567891'],
        ['0000000003', True, '1234567892'],
    ]
    header = [
        "PatientDetailsRequestReference", "PatientFoundFlag",
        "PatientDetailsResponseReference", "MatchedChiNumber"]
    write_to_csv(target, header, results)


def generate_errors():
    target = 'errors.csv'
    results = [
        ['DentalPatientDetailsResponse', '0000000002', '2222222', 'No idea'],
    ]
    header = [
        "MessageType",
        "PatientDetailsRequestReference", "ErrorCode",
        "ErrorText", "ErrorAdditionalInfo"]
    write_to_csv(target, header, results)


def generate_patient_list():
    target = 'patient_list.csv'
    header = ['PatientDetailsRequestReference', 'Title', 'FamilyName',
              'BirthFamilyName', 'GivenName', 'DateOfBirth', 'Sex',
              'PostCode']
    results = [
        ['0000000003', 'Mr', 'Coburn', 'Harrison', 'James',
         '1928-08-31', 'M', 'AB16 5HH'],
        ['0000000003', 'Ms', 'Coolidge', '', 'Rita', '1945-05-01', 'F',
         'FK17 8BG'],
        ['0000000003', 'Mr', 'Pickens', 'Lindley Jr', 'Slim',
         '1919-06-29', 'M', 'FK17 8AS'],
    ]
    write_to_csv(target, header, results)


def generate_patient_treatment():
    target = 'patient_treatment.csv'
    header = ['PatientDetailsRequestReference', 'TreatmentItem',
              'TreatmentDescription', 'ToothNotation',
              'LastTreatmentClaimDate']
    results = [
        ['000000001', '1001', 'Scale and polish', '', '1998-04-02'],
        ['000000001', '1401', 'Filling', '12', '1998-04-02'],
    ]
    write_to_csv(target, header, results)


def main():
    generate_found_patient()
    generate_errors()
    generate_patient_list()
    generate_patient_treatment()


if __name__ == '__main__':
    sys.exit(main())

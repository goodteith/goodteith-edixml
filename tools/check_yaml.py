#!/usr/bin/env python3
# vim:fileencoding=utf-8
from __future__ import print_function

import pprint
import sys
import yaml

PRINT = False


def usage():
    """Print usage information for script
    :returns: TODO

    """
    print("Check that a file is correct YAML syntax")
    print("python {} filename".format(sys.argv[0]))


def main():
    """Parses a text file and checks to see if it is valid YAML

    """
    if len(sys.argv) != 2:
        print("You didn't give me a file to check.", file=sys.stderr)
        usage()
        return 1

    with open(sys.argv[1], 'r') as f:
        try:
            data = yaml.safe_load(f)
            print("YAML is valid")
            if PRINT:
                pp = pprint.PrettyPrinter(indent=4)
                pp.pprint(data)

        except yaml.parser.ParserError as e:
            print("Not valid YAML", file=sys.stderr)
            print("Here is the error message: ", file=sys.stderr)
            print(e.problem, file=sys.stderr)

            return 1
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(data)


if __name__ == '__main__':
    sys.exit(main())

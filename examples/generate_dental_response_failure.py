#!/usr/bin/env python3
# vim:fileencoding=utf-8

from lxml import etree
from lxml.builder import ElementMaker

E = ElementMaker(namespace="http://www.eps.nds.scot.nhs.uk",
                 nsmap={
                     None: "http://www.eps.nds.scot.nhs.uk",
                     "xsd": "http://www.w3.org/2001/XMLSchema",
                     "ds": "http://www.w3.org/2000/09/xmldsig#"
                 })

output_file = 'failed_dental_claim_response.xml'

root = E.DentalClaimResponse(
    E.ClaimedFormDetails(
        E.FormType('GP17-1'),
        E.FormVersion('0613'),
        E.Source('PMS'),
    ),
    E.ClaimingOrganisationDetails(
        E.OrganisationId(
            E.IdValue('04818'),
        ),
        E.OrganisationName('CALLANDER DENTAL PRACTICE'),
        E.OrganisationType('dentalpractice'),
        E.Address(
            E.AddressLines(
                E.AddressLine('171 Main Street'),
                E.AddressLine('Callander'),
            ),
            E.PostCode('FK17 8BJ'),
        ),
    ),
    E.ClaimingPractitionerDetails(
        E.HcpName(
            E.StructuredName(
                E.FamilyName('Spam'),
                E.GivenName('Ham'),
            ),
        ),
        E.ProfType('Dentist'),
        E.HcpId(
            E.ProfCode('80925'),
            E.ProfCodeScheme('Dental List Number'),
        ),
    ),
    E.ClaimReferenceDetails(
        E.PracticeClaimReference('001476'),
        E.SubmissionClaimCount('1'),
        E.PractitionerPinNumber('123123'),
        E.DateAuthorised('2017-08-23'),
        E.PMSVersion('CALLDENT:2'),
    ),
    E.ClaimResponseDetails(
        E.ClaimValidationFlag('false'),
        E.Errors(
            E.ErrorCode('1234567'),
            E.ErrorDescription('Someone set us up'),
        ),
        E.Errors(
            E.ErrorCode('7654321'),
            E.ErrorDescription('Trouble in paradise'),
            E.ErrorAdditionalInfo('What just happened'),
        )
    ),
)

et = etree.ElementTree(root)

with open(output_file, 'wb') as f:
    et.write(f, pretty_print=True, encoding="utf-8", xml_declaration=True)

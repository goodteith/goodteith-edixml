#!/usr/bin/env python3
# vim:fileencoding=utf-8

# Validate with:
# xmllint --noout --schema \
# ../schema/DentalclaimRequest-v1-0.xsd
# dental_claim_request.xml

from lxml import etree
from lxml.builder import ElementMaker

schema = '../schema/DentalClaimRequest-v1-0.xsd'

E = ElementMaker(namespace="http://www.eps.nds.scot.nhs.uk",
                 nsmap={
                     None: "http://www.eps.nds.scot.nhs.uk",
                     "xsd": "http://www.w3.org/2001/XMLSchema",
                     "ds": "http://www.w3.org/2000/09/xmldsig#"
                 })

output_file = 'dental_claim_request.xml'

root = E.DentalClaimRequest(
    E.FormDetails(
        E.FormType('GP17-1'),
        E.FormVersion('0613'),
        E.Source('PMS'),
    ),
    E.ClaimReferenceDetails(
        E.PracticeClaimReference('001204'),
        E.SubmissionClaimCount('1'),
        E.PractitionerPinNumber('123123'),
        E.DateAuthorised('2017-08-23'),
        E.PMSVersion('CALLDENT:2'),
    ),
    E.PractitionerOrganisationDetails(
        E.OrganisationId(
            E.IdValue('04818'),
        ),
        E.OrganisationName('CALLANDER DENTAL PRACTICE'),
        E.OrganisationType('dentalpractice'),
        E.Address(
            E.AddressLines(
                E.AddressLine('171 Main Street'),
                E.AddressLine('Callander'),
            ),
            E.PostCode('FK17 8BJ'),
        ),
    ),
    E.PractitionerDetails(
        E.HcpName(
            E.StructuredName(
                E.FamilyName('Spam'),
                E.GivenName('Ham'),
            ),
        ),
        E.ProfType('Dentist'),
        E.HcpId(
            E.ProfCode('80925'),
            E.ProfCodeScheme('Dental List Number'),
        ),
    ),
    E.PatientDetails(
        E.PatientDetails(
            E.CHINumber('2775533771'),
            E.Name(
                E.FamilyName('COBURN'),
                E.GivenName('James'),
            ),
            E.Address(
                E.AddressLines(
                    E.AddressLine('WILL O\' WHISP COTTAGE'),
                    E.AddressLine('BRIG O\' TURK'),
                    E.AddressLine('PERTHSHIRE'),
                ),
                E.PostCode('FK17 8HT'),
            ),
            E.DateOfBirth('1923-09-27'),
            E.Sex('m'),
        ),
        E.PatientDetailsRequestCheck(
            E.PatientDetailsRequestFlag('true'),
        ),
    ),
    E.ContinuationCaseDetails(
        E.ContinuationPartNumber('1'),
    ),
    E.TreatmentDetails(
        E.TreatmentDateDetails(
            E.DateOfAcceptance('2017-08-21'),
            E.DateOfCompletion('2017-08-23'),
        ),
        E.TreatmentTypeDetails(
            E.TypeOfClaimDetails(
                E.TypeOfClaimCode('1'),
                E.TypeOfClaimDescription(
                    'Initial registration/continued registration with dentist'
                ),
            ),
            E.GDSClaim('true'),
            E.NonGDSClaim('false'),
        ),
        E.BPEDetails(
            E.UpperLeft('2'),
            E.UpperAnterior('0'),
            E.UpperRight('2'),
            E.LowerLeft('2'),
            E.LowerAnterior('2'),
            E.LowerRight('2'),
        ),
    ),
    E.DentistsDeclarationDetails(
        E.DentistDeclaration(
            E.Declaration('true'),
        ),
        E.PatientDeclaration(
            E.DeclarationOnAcceptance(
                E.PatientOrRepresentativeDetails(
                    E.PatientOrRepresentativeCode('1'),
                    E.PatientOrRepresentativeDescription('patient'),
                ),
                E.PatientOrRepresentativeSignature('true'),
                E.PatientOrRepresentativeSignatureDate('2017-08-21'),
            ),
            E.DeclarationOnCompletion(
                E.PatientOrRepresentativeDetails(
                    E.PatientOrRepresentativeCode('1'),
                    E.PatientOrRepresentativeDescription('patient'),
                ),
                E.PatientOrRepresentativeSignature('true'),
                E.PatientOrRepresentativeSignatureDate('2017-08-23'),
            ),
        ),
    ),
    E.PaymentClaimDetails(
        E.TreatmentAmountDetails(
            E.PatientCharge('0024.84'),
            E.AmountClaimed('0039.75'),
        ),
    ),
    SchemaVersion='1.0',
)

et = etree.ElementTree(root)
with open(schema) as f:
    xmlschema_doc = etree.parse(f)
xmlschema = etree.XMLSchema(xmlschema_doc)

xmlschema.assertValid(et)

with open(output_file, 'wb') as f:
    et.write(f, pretty_print=True, encoding="utf-8", xml_declaration=True)

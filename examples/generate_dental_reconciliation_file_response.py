#!/usr/bin/env python3
# vim:fileencoding=utf-8

# Validate with:
# xmllint --noout --schema \
# ../schema/DentalReconciliationFileResponse-v1-0.xsd
# dental_reconciliation_file_response.xml

from lxml import etree
from lxml.builder import ElementMaker

schema = '../schema/DentalReconciliationFileResponse-v1-0.xsd'

E = ElementMaker(namespace="http://www.eps.nds.scot.nhs.uk",
                 nsmap={
                     None: "http://www.eps.nds.scot.nhs.uk",
                     "xsd": "http://www.w3.org/2001/XMLSchema",
                     "ds": "http://www.w3.org/2000/09/xmldsig#"
                 })

output_file = 'dental_reconciliation_file_response.xml'

root = E.DentalReconciliationFileResponse(
    E.RequestingOrganisationDetails(
        E.OrganisationId(
            E.IdValue('123456'),
        ),
        E.OrganisationName('Some dental practice'),
        E.OrganisationType('dentalpractice'),
        E.Address(
            E.AddressLines(
                E.AddressLine('156 Main Street'),
                E.AddressLine('Callander'),
                E.AddressLine('Perthshire'),
            ),
            E.PostCode('FK17 8BG'),
        ),
    ),
    E.ResponseDetails(
        E.DetailsAvailableFlag('true'),
        E.Errors(
            E.ErrorCode('1234567'),
            E.ErrorDescription('Something went horribly wrong'),
            E.ErrorAdditionalInfo('Have you tried turning it off?'),
        ),
        E.ScheduleDate('2017-10-01'),
        E.ScheduleDetails(
            E.ListNumber('1234567'),
            E.ScheduleNumber('71015'),
            E.ItemsOfService(
                E.ReferenceNumber('654321'),
                E.AmountAuthorised('0000.05'),
                E.PatientChargeAuthorised('0010.00'),
                E.RemissionAmount('0010.05'),
                E.AdditionalPercentage('0000.30'),
                E.CHINumber('3555555555'),
                E.AdjustmentNarratives(
                    E.Item('24'),
                    E.FeeCode('100401'),
                    E.AdjustmentText('A wee adjustment'),
                    E.DentistFee('0015.31'),
                ),
            ),
            E.ItemsOfService(
                E.ReferenceNumber('654321'),
                E.AmountAuthorised('0100.00'),
                E.PatientChargeAuthorised('0010.23'),
                E.RemissionAmount('0017.15'),
                E.AdditionalPercentage('0001.30'),
                E.CHINumber('3666666666'),
            ),
            E.ItemsOfService(
                E.ReferenceNumber('654321'),
                E.AmountAuthorised('0013.05'),
                E.PatientChargeAuthorised('0098.42'),
                E.RemissionAmount('0103.19'),
                E.AdditionalPercentage('0017.22'),
                E.CHINumber('3777777777'),
                E.AdjustmentNarratives(
                    E.Item('28'),
                    E.FeeCode('101541'),
                    E.AdjustmentText('Jackpot'),
                    E.DentistFee('7042.56'),
                    E.ToothSpecificNarratives(
                        E.Item('1235'),
                        E.FeeCode('849064'),
                        E.AdjustmentText('Jings'),
                        E.DentistFee('0142.11'),
                    ),
                ),
            ),
        ),
    ),

    SchemaVersion='1.0',
)

et = etree.ElementTree(root)
with open(schema) as f:
    xmlschema_doc = etree.parse(f)
xmlschema = etree.XMLSchema(xmlschema_doc)

xmlschema.assertValid(et)

with open(output_file, 'wb') as f:
    et.write(f, pretty_print=True, encoding="utf-8", xml_declaration=True)

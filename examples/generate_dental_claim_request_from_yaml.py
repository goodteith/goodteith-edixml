#!/usr/bin/env python3
# vim:fileencoding=utf-8

import glob
from lxml import etree
from edixml.interfaces import yaml_to_class

sources = glob.glob('*claim*.yaml')
schema = '../schema/DentalClaimRequest-v1-0.xsd'
output_file = 'basic-claim.xml'

with open(schema) as f:
    xmlschema_doc = etree.parse(f)

xmlschema = etree.XMLSchema(xmlschema_doc)

log = xmlschema.error_log

for source in sources:
    with open(source) as f:
        data = f.read()

    cls = yaml_to_class(data)
    tree = cls.as_etree()
    root = etree.ElementTree(tree)

    output_file = source.replace('yaml', 'xml')
    with open(output_file, 'wb') as f:
        root.write(
            f, pretty_print=True, encoding='utf-8', xml_declaration=True
        )

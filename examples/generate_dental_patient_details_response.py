#!/usr/bin/env python3
# vim:fileencoding=utf-8

# Validate with:
# xmllint --noout --schema \
# ../schema/DentalReconciliationFileResponse-v1-0.xsd
# dental_reconciliation_file_response.xml

from lxml import etree
from lxml.builder import ElementMaker

schema = '../schema/DentalPatientDetailsResponse-v1-1.xsd'

E = ElementMaker(namespace="http://www.eps.nds.scot.nhs.uk",
                 nsmap={
                     None: "http://www.eps.nds.scot.nhs.uk",
                     "xsd": "http://www.w3.org/2001/XMLSchema",
                     "ds": "http://www.w3.org/2000/09/xmldsig#"
                 })

output_file = 'dental_patient_details_response.xml'

reference = 'abcdefghij'
secondary = 'true'
old_ref = '1234567890'
sex = 'm'
chi_number = '0987654321'
date_of_birth = '1900-03-04'
id_value = '1234567'
org_name = 'Callander Dental Practice'
org_type = 'dentalpractice'
org_addr_line_1 = '156 Main Street'
org_addr_line_2 = 'Callander'
org_post_code = 'FK17 8BG'
hcp_family_name = 'Van Cleef'
hcp_birth_family_name = 'Clifford'
hcp_given_name = 'Lee'
prof_type = 'Dentist'
prof_code = '123456'
prof_code_scheme = 'Dental List Number'
title = 'Ms'
pat_family_name = 'Fonda'
pat_birth_family_name = 'Fondue'
pat_given_name = 'Janet'
pat_addr_line_1 = '1 wolleyway way'
pat_addr_line_2 = 'WoopWoop'
pat_post_code = 'AB16 5HH'
request_date = '2017-10-10'
resp_reference = '0123456789'

root = E.DentalPatientDetailsResponse(
    E.RequestingOrganisationDetails(
        E.OrganisationId(
            E.IdValue(id_value),
        ),
        E.OrganisationName(org_name),
        E.OrganisationType(org_type),
        E.Address(
            E.AddressLines(
                E.AddressLine(org_addr_line_1),
                E.AddressLine(org_addr_line_2),
            ),
            E.PostCode(org_post_code),
        ),
    ),
    E.RequestingPractitionerDetails(
        E.HcpName(
            E.StructuredName(
                E.FamilyName(hcp_family_name),
                E.BirthFamilyName(hcp_birth_family_name),
                E.GivenName(hcp_given_name),
            ),
        ),
        E.ProfType(prof_type),
        E.HcpId(
            E.ProfCode(prof_code),
            E.ProfCodeScheme(prof_code_scheme),
        ),
    ),
    E.PatientDetailsRequestReferenceDetails(
        E.PatientDetailsRequestReference(reference),
        E.PatientDetailsRequestDate(request_date),
        E.PatientDetailsResponseReference(resp_reference),
    ),
    E.ResponseDetails(
        E.PatientFoundFlag('true'),
        E.Errors(
            E.ErrorCode('0123456'),
            E.ErrorDescription('An error description'),
            E.ErrorAdditionalInfo('Something funny going on'),
        ),
        E.MatchedCHINumber('0123456789'),
        E.TreatmentDetails(
            E.Items(
                E.TreatmentItem('treatment item'),
                E.TreatmentDescription('did a thing'),
                E.ToothNotation('10'),
                E.LastTreatmentClaimDate('2017-01-01'),
            ),
        ),
        E.PatientsList(
            E.PatientDetails(
                E.FamilyName('McGuvvin'),
                E.GivenName('Bettie'),
                E.DateOfBirth('1930-01-01'),
                E.Sex('f'),
            ),
            E.PatientDetails(
                E.FamilyName('MacGuvvin'),
                E.GivenName('Battie'),
                E.DateOfBirth('1931-01-01'),
                E.Sex('f'),
            ),
        ),
    ),
    E.PatientSuppliedDetails(
        E.CHINumber(chi_number),
        E.Name(
            E.Title('ms'),
            E.FamilyName('Guvvin'),
            E.BirthFamilyName('MacGuvvin'),
            E.GivenName('Bettie'),
        ),
        E.Address(
            E.AddressLines(
                E.AddressLine(pat_addr_line_1),
                E.AddressLine(pat_addr_line_2),
            ),
            E.PostCode(pat_post_code),
        ),
        E.DateOfBirth('1929-01-01'),
        E.Sex(sex),
    ),

    SchemaVersion='1.1',
)

et = etree.ElementTree(root)
with open(schema) as f:
    xmlschema_doc = etree.parse(f)
xmlschema = etree.XMLSchema(xmlschema_doc)

xmlschema.assertValid(et)

with open(output_file, 'wb') as f:
    et.write(f, pretty_print=True, encoding="utf-8", xml_declaration=True)
